package hama.alsaygh.kw.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class PopUpConfirmSolution extends BottomSheetDialogFragment {
    Button button3;
    TextView textView47,textView46;
    LinearLayout parent_pop;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable
            ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.pop_up_confirm_solution,
                container, false);
        fragmentManager = getFragmentManager();
        textView47=(TextView)v.findViewById(R.id.textView47);
        textView46=(TextView)v.findViewById(R.id.textView46);
        parent_pop=(LinearLayout)v.findViewById(R.id.parent_pop);
        button3=(Button)v.findViewById(R.id.button3);

        if(SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            textView46.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            parent_pop.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.sign_in));
            button3.setBackgroundResource(R.drawable.back_button_dark);
            textView47.setTextColor(ContextCompat.getColor(getContext(),R.color.sign_in_dark));


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        return v;

    }
}
