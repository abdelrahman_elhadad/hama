package hama.alsaygh.kw.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.checkout.CheckOutSummeryActivity;
import hama.alsaygh.kw.fragment.settings.storePackage.AllStoresPackage;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class PopUpCheckOut extends BottomSheetDialogFragment {
    Button button3;
    TextView textView46, textView407, textView47;
    LinearLayout parent_pop;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable
            ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pop_up_check_out,
                container, false);
        fragmentManager = getFragmentManager();
        textView46 = (TextView) v.findViewById(R.id.textView46);
        textView407 = (TextView) v.findViewById(R.id.textView407);
        textView47 = (TextView) v.findViewById(R.id.textView47);
        parent_pop = (LinearLayout) v.findViewById(R.id.parent_pop);
        button3 = (Button) v.findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new AllStoresPackage());
                fragmentTransaction.commit();
                dismiss();
            }
        });
        textView47.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), CheckOutSummeryActivity.class);
                startActivity(intent);
                dismiss();
            }
        });
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            textView407.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            parent_pop.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            button3.setBackgroundResource(R.drawable.back_button_dark);
            textView46.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        return v;

    }

}
