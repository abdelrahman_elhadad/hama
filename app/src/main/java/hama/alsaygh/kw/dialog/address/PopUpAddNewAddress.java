package hama.alsaygh.kw.dialog.address;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.AddressResponse;
import hama.alsaygh.kw.api.responce.CountriesResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnAddressListener;
import hama.alsaygh.kw.model.address.Address;
import hama.alsaygh.kw.model.country.Country;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class PopUpAddNewAddress extends BottomSheetDialogFragment {

    Button button3;
    TextView tv_not_now, tv_title;
    FragmentManager fragmentManager;
    View view10;
    LinearLayout parent_pop;

    OnAddressListener onCardClickListener;
    RelativeLayout ll_country;
    private Country selectedCountry;
    EditText edt_city, edt_street, edt_zip, edt_building_no;
    Spinner sp_country;
    TextView tv_country;

    public void setOnAddressClickListener(OnAddressListener onCardClickListener) {
        this.onCardClickListener = onCardClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable
            ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.pop_up_add_new_address, container, false);
        fragmentManager = getFragmentManager();
        tv_not_now = (TextView) v.findViewById(R.id.textView78);
        view10 = (View) v.findViewById(R.id.view10);
        tv_title = (TextView) v.findViewById(R.id.textView46);

        edt_city = (EditText) v.findViewById(R.id.editText99);
        sp_country = (Spinner) v.findViewById(R.id.editText19);
        edt_street = (EditText) v.findViewById(R.id.editText1);
        edt_zip = (EditText) v.findViewById(R.id.editText);
        edt_building_no = (EditText) v.findViewById(R.id.editTex);
        ll_country = v.findViewById(R.id.ll_country);
        tv_country = v.findViewById(R.id.tv_country);

        parent_pop = (LinearLayout) v.findViewById(R.id.parent_pop);
        button3 = (Button) v.findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (MainApplication.isConnected) {

                    if (isValid()) {

                        final Address card = new Address();
                        card.setStreet(edt_street.getEditableText().toString());
                        card.setBuilding_no(edt_building_no.getEditableText().toString());
                        card.setCity(edt_city.getEditableText().toString());
                        card.setZip_code(edt_zip.getEditableText().toString());
                        card.setCountry(selectedCountry);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                final AddressResponse cardResponse = RequestWrapper.getInstance().addAddress(getContext(), card);

                                if (cardResponse.isStatus()) {
                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                Snackbar.make(v, cardResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                                if (onCardClickListener != null)
                                                    onCardClickListener.onAddressRefresh();

                                                dismiss();
                                            }
                                        });
                                } else {
                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                Snackbar.make(v, cardResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                            }
                                        });
                                }
                            }
                        }).start();

                    }

                } else {
                    Snackbar.make(v, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }

            }
        });

        tv_not_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            tv_title.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            parent_pop.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            button3.setBackgroundResource(R.drawable.back_button_dark);

            sp_country.setBackgroundResource(R.drawable.back_edittext_dark);
            tv_country.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            edt_city.setBackgroundResource(R.drawable.back_edittext_dark);
            edt_city.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            edt_street.setBackgroundResource(R.drawable.back_edittext_dark);
            edt_street.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            edt_zip.setBackgroundResource(R.drawable.back_edittext_dark);
            edt_zip.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            edt_building_no.setBackgroundResource(R.drawable.back_edittext_dark);
            edt_building_no.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            edt_zip.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            ll_country.setBackgroundResource(R.drawable.back_edittext_dark);


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        new Thread(new Runnable() {
            @Override
            public void run() {

                final CountriesResponse countriesResponse = RequestWrapper.getInstance().getCountry(getContext());
                if (countriesResponse.isStatus()) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            List<Country> countryList = new ArrayList<>(countriesResponse.getData());
                            Country country = new Country();
                            country.setId(-1);
                            country.setName("");
                            countryList.add(0, country);

                            ArrayAdapter<Country> arrayCountry = new ArrayAdapter<Country>(getContext(), android.R.layout.simple_spinner_dropdown_item, countryList);
                            arrayCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_country.setAdapter(arrayCountry);
                        }
                    });
                }
            }
        }).start();

        tv_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sp_country.performClick();
                tv_country.setVisibility(View.GONE);
                tv_country.setEnabled(false);
                sp_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Country country = (Country) sp_country.getSelectedItem();
                        if (country.getId() == -1) {
                            tv_country.setVisibility(View.VISIBLE);
                            selectedCountry = null;
                        } else {
                            selectedCountry = (Country) sp_country.getSelectedItem();
                            tv_country.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        tv_country.setVisibility(View.VISIBLE);
                    }
                });

            }
        });


        return v;

    }

    private boolean isValid() {
        boolean isValid = true;

        if (edt_building_no.getEditableText().toString().isEmpty()) {
            isValid = false;
            edt_building_no.setBackgroundResource(R.drawable.back_edittext_red);
        } else {

            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext()))
                edt_building_no.setBackgroundResource(R.drawable.back_edittext_dark);
            else
                edt_building_no.setBackgroundResource(R.drawable.back_ground_edit_calcuator);
        }


        if (edt_street.getEditableText().toString().isEmpty()) {
            isValid = false;
            edt_street.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext()))
                edt_street.setBackgroundResource(R.drawable.back_edittext_dark);
            else
                edt_street.setBackgroundResource(R.drawable.back_ground_edit_calcuator);
        }


        if (edt_zip.getEditableText().toString().isEmpty()) {
            isValid = false;
            edt_zip.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext()))
                edt_zip.setBackgroundResource(R.drawable.back_edittext_dark);
            else
                edt_zip.setBackgroundResource(R.drawable.back_ground_edit_calcuator);
        }

        if (selectedCountry == null) {
            isValid = false;
            ll_country.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext()))
                ll_country.setBackgroundResource(R.drawable.back_edittext_dark);
            else
                ll_country.setBackgroundResource(R.drawable.back_ground_edit_calcuator);
        }


        return isValid;
    }
}
