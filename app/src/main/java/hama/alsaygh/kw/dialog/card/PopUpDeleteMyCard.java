package hama.alsaygh.kw.dialog.card;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.CardResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnCardClickListener;
import hama.alsaygh.kw.model.paymentCard.Card;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class PopUpDeleteMyCard extends BottomSheetDialogFragment {

    Button button9, button8;
    TextView textView79, textView46;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    View view10;
    ConstraintLayout con;
    ImageView imageView48, imageView88, imageView43;
    TextView tv_type, tv_card_number, tv_card_holder_name, tv_expire;
    LinearLayout parent_pop;
    RelativeLayout parent_payment_method;

    Card card;
    OnCardClickListener onCardClickListener;

    public void setCard(Card card) {
        this.card = card;
    }

    public void setOnCardClickListener(OnCardClickListener onCardClickListener) {
        this.onCardClickListener = onCardClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable
            ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pop_delete_my_card,
                container, false);
        fragmentManager = getFragmentManager();
        textView79 = (TextView) v.findViewById(R.id.textView79);
        view10 = (View) v.findViewById(R.id.view10);
        textView46 = (TextView) v.findViewById(R.id.textView46);
        imageView48 = (ImageView) v.findViewById(R.id.imageView48);
        button9 = v.findViewById(R.id.button9);
        button8 = v.findViewById(R.id.button8);
        tv_card_number = v.findViewById(R.id.tv_card_number);
        tv_type = v.findViewById(R.id.tv_type);
        tv_card_holder_name = v.findViewById(R.id.tv_card_holder_name);
        tv_expire = v.findViewById(R.id.tv_expire);
        parent_payment_method = v.findViewById(R.id.parent_payment_method);
        parent_pop = (LinearLayout) v.findViewById(R.id.parent_pop);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            parent_payment_method.setBackgroundResource(R.drawable.shape_round_light_brawon);
            textView46.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView79.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            parent_pop.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            button9.setBackgroundResource(R.drawable.back_button_dark);


//            imageView88.setImageResource(R.drawable.ic_dark_pop2);
//            imageView43.setImageResource(R.drawable.ic_dark_pop3);
//            con.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.textviewhome));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (MainApplication.isConnected) {

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            final CardResponse cardResponse = RequestWrapper.getInstance().setDeleteCard(getContext(), card.getId());

                            if (cardResponse.isStatus()) {
                                if (getActivity() != null)
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            Snackbar.make(v, cardResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                            if (onCardClickListener != null)
                                                onCardClickListener.onCardRefresh();

                                            dismiss();
                                        }
                                    });
                            } else {
                                if (getActivity() != null)
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            Snackbar.make(v, cardResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                        }
                                    });
                            }
                        }
                    }).start();


                } else {
                    Snackbar.make(v, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }

            }
        });

        if (card != null) {
            tv_card_holder_name.setText(card.getHolder_name());
            tv_card_number.setText(card.getCard_number());
            tv_type.setText(card.getType());
            String date = card.getMonth() + "/" + card.getYear();
            tv_expire.setText(date);
        }

        return v;

    }
}
