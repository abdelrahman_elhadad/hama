package hama.alsaygh.kw.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.GeneralResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnMyCartListener;
import hama.alsaygh.kw.model.cart.CartItem;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class MyCartDeleteProduct extends BottomSheetDialogFragment {

    FragmentManager fragmentManager;
    LinearLayout parent_delete_p;
    ImageView imageView28;
    TextView textView80, textView135;
    View view25;
    Button button3;
    ProgressBar pb_delete;
    CartItem cartItem;
    OnMyCartListener onMyCartListener;

    public void setOnMyCartListener(OnMyCartListener onMyCartListener) {
        this.onMyCartListener = onMyCartListener;
    }

    public void setCartItem(CartItem cartItem) {
        this.cartItem = cartItem;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        ((AppCompatActivity)getActivity()).getSupportActionBar().hide(); //<< this
//        SharedPreferences editor = getActivity().getApplicationContext().getSharedPreferences("share", MODE_PRIVATE);
//        if (editor.getBoolean("checked",true))
//            getActivity().setTheme(R.style.darktheme);
//        else
//            getActivity().setTheme(R.style.AppTheme);
//        setLightStatusBar();
        final View view = inflater.inflate(R.layout.my_cart_delete_product, container, false);
        fragmentManager = getFragmentManager();
        imageView28 = (ImageView) view.findViewById(R.id.imageView28);
        ImageView imageView50 = view.findViewById(R.id.imageView50);
        textView80 = (TextView) view.findViewById(R.id.textView80);
        textView135 = (TextView) view.findViewById(R.id.textView135);
        view25 = (View) view.findViewById(R.id.view25);
        button3 = (Button) view.findViewById(R.id.button3);
        pb_delete = view.findViewById(R.id.pb_delete);
        parent_delete_p = (LinearLayout) view.findViewById(R.id.parent_delete_p);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            parent_delete_p.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            imageView28.setImageResource(R.drawable.ic_close_dark);
            textView80.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView135.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            view25.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_buttom_navigation2));
            button3.setBackgroundResource(R.drawable.back_button_dark);
            pb_delete.setBackgroundResource(R.drawable.back_button_dark);
        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        imageView28.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        if (!cartItem.getProduct().getMedia().isEmpty()) {
            String image = cartItem.getProduct().getMedia().get(0).getLink();
            if (image != null && !image.isEmpty()) {
                Picasso.get().load(image).error(R.drawable.logo).resize(150, 150).into(imageView50);
            } else
                Picasso.get().load(R.drawable.logo).error(R.drawable.logo).resize(150, 150).into(imageView50);
        } else
            Picasso.get().load(R.drawable.logo).error(R.drawable.logo).resize(150, 150).into(imageView50);


        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (MainApplication.isConnected) {

                    pb_delete.setVisibility(View.VISIBLE);
                    button3.setVisibility(View.GONE);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            final GeneralResponse generalResponse = RequestWrapper.getInstance().deleteProductFromCart(getContext(), cartItem.getId());
                            if (generalResponse.isStatus()) {
                                if (getActivity() != null)
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            if (onMyCartListener != null)
                                                onMyCartListener.onCountChange();
                                            pb_delete.setVisibility(View.GONE);
                                            button3.setVisibility(View.VISIBLE);
                                            dismiss();
                                        }
                                    });
                            } else {
                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            pb_delete.setVisibility(View.GONE);
                                            button3.setVisibility(View.VISIBLE);
                                            Snackbar.make(v, generalResponse.getMessage(), Snackbar.LENGTH_SHORT).show();

                                        }
                                    });
                                }
                            }
                        }
                    }).start();

                } else {
                    Snackbar.make(v, v.getContext().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }

            }
        });


        return view;
    }
}
