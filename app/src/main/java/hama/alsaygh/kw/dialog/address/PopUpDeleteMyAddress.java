package hama.alsaygh.kw.dialog.address;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.AddressResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnAddressListener;
import hama.alsaygh.kw.model.address.Address;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class PopUpDeleteMyAddress extends BottomSheetDialogFragment {

    Button button9, button8;
    TextView textView79, textView46, tv_address;
    FragmentManager fragmentManager;
    View view10;
    LinearLayout parent_pop;

    Address card;
    OnAddressListener onCardClickListener;

    public void setCard(Address card) {
        this.card = card;
    }

    public void setOnAddressClickListener(OnAddressListener onCardClickListener) {
        this.onCardClickListener = onCardClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable
            ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pop_delete_my_address, container, false);
        fragmentManager = getFragmentManager();
        textView79 = (TextView) v.findViewById(R.id.textView79);
        view10 = (View) v.findViewById(R.id.view10);
        textView46 = (TextView) v.findViewById(R.id.textView46);
        button9 = v.findViewById(R.id.button9);
        button8 = v.findViewById(R.id.button8);
        parent_pop = (LinearLayout) v.findViewById(R.id.parent_pop);
        tv_address = v.findViewById(R.id.tv_address);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            textView46.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView79.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            parent_pop.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            button9.setBackgroundResource(R.drawable.back_button_dark);
            tv_address.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (MainApplication.isConnected) {

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            final AddressResponse cardResponse = RequestWrapper.getInstance().setDeleteAddress(getContext(), card.getId());

                            if (cardResponse.isStatus()) {
                                if (getActivity() != null)
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            Snackbar.make(v, cardResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                            if (onCardClickListener != null)
                                                onCardClickListener.onAddressRefresh();

                                            dismiss();
                                        }
                                    });
                            } else {
                                if (getActivity() != null)
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            Snackbar.make(v, cardResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                        }
                                    });
                            }
                        }
                    }).start();


                } else {
                    Snackbar.make(v, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }

            }
        });

        if (card != null) {
            String address = card.getCountry().getName() + "," + card.getCity() + "," + card.getStreet() + "," + card.getBuilding_no();
            tv_address.setText(address);
        }

        return v;

    }
}
