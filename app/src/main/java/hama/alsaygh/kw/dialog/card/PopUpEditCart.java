package hama.alsaygh.kw.dialog.card;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.CardResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnCardClickListener;
import hama.alsaygh.kw.model.paymentCard.Card;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class PopUpEditCart extends BottomSheetDialogFragment {

    Button button3;
    TextView textView78, textView46;
    FragmentManager fragmentManager;
    View view10;
    ImageView imageView48;
    EditText editText13, editText14, editText15, editText16, editText17;
    LinearLayout parent_pop;

    OnCardClickListener onCardClickListener;
    Card card;

    public void setCard(Card card) {
        this.card = card;
    }

    public void setOnCardClickListener(OnCardClickListener onCardClickListener) {
        this.onCardClickListener = onCardClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable
            ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.pop_up_add_new_card,
                container, false);
        fragmentManager = getFragmentManager();
        textView78 =  v.findViewById(R.id.textView78);
        view10 =  v.findViewById(R.id.view10);
        textView46 =  v.findViewById(R.id.textView46);
        imageView48 =  v.findViewById(R.id.imageView48);
        editText13 =  v.findViewById(R.id.editText13);
        editText14 =  v.findViewById(R.id.editText14);
        editText15 =  v.findViewById(R.id.editText15);
        editText16 =  v.findViewById(R.id.editText16);
        editText17 =  v.findViewById(R.id.editText17);
        parent_pop =  v.findViewById(R.id.parent_pop);
        button3 =  v.findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (MainApplication.isConnected) {

                    if (isValid()) {

                        final String cvv = editText16.getEditableText().toString();
                        final Card card2 = new Card();
                        card2.setId(card.getId());
                        card2.setYear(editText17.getEditableText().toString());
                        card2.setMonth(editText15.getEditableText().toString());
                        card2.setCard_number(editText14.getEditableText().toString());
                        card2.setHolder_name(editText13.getEditableText().toString());

                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                final CardResponse cardResponse = RequestWrapper.getInstance().editCard(getContext(), card2, cvv);

                                if (cardResponse.isStatus()) {
                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                Snackbar.make(v, cardResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                                if (onCardClickListener != null)
                                                    onCardClickListener.onCardRefresh();

                                                dismiss();
                                            }
                                        });
                                } else {
                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                Snackbar.make(v, cardResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                            }
                                        });
                                }
                            }
                        }).start();

                    }

                } else {
                    Snackbar.make(v, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }

            }
        });

        textView78.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            textView46.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            parent_pop.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            button3.setBackgroundResource(R.drawable.back_button_dark);
            editText13.setBackgroundResource(R.drawable.back_edittext_dark);
            editText14.setBackgroundResource(R.drawable.back_edittext_dark);
            editText15.setBackgroundResource(R.drawable.back_edittext_dark);
            editText16.setBackgroundResource(R.drawable.back_edittext_dark);
            editText17.setBackgroundResource(R.drawable.back_edittext_dark);
            editText13.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            editText14.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            editText15.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            editText16.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            editText17.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            imageView48.setImageResource(R.drawable.ic_pay_card_dark);
//            imageView88.setImageResource(R.drawable.ic_dark_pop2);
//            imageView43.setImageResource(R.drawable.ic_dark_pop3);
//            con.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.textviewhome));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        button3.setText(getString(R.string.edit_card));
        if (card != null) {
            editText13.setText(card.getHolder_name());
            editText15.setText(card.getMonth());
            editText17.setText(card.getYear());
        }


        return v;

    }

    private boolean isValid() {
        boolean isValid = true;


        if (editText13.getEditableText().toString().isEmpty()) {
            isValid = false;
            editText13.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext()))
                editText13.setBackgroundResource(R.drawable.back_edittext_dark);
            else editText13.setBackgroundResource(R.drawable.back_ground_edit_calcuator);
        }
        if (editText14.getEditableText().toString().isEmpty()) {
            isValid = false;
            editText14.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext()))
                editText14.setBackgroundResource(R.drawable.back_edittext_dark);
            else
                editText14.setBackgroundResource(R.drawable.back_ground_edit_calcuator);
        }

        if (editText15.getEditableText().toString().isEmpty() || editText15.getEditableText().toString().length() != 2 || Integer.parseInt(editText15.getEditableText().toString()) > 12 || Integer.parseInt(editText15.getEditableText().toString()) <= 0) {
            isValid = false;
            editText15.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext()))
                editText15.setBackgroundResource(R.drawable.back_edittext_dark);
            else
                editText15.setBackgroundResource(R.drawable.back_ground_edit_calcuator);
        }

        if (editText16.getEditableText().toString().isEmpty()) {
            isValid = false;
            editText16.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext()))
                editText16.setBackgroundResource(R.drawable.back_edittext_dark);
            else
                editText16.setBackgroundResource(R.drawable.back_ground_edit_calcuator);
        }

        if (editText17.getEditableText().toString().isEmpty() || editText17.getEditableText().toString().length() != 2) {
            isValid = false;
            editText17.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext()))
                editText17.setBackgroundResource(R.drawable.back_edittext_dark);
            else
                editText17.setBackgroundResource(R.drawable.back_ground_edit_calcuator);
        }
        return isValid;
    }
}
