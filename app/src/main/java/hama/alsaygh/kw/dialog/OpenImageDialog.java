package hama.alsaygh.kw.dialog;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.imageSlider.SliderZoomAdapter;
import hama.alsaygh.kw.model.product.Media;
import me.relex.circleindicator.CircleIndicator;

public class OpenImageDialog extends DialogFragment {

    List<Media> mediaList = new ArrayList<>();
    CircleIndicator indicator;
    ViewPager viewPager;
    int position = 0;

    public static OpenImageDialog newInstance(List<Media> mediaList, int path) {

        OpenImageDialog fragment = new OpenImageDialog();
        fragment.setMediaList(mediaList);
        fragment.setPosition(path);
        return fragment;
    }

    public void setMediaList(List<Media> mediaList) {
        this.mediaList.clear();
        this.mediaList.addAll(mediaList);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View view = View.inflate(getActivity(), R.layout.dialog_open_image, null);

        indicator = view.findViewById(R.id.pageIndicatorView1);
        viewPager = view.findViewById(R.id.viewPager11);

        AppCompatImageView ivClose = view.findViewById(R.id.iv_close);

        SliderZoomAdapter sliderAdapter = new SliderZoomAdapter(getContext(), getActivity().getLayoutInflater(), getChildFragmentManager(), mediaList);
        viewPager.setAdapter(sliderAdapter);
        indicator.setViewPager(viewPager);
        viewPager.setCurrentItem(position);

        final Dialog dialog = new Dialog(Objects.requireNonNull(getActivity()), android.R.style.Theme_NoTitleBar_OverlayActionModes);
//        final Dialog dialog = new Dialog(getActivity());
        dialog.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(0));


        ivClose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                dialog.dismiss();

            }
        });

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.7f;
        lp.y = -200;
        lp.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setContentView(view);
        dialog.show();
        return dialog;
    }

    @Override
    public void show(@NotNull FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();

            if (isAdded()) {
                ft.remove(this);
                ft.commitAllowingStateLoss();
            }
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        } catch (Exception e) {
            Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }
}
