package hama.alsaygh.kw.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class CheckOutConfirmPayment extends BottomSheetDialogFragment {
    Button button3;
    TextView textView130, textView129, textView131;
    LinearLayout parent_pop;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable
            ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.check_out_confirm_payment,
                container, false);
        fragmentManager = getFragmentManager();
        textView130 = (TextView) v.findViewById(R.id.textView130);
        textView129 = (TextView) v.findViewById(R.id.textView129);
        textView131 = (TextView) v.findViewById(R.id.textView131);
        parent_pop = (LinearLayout) v.findViewById(R.id.parent_pop);
        button3 = (Button) v.findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), HomeActivity.class);
                intent.putExtra("from_order", true);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        textView131.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            textView129.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            parent_pop.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            button3.setBackgroundResource(R.drawable.back_button_dark);
            textView130.setTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        setCancelable(false);
        return v;

    }
}
