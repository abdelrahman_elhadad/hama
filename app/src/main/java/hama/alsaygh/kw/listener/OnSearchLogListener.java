package hama.alsaygh.kw.listener;

import hama.alsaygh.kw.model.searchLog.SearchLog;

public interface OnSearchLogListener {
    void onSearchLogClick(SearchLog searchLog, int position);
}
