package hama.alsaygh.kw.listener;

import android.view.View;

import hama.alsaygh.kw.model.paymentCard.Card;

public interface OnCardClickListener {
    void onCardClick(View view, Card card, int position);
    void onCardRefresh();
}
