package hama.alsaygh.kw.listener;

import hama.alsaygh.kw.model.product.Product;

public interface OnMyCartListener {
    void onCountChange();
    void onProductClick(Product product, int position);
}
