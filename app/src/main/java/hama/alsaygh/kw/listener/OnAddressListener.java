package hama.alsaygh.kw.listener;

import android.view.View;

import hama.alsaygh.kw.model.address.Address;

public interface OnAddressListener {
    void onAddressClick(View view, Address address, int position);

    void onAddressRefresh();
}
