package hama.alsaygh.kw.listener;

import hama.alsaygh.kw.model.store.Store;

public interface OnStoreClickListener {
    void onStoreClick(Store store, int position, String tag);

}
