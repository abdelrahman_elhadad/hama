package hama.alsaygh.kw.listener;

import hama.alsaygh.kw.model.product.Option;
import hama.alsaygh.kw.model.product.OptionChild;

public interface OnChildOptionListener {
    void onChildOptionSelect(Option optionChild);
    void onChildOptionUnSelect(OptionChild optionChild);
}
