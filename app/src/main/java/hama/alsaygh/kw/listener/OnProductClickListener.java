package hama.alsaygh.kw.listener;

import hama.alsaygh.kw.model.product.Product;

public interface OnProductClickListener {
    void onProductClick(Product product, int position);

}
