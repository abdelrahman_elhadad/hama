package hama.alsaygh.kw.api.responce;

import hama.alsaygh.kw.model.complaint.Complaint;

import com.google.gson.annotations.SerializedName;


public class ReplayComplaintsResponse {

    @SerializedName("status")
    private boolean status;

    @SerializedName("code")
    private String code;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private Complaint data;


    @SerializedName("errors_object")
    private String errors_object;

    public Complaint getData() {
        return data;
    }

    public void setData(Complaint data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        if (code == null)
            code = "";
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrors_object() {
        return errors_object;
    }

    public void setErrors_object(String errors_object) {
        this.errors_object = errors_object;
    }
}
