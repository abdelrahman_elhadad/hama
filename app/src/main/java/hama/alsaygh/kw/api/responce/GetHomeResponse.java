package hama.alsaygh.kw.api.responce;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.model.store.Store;


public class GetHomeResponse {

    @SerializedName("status")
    private boolean status;

    @SerializedName("code")
    private String code;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private DataHome data;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        if (code == null)
            code = "";
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataHome getData() {
        return data;
    }

    public void setData(DataHome data) {
        this.data = data;
    }

    public List<Store> getStores() {
        return data.getStores();
    }

    public List<Product> getBestSells() {
        return data.getBestSells();
    }
}

class DataHome {

    @SerializedName("best_stores")
    private List<Store> stores;

    @SerializedName("best_sells")
    private List<Product> bestSells;

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

    public List<Product> getBestSells() {
        return bestSells;
    }

    public void setBestSells(List<Product> bestSells) {
        this.bestSells = bestSells;
    }
}