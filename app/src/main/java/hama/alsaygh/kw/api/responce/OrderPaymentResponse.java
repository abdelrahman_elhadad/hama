package hama.alsaygh.kw.api.responce;

import hama.alsaygh.kw.model.order.OrderPayment;
import com.google.gson.annotations.SerializedName;


public class OrderPaymentResponse {

    @SerializedName("status")
    private boolean status;

    @SerializedName("code")
    private String code;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private OrderPayment data;

    public OrderPayment getData() {
        return data;
    }

    public void setData(OrderPayment data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        if (code == null)
            code = "";
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
