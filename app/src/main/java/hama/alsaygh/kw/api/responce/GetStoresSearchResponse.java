package hama.alsaygh.kw.api.responce;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import hama.alsaygh.kw.model.store.Store;


public class GetStoresSearchResponse {

    @SerializedName("status")
    private boolean status;

    @SerializedName("code")
    private String code;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private SearchDataStore data;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        if (code == null)
            code = "";
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SearchDataStore getData() {
        return data;
    }

    public void setData(SearchDataStore data) {
        this.data = data;
    }

    public List<Store> getStores() {
        return data.getStores();
    }
}

class SearchDataStore{

    @SerializedName("result")
    private List<Store> stores;

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }
}