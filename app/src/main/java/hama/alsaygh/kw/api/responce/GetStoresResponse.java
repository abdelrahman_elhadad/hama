package hama.alsaygh.kw.api.responce;

import hama.alsaygh.kw.model.store.Store;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class GetStoresResponse {

    @SerializedName("status")
    private boolean status;

    @SerializedName("code")
    private String code;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private DataStore data;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        if (code == null)
            code = "";
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataStore getData() {
        return data;
    }

    public void setData(DataStore data) {
        this.data = data;
    }

    public List<Store> getStores() {
        return data.getStores();
    }
}

class DataStore{

    @SerializedName("user")
    private List<Store> stores;

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }
}