package hama.alsaygh.kw.api;


import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.responce.AddComplaintsResponse;
import hama.alsaygh.kw.api.responce.AddressResponse;
import hama.alsaygh.kw.api.responce.AddressesResponse;
import hama.alsaygh.kw.api.responce.AllComplaintsResponse;
import hama.alsaygh.kw.api.responce.AllFilterProductResponse;
import hama.alsaygh.kw.api.responce.AppointmentResponse;
import hama.alsaygh.kw.api.responce.BannerAdsResponse;
import hama.alsaygh.kw.api.responce.CalculatorResponse;
import hama.alsaygh.kw.api.responce.CardResponse;
import hama.alsaygh.kw.api.responce.CardsResponse;
import hama.alsaygh.kw.api.responce.CartResponse;
import hama.alsaygh.kw.api.responce.CartsResponse;
import hama.alsaygh.kw.api.responce.CategoriesResponse;
import hama.alsaygh.kw.api.responce.CitiesResponse;
import hama.alsaygh.kw.api.responce.ContactUsResponse;
import hama.alsaygh.kw.api.responce.CountriesResponse;
import hama.alsaygh.kw.api.responce.EventsResponse;
import hama.alsaygh.kw.api.responce.GeneralResponse;
import hama.alsaygh.kw.api.responce.GetHomeResponse;
import hama.alsaygh.kw.api.responce.GetStoreProductResponse;
import hama.alsaygh.kw.api.responce.GetStoresPackageResponse;
import hama.alsaygh.kw.api.responce.GetStoresResponse;
import hama.alsaygh.kw.api.responce.GetStoresSearchResponse;
import hama.alsaygh.kw.api.responce.ImageResponse;
import hama.alsaygh.kw.api.responce.LoginResponse;
import hama.alsaygh.kw.api.responce.MainAdsResponse;
import hama.alsaygh.kw.api.responce.MainCategoriesResponse;
import hama.alsaygh.kw.api.responce.NotificationsResponse;
import hama.alsaygh.kw.api.responce.OnBoardResponse;
import hama.alsaygh.kw.api.responce.OrderPaymentResponse;
import hama.alsaygh.kw.api.responce.OrderResponse;
import hama.alsaygh.kw.api.responce.OrdersResponse;
import hama.alsaygh.kw.api.responce.PageResponse;
import hama.alsaygh.kw.api.responce.ProductResponse;
import hama.alsaygh.kw.api.responce.ProductsSearchResponse;
import hama.alsaygh.kw.api.responce.ProfileResponse;
import hama.alsaygh.kw.api.responce.ReplayComplaintsResponse;
import hama.alsaygh.kw.api.responce.SearchLogsResponse;
import hama.alsaygh.kw.api.responce.SocialMediaResponse;
import hama.alsaygh.kw.api.responce.SubAdsResponse;
import hama.alsaygh.kw.api.responce.WishListResponse;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.db.table.FilterProduct;
import hama.alsaygh.kw.db.table.User;
import hama.alsaygh.kw.model.address.Address;
import hama.alsaygh.kw.model.order.CreateOrder;
import hama.alsaygh.kw.model.paymentCard.Card;
import hama.alsaygh.kw.utils.LocalUtils;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

public class RequestWrapper {

    private final Gson gson;
    private final String TAG = getClass().getName();
    private final OkHttpClient client;
    private static RequestWrapper requestWrapper;
    private final String PATH = "https://hamakw.com/api";
    private final String VERSION = "v2";
    private final String MODULE_USER = "user";
    private final String MODULE_CONSTANTS = "constants";
    private final String FULL_PATH_USER = PATH + "/" + VERSION + "/" + MODULE_USER + "/";
    private final String FULL_PATH_CONSTANTS = PATH + "/" + VERSION + "/" + MODULE_CONSTANTS + "/";

    private final MediaType MEDIA_TYPE = MediaType.parse("image/*");

    private final static String CONTENT_TYPE = "application/json";
    private final static String CONTENT_TYPE_DATA = "application/x-www-form-urlencoded";


    private RequestWrapper() {

        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls().create();
        int timeout = 2;
        client = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.MINUTES)
                .writeTimeout(timeout, TimeUnit.MINUTES)
                .readTimeout(timeout, TimeUnit.MINUTES)
                .retryOnConnectionFailure(true)
                .build();

    }

    public static RequestWrapper getInstance() {
        if (requestWrapper == null)
            requestWrapper = new RequestWrapper();
        return requestWrapper;
    }


    public void Logout(final Context context) {
        try {
            String url = FULL_PATH_USER + "auth/logout";

            FormBody body = new FormBody.Builder()
//                            .add("email", "")
//                            .add("password", "")
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                            .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " +
                    requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null)
                responseString = response.body().string();
            Log.i(TAG, "Response: logout " + responseString);

            //checkCountryResponse = gson.fromJson(responseString, CheckResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
//                    checkCountryResponse = new CheckResponse();
//                    checkCountryResponse.setStatus(false);
//                    checkCountryResponse.setMessage(Utils.getInstance().getContext().getResources().getString(R.string.server_error) );
        }
    }


    public LoginResponse Login(Context context, String email, String password, String token) {

        LoginResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "auth/login";

            FormBody body = new FormBody.Builder()
                    .add("email", email)
                    .add("password", password)
                    .add("fcm_token", token)
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("fcm_token", token)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response:login " + responseString);


            loginSocialResponse = gson.fromJson(responseString, LoginResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new LoginResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public GeneralResponse resetPassword(Context context, String token, String password) {

        GeneralResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "auth/reset";

            FormBody body = new FormBody.Builder()
                    .add("token", token)
                    .add("password", password)
                    .add("password_confirmation", password)
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response:reset Password " + responseString);


            loginSocialResponse = gson.fromJson(responseString, GeneralResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GeneralResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public GeneralResponse forgetPassword(Context context, String email) {

        GeneralResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "auth/forget";

            FormBody body = new FormBody.Builder()
                    .add("email_or_mobile", email)
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response:forgetPassword " + responseString);


            loginSocialResponse = gson.fromJson(responseString, GeneralResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GeneralResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public LoginResponse verifyCode(Context context, String email, String code) {

        LoginResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "auth/verify";

            FormBody body = new FormBody.Builder()
                    .add("email_or_mobile", email)
                    .add("code", code)
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response:verify code " + responseString);

            loginSocialResponse = gson.fromJson(responseString, LoginResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new LoginResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public LoginResponse registerUserStep1(Context context, String firstName, String lastName, String email, String password, String token) {

        LoginResponse user;
        try {
            String url = FULL_PATH_USER + "auth/register/step/1";
            FormBody body = new FormBody.Builder()
                    .add("f_name", firstName)
                    .add("l_name", lastName)
                    .add("email", email)
                    .add("password", password)
                    .add("fcm_token", token)
                    .build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("fcm_token", token)
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            Log.i(TAG, "Response: register " + responseString);

            user = gson.fromJson(responseString, LoginResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new LoginResponse();
            user.setStatus(false);
            user.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return user;
    }


    public LoginResponse registerUserStep2(Context context, String gender, String birthDate, int countryId, String email, String token) {

        LoginResponse user;
        try {
            String url = FULL_PATH_USER + "auth/register/step/2";
            FormBody body = new FormBody.Builder()
                    .add("gender", gender)
                    .add("birth_date", birthDate)
                    .add("email", email)
                    .add("country_id", countryId + "")
                    .add("fcm_token", token)
                    .build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("fcm_token", token)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response: register " + responseString);

            user = gson.fromJson(responseString, LoginResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new LoginResponse();
            user.setStatus(false);
            user.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return user;
    }

    public ProfileResponse updateProfile(Context context, String firstName, String lastName, String email, String gender, String birth_date, String mobile, String identifier_type, String identifier, String identifier_image) {

        ProfileResponse user;
        try {
            String url = FULL_PATH_USER + "updateProfile";
            FormBody body = new FormBody.Builder()
                    .add("f_name", firstName)
                    .add("l_name", lastName)
                    .add("email", email)
                    .add("gender", gender)
                    .add("birth_date", birth_date)
                    .add("mobile", mobile)
                    .add("identifier_type", identifier_type)
                    .add("identifier", identifier)
                    .add("identifier_image", identifier_image)
                    .build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).put(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            Log.i(TAG, "Response: update profile " + responseString);

            user = gson.fromJson(responseString, ProfileResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new ProfileResponse();
            user.setStatus(false);
            user.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return user;
    }

    public ProfileResponse updateAvatar(Context context, String avatar) {

        ProfileResponse user;
        try {
            String url = FULL_PATH_USER + "updateProfile";
            FormBody body = new FormBody.Builder()
                    .add("avatar", avatar)
                    .build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).put(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            Log.i(TAG, "Response: update profile " + responseString);

            user = gson.fromJson(responseString, ProfileResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new ProfileResponse();
            user.setStatus(false);
            user.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return user;
    }

    public ProfileResponse updateIdentifer(Context context, String avatar) {

        ProfileResponse user;
        try {
            String url = FULL_PATH_USER + "updateProfile";
            FormBody body = new FormBody.Builder()
                    .add("identifier_image", avatar)
                    .build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).put(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            Log.i(TAG, "Response: update profile " + responseString);

            user = gson.fromJson(responseString, ProfileResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new ProfileResponse();
            user.setStatus(false);
            user.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return user;
    }

    public ProfileResponse updateNotification(Context context, User user) {

        ProfileResponse loginResponse;
        try {
            String url = FULL_PATH_USER + "settings/notifications";
            FormBody body = new FormBody.Builder()
                    .add("general_notifications", user.isGeneral_notifications() ? "1" : "0")
                    .add("new_offers_notification", user.isNew_offers_notification() ? "1" : "0")
                    .add("order_notification", user.isOrder_notification() ? "1" : "0")
                    .add("event_notification", user.isEvent_notification() ? "1" : "0")
                    .add("adv_notification", user.isAdv_notification() ? "1" : "0")
                    .add("payment_method_notification", user.isPayment_method_notification() ? "1" : "0")
                    .build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            Log.i(TAG, "Response: update notification " + responseString);

            loginResponse = gson.fromJson(responseString, ProfileResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            loginResponse = new ProfileResponse();
            loginResponse.setStatus(false);
            loginResponse.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return loginResponse;
    }


    public CountriesResponse getCountry(Context context) {

        CountriesResponse user;
        try {
            String url = FULL_PATH_CONSTANTS + "countries";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            Log.i(TAG, "Response: countries " + responseString);

            user = gson.fromJson(responseString, CountriesResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new CountriesResponse();
            user.setStatus(false);
            user.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return user;
    }
    public CitiesResponse getCities(Context context, int country_id) {

        CitiesResponse user;
        try {
            String url = FULL_PATH_CONSTANTS + "cities?country_id="+country_id;

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            Log.i(TAG, "Response: cities " + responseString);

            user = gson.fromJson(responseString, CitiesResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new CitiesResponse();
            user.setStatus(false);
            user.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return user;
    }

    public OnBoardResponse getOnBoard(Context context) {

        OnBoardResponse user;
        try {
            String url = FULL_PATH_CONSTANTS + "on-bording";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            Log.i(TAG, "Response: on-bording " + responseString);

            user = gson.fromJson(responseString, OnBoardResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new OnBoardResponse();
            user.setStatus(false);
            user.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return user;
    }

    public MainAdsResponse getMainAds(Context context) {

        MainAdsResponse user;
        try {
            String url = FULL_PATH_CONSTANTS + "main-advertisment";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            Log.i(TAG, "Response: MainAds " + responseString);

            user = gson.fromJson(responseString, MainAdsResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new MainAdsResponse();
            user.setStatus(false);
            user.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return user;
    }

    public SubAdsResponse getSubAds(Context context) {

        SubAdsResponse user;
        try {
            String url = FULL_PATH_CONSTANTS + "sub-advertisment";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            Log.i(TAG, "Response: SubAds " + responseString);

            user = gson.fromJson(responseString, SubAdsResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new SubAdsResponse();
            user.setStatus(false);
            user.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return user;
    }

    public BannerAdsResponse getBanner(Context context) {

        BannerAdsResponse user;
        try {
            String url = FULL_PATH_CONSTANTS + "banner";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context) + "")
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            Log.i(TAG, "Response: SubAds " + responseString);

            user = gson.fromJson(responseString, BannerAdsResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new BannerAdsResponse();
            user.setStatus(false);
            if (context != null)
                user.setMessage(context.getResources().getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return user;
    }


    public ContactUsResponse contactUs(Context context, String name, String email, String phone, String subject, String msg) {

        ContactUsResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "contact";

            FormBody body = new FormBody.Builder()
                    .add("name", name + "")
                    .add("email", email + "")
                    .add("phone", phone + "")
                    .add("subject", subject + "")
                    .add("message", msg + "")
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response:contact us " + responseString);


            loginSocialResponse = gson.fromJson(responseString, ContactUsResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new ContactUsResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public ContactUsResponse makeAppointment(Context context, String name, String email, String phone, String subject, String msg) {

        ContactUsResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "appointment";

            FormBody body = new FormBody.Builder()
                    .add("name", name + "")
                    .add("email", email + "")
                    .add("mobile", phone + "")
                    .add("title", subject + "")
                    .add("message", msg + "")
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response:appointment " + responseString);


            loginSocialResponse = gson.fromJson(responseString, ContactUsResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new ContactUsResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public AddComplaintsResponse addComplaint(Context context, String fullName, String email, String subject, String msg) {

        AddComplaintsResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "complaint/store";

            FormBody body = new FormBody.Builder()
                    .add("full_name", fullName + "")
                    .add("subject", subject + "")
                    .add("description", msg + "")
                    .add("email", email + "")
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response:send complaints " + responseString);


            loginSocialResponse = gson.fromJson(responseString, AddComplaintsResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new AddComplaintsResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public ReplayComplaintsResponse replayComplaint(Context context, int complaint_id, String msg) {

        ReplayComplaintsResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "complaint/" + complaint_id + "/reply";

            FormBody body = new FormBody.Builder()
                    .add("message", msg + "")
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response:Replay to admin " + responseString);


            loginSocialResponse = gson.fromJson(responseString, ReplayComplaintsResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new ReplayComplaintsResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public GeneralResponse markAsSolvedComplaint(Context context, int complaint_id) {

        GeneralResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "complaint/" + complaint_id + "/mark-as-solved";

            FormBody body = new FormBody.Builder()
                    .add("message", "")
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response:mark as solved " + responseString);


            loginSocialResponse = gson.fromJson(responseString, GeneralResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GeneralResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public AllComplaintsResponse getComplaints(Context context) {

        AllComplaintsResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "complaint/index";


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)

                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
//            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response:all complaints " + responseString);


            loginSocialResponse = gson.fromJson(responseString, AllComplaintsResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new AllComplaintsResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public GetStoresPackageResponse getStoresPackaging(Context context) {

        GetStoresPackageResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "packaging-stores/index";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            Log.i(TAG, "Response:get Store Package" + responseString);


            loginSocialResponse = gson.fromJson(responseString, GetStoresPackageResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GetStoresPackageResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public GetStoreProductResponse getStoreProductPackage(Context context, int store_id) {

        GetStoreProductResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "packaging-stores/" + store_id + "/products/index?";

            if (!Utils.getInstance().getSort_key().isEmpty()) {
                url = url + "sort_key=" + Utils.getInstance().getSort_key();
            }
            if (Utils.getInstance().getCategory_level_1() != -1) {
                url = url + "&category_level_1=" + Utils.getInstance().getCategory_level_1();
            }

            if (Utils.getInstance().getCategory_level_2() != -1) {
                url = url + "&category_level_2=" + Utils.getInstance().getCategory_level_2();
            }

            if (Utils.getInstance().getCategory_level_3() != -1) {
                url = url + "&category_level_3=" + Utils.getInstance().getCategory_level_3();
            }

            if (!Utils.getInstance().getType_of_price().isEmpty()) {
                url = url + "&type_of_price=" + Utils.getInstance().getType_of_price();
            }

            if (!Utils.getInstance().getRange_price_from().isEmpty() && !Utils.getInstance().getRange_price_to().isEmpty()) {
                url = url + "&range_price_from=" + Utils.getInstance().getRange_price_from();
                url = url + "&range_price_to=" + Utils.getInstance().getRange_price_to();
            }


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get Store product " + responseString);


            loginSocialResponse = gson.fromJson(responseString, GetStoreProductResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GetStoreProductResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public GetHomeResponse getHome(Context context) {

        GetHomeResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "home";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            Log.i(TAG, "Response:get Home" + responseString);

            loginSocialResponse = gson.fromJson(responseString, GetHomeResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GetHomeResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public GetStoresResponse getStores(Context context) {

        GetStoresResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "stores/index";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }
            responseString = responseString.replace("\"data\":[]", "\"data\": {}");
            Log.i(TAG, "Response:get Store package " + responseString);


            loginSocialResponse = gson.fromJson(responseString, GetStoresResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GetStoresResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public GetStoreProductResponse getStoreProduct(Context context, int store_id) {

        GetStoreProductResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "stores/" + store_id + "/products/index?";

            if (!Utils.getInstance().getSort_key().isEmpty()) {
                url = url + "sort_key=" + Utils.getInstance().getSort_key();
            }

            if (Utils.getInstance().getCategory_level_1() != -1) {
                url = url + "&category_level_1=" + Utils.getInstance().getCategory_level_1();
            }

            if (Utils.getInstance().getCategory_level_2() != -1) {
                url = url + "&category_level_2=" + Utils.getInstance().getCategory_level_2();
            }

            if (Utils.getInstance().getCategory_level_3() != -1) {
                url = url + "&category_level_3=" + Utils.getInstance().getCategory_level_3();
            }

            if (!Utils.getInstance().getType_of_price().isEmpty()) {
                url = url + "&type_of_price=" + Utils.getInstance().getType_of_price();
            }

            if (!Utils.getInstance().getRange_price_from().isEmpty() && !Utils.getInstance().getRange_price_to().isEmpty()) {
                url = url + "&range_price_from=" + Utils.getInstance().getRange_price_from();
                url = url + "&range_price_to=" + Utils.getInstance().getRange_price_to();
            }

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get Store product " + responseString);


            loginSocialResponse = gson.fromJson(responseString, GetStoreProductResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GetStoreProductResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public AllFilterProductResponse getStoreProductSort(Context context) {

        AllFilterProductResponse loginSocialResponse;
        try {
            String url = FULL_PATH_CONSTANTS + "products-sort";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get products-sort " + responseString);

            loginSocialResponse = gson.fromJson(responseString, AllFilterProductResponse.class);

            AppDatabase appDatabase = AppDatabase.newInstance(context);
            appDatabase.filterProductDao().deleteAll();

            if (loginSocialResponse.isStatus()) {
                for (FilterProduct filterProduct : loginSocialResponse.getData())
                    appDatabase.filterProductDao().insertAll(filterProduct);
            }

        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new AllFilterProductResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public ProductResponse getProductDetailsPackage(Context context, int store_id, int id) {

        ProductResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "packaging-stores/" + store_id + "/products/" + id + "/show";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get products Details " + responseString);

            loginSocialResponse = gson.fromJson(responseString, ProductResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new ProductResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public ProductResponse getProductDetails(Context context, int id) {

        ProductResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "products/product/" + id;

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get products Details " + responseString);

            loginSocialResponse = gson.fromJson(responseString, ProductResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new ProductResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public GeneralResponse getProductLikeUnlike(Context context, int id) {

        GeneralResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "wishlist";

            FormBody body = new FormBody.Builder()
                    .add("product_id", id + "")
                    .build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get products like unlike  " + responseString);

            loginSocialResponse = gson.fromJson(responseString, GeneralResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GeneralResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public WishListResponse getWishList(Context context) {

        WishListResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "wishlist";


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get wish list  " + responseString);

            loginSocialResponse = gson.fromJson(responseString, WishListResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new WishListResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public CartResponse addToCart(Context context, int id, int quantity) {

        CartResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "cart/add_to_cart";

            FormBody body = new FormBody.Builder()
                    .add("product_id", id + "")
                    .add("quantity", quantity + "")
                    .build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:add to cart  " + responseString);
            responseString = responseString.replace("\"data\":[]", "\"data\":{}");

            loginSocialResponse = gson.fromJson(responseString, CartResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new CartResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public CartResponse addToCart(Context context, int id, int quantity, int option) {

        CartResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "cart/add_to_cart";

            FormBody body = new FormBody.Builder()
                    .add("product_id", id + "")
                    .add("quantity", quantity + "")
                    .add("option_id", option + "")
                    .build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:add to cart  " + responseString);
            responseString = responseString.replace("\"data\":[]", "\"data\":{}");

            loginSocialResponse = gson.fromJson(responseString, CartResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new CartResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public CartsResponse getCart(Context context) {

        CartsResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "cart/index";


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:add to cart  " + responseString);
            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, CartsResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new CartsResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public CartResponse getCart(Context context,int id) {

        CartResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "cart/index/"+id;


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:add to cart  "+id+" -- " + responseString);

            loginSocialResponse = gson.fromJson(responseString, CartResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new CartResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public GeneralResponse deleteProductFromCart(Context context, int id) {

        GeneralResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "cart/" + id + "/delete";


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).delete().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:delete from cart  " + responseString);

            loginSocialResponse = gson.fromJson(responseString, GeneralResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GeneralResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public CardsResponse getCards(Context context) {

        CardsResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "cards/index";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:Cards  " + responseString);

            loginSocialResponse = gson.fromJson(responseString, CardsResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new CardsResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public CardResponse addCard(Context context, Card card, String cvv) {

        CardResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "cards/store";

            FormBody body = new FormBody.Builder()
                    .add("cvv", cvv + "")
                    .add("year", card.getYear() + "")
                    .add("month", card.getMonth() + "")
                    .add("card_number", card.getCard_number() + "")
                    .add("holder_name", card.getHolder_name() + "")
                    .build();


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:add card  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, CardResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new CardResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public CardResponse editCard(Context context, Card card, String cvv) {

        CardResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "cards/" + card.getId() + "/edit";

            FormBody body = new FormBody.Builder()
                    .add("cvv", cvv + "")
                    .add("year", card.getYear() + "")
                    .add("month", card.getMonth() + "")
                    .add("card_number", card.getCard_number() + "")
                    .add("holder_name", card.getHolder_name() + "")
                    .build();


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).put(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:edit card  " + responseString);
            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, CardResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new CardResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public CardResponse setAsPrimaryCard(Context context, int id) {

        CardResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "cards/" + id + "/set-as-primary";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(new FormBody.Builder().build()).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:primary card  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, CardResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new CardResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public CardResponse setDeleteCard(Context context, int id) {

        CardResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "cards/" + id + "/delete";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).delete().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:delete card  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, CardResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new CardResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public OrderPaymentResponse getOrderPayment(Context context, String order, int payment_method) {

        OrderPaymentResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "order/" + order + "/pay?payment_method_id=" + payment_method;


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get OrderPaymentResponse  " + order + " " + responseString);

            loginSocialResponse = gson.fromJson(responseString, OrderPaymentResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new OrderPaymentResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public OrderPaymentResponse createOrder(Context context, CreateOrder order) {

        OrderPaymentResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "order/"+order.getStore_id()+"/create";

            FormBody.Builder builder = new FormBody.Builder()
                    .add("delivery_type", order.getDelivery_type() + "")
                    //.add("card_id", order.getCard_id() + "")
                    .add("payment_method_id", order.getPayment_method_id() + "")
                    .add("gift_note", order.getGift_note() + "");

            if (order.getDelivery_type().equalsIgnoreCase("hama")) {
                builder.add("country_id", order.getCountry_id() + "");
                builder.add("city_id", order.getCity_id() + "");
                builder.add("city", order.getCity_id() + "");
                builder.add("street", order.getStreet() + "");
                builder.add("zip_code", order.getZip() + "");
                builder.add("building_no", order.getBuilding() + "");
                builder.add("save_my_location_check", order.getSave_my_location_check() + "");
            } else {
                builder.add("receipt_at", order.getReceipt_at() + "");
            }

            FormBody body = builder.build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:create order  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, OrderPaymentResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new OrderPaymentResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public OrdersResponse getOrders(Context context, String order) {

        OrdersResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "order/index/" + order;


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get orders  " + order + " " + responseString);

            loginSocialResponse = gson.fromJson(responseString, OrdersResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new OrdersResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public OrderResponse getOrder(Context context, int order_id) {

        OrderResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "order/" + order_id + "/show";


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get orders  " + order_id + " " + responseString);

            loginSocialResponse = gson.fromJson(responseString, OrderResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new OrderResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public OrderResponse cancelOrder(Context context, int order_id) {

        OrderResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "order/" + order_id + "/cancel";

            FormBody body = new FormBody.Builder().build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:cancel order  " + order_id + " " + responseString);
            responseString=responseString.replace("\"data\":[]","\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, OrderResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new OrderResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }
    public OrderResponse deliveredOrder(Context context, int order_id) {

        OrderResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "order/" + order_id + "/delivered";

            FormBody body = new FormBody.Builder().build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:delivered order  " + order_id + " " + responseString);

            responseString=responseString.replace("\"data\":[]","\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, OrderResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new OrderResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }
    public OrderResponse rateOrder(Context context, int order_id,String  rate, String comment) {

        OrderResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "order/" + order_id + "/rate";

            FormBody body = new FormBody.Builder()
                    .add("rate",rate)
                    .add("review",comment)
                    .build();
            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:rate order  " + order_id + " " + responseString);

            responseString=responseString.replace("\"data\":[]","\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, OrderResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new OrderResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public EventsResponse getEvents(Context context) {

        EventsResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "user-events/index";


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get Events :" + responseString);

            loginSocialResponse = gson.fromJson(responseString, EventsResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new EventsResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public GeneralResponse addEvent(Context context, String title, String date, String time) {

        GeneralResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "user-events/store";

            FormBody.Builder builder = new FormBody.Builder()
                    .add("title", title + "")
                    .add("date", date + "")
                    .add("time", time + "");

            FormBody body = builder.build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:add event  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, GeneralResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GeneralResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public AddressResponse setAsPrimaryAddress(Context context, int id) {

        AddressResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "addresses/" + id + "/set-as-primary";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(new FormBody.Builder().build()).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:primary adress  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, AddressResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new AddressResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public AddressesResponse getAddresses(Context context) {

        AddressesResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "addresses/index";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:get address  " + responseString);

            loginSocialResponse = gson.fromJson(responseString, AddressesResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new AddressesResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public AddressResponse setDeleteAddress(Context context, int id) {

        AddressResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "addresses/" + id + "/delete";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).delete().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:delete address  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, AddressResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new AddressResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public AddressResponse addAddress(Context context, Address card) {

        AddressResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "addresses/store";

            FormBody body = new FormBody.Builder()
                    .add("building_no", card.getBuilding_no() + "")
                    .add("zip_code", card.getZip_code() + "")
                    .add("street", card.getStreet() + "")
                    .add("city", card.getCity() + "")
                    .add("country_id", card.getCountry().getId() + "")
                    .build();


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:add card  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, AddressResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new AddressResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public AddressResponse editAddress(Context context, Address card) {

        AddressResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "addresses/" + card.getId() + "/edit";

            FormBody body = new FormBody.Builder()
                    .add("building_no", card.getBuilding_no() + "")
                    .add("zip_code", card.getZip_code() + "")
                    .add("street", card.getStreet() + "")
                    .add("city", card.getCity() + "")
                    .add("country_id", card.getCountry().getId() + "")
                    .build();

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).put(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:edit card  " + responseString);
            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, AddressResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new AddressResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public ImageResponse uploadImage(Context context, String image) {

        ImageResponse user;
        try {
            String url = FULL_PATH_CONSTANTS + "image-upload";
            MultipartBody.Builder builder = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM);

            String IMG_Name = Utils.getInstance().encodeString(image.substring(image.lastIndexOf("/") + 1));
            builder.addFormDataPart("image", IMG_Name, RequestBody.create(MEDIA_TYPE, new File(image)));
            MultipartBody requestBody = builder.build();

            Request request = new Request.Builder()
                    .addHeader("Content-Type", CONTENT_TYPE_DATA)
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))

                    .url(url).post(requestBody).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null)
                responseString = response.body().string();
            Log.i(TAG, "Response: register " + responseString);
            user = gson.fromJson(responseString, ImageResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            user = new ImageResponse();
            user.setStatus(false);
            user.setMessage(Utils.getInstance().getContext().getResources().getString(R.string.server_error));
        }

        return user;
    }

    public GeneralResponse addProductSale(Context context, String name, String details, JSONArray jsonArray) {

        GeneralResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "sale-products/store";

            FormBody body = new FormBody.Builder()
                    .add("name", name + "")
                    .add("details", details + "")
                    .add("images", jsonArray.toString())
                    .build();


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).post(body).build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:add card  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, GeneralResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GeneralResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public PageResponse getAboutUs(Context context) {

        PageResponse loginSocialResponse;
        try {
            String url = FULL_PATH_CONSTANTS + "about";


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:about  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, PageResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new PageResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public PageResponse getTermsConditions(Context context) {

        PageResponse loginSocialResponse;
        try {
            String url = FULL_PATH_CONSTANTS + "terms-conditions";


            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:terms-conditions  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, PageResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new PageResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public CalculatorResponse getCalculator(Context context) {

        CalculatorResponse loginSocialResponse;
        try {
            String url = FULL_PATH_CONSTANTS + "calculator";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:calculator  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, CalculatorResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new CalculatorResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public SocialMediaResponse getSocialMedia(Context context) {

        SocialMediaResponse loginSocialResponse;
        try {
            String url = FULL_PATH_CONSTANTS + "social-media";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response:social-media  " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, SocialMediaResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new SocialMediaResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public AppointmentResponse getAppointment(Context context) {

        AppointmentResponse loginSocialResponse;
        try {
            String url = FULL_PATH_CONSTANTS + "appointment";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response: Appointment " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, AppointmentResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new AppointmentResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }


    public SearchLogsResponse getSearchLogs(Context context) {

        SearchLogsResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "search-log/index";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response: search-log/index " + responseString);

            // responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, SearchLogsResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new SearchLogsResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public SearchLogsResponse deleteSearchLogs(Context context) {

        SearchLogsResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "search-log/clear";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).delete().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response: search-log/clear " + responseString);

            // responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, SearchLogsResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new SearchLogsResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public GetStoresSearchResponse getSearchStore(Context context, String search) {

        GetStoresSearchResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "search-log/search?key=" + search + "&type=stores";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response: search-log/search store " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, GetStoresSearchResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GetStoresSearchResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public GetStoresSearchResponse getSearchStorePackage(Context context, String search) {

        GetStoresSearchResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "search-log/search?key=" + search + "&type=packaging_stores";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response: search-log/search store " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, GetStoresSearchResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new GetStoresSearchResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public ProductsSearchResponse getSearchSProducts(Context context, String search) {

        ProductsSearchResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "search-log/search?key=" + search + "&type=products";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response: search-log/search products " + responseString);

            responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, ProductsSearchResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new ProductsSearchResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public MainCategoriesResponse getMainCategoies(Context context) {

        MainCategoriesResponse loginSocialResponse;
        try {
            String url = FULL_PATH_CONSTANTS + "categories/v1";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response: search-log/search products " + responseString);

            // responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, MainCategoriesResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new MainCategoriesResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public CategoriesResponse getCategoies(Context context) {

        CategoriesResponse loginSocialResponse;
        try {
            String url = FULL_PATH_CONSTANTS + "categories/v2";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response: search-log/search products " + responseString);

            // responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, CategoriesResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new CategoriesResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    public NotificationsResponse getNotifications(Context context) {

        NotificationsResponse loginSocialResponse;
        try {
            String url = FULL_PATH_USER + "notifications";

            Request request = new Request.Builder()
                    .addHeader("Accept", CONTENT_TYPE)
//                    .addHeader("Content-Type", CONTENT_TYPE)
                    .addHeader("UUID", Utils.getInstance().getUUID())
                    .addHeader("Accept-Language", LocalUtils.getInstance().getLanguageShort(context))
                    .addHeader("Authorization", "Bearer " + SharedPreferenceConstant.getSharedPreferenceUserToken(context))
                    .url(url).get().build();
            Log.i(TAG, "Request: " + request + "\n " + requestBodyToString(request));
            Response response = client.newCall(request).execute();
            String responseString = "";
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.i(TAG, "Response: notifications " + responseString);

            // responseString = responseString.replace("\"data\":[]", "\"data\":{}");
            loginSocialResponse = gson.fromJson(responseString, NotificationsResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
            loginSocialResponse = new NotificationsResponse();
            loginSocialResponse.setStatus(false);
            loginSocialResponse.setMessage(context.getString(R.string.server_error));
//            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return loginSocialResponse;
    }

    private String requestBodyToString(final Request request) {

        try {
            final Buffer buffer = new Buffer();
            if (request != null && request.body() != null) {
                request.body().writeTo(buffer);
                return request.headers().toString() + "\n " + buffer.readUtf8();
            }
            return "";
        } catch (final IOException e) {
            return request.headers().toString() + "\n Failed to read request body";
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return request.headers().toString() + "\n Failed to read request body";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return request != null ? request.headers().toString() + "\n Exception" : "";
    }
}
