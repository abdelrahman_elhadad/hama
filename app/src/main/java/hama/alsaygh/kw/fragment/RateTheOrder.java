package hama.alsaygh.kw.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.AdapterRateHamaSlider;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class RateTheOrder extends BaseFragment {
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    ViewPager2 viewPager;
    AdapterRateHamaSlider adapterRateHamaSlider;
    int[] images ={R.drawable.twinzz,R.drawable.twinzz,R.drawable.twinzz};
    ImageView img_back;
    Button button3;
    LinearLayout parent_a;
    TextView txt_toolbar,textView93,textView97,textView96;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        ((AppCompatActivity) getActivity()).getSupportActionBar().hide(); //<< this
//        SharedPreferences editor = getActivity().getApplicationContext().getSharedPreferences("share", MODE_PRIVATE);
//        if (editor.getBoolean("checked",true))
//            getActivity().setTheme(R.style.darktheme);
//        else
//            getActivity().setTheme(R.style.AppTheme);
        View view = inflater.inflate(R.layout.rate_the_order, container, false);
        fragmentManager = getFragmentManager();
        viewPager = (ViewPager2) view.findViewById(R.id.viewpager);
        adapterRateHamaSlider = new AdapterRateHamaSlider(getLayoutInflater(), getActivity(), new ArrayList<>());
        viewPager.setAdapter(adapterRateHamaSlider);
        viewPager.setClipToPadding(false);
        viewPager.setClipChildren(false);
        viewPager.setOffscreenPageLimit(3);
        // viewPager.getChildAt(1).setOverScrollMode(View.OVER_SCROLL_NEVER);
        CompositePageTransformer transformer = new CompositePageTransformer();
        transformer.addTransformer(new MarginPageTransformer(8));
        transformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float v = 1- Math.abs(position);
                page.setScaleY(0.8f + v * 0.2f);

            }
        });
        viewPager.setPageTransformer(transformer);
        ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        };
       // viewPager.addOnPageChangeListener(viewListener);
        fragmentManager=getFragmentManager();
        img_back=(ImageView)view.findViewById(R.id.img_back);
        txt_toolbar=(TextView)view.findViewById(R.id.txt_toolbar);
        parent_a=(LinearLayout)view.findViewById(R.id.parent_a);
        textView93=(TextView)view.findViewById(R.id.textView93);
        textView97=(TextView)view.findViewById(R.id.textView97);
        textView96=(TextView)view.findViewById(R.id.textView96);
        button3=(Button)view.findViewById(R.id.button3);
        if(SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            textView93.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            textView97.setTextColor(ContextCompat.getColor(getContext(),R.color.color_navigation));
            parent_a.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.sign_in));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            button3.setBackgroundResource(R.drawable.back_button_dark);
            textView96.setTextColor(ContextCompat.getColor(getContext(),R.color.white_tranperant));
        }  else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        return view;

    }


}
