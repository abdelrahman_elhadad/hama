package hama.alsaygh.kw.fragment.settings.pages;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.PageResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class TermsAndConditions extends BaseFragment {

    static FragmentManager fragmentManager;
    ImageView img_back;
    TextView txt_toolbar, textView85, textView86, textView90;
    View view13;
    LinearLayout parent_term;
    private Skeleton skeleton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.terms_and_conditions, container, false);
        fragmentManager = getFragmentManager();
        img_back = (ImageView) view.findViewById(R.id.img_back);
        txt_toolbar = (TextView) view.findViewById(R.id.txt_toolbar);
        parent_term = (LinearLayout) view.findViewById(R.id.parent_term);
        textView85 = (TextView) view.findViewById(R.id.textView85);
        textView86 = (TextView) view.findViewById(R.id.textView86);
        textView90 = (TextView) view.findViewById(R.id.textView90);
        view13 = (View) view.findViewById(R.id.view13);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        });

        skeleton = SkeletonLayoutUtils.createSkeleton(textView90);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);


        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);

            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            parent_term.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            textView85.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView86.setTextColor(ContextCompat.getColor(getContext(), R.color.white_tranperant));
            textView90.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            view13.setBackgroundResource(R.drawable.back_view_hama_eng_dark);

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        if (MainApplication.isConnected) {

            skeleton.showSkeleton();

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final PageResponse getStoresResponse = RequestWrapper.getInstance().getTermsConditions(getContext());

                    if (getStoresResponse.isStatus()) {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    textView90.setText(Html.fromHtml(getStoresResponse.getData().getContent(), Html.FROM_HTML_MODE_LEGACY));
                                    String lastUpdate = getString(R.string.Language_Settings15) + " " + getStoresResponse.getData().getLast_update();
                                    textView86.setText(lastUpdate);
                                }
                            });
                    } else {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    Snackbar.make(parent_term, getStoresResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });
                    }

                }
            }).start();
        }


        return view;

    }
}
