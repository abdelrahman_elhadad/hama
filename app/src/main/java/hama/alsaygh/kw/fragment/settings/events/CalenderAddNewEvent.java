package hama.alsaygh.kw.fragment.settings.events;

import android.app.TimePickerDialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.GeneralResponse;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class CalenderAddNewEvent extends BaseFragment {
    FragmentManager fragmentManager;
    LinearLayout parent_calender;

    CalendarView calender;
    EditText editText17;
    TextView editText18;
    Button button10;
    ProgressBar pb_send;
    String date, time;

    public CalenderAddNewEvent() {
    }

    public static EventFragment newInstance() {
        EventFragment fragment = new EventFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.calender_add_new_event, container, false);
        fragmentManager = getChildFragmentManager();
        calender = view.findViewById(R.id.calender);
        button10 = view.findViewById(R.id.button10);
        editText17 = view.findViewById(R.id.editText17);
        editText18 = view.findViewById(R.id.editText18);
        parent_calender = view.findViewById(R.id.parent_calender);
        pb_send = view.findViewById(R.id.pb_send);

        if (pb_send.getIndeterminateDrawable() != null)
            pb_send.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.whiteColor), PorterDuff.Mode.SRC_IN);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            editText17.setBackgroundResource(R.drawable.back_edittext_dark);
            editText18.setBackgroundResource(R.drawable.back_edittext_dark);
            editText17.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            editText18.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            button10.setBackgroundResource(R.drawable.back_button_dark);
            pb_send.setBackgroundResource(R.drawable.back_button_dark);
            parent_calender.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        editText18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.MINUTE, minute);
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.SECOND, 0);
                        calendar.set(Calendar.MILLISECOND, 0);

                        time = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(calendar.getTime());
                        editText18.setText(time);
                    }
                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
        });


        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValid()) {

                    button10.setVisibility(View.GONE);
                    pb_send.setVisibility(View.VISIBLE);
                    final String title = editText17.getEditableText().toString();

                    Calendar selectedCalendar = Calendar.getInstance();
                    selectedCalendar.setTimeInMillis(calender.getDate());
                    date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(selectedCalendar.getTime());
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            final GeneralResponse generalResponse = RequestWrapper.getInstance().addEvent(getContext(), title, date, time);
                            if (generalResponse.isStatus()) {
                                if (getActivity() != null)
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            pb_send.setVisibility(View.GONE);
                                            button10.setVisibility(View.VISIBLE);

                                            Snackbar.make(view, generalResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                            editText17.setText("");
                                            time = null;
                                            editText18.setText("");

                                        }
                                    });
                            } else {
                                if (getActivity() != null)
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            pb_send.setVisibility(View.GONE);
                                            button10.setVisibility(View.VISIBLE);
                                            Snackbar.make(view, generalResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                            if (generalResponse.getCode().equalsIgnoreCase("401")) {
                                                LoginDialog loginDialog = LoginDialog.newInstance();
                                                loginDialog.show(getChildFragmentManager(), "login");
                                            }
                                        }
                                    });
                            }
                        }
                    }).start();


                }

            }
        });


        return view;

    }

    private boolean isValid() {
        boolean isValid = true;


        if (editText17.getEditableText().toString().isEmpty()) {
            isValid = false;
            editText17.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                editText17.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editText17.setBackgroundResource(R.drawable.back_ground_edit_text);

        }

        if (time == null || time.isEmpty()) {
            isValid = false;
            editText18.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                editText18.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editText18.setBackgroundResource(R.drawable.back_ground_edit_text);

        }

        return isValid;
    }


}
