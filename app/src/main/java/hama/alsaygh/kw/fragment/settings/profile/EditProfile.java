package hama.alsaygh.kw.fragment.settings.profile;

import static android.app.Activity.RESULT_OK;
import static android.os.Build.VERSION_CODES.M;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.ImageResponse;
import hama.alsaygh.kw.api.responce.ProfileResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.db.table.User;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.CheckAndRequestPermission;
import hama.alsaygh.kw.utils.FileUtils;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;
import hama.alsaygh.kw.utils.image.CircleTransform;

public class EditProfile extends BaseFragment implements AdapterView.OnItemSelectedListener {
    FragmentManager fragmentManager;
    ImageView imageback, iv_profile, iv_identifier;
    LinearLayout parent_edit_profile, ll_main;
    TextView txt_toolbar, text_view11;
    EditText editText, editText1, editText11, editTextMobile, editTextID;
    Spinner spinner, spinner_ID;
    Button button3, buttonUplaod;
    ProgressBar pb_update;
    String[] arrayForSpinner, arrayForIdentifier;
    private String selectedGender, selectedIdentifier;
    private View view_image;
    private Skeleton skeleton;
    int type = Utils.PICK_IMAGE_ID;
    String identifierImagePath;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.edit_profile, container, false);

        fragmentManager = getFragmentManager();
        imageback = (ImageView) view.findViewById(R.id.imageView_back);
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        });
        view_image = view.findViewById(R.id.view_image);
        parent_edit_profile = (LinearLayout) view.findViewById(R.id.parent_edit_profile);
        txt_toolbar = (TextView) view.findViewById(R.id.txt_toolbar);
        editText = (EditText) view.findViewById(R.id.editText);
        editText1 = (EditText) view.findViewById(R.id.editText1);
        editText11 = (EditText) view.findViewById(R.id.editText11);
        editTextMobile = (EditText) view.findViewById(R.id.editTextMobile);
        text_view11 = (TextView) view.findViewById(R.id.text_view11);
        spinner = (Spinner) view.findViewById(R.id.spinner);
        spinner_ID = (Spinner) view.findViewById(R.id.spinner_ID);

        button3 = (Button) view.findViewById(R.id.button3);
        buttonUplaod = view.findViewById(R.id.buttonUplaod);
        pb_update = view.findViewById(R.id.pb_update);
        iv_profile = view.findViewById(R.id.iv_profile);
        iv_identifier = view.findViewById(R.id.iv_identifier);
        ll_main = view.findViewById(R.id.ll_main);
        editTextID = view.findViewById(R.id.editTextID);
        if (pb_update.getIndeterminateDrawable() != null) {
            pb_update.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.whiteColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        skeleton = SkeletonLayoutUtils.createSkeleton(ll_main);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
            parent_edit_profile.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            editText.setBackgroundResource(R.drawable.back_zakat_dark);
            editText1.setBackgroundResource(R.drawable.back_zakat_dark);
            editText11.setBackgroundResource(R.drawable.back_zakat_dark);
            editTextMobile.setBackgroundResource(R.drawable.back_zakat_dark);
            editTextID.setBackgroundResource(R.drawable.back_zakat_dark);
            text_view11.setBackgroundResource(R.drawable.back_zakat_dark);
            spinner.setBackgroundResource(R.drawable.back_zakat_dark);
            spinner_ID.setBackgroundResource(R.drawable.back_zakat_dark);
            editText.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            editText1.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            editText11.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            editTextMobile.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            editTextID.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            button3.setBackgroundResource(R.drawable.back_button_dark);
            buttonUplaod.setBackgroundResource(R.drawable.back_button_dark);
            pb_update.setBackgroundResource(R.drawable.back_button_dark);
            view_image.setBackgroundResource(R.drawable.shape_oval_ttee_dark);
        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        final DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                text_view11.setText(date);
            }
        };

        text_view11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();

                Date date = calendar.getTime();
                try {

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    date = sdf.parse(text_view11.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                int year = date.getYear() + 1900;
                int month = date.getMonth();
                int day = date.getDate();

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), android.R.style.Theme_Holo_Dialog_MinWidth
                        , mDateSetListener, year, month, day);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        arrayForSpinner = new String[]{"", getString(R.string.male), getString(R.string.female), getString(R.string.other)};

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, arrayForSpinner);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(this);


        arrayForIdentifier = new String[]{"", getString(R.string.identity), getString(R.string.passport), getString(R.string.other)};

        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, arrayForIdentifier);
        arrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_ID.setAdapter(arrayAdapter1);
        spinner_ID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {

                    case 0:
                        selectedIdentifier = "";
                        break;
                    case 1:
                        selectedIdentifier = "identity";
                        break;

                    case 2:
                        selectedIdentifier = "passport";
                        break;

                    case 3:
                        selectedIdentifier = "others";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        skeleton.showSkeleton();
        new Thread(() -> {

            AppDatabase appDatabase = AppDatabase.newInstance(getContext());
            List<User> userList = appDatabase.userDao().getAll();
            if (userList != null && !userList.isEmpty()) {
                final User user = userList.get(0);

                if (getActivity() != null)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            skeleton.showOriginal();
                            editText.setText(user.getF_name());
                            editText1.setText(user.getL_name());
                            editText11.setText(user.getEmail());
                            text_view11.setText(user.getBirth_date());
                            editTextMobile.setText(user.getMobile());

                            if (user.getGender().equalsIgnoreCase("female")) {

                                selectedGender = "female";
                                spinner.setSelection(2);
                            } else if (user.getGender().equalsIgnoreCase("male")) {
                                selectedGender = "male";
                                spinner.setSelection(1);
                            } else if (user.getGender().equalsIgnoreCase("other")) {
                                selectedGender = "other";
                                spinner.setSelection(3);
                            } else {
                                selectedGender = "";
                                spinner.setSelection(0);
                            }

                            if (user.getIdentifier_type().equalsIgnoreCase("")) {
                                selectedIdentifier = "";
                                spinner_ID.setSelection(0);
                            } else if (user.getIdentifier_type().equalsIgnoreCase("identity")) {
                                selectedIdentifier = "identity";
                                spinner_ID.setSelection(1);
                            } else if (user.getIdentifier_type().equalsIgnoreCase("passport")) {
                                selectedIdentifier = "passport";
                                spinner_ID.setSelection(2);
                            } else if (user.getIdentifier_type().equalsIgnoreCase("others")) {
                                selectedIdentifier = "others";
                                spinner_ID.setSelection(3);
                            }
                            editTextID.setText(user.getIdentifier());

                            if (user.getAvatar() != null && !user.getAvatar().trim().isEmpty()) {
                                Picasso.get().load(user.getAvatar()).placeholder(R.drawable.ic_default_user).error(R.drawable.ic_default_user).transform(new CircleTransform()).into(iv_profile);
                            } else
                                Picasso.get().load(R.drawable.ic_default_user).placeholder(R.drawable.ic_default_user).error(R.drawable.ic_default_user).into(iv_profile);


                            if (user.getIdentifier_image() != null && !user.getIdentifier_image().trim().isEmpty()) {
                                Picasso.get().load(user.getIdentifier_image()).placeholder(R.drawable.image_not_foundpng).error(R.drawable.image_not_foundpng).into(iv_identifier);
                                iv_identifier.setVisibility(View.VISIBLE);
                                identifierImagePath=user.getIdentifier_image();
                            } else {
                                iv_identifier.setVisibility(View.GONE);
                            }
                        }
                    });
            }
        }).start();


        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {


                if (MainApplication.isConnected) {

                    if (isValid()) {

                        pb_update.setVisibility(View.VISIBLE);
                        button3.setVisibility(View.GONE);


                        final String firstname = editText.getEditableText().toString();
                        final String lastname = editText1.getEditableText().toString();
                        final String email = editText11.getEditableText().toString();
                        final String birthdate = text_view11.getEditableText().toString();
                        final String mobile = editTextMobile.getEditableText().toString();
                        final String identity = editTextID.getEditableText().toString();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                final ProfileResponse loginResponse = RequestWrapper.getInstance().updateProfile(getContext(), firstname, lastname, email, selectedGender, birthdate, mobile,selectedIdentifier,identity,identifierImagePath);

                                if (loginResponse.isStatus()) {

                                    AppDatabase appDatabase = AppDatabase.newInstance(getContext());
                                    List<User> userList = appDatabase.userDao().getAll();
                                    if (userList != null && !userList.isEmpty()) {
                                        appDatabase.userDao().update(loginResponse.getData());
                                    }
                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pb_update.setVisibility(View.GONE);
                                                button3.setVisibility(View.VISIBLE);


                                            }
                                        });
                                } else {

                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pb_update.setVisibility(View.GONE);
                                                button3.setVisibility(View.VISIBLE);

                                                if (loginResponse.getCode().equalsIgnoreCase("401")) {
                                                    LoginDialog loginDialog = LoginDialog.newInstance();
                                                    loginDialog.show(fragmentManager, "login");
                                                } else {
                                                    Snackbar.make(v, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                }

                            }
                        }).start();
                    }
                } else {
                    Snackbar.make(v, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }

            }
        });


        iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = Utils.PICK_IMAGE;
                permissionGallery();
            }
        });

        buttonUplaod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = Utils.PICK_IMAGE_ID;
                permissionGallery();
            }
        });

        return view;
    }

    private void permissionGallery() {
        if (Build.VERSION.SDK_INT >= M) {
            String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (!CheckAndRequestPermission.hasPermissions(getContext(), permissions)) {
                CheckAndRequestPermission.requestStorage(this);
            } else {
                openGallery();
            }
        } else {
            openGallery();
        }

    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), type);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utils.PICK_IMAGE) {
            if (resultCode == RESULT_OK) {

                final String profilePath = FileUtils.getPath(getContext(), data.getData());
                Picasso.get().load(data.getData()).transform(new CircleTransform()).into(iv_profile);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ImageResponse imageResponse = RequestWrapper.getInstance().uploadImage(getContext(), profilePath);
                        if (imageResponse.isStatus()) {

                            final ProfileResponse loginResponse = RequestWrapper.getInstance().updateAvatar(getContext(), imageResponse.getData().getFile_name());

                            if (loginResponse.isStatus()) {

                                AppDatabase appDatabase = AppDatabase.newInstance(getContext());
                                appDatabase.userDao().update(loginResponse.getData());

                            }
                        }

                    }
                }).start();
            }
        } else if (requestCode == Utils.PICK_IMAGE_ID) {
            if (resultCode == RESULT_OK) {

                final String profilePath = FileUtils.getPath(getContext(), data.getData());
                Picasso.get().load(data.getData()).into(iv_identifier);
                iv_identifier.setVisibility(View.VISIBLE);

                new Thread(() -> {
                    ImageResponse imageResponse = RequestWrapper.getInstance().uploadImage(getContext(), profilePath);
                    if (imageResponse.isStatus()) {

                     identifierImagePath=   imageResponse.getData().getFile_name();

//                        final ProfileResponse loginResponse = RequestWrapper.getInstance().updateIdentifer(getContext(), imageResponse.getData().getFile_name());
//
//                        if (loginResponse.isStatus()) {
//
//                            AppDatabase appDatabase = AppDatabase.newInstance(getContext());
//                            appDatabase.userDao().update(loginResponse.getData());
//
//                        }
                    }

                }).start();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && !CheckAndRequestPermission.isFoundPermissionDenied(grantResults)) {
            openGallery();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (position) {

            case 0:
                selectedGender = "";
                break;
            case 1:
                selectedGender = "male";
                break;

            case 2:
                selectedGender = "female";
                break;

            case 3:
                selectedGender = "other";
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {


    }


    private boolean isValid() {

        boolean isValid = true;


        if (editText11.getEditableText().toString().isEmpty() || !Utils.getInstance().isValidEmail(editText11.getEditableText().toString())) {
            isValid = false;
            editText11.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                editText11.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editText11.setBackgroundResource(R.drawable.back_ground_edit_text);

        }

        if (editText.getEditableText().toString().isEmpty()) {
            isValid = false;
            editText.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                editText.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editText.setBackgroundResource(R.drawable.back_ground_edit_text);

        }
        if (editTextMobile.getEditableText().toString().isEmpty()) {
            isValid = false;
            editTextMobile.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                editTextMobile.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editTextMobile.setBackgroundResource(R.drawable.back_ground_edit_text);
        }

        if (editText1.getEditableText().toString().isEmpty()) {
            isValid = false;
            editText1.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                editText1.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editText1.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        if (selectedGender == null || selectedGender.trim().isEmpty()) {
            isValid = false;
            spinner.setBackgroundResource(R.drawable.back_edittext_red);
        } else {

            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                spinner.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                spinner.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        if (selectedIdentifier == null || selectedIdentifier.trim().isEmpty()) {
            isValid = false;
            spinner_ID.setBackgroundResource(R.drawable.back_edittext_red);
        } else {

            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                spinner_ID.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                spinner_ID.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        if (editTextID.getEditableText().toString().isEmpty()) {
            isValid = false;
            editTextID.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                editTextID.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editTextID.setBackgroundResource(R.drawable.back_ground_edit_text);

        }

        return isValid;
    }

}
