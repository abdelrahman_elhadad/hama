package hama.alsaygh.kw.fragment.search;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.adapter.search.AdapterSearchLog;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.SearchLogsResponse;
import hama.alsaygh.kw.listener.OnSearchLogListener;
import hama.alsaygh.kw.model.searchLog.SearchLog;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class SearchFragment extends BaseFragment implements OnSearchLogListener {

    LinearLayout rl_search_view;
    TextView textView, homeText, textView62, tvClearLog, textView26, textView226, textView2226, textView266;
    SearchView editText3;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    Button button;
    LinearLayout liner_parent_search;
    ImageView imgdown1, iv_arrow_down, getImgdown2, getImgdown3, serch_storer_img_1;
    RecyclerView rv_search_log;
    Spinner spinner;
    String[] arrayForSpinner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.search, container, false);
        fragmentManager = getFragmentManager();
        ConstraintLayout cl_search_store = view.findViewById(R.id.cl_search_store);
        ConstraintLayout cl_search_cat = view.findViewById(R.id.cl_search_cat);
        ConstraintLayout cl_search_store_package = view.findViewById(R.id.cl_search_store_package);
        ConstraintLayout cl_search_product = view.findViewById(R.id.cl_search_product);

        iv_arrow_down = view.findViewById(R.id.iv_arrow_down);
        textView266 = view.findViewById(R.id.textView266);
        spinner = (Spinner) view.findViewById(R.id.spinner);
        rl_search_view = view.findViewById(R.id.rl_search_view);
        rv_search_log = view.findViewById(R.id.rv_search_log);
        homeText = (TextView) view.findViewById(R.id.textView9);
        editText3 = view.findViewById(R.id.editText3);
        textView62 = (TextView) view.findViewById(R.id.textView62);
        tvClearLog = (TextView) view.findViewById(R.id.tv_clear_log);
        textView26 = (TextView) view.findViewById(R.id.textView26);
        textView226 = (TextView) view.findViewById(R.id.textView226);
        textView2226 = (TextView) view.findViewById(R.id.textView2226);
        imgdown1 = (ImageView) view.findViewById(R.id.serch_storer_img);
        getImgdown2 = (ImageView) view.findViewById(R.id.imageView119);
        getImgdown3 = (ImageView) view.findViewById(R.id.imageView1119);
        liner_parent_search = (LinearLayout) view.findViewById(R.id.liner_parent_search);
        serch_storer_img_1 = view.findViewById(R.id.serch_storer_img_1);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            homeText.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            rl_search_view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark11));

            ImageView searchIcon = editText3.findViewById(androidx.appcompat.R.id.search_button);
            searchIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_icon_search));
            TextView searchAutoComplete = editText3.findViewById(androidx.appcompat.R.id.search_src_text);
            searchAutoComplete.setHintTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            searchAutoComplete.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));

            tvClearLog.setTextColor(ContextCompat.getColor(getContext(), R.color.color_navigation));
            textView62.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));

            textView266.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView26.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView226.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView2226.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            imgdown1.setImageResource(R.drawable.ic_icon_arow_down_dark);
            getImgdown2.setImageResource(R.drawable.ic_icon_arow_down_dark);
            getImgdown3.setImageResource(R.drawable.ic_icon_arow_down_dark);
            serch_storer_img_1.setImageResource(R.drawable.ic_icon_arow_down_dark);
            liner_parent_search.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            spinner.setBackgroundResource(R.drawable.back_zakat_dark);
            iv_arrow_down.setImageResource(R.drawable.ic_icon_dawn);
        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        tvClearLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdapterSearchLog adapterSearchLog = new AdapterSearchLog(SearchFragment.this, new ArrayList<>());
                rv_search_log.setAdapter(adapterSearchLog);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        RequestWrapper.getInstance().deleteSearchLogs(getContext());
                    }
                }).start();
            }
        });
        editText3.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                editText3.setQuery("", false);
                return false;
            }
        });


        editText3.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                int position = spinner.getSelectedItemPosition();
                switch (position) {
                    case 0:
                        openStore(query);
                        break;

                   /* case 1:
                        break;*/

                    case 1:
                        openStorePackage(query);
                        break;

                    case 2:
                        openProduct(query);
                        break;
                }


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        cl_search_store.setOnClickListener(v -> {

            openStore("");
        });

        cl_search_store_package.setOnClickListener(v -> {

            openStorePackage("");
        });

        cl_search_product.setOnClickListener(v -> {

            openProduct("");
        });


        arrayForSpinner = new String[]{getString(R.string.Stores),/* getString(R.string.Categories),*/ getString(R.string.Packaging_Stores), getString(R.string.products)};

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, arrayForSpinner);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);


        return view;

    }

    private void openStore(String s) {
        HomeActivity.position = HomeActivity.SearchResult;
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.liner1, SearchResultStoreFragment.newInstance(s));
        fragmentTransaction.commit();
    }

    private void openStorePackage(String s) {
        HomeActivity.position = HomeActivity.SearchResult;
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.liner1, SearchResultStorePackageFragment.newInstance(s));
        fragmentTransaction.commit();
    }

    private void openProduct(String s) {
        HomeActivity.position = HomeActivity.SearchResult;
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.liner1, SearchResultProductFragment.newInstance(s));
        fragmentTransaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();

        new Thread(new Runnable() {
            @Override
            public void run() {

                SearchLogsResponse searchLogsResponse = RequestWrapper.getInstance().getSearchLogs(getContext());
                if (searchLogsResponse.isStatus()) {
                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AdapterSearchLog adapterSearchLog = new AdapterSearchLog(SearchFragment.this, searchLogsResponse.getData());
                                rv_search_log.setAdapter(adapterSearchLog);
                                rv_search_log.setLayoutManager(new LinearLayoutManager(getContext()));
                            }
                        });
                } else {
                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AdapterSearchLog adapterSearchLog = new AdapterSearchLog(SearchFragment.this, new ArrayList<>());
                                rv_search_log.setAdapter(adapterSearchLog);
                                rv_search_log.setLayoutManager(new LinearLayoutManager(getContext()));

                            }
                        });
                }
            }
        }).start();
    }

    private void setLightStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = getActivity().getWindow().getDecorView().getSystemUiVisibility(); // get current flag
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;   // add LIGHT_STATUS_BAR to flag
            getActivity().getWindow().getDecorView().setSystemUiVisibility(flags);
            getActivity().getWindow().setStatusBarColor(Color.TRANSPARENT); // optional
        }
    }

    @Override
    public void onSearchLogClick(SearchLog searchLog, int position) {

        if (searchLog.getType().equalsIgnoreCase("packaging_stores")) {
            openStorePackage(searchLog.getKey());
        } else if (searchLog.getType().equalsIgnoreCase("stores")) {
            openStore(searchLog.getKey());
        } else if (searchLog.getType().equalsIgnoreCase("products")) {
            openProduct(searchLog.getKey());
        }
    }
}
