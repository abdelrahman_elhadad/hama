package hama.alsaygh.kw.fragment.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.activity.marketPrice.MarketPriceActivity;
import hama.alsaygh.kw.activity.notification.NotificationsActivity;
import hama.alsaygh.kw.activity.product.ProductDetailsActivity;
import hama.alsaygh.kw.activity.product.WishListActivity;
import hama.alsaygh.kw.adapter.ads.MainAdsHomeAdapterSlider;
import hama.alsaygh.kw.adapter.ads.SubAdsAdapterSlider;
import hama.alsaygh.kw.adapter.home.AdapterHomeBestProduct;
import hama.alsaygh.kw.adapter.home.AdapterHomeBestProductDark;
import hama.alsaygh.kw.adapter.home.AdapterHomeBestStores;
import hama.alsaygh.kw.adapter.home.AdapterHomeBestStoresDark;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.GetHomeResponse;
import hama.alsaygh.kw.api.responce.MainAdsResponse;
import hama.alsaygh.kw.api.responce.SubAdsResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.fragment.home.store.StorePage;
import hama.alsaygh.kw.listener.OnAdsClickListener;
import hama.alsaygh.kw.listener.OnProductClickListener;
import hama.alsaygh.kw.listener.OnStoreClickListener;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.Cons;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;
import me.relex.circleindicator.CircleIndicator;

public class HomeFragment extends BaseFragment implements OnStoreClickListener, OnProductClickListener, OnAdsClickListener {
    RecyclerView best_storesRv, best_selingRv;
    private ViewPager viewPager, vp_sub_ads;
    AdapterHomeBestStores adapterHomeBestStores;
    AdapterHomeBestStoresDark adapterHomeBestStoresDark;
    AdapterHomeBestProduct adapterHomeRecentlyAdded;
    AdapterHomeBestProductDark adapterHomeRecentlyAddedDark;
    ImageView toolbar_img, love_img, notification_img, see_all1, see_all2;

    TextView SeeAll_bestStores, SeeAll_bestSelling, best_stores_text, best_seling_text;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    MainAdsHomeAdapterSlider sliderAdapter;
    SubAdsAdapterSlider subAdsAdapterSlider;
    CircleIndicator indicator, indicator1;
    TextView homeText;
    LinearLayout liner1;
    ConstraintLayout cl_pager, cl_subAds;


    Skeleton skeletonStore;
    Skeleton skeletonProduct;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home1, container, false);
    }


    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragmentManager = getParentFragmentManager();
        cl_pager = view.findViewById(R.id.cl_pager);
        cl_subAds = view.findViewById(R.id.cl_subAds);
        viewPager =  view.findViewById(R.id.viewPager);

        homeText =  view.findViewById(R.id.home1);
        toolbar_img =  view.findViewById(R.id.toolbar_img);
        notification_img =  view.findViewById(R.id.notification_img);
        love_img =  view.findViewById(R.id.love_img);
        //  toolbar_home=(Toolbar)view.findViewById(R.id.toolbar);
        best_stores_text =  view.findViewById(R.id.best_stores);
        best_seling_text =  view.findViewById(R.id.best_seling);
        see_all1 =  view.findViewById(R.id.see_all1);
        see_all2 =  view.findViewById(R.id.see_all2);

        vp_sub_ads =  view.findViewById(R.id.vp_sub_ads);
        indicator1 =  view.findViewById(R.id.pageIndicatorView_sub_ads);


        //  home1_img3_dark=(ImageView)view.findViewById(R.id.home1_img3_dark);
        liner1 =  view.findViewById(R.id.liner1);
        toolbar_img.setOnClickListener(v -> startActivity(new Intent(v.getContext(), MarketPriceActivity.class)));

        indicator =  view.findViewById(R.id.pageIndicatorView);

        ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // pageIndicatorView.setSelection(position);
                //   pageIndicatorView.setAnimationType(AnimationType.WORM);
            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        };
        viewPager.addOnPageChangeListener(viewListener);

        SeeAll_bestStores =  view.findViewById(R.id.see_all_best_stores);
        SeeAll_bestStores.setOnClickListener(v -> {


            if (getActivity() != null && getActivity() instanceof HomeActivity) {
                ((HomeActivity) getActivity()).openAllStore();
            }
        });
        SeeAll_bestSelling =  view.findViewById(R.id.Seeall_bestSelling);
        SeeAll_bestSelling.setOnClickListener(v -> {

        });

        best_storesRv =  view.findViewById(R.id.home_rf);
        best_selingRv =  view.findViewById(R.id.home_rf1);

        skeletonStore = SkeletonLayoutUtils.applySkeleton(best_storesRv, R.layout.card_view_home_rf, 2);
        skeletonProduct = SkeletonLayoutUtils.applySkeleton(best_selingRv, R.layout.card_view_home_rf2_dark, 2);

        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeletonProduct);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeletonStore);

        love_img.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), WishListActivity.class);
            startActivity(intent);
        });


        best_selingRv.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        best_storesRv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        notification_img.setOnClickListener(v -> startActivity(new Intent(v.getContext(), NotificationsActivity.class)));

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
           // homeText.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
           // toolbar_img.setImageResource(R.drawable.ic_toolbar_img_dark);
            love_img.setImageResource(R.drawable.ic_love_dark);
           // notification_img.setImageResource(R.drawable.ic_notifigation_img_dark);
            //  toolbar_home.setBackground(R.color.backgroundColor);
           // best_stores_text.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
           // best_seling_text.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
           // see_all1.setImageResource(R.drawable.ic_see_all_dark);
           // see_all2.setImageResource(R.drawable.ic_see_all_dark);
           // liner1.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeletonProduct);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeletonStore);

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        if (MainApplication.isConnected) {

            skeletonProduct.showSkeleton();
            skeletonStore.showSkeleton();
            new Thread(() -> {

                final GetHomeResponse mainAdsResponse = RequestWrapper.getInstance().getHome(view.getContext());
                if (mainAdsResponse.isStatus()) {

                    if (getActivity() != null)
                        getActivity().runOnUiThread(() -> {

                            skeletonProduct.showOriginal();
                            skeletonStore.showOriginal();
                            if (mainAdsResponse.getData() != null) {
                                if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {

                                    adapterHomeBestStoresDark = new AdapterHomeBestStoresDark(HomeFragment.this, mainAdsResponse.getStores(), getActivity());
                                    adapterHomeRecentlyAddedDark = new AdapterHomeBestProductDark(mainAdsResponse.getBestSells(), HomeFragment.this);

                                    best_storesRv.setAdapter(adapterHomeBestStoresDark);
                                    best_selingRv.setAdapter(adapterHomeRecentlyAddedDark);

                                } else {

                                    adapterHomeRecentlyAdded = new AdapterHomeBestProduct(mainAdsResponse.getBestSells(), HomeFragment.this);
                                    adapterHomeBestStores = new AdapterHomeBestStores(HomeFragment.this, mainAdsResponse.getStores(), getActivity());
                                    best_storesRv.setAdapter(adapterHomeBestStores);
                                    best_selingRv.setAdapter(adapterHomeRecentlyAdded);

                                }
                            }
                        });

                } else {


                    if (getActivity() != null)
                        getActivity().runOnUiThread(() -> {
                            skeletonProduct.showOriginal();
                            skeletonStore.showOriginal();

                            Snackbar.make(view, mainAdsResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                        });
                }

            }).start();
        }


        if (MainApplication.isConnected) {
            new Thread(() -> {

                final MainAdsResponse mainAdsResponse = RequestWrapper.getInstance().getMainAds(view.getContext());
                if (mainAdsResponse.isStatus()) {

                    if (getActivity() != null)
                        getActivity().runOnUiThread(() -> {
                            if (mainAdsResponse.getData() != null && !mainAdsResponse.getData().isEmpty()) {
                                sliderAdapter = new MainAdsHomeAdapterSlider(getLayoutInflater(), getActivity(), mainAdsResponse.getData(), HomeFragment.this);
                                viewPager.setAdapter(sliderAdapter);
                                indicator.setViewPager(viewPager);
                                cl_pager.setVisibility(View.VISIBLE);
                            } else {
                                cl_pager.setVisibility(View.GONE);
                            }
                        });
                } else {
                    if (getActivity() != null)
                        getActivity().runOnUiThread(() -> {
                            cl_pager.setVisibility(View.GONE);
                            Snackbar.make(view, mainAdsResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                        });
                }
            }).start();
        }

        if (MainApplication.isConnected) {
            new Thread(() -> {

                final SubAdsResponse mainAdsResponse = RequestWrapper.getInstance().getSubAds(view.getContext());
                if (mainAdsResponse.isStatus()) {

                    if (getActivity() != null && isAdded())
                        getActivity().runOnUiThread(() -> {
                            if (mainAdsResponse.getData() != null && !mainAdsResponse.getData().isEmpty()) {
                                subAdsAdapterSlider = new SubAdsAdapterSlider(getLayoutInflater(), getActivity(), mainAdsResponse.getData(), HomeFragment.this);
                                vp_sub_ads.setAdapter(subAdsAdapterSlider);
                                indicator1.setViewPager(vp_sub_ads);
                                cl_subAds.setVisibility(View.VISIBLE);
                            } else {
                                cl_subAds.setVisibility(View.GONE);
                            }
                        });
                } else {
                    if (getActivity() != null)
                        getActivity().runOnUiThread(() -> {
                            cl_subAds.setVisibility(View.GONE);
                            Snackbar.make(view, mainAdsResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                        });
                }
            }).start();
        }
    }


    @Override
    public void onStoreClick(Store store, int position, String tag) {
        HomeActivity.position = HomeActivity.StoreDetails;
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.liner1, StorePage.newInstance(store));
        fragmentTransaction.commit();
    }

    @Override
    public void onProductClick(Product product, int position) {
        Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
        intent.putExtra(Cons.PRODUCT, product);
        intent.putExtra(Cons.PRODUCT_ID, product.getId());
        startActivity(intent);
    }

    @Override
    public void onAdsClick(String redirect_type, String id) {
        if (redirect_type.equalsIgnoreCase("store")) {
            Store store = new Store();
            store.setId(Integer.parseInt(id));
            store.setStore_name("");
            store.setStore_description("");
            HomeActivity.position = HomeActivity.StoreDetails;
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.liner1, StorePage.newInstance(store));
            fragmentTransaction.commit();
        } else if (redirect_type.equalsIgnoreCase("product")) {
            Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
            intent.putExtra(Cons.PRODUCT_ID, Integer.parseInt(id));
            startActivity(intent);
        }
    }
}
