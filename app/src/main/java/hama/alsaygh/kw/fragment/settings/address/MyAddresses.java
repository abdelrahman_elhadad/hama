package hama.alsaygh.kw.fragment.settings.address;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.address.AdapterMyAddress;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.AddressesResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.dialog.address.PopUpAddNewAddress;
import hama.alsaygh.kw.dialog.address.PopUpDeleteMyAddress;
import hama.alsaygh.kw.dialog.address.PopUpEditAddress;
import hama.alsaygh.kw.listener.OnAddressListener;
import hama.alsaygh.kw.model.address.Address;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class MyAddresses extends BaseFragment implements OnAddressListener {
    FragmentManager fragmentManager;
    TextView txt_toolbar;
    ImageView img_back;
    LinearLayout parent_address_lan;
    Button button3;
    View view;
    Skeleton skeleton;
    RecyclerView rv_address;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.my_addresses, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        parent_address_lan = (LinearLayout) view.findViewById(R.id.parent_address_lan);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        txt_toolbar = (TextView) view.findViewById(R.id.txt_toolbar);
        button3 = view.findViewById(R.id.button3);
        rv_address = view.findViewById(R.id.rv_address);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        });

        rv_address.setLayoutManager(new LinearLayoutManager(getActivity()));
        skeleton = SkeletonLayoutUtils.applySkeleton(rv_address, R.layout.row_address_item, 1);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);
            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            parent_address_lan.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            img_back.setImageTintList(ContextCompat.getColorStateList(getContext(), R.color.whiteColor));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopUpAddNewAddress popUpAddNewAddress = new PopUpAddNewAddress();
                popUpAddNewAddress.setOnAddressClickListener(MyAddresses.this);
                popUpAddNewAddress.show(fragmentManager, "add new address");

            }
        });

        refresh(view);
    }

    private void refresh(@NonNull final View view) {
        if (MainApplication.isConnected) {

            skeleton.showSkeleton();
            new Thread(new Runnable() {
                @Override
                public void run() {

                    final AddressesResponse cardsResponse = RequestWrapper.getInstance().getAddresses(getContext());
                    if (cardsResponse.isStatus()) {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    skeleton.showOriginal();
                                    AdapterMyAddress adapterPaymentCard = new AdapterMyAddress(getActivity(), MyAddresses.this, cardsResponse.getData());
                                    rv_address.setAdapter(adapterPaymentCard);
                                }
                            });
                    } else {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    if (cardsResponse.getCode().equalsIgnoreCase("401")) {
                                        LoginDialog loginDialog = LoginDialog.newInstance();
                                        loginDialog.show(fragmentManager, "login");
                                    } else
                                        Snackbar.make(view, cardsResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });
                    }

                }
            }).start();

        } else {
            Snackbar.make(view, getActivity().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
        }
    }

    public void showDialog(final View view, final Address card) {
        PopupMenu popup = new PopupMenu(getActivity(), view);
        popup.getMenuInflater()
                .inflate(R.menu.address_popup_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.page_3:
                        PopUpEditAddress popUpEditCart = PopUpEditAddress.newInstance(card, MyAddresses.this);
                        popUpEditCart.show(fragmentManager, "pop up edit card");
                        return true;
                    case R.id.page_4:
                        PopUpDeleteMyAddress popUpDeleteMyCard = new PopUpDeleteMyAddress();
                        popUpDeleteMyCard.setCard(card);
                        popUpDeleteMyCard.setOnAddressClickListener(MyAddresses.this);
                        popUpDeleteMyCard.show(fragmentManager, "delete my address");
                        return true;
                    default:
                        return false;
                }
            }
        });

        popup.show(); //showing popup menu
    }

    @Override
    public void onAddressClick(View view, Address address, int position) {

        showDialog(view, address);
    }

    @Override
    public void onAddressRefresh() {

        refresh(view);
    }
}
