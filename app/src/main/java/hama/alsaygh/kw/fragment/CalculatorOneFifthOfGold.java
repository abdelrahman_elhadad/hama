package hama.alsaygh.kw.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.MyorderArray;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class CalculatorOneFifthOfGold extends BaseFragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    RecyclerView recycle_complited;
    ArrayList<MyorderArray> myordersArray;
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    TextView  salary,gold,clear_all,one_fifth;
    EditText ed_salary,ed_gold,ed_one_fifth1;
    Button btn_calculate;

    public CalculatorOneFifthOfGold() {
    }
    public static CalculatorOneFifthOfGold newInstance(String param1, String param2) {
        CalculatorOneFifthOfGold fragment = new CalculatorOneFifthOfGold();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // return inflater.inflate(R.layout.fragment_canceled, container, false);
        fragmentManager=getChildFragmentManager();
        View view = inflater.inflate(R.layout.calculator_one_fifth_of_gold, container, false);
        salary=(TextView)view.findViewById(R.id.salary);
        gold=(TextView)view.findViewById(R.id.gold);
        clear_all=(TextView)view.findViewById(R.id.clear_all);
        one_fifth=(TextView)view.findViewById(R.id.one_fifth);
        ed_salary=(EditText)view.findViewById(R.id.ed_salary);
        ed_gold=(EditText)view.findViewById(R.id.ed_gold);
        ed_one_fifth1=(EditText)view.findViewById(R.id.ed_one_fifth1);
        btn_calculate=(Button)view.findViewById(R.id.btn_calculate);

        if(SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            ed_salary.setBackgroundResource(R.drawable.back_edittext_dark);
            ed_gold.setBackgroundResource(R.drawable.back_edittext_dark);
            ed_one_fifth1.setBackgroundResource(R.drawable.back_edittext_dark);
            btn_calculate.setBackgroundResource(R.drawable.back_button_dark);
            clear_all.setTextColor(ContextCompat.getColor(getContext(),R.color.color_navigation));
            salary.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            gold.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            one_fifth.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            ed_salary.setHintTextColor(ContextCompat.getColor(getContext(),R.color.sign_in_dark));
            ed_gold.setHintTextColor(ContextCompat.getColor(getContext(),R.color.sign_in_dark));
            ed_one_fifth1.setHintTextColor(ContextCompat.getColor(getContext(),R.color.sign_in_dark));



        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        return view;
    }
}
