package hama.alsaygh.kw.fragment.settings.appSetting;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Locale;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.ProfileResponse;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.db.table.User;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class NotificationSetting extends BaseFragment implements CompoundButton.OnCheckedChangeListener {

    FragmentManager fragmentManager;
    View view11, view1, view111, view21, view14, view17;
    TextView txt_toolbar, textView11, textView1, textView121, textView21, textView18, textView19;
    LinearLayout n_setting;
    AppCompatImageView img_back;
    SwitchCompat switchGeneral, switchOrderStatus, switchNewOffer, switchEvent, switchNewAds, switchPaymentMethod;
    User user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.notification_setting, container, false);
        fragmentManager = getFragmentManager();
        img_back = view.findViewById(R.id.img_back);
        txt_toolbar = view.findViewById(R.id.txt_toolbar);
        textView11 = view.findViewById(R.id.textView11);
        textView1 = view.findViewById(R.id.textView1);
        textView121 = view.findViewById(R.id.textView121);
        textView21 = view.findViewById(R.id.textView21);
        textView18 = view.findViewById(R.id.textView18);
        textView19 = view.findViewById(R.id.textView19);
        view1 = view.findViewById(R.id.view1);
        view111 = view.findViewById(R.id.view111);
        view21 = view.findViewById(R.id.view21);
        view14 = view.findViewById(R.id.view14);
        view17 = view.findViewById(R.id.view17);
        view11 = view.findViewById(R.id.view11);
        n_setting = view.findViewById(R.id.n_setting);
        if (Locale.getDefault().getLanguage().equals("ar")) {
            if (getContext() != null) {
                Drawable d = ContextCompat.getDrawable(getContext(), R.drawable.back_view_seeting_ar);
                view1.setBackground(d);
                view111.setBackground(d);
                view21.setBackground(d);
                view14.setBackground(d);
                view17.setBackground(d);
                view11.setBackground(d);
            }

        }

        switchGeneral = view.findViewById(R.id.switch2);
        switchOrderStatus = view.findViewById(R.id.switch4);
        switchNewOffer = view.findViewById(R.id.switch3);
        switchEvent = view.findViewById(R.id.switch6);
        switchNewAds = view.findViewById(R.id.switch7);
        switchPaymentMethod = view.findViewById(R.id.switch8);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().onBackPressed();

            }
        });
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            img_back.setImageResource(R.drawable.ic_back_icon_dark);

            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView11.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView1.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView21.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView18.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView19.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView121.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            n_setting.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));

            switchGeneral.setTrackTintList(ContextCompat.getColorStateList(getContext(), R.color.switch_tracker_dark));
            //  switch2.setThumbTintList(ContextCompat.getColorStateList(getContext(),R.color.switch_thum_dark));

            switchOrderStatus.setTrackTintList(ContextCompat.getColorStateList(getContext(), R.color.switch_tracker_dark));
            // switch4.setThumbTintList(ContextCompat.getColorStateList(getContext(),R.color.switch_thum_dark));
            switchOrderStatus.setTrackTintList(ContextCompat.getColorStateList(getContext(), R.color.switch_tracker_dark));
            switchNewOffer.setTrackTintList(ContextCompat.getColorStateList(getContext(), R.color.switch_tracker_dark));
            switchEvent.setTrackTintList(ContextCompat.getColorStateList(getContext(), R.color.switch_tracker_dark));
            switchNewAds.setTrackTintList(ContextCompat.getColorStateList(getContext(), R.color.switch_tracker_dark));
            switchPaymentMethod.setTrackTintList(ContextCompat.getColorStateList(getContext(), R.color.switch_tracker_dark));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        new Thread(new Runnable() {
            @Override
            public void run() {

                AppDatabase appDatabase = AppDatabase.newInstance(getContext());
                List<User> userList = appDatabase.userDao().getAll();
                if (userList != null && !userList.isEmpty()) {
                    user = userList.get(0);

                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                switchGeneral.setChecked(user.isGeneral_notifications());
                                switchNewOffer.setChecked(user.isNew_offers_notification());
                                switchEvent.setChecked(user.isEvent_notification());
                                switchOrderStatus.setChecked(user.isOrder_notification());
                                switchNewAds.setChecked(user.isAdv_notification());
                                switchPaymentMethod.setChecked(user.isPayment_method_notification());

                                switchGeneral.setOnCheckedChangeListener(NotificationSetting.this);
                                switchNewOffer.setOnCheckedChangeListener(NotificationSetting.this);
                                switchEvent.setOnCheckedChangeListener(NotificationSetting.this);
                                switchOrderStatus.setOnCheckedChangeListener(NotificationSetting.this);
                                switchNewAds.setOnCheckedChangeListener(NotificationSetting.this);
                                switchPaymentMethod.setOnCheckedChangeListener(NotificationSetting.this);

                            }
                        });

                }
            }
        }).start();


        return view;

    }

    @Override
    public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {

        final int id = buttonView.getId();
        if (id == R.id.switch2) {
            user.setGeneral_notifications(isChecked);
        } else if (id == R.id.switch3) {
            user.setNew_offers_notification(isChecked);
        } else if (id == R.id.switch4) {
            user.setOrder_notification(isChecked);
        } else if (id == R.id.switch6) {
            user.setEvent_notification(isChecked);
        } else if (id == R.id.switch7) {
            user.setAdv_notification(isChecked);
        } else if (id == R.id.switch8) {
            user.setPayment_method_notification(isChecked);
        }
        new Thread(new Runnable() {
            @Override
            public void run() {

                final ProfileResponse loginResponse = RequestWrapper.getInstance().updateNotification(getContext(), user);

                if (loginResponse.isStatus()) {

                    AppDatabase appDatabase = AppDatabase.newInstance(getContext());
                    appDatabase.userDao().update(loginResponse.getData());

                } else {

                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Snackbar.make(buttonView, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        });

                }
            }
        }).start();
    }
}
