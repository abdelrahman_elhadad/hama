package hama.alsaygh.kw.fragment.search;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.product.ProductDetailsActivity;
import hama.alsaygh.kw.adapter.product.AdapterStorePageProducts;
import hama.alsaygh.kw.adapter.product.AdapterStorePageProductsDark;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.ProductsSearchResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnProductClickListener;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.Cons;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class SearchResultProductFragment extends BaseFragment implements OnProductClickListener {
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    ImageView imageback;
    TextView text_store_serch, recently_add;
    LinearLayout parent_serch_stores, liner_search_stores;
    SearchView edit_serch_for_stores;
    RecyclerView rv_search_for_stores;
    ArrayList<Product> best_stores;
    private Skeleton skeleton;
    String search;

    public static SearchResultProductFragment newInstance(String search) {

        SearchResultProductFragment fragment = new SearchResultProductFragment();
        fragment.setSearch(search);
        return fragment;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        ((AppCompatActivity)getActivity()).getSupportActionBar().hide(); //<< this
//        SharedPreferences editor = getActivity().getApplicationContext().getSharedPreferences("share", MODE_PRIVATE);
//        if (editor.getBoolean("checked",true))
//            getActivity().setTheme(R.style.darktheme);
//        else
//            getActivity().setTheme(R.style.AppTheme);
        View view = inflater.inflate(R.layout.search_for_stores, container, false);
//        setLightStatusBar();
        fragmentManager = getFragmentManager();
        imageback =  view.findViewById(R.id.imageView4);
        imageback.setOnClickListener(v -> {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.liner1, new SearchFragment());
            fragmentTransaction.commit();
        });
        edit_serch_for_stores = view.findViewById(R.id.edit_serch_for_stores);
        parent_serch_stores =  view.findViewById(R.id.parent_serch_stores);
        recently_add =  view.findViewById(R.id.recently_add);
        text_store_serch =  view.findViewById(R.id.text_store_serch);
        liner_search_stores =  view.findViewById(R.id.liner_search_stores);
        rv_search_for_stores =  view.findViewById(R.id.recycle_search_for_stores);
        best_stores = new ArrayList<>();
        rv_search_for_stores.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        skeleton = SkeletonLayoutUtils.applySkeleton(rv_search_for_stores, R.layout.row_stores_spanned, 3);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);

            text_store_serch.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            edit_serch_for_stores.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark11));
            ImageView searchIcon = edit_serch_for_stores.findViewById(androidx.appcompat.R.id.search_button);
            searchIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_icon_search));
            TextView searchAutoComplete = edit_serch_for_stores.findViewById(androidx.appcompat.R.id.search_src_text);
            searchAutoComplete.setHintTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            searchAutoComplete.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));


            recently_add.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
            parent_serch_stores.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            liner_search_stores.setBackgroundResource(R.drawable.back_liner_search_dark);


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        edit_serch_for_stores.setQueryHint(getString(R.string.search_products));

        if (search != null && !search.isEmpty())
            getSearch();


        edit_serch_for_stores.setOnCloseListener(() -> {
            search = "";
            edit_serch_for_stores.setQuery("", true);
            return false;
        });


        edit_serch_for_stores.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search = query;
                getSearch();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        return view;

    }

    private void getSearch() {
        if (MainApplication.isConnected) {

            skeleton.showSkeleton();

            new Thread(() -> {

                final ProductsSearchResponse getStoresResponse = RequestWrapper.getInstance().getSearchSProducts(getContext(), search);
                if (getStoresResponse.isStatus()) {

                    if (getActivity() != null)
                        getActivity().runOnUiThread(() -> {
                            skeleton.showOriginal();
                            best_stores.clear();
                            best_stores.addAll(getStoresResponse.getSProducts());

                            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                                AdapterStorePageProductsDark adapterStoresSpannedDark = new AdapterStorePageProductsDark( getActivity(),SearchResultProductFragment.this,best_stores);
                                rv_search_for_stores.setAdapter(adapterStoresSpannedDark);
                            } else {
                                AdapterStorePageProducts adapterStoresSpanned = new AdapterStorePageProducts(getActivity(),SearchResultProductFragment.this,best_stores );
                                rv_search_for_stores.setAdapter(adapterStoresSpanned);
                            }
                        });
                }

            }).start();
        }

    }

    private void setLightStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = getActivity().getWindow().getDecorView().getSystemUiVisibility(); // get current flag
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;   // add LIGHT_STATUS_BAR to flag
            getActivity().getWindow().getDecorView().setSystemUiVisibility(flags);
            getActivity().getWindow().setStatusBarColor(Color.TRANSPARENT); // optional
        }
    }



    @Override
    public void onProductClick(Product product, int position) {
        Intent intent = new Intent(getContext(), ProductDetailsActivity.class);
        intent.putExtra(Cons.PRODUCT_ID, product.getId());
        intent.putExtra(Cons.PRODUCT, product);
        startActivity(intent);
    }
}
