package hama.alsaygh.kw.fragment.home.store;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arasthel.spannedgridlayoutmanager.SpanSize;
import com.arasthel.spannedgridlayoutmanager.SpannedGridLayoutManager;
import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.activity.marketPrice.MarketPriceActivity;
import hama.alsaygh.kw.activity.notification.NotificationsActivity;
import hama.alsaygh.kw.activity.product.WishListActivity;
import hama.alsaygh.kw.adapter.store.AdapterStoresSpanned;
import hama.alsaygh.kw.adapter.store.AdapterStoresSpannedDark;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.BannerAdsResponse;
import hama.alsaygh.kw.api.responce.GetStoresResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnStoreClickListener;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;
import kotlin.jvm.functions.Function1;

public class AllStores extends BaseFragment implements OnStoreClickListener {
    RecyclerView recyclerView1;
    ArrayList<Serializable> best_stores;
    ImageView imagetoolbar;
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    boolean isReverse = false;
    ImageView img_toolbar, img_notification, img_love;
    TextView homeText;
    RelativeLayout liner_all_stores;
    private Skeleton skeleton;
    private ImageView iv_ads;
    private TextView tv_ads;
    private ProgressBar pb_ads;
    private ConstraintLayout cl_ads;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.all_stores, container, false);
        recyclerView1 = (RecyclerView) view.findViewById(R.id.home_rf);
        imagetoolbar = (ImageView) view.findViewById(R.id.img_toolbar);
        iv_ads = view.findViewById(R.id.iv_ads);
        tv_ads = view.findViewById(R.id.tv_ads);
        pb_ads = view.findViewById(R.id.pb_ads);
        cl_ads = view.findViewById(R.id.cl_ads);
        fragmentManager = getFragmentManager();
        imagetoolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), MarketPriceActivity.class));

            }
        });
        best_stores = new ArrayList<>();

        img_love = (ImageView) view.findViewById(R.id.img_love);
        img_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), WishListActivity.class);
                startActivity(intent);
            }
        });
        img_notification = (ImageView) view.findViewById(R.id.img_notification);
        homeText = (TextView) view.findViewById(R.id.toolbar_text);
        img_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(v.getContext(), NotificationsActivity.class));
            }
        });
        liner_all_stores = (RelativeLayout) view.findViewById(R.id.allstore);
        recyclerView1.setLayoutManager(new GridLayoutManager(getContext(), 2));
        skeleton = SkeletonLayoutUtils.applySkeleton(recyclerView1, R.layout.row_stores_spanned, 3);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
           // imagetoolbar.setImageResource(R.drawable.ic_toolbar_img_dark);
            img_love.setImageResource(R.drawable.ic_love_dark);
           // img_notification.setImageResource(R.drawable.ic_notifigation_img_dark);
           // homeText.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));

           // liner_all_stores.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        if (MainApplication.isConnected) {

            skeleton.showSkeleton();

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final GetStoresResponse getStoresResponse = RequestWrapper.getInstance().getStores(getContext());
                    final BannerAdsResponse mainAdsResponse = RequestWrapper.getInstance().getBanner(getContext());

                    if (getStoresResponse.isStatus()) {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    best_stores.clear();
                                    best_stores.addAll(getStoresResponse.getStores());
                                }
                            });
                    }


                    if (mainAdsResponse.isStatus()) {

                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    skeleton.showOriginal();
                                    if (!best_stores.isEmpty() && !mainAdsResponse.getData().isEmpty()) {

                                        layoutMangerWithAds();
                                        if (best_stores.size() == 3) {
                                            best_stores.add(3, mainAdsResponse.getData().get(0));
                                            cl_ads.setVisibility(View.GONE);
                                        } else if (best_stores.size() <= 2) {

                                            cl_ads.setVisibility(View.VISIBLE);


                                            tv_ads.setText(mainAdsResponse.getData().get(0).getTitle() + "\n" + mainAdsResponse.getData().get(0).getDescription());

                                            Picasso.get().load(mainAdsResponse.getData().get(0).getImage()).into(iv_ads, new Callback() {
                                                @Override
                                                public void onSuccess() {
                                                    pb_ads.setVisibility(View.GONE);
                                                }

                                                @Override
                                                public void onError(Exception e) {
                                                    pb_ads.setVisibility(View.GONE);
                                                }
                                            });

                                        } else
                                            cl_ads.setVisibility(View.GONE);


                                        int position = 3;
                                        for (int i = 0; i < mainAdsResponse.getData().size(); i++) {

                                            Log.i("ppppppp", "postion :" + position + " - i :" + i + " - size/3 : " + getStoresResponse.getStores().size() / 3);
                                            if (i < getStoresResponse.getStores().size() / 3) {

                                                best_stores.add(position, mainAdsResponse.getData().get(i));
                                                position += 4;
                                            } else
                                                break;
                                        }


                                    } else
                                        layoutMangerWithOutAds();
                                }
                            });
                    } else {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    layoutMangerWithOutAds();
                                    Snackbar.make(recyclerView1, mainAdsResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });
                    }


                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                                if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                                    AdapterStoresSpannedDark adapterStoresSpannedDark = new AdapterStoresSpannedDark(AllStores.this, best_stores, getActivity());
                                    recyclerView1.setAdapter(adapterStoresSpannedDark);
                                } else {
                                    AdapterStoresSpanned adapterStoresSpanned = new AdapterStoresSpanned(AllStores.this, best_stores, getActivity());
                                    recyclerView1.setAdapter(adapterStoresSpanned);
                                }
                            }
                        });

                }
            }).start();
        }


        return view;

    }

    private void layoutMangerWithAds() {
        SpannedGridLayoutManager spannedGridLayoutManager = new SpannedGridLayoutManager(SpannedGridLayoutManager.Orientation.VERTICAL, 4);
        spannedGridLayoutManager.setItemOrderIsStable(true);

        spannedGridLayoutManager.setSpanSizeLookup(new SpannedGridLayoutManager.SpanSizeLookup(new Function1<Integer, SpanSize>() {
            @Override
            public SpanSize invoke(Integer position) {
                Log.i("nnnnnnnnnn", " with ads  best_stores.size() " + best_stores.size() + " pppp " + (position + 1));

                if (position != 0 && ((position + 1) % 4 == 0)) {

                    return new SpanSize(4, 1);
                } else if (position != 1 && position % 4 == 1) {
                    if (position + 1 == best_stores.size()) {
                        return new SpanSize(2, 2);
                    } else {
                        if (!isReverse)
                            return new SpanSize(2, 4);
                        else
                            return new SpanSize(2, 2);
                    }
                } else if (position % 4 == 0) {

                    if (position + 1 == best_stores.size()) {
                        return new SpanSize(2, 2);
                    } else {

                        if (!isReverse) {
                            isReverse = true;
                            return new SpanSize(2, 4);
                        } else {
                            isReverse = false;
                            return new SpanSize(2, 2);
                        }
                    }
                } else {
                    return new SpanSize(2, 2);
                }
            }
        }));

        recyclerView1.setLayoutManager(spannedGridLayoutManager);
    }

    private void layoutMangerWithOutAds() {
        SpannedGridLayoutManager spannedGridLayoutManager = new SpannedGridLayoutManager(SpannedGridLayoutManager.Orientation.VERTICAL, 4);
        spannedGridLayoutManager.setItemOrderIsStable(true);

        spannedGridLayoutManager.setSpanSizeLookup(new SpannedGridLayoutManager.SpanSizeLookup(new Function1<Integer, SpanSize>() {
            @Override
            public SpanSize invoke(Integer position) {

                Log.i("nnnnnnnnnn", "best_stores.size() " + best_stores.size() + " pppp " + (position + 1));
                if (position % 6 == 1 || position % 6 == 3) {
                    if (position + 1 == best_stores.size())
                        return new SpanSize(2, 2);
                    else
                        return new SpanSize(2, 4);
                } else
                    return new SpanSize(2, 2);
            }
        }));

        recyclerView1.setLayoutManager(spannedGridLayoutManager);
    }

    @Override
    public void onStoreClick(Store store, int position, String tag) {
        HomeActivity.position = HomeActivity.StoreDetailsFromAllStore;
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.liner1, StorePage.newInstance(store));
        fragmentTransaction.commit();
    }
}
