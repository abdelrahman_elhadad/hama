package hama.alsaygh.kw.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class MyOrderTrackingOutOfDeliveryTrackOnTheMap extends BaseFragment implements RoutingListener, OnMapReadyCallback {
    static FragmentManager fragmentManager;
    TextView txt_order_tracking_toolbar;
    ImageView imageback;
    LinearLayout liner_order_traking;
    MapFragment mapFragment;
    ArrayList route;
    private GoogleMap mMap;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.order_traking_collapse, container, false);
        fragmentManager = getFragmentManager();
        liner_order_traking = (LinearLayout) view.findViewById(R.id.liner_order_traking);
        txt_order_tracking_toolbar = (TextView) view.findViewById(R.id.txt_order_tracking_toolbar);
        imageback = (ImageView) view.findViewById(R.id.imageback);

//
//        Routing routing = new Routing.Builder()
//                .travelMode(/* Travel Mode */)
//                .withListener(/* Listener that delivers routing results.*/)
//                .waypoints(/*waypoints*/)
//                .key(/*api key for quota management*/)
//                .build();
//        routing.execute();

//
//        start = new LatLng(18.015365, -77.499382);
//        waypoint= new LatLng(18.01455, -77.499333);
//        end = new LatLng(18.012590, -77.500659);
//
//        Routing routing = new Routing.Builder()
//                .travelMode(Routing.TravelMode.WALKING)
//                .withListener(this)
//                .waypoints(start, waypoint, end)
//                .build();
//        routing.execute();
//
//
//        @Override
//        public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex)
//        {
//            //code to add route to map here. See sample app for more details.
//        }
//
//
//        PolylineOptions polyOptions = new PolylineOptions();
//        polyOptions.width(10);
//        polyOptions.geodesic(true);
//        polyOptions.color(ContextCompat.getColor(Utils.getInstance().getContext(), R.color.home1));
//        polyOptions.addAll(routeTrip);
//        polyOptions.zIndex(10);
//        mMap.addPolyline(polyOptions);


        if(SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            txt_order_tracking_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
            liner_order_traking.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        return view;
    }

    @Override
    public void onRoutingFailure(RouteException e) {

    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        //code to add route to map here. See sample app for more details.
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.width(10);
        polyOptions.geodesic(true);
        polyOptions.color(ContextCompat.getColor(getContext(), R.color.home1));
        polyOptions.addAll(route.get(shortestRouteIndex).getPoints());
        polyOptions.zIndex(10);
        mMap.addPolyline(polyOptions);

    }

    @Override
    public void onRoutingCancelled() {

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        LatLng start = new LatLng(18.015365, -77.499382);
        LatLng waypoint = new LatLng(18.01455, -77.499333);
        LatLng end = new LatLng(18.012590, -77.500659);

        Routing routing = new Routing.Builder()
                .travelMode(Routing.TravelMode.WALKING)
                .withListener(this)
                .waypoints(start, waypoint, end)
                .build();
        routing.execute();
    }
}
