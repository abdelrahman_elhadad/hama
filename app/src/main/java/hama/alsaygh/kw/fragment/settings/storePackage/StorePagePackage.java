package hama.alsaygh.kw.fragment.settings.storePackage;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.activity.product.ProductDetailsPackageActivity;
import hama.alsaygh.kw.adapter.product.AdapterStorePageProducts;
import hama.alsaygh.kw.adapter.product.AdapterStorePageProductsDark;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.GetStoreProductResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.fragment.home.store.AllStores;
import hama.alsaygh.kw.fragment.home.store.Filterby;
import hama.alsaygh.kw.fragment.home.store.Sortingby;
import hama.alsaygh.kw.listener.OnProductClickListener;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.Cons;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;
import hama.alsaygh.kw.utils.image.CircleTransform;

public class StorePagePackage extends BaseFragment implements OnProductClickListener {
    private AllStores mfragment;
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    LinearLayout linerFilter, linerSort;
    AdapterStorePageProducts adapterStorePageProducts;
    AdapterStorePageProductsDark adapterStorePageProductsDark;
    ArrayList<Product> bestpackgings;
    RecyclerView store_pagrRV;
    ImageView imageView_filter, img_sort;
    TextView sort, filter, storeDescription;

    private Store store;
    Skeleton skeleton;

    public static StorePagePackage newInstance(Store store) {
        StorePagePackage fragment = new StorePagePackage();
        fragment.setStore(store);
        return fragment;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.store_page, container, false);

        fragmentManager = getFragmentManager();
        final ProgressBar pb_product = view.findViewById(R.id.pb_product);
        linerFilter = (LinearLayout) view.findViewById(R.id.liner_filter);
        linerFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.SORT_PRODUCT_PACKAGE;
                HomeActivity.store = store;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, Filterby.newInstance(store,2));
                fragmentTransaction.commit();
            }
        });
        linerSort = (LinearLayout) view.findViewById(R.id.liner_sort);
        linerSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HomeActivity.position = HomeActivity.SORT_PRODUCT_PACKAGE;
                HomeActivity.store = store;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, Sortingby.newInstance(store,2));
                fragmentTransaction.commit();

            }
        });

        bestpackgings = new ArrayList<>();


        store_pagrRV = (RecyclerView) view.findViewById(R.id.store_pageRv);
        store_pagrRV.setLayoutManager(new GridLayoutManager(getActivity(), 2));


        img_sort = (ImageView) view.findViewById(R.id.imageView319);
        imageView_filter = (ImageView) view.findViewById(R.id.imageView39);
        sort = (TextView) view.findViewById(R.id.textView517);
        filter = (TextView) view.findViewById(R.id.textView57);
        storeDescription = (TextView) view.findViewById(R.id.textView56);

        skeleton = SkeletonLayoutUtils.applySkeleton(store_pagrRV, R.layout.card_store_page_dark, 4);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);
            linerFilter.setBackgroundResource(R.drawable.back_liner_store_page_dark);
            linerSort.setBackgroundResource(R.drawable.back_liner_store_page_dark);
            filter.setTextColor(ContextCompat.getColor(getContext(), R.color.color_navigation));
            sort.setTextColor(ContextCompat.getColor(getContext(), R.color.color_navigation));
            img_sort.setImageResource(R.drawable.ic_sort);
            imageView_filter.setImageResource(R.drawable.ic_filter);
            storeDescription.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            store_pagrRV.setAdapter(adapterStorePageProductsDark);


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        if (store != null) {

            TextView storeName = view.findViewById(R.id.textView16);
            ImageView ivCover = view.findViewById(R.id.imageView35);
            ImageView ivLogo = view.findViewById(R.id.imageView38);
            storeName.setText(store.getStore_name());
            storeDescription.setText(store.getStore_description());

            if (store.getLogo() != null && !store.getLogo().isEmpty()) {
                Picasso.get().load(store.getLogo()).fit().transform(new CircleTransform()).into(ivLogo);
            }
            if (store.getLogo() != null && !store.getLogo().isEmpty()) {
                Picasso.get().load(store.getLogo()).fit().into(ivCover);
            }

            if (MainApplication.isConnected) {
                pb_product.setVisibility(View.GONE);
                store_pagrRV.setVisibility(View.VISIBLE);
                skeleton.showSkeleton();
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        final GetStoreProductResponse getStoreProductResponse = RequestWrapper.getInstance().getStoreProductPackage(getContext(), store.getId());

                        if (getStoreProductResponse.isStatus()) {
                            if (getActivity() != null)
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        skeleton.showOriginal();
                                        bestpackgings.clear();
                                        bestpackgings.addAll(getStoreProductResponse.getData());

                                        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                                            adapterStorePageProductsDark = new AdapterStorePageProductsDark(getContext(), StorePagePackage.this, bestpackgings);
                                            store_pagrRV.setAdapter(adapterStorePageProductsDark);

                                        } else {

                                            adapterStorePageProducts = new AdapterStorePageProducts(getContext(), StorePagePackage.this, bestpackgings);
                                            store_pagrRV.setAdapter(adapterStorePageProducts);
                                        }
                                    }
                                });
                        } else {
                            if (getActivity() != null)
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        skeleton.showOriginal();
                                        Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                                    }
                                });
                        }
                    }
                }).start();

            } else {
                pb_product.setVisibility(View.GONE);
                Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
            }

        } else {
            pb_product.setVisibility(View.GONE);
        }
        return view;
    }


    @Override
    public void onProductClick(Product product, int position) {
        Intent intent = new Intent(getContext(), ProductDetailsPackageActivity.class);
        intent.putExtra(Cons.STORE_ID, store.getId());
        intent.putExtra(Cons.PRODUCT_ID, product.getId());
        intent.putExtra(Cons.PRODUCT, product);
        startActivity(intent);
    }
}
