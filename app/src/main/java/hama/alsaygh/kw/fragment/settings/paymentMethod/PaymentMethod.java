package hama.alsaygh.kw.fragment.settings.paymentMethod;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.paymentCard.AdapterPaymentCard;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.CardResponse;
import hama.alsaygh.kw.api.responce.CardsResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.dialog.card.PopUpAddNewCart;
import hama.alsaygh.kw.dialog.card.PopUpDeleteMyCard;
import hama.alsaygh.kw.dialog.card.PopUpEditCart;
import hama.alsaygh.kw.listener.OnCardClickListener;
import hama.alsaygh.kw.model.paymentCard.Card;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class PaymentMethod extends BaseFragment implements OnCardClickListener {
    FragmentManager fragmentManager;
    LinearLayout parent_payment_method, liner;
    TextView textView77, txt_toolbar;
    Button button3;
    ImageView back_img;
    RecyclerView rv_cards;
    Skeleton skeleton;
    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.payment_methods, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        fragmentManager = getFragmentManager();
        rv_cards = view.findViewById(R.id.rv_cards);
        parent_payment_method = view.findViewById(R.id.parent_payment_method);
        textView77 = view.findViewById(R.id.textView77);
        button3 = view.findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopUpAddNewCart popUpAddNewCart = new PopUpAddNewCart();
                popUpAddNewCart.setOnCardClickListener(PaymentMethod.this);
                popUpAddNewCart.show(getFragmentManager(), "pop up add new card");
            }
        });
        liner = view.findViewById(R.id.liner);
        back_img = view.findViewById(R.id.back_img);
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();

            }
        });
        txt_toolbar = view.findViewById(R.id.txt_toolbar);
        rv_cards.setLayoutManager(new LinearLayoutManager(getContext()));
        skeleton = SkeletonLayoutUtils.applySkeleton(rv_cards, R.layout.row_item_payment_card, 1);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);
            parent_payment_method.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            textView77.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            button3.setBackgroundResource(R.drawable.back_button_dark);
            liner.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark11));
            back_img.setImageResource(R.drawable.ic_back_icon_dark);
            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        refresh(view);

    }

    private void refresh(@NonNull final View view) {
        if (MainApplication.isConnected) {

            skeleton.showSkeleton();
            new Thread(new Runnable() {
                @Override
                public void run() {

                    final CardsResponse cardsResponse = RequestWrapper.getInstance().getCards(getContext());
                    if (cardsResponse.isStatus()) {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    skeleton.showOriginal();
                                    AdapterPaymentCard adapterPaymentCard = new AdapterPaymentCard(PaymentMethod.this, cardsResponse.getData());
                                    rv_cards.setAdapter(adapterPaymentCard);
                                }
                            });
                    } else {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    if(cardsResponse.getCode().equalsIgnoreCase("401"))
                                    {
                                        LoginDialog loginDialog=LoginDialog.newInstance();
                                        loginDialog.show(fragmentManager,"login");
                                    }else
                                        Snackbar.make(view, cardsResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });
                    }

                }
            }).start();

        } else {
            Snackbar.make(view, getActivity().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
        }
    }

    public void showDialog(final View view, final Card card) {
        PopupMenu popup = new PopupMenu(getActivity(), view);
        popup.getMenuInflater()
                .inflate(R.menu.popup_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.page_1:
                        if (MainApplication.isConnected) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {

                                    final CardResponse cardResponse = RequestWrapper.getInstance().setAsPrimaryCard(getContext(), card.getId());

                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                Snackbar.make(view, cardResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                            }
                                        });

                                }
                            }).start();
                        } else {
                            Snackbar.make(view, getActivity().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                        }
                        return true;
                    case R.id.page_3:
                        PopUpEditCart popUpEditCart = new PopUpEditCart();
                        popUpEditCart.setCard(card);
                        popUpEditCart.setOnCardClickListener(PaymentMethod.this);
                        popUpEditCart.show(getFragmentManager(), "pop up edit card");
                        return true;
                    case R.id.page_4:
                        PopUpDeleteMyCard popUpDeleteMyCard = new PopUpDeleteMyCard();
                        popUpDeleteMyCard.setCard(card);
                        popUpDeleteMyCard.setOnCardClickListener(PaymentMethod.this);
                        popUpDeleteMyCard.show(getFragmentManager(), "delete my cart");
                        return true;
                    default:
                        return false;
                }
            }
        });

        popup.show(); //showing popup menu
    }

    @Override
    public void onCardClick(View view, Card card, int position) {
        showDialog(view, card);
    }

    @Override
    public void onCardRefresh() {
        refresh(view);
    }
}
