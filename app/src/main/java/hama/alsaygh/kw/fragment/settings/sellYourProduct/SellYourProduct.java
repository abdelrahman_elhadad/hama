package hama.alsaygh.kw.fragment.settings.sellYourProduct;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.sellYourProduct.AdapterImageSellYourProduct;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.GeneralResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.model.image.ImageUpload;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.CheckAndRequestPermission;
import hama.alsaygh.kw.utils.FileUtils;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class SellYourProduct extends BaseFragment {
    FragmentManager fragmentManager;
    ImageView imageback, imageView10;
    TextView txt_toolbar, textView106, textView107, textView108, textView7, textViewError;
    LinearLayout parent_sell, up_load;
    EditText ed_p_name, ed_p_details;
    View view18;
    Button btn_send;
    AdapterImageSellYourProduct adapter;
    RecyclerView rv_images;
    ProgressBar pb_send;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.sell_your_product, container, false);
        fragmentManager = getFragmentManager();
        pb_send = view.findViewById(R.id.pb_send);
        textView106 = view.findViewById(R.id.textView106);
        txt_toolbar = view.findViewById(R.id.txt_toolbar);
        textView107 = view.findViewById(R.id.textView107);
        ed_p_name = view.findViewById(R.id.ed_p_name);
        ed_p_details = view.findViewById(R.id.ed_p_details);
        view18 = view.findViewById(R.id.view18);
        textView108 = view.findViewById(R.id.textView108);
        textView7 = view.findViewById(R.id.textView7);
        btn_send = view.findViewById(R.id.button10);
        parent_sell = view.findViewById(R.id.parent_sell);
        textViewError = view.findViewById(R.id.textViewError);
        rv_images = view.findViewById(R.id.rv_images);
        imageback = view.findViewById(R.id.img_back);
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().onBackPressed();

            }
        });
        up_load = view.findViewById(R.id.up_load);
        imageView10 = view.findViewById(R.id.imageView10);

        if (pb_send.getIndeterminateDrawable() != null) {
            pb_send.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.whiteColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            parent_sell.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
            ed_p_name.setBackgroundResource(R.drawable.back_edittext_dark);
            ed_p_details.setBackgroundResource(R.drawable.back_edittext_dark);
            ed_p_name.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            ed_p_details.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            textView108.setTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            up_load.setBackgroundResource(R.drawable.sell_dark);
            textView7.setTextColor(ContextCompat.getColor(getContext(), R.color.color_navigation));
            imageView10.setBackgroundResource(R.drawable.ui_dark);
            //  imageView12.setBackgroundResource(R.drawable.upload_file);
            btn_send.setBackgroundResource(R.drawable.back_button_dark);
            pb_send.setBackgroundResource(R.drawable.back_button_dark);
            view18.setBackgroundResource(R.drawable.back_sell_dark);
            textView106.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView107.setTextColor(ContextCompat.getColor(getContext(), R.color.white_tranperant));

        }

        adapter = new AdapterImageSellYourProduct(getActivity(), new ArrayList<ImageUpload>());
        rv_images.setAdapter(adapter);
        rv_images.setLayoutManager(new LinearLayoutManager(getActivity()));


        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (MainApplication.isConnected) {
                    if (isValid()) {
                        pb_send.setVisibility(View.VISIBLE);
                        btn_send.setVisibility(View.GONE);
                        final String name = ed_p_name.getEditableText().toString();
                        final String msg = ed_p_details.getEditableText().toString();
                        final JSONArray images = adapter.getImageName();


                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                final GeneralResponse loginResponse = RequestWrapper.getInstance().addProductSale(view.getContext(), name, msg, images);
                                if (loginResponse.isStatus()) {

                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                btn_send.setVisibility(View.VISIBLE);
                                                pb_send.setVisibility(View.GONE);
                                                ed_p_name.setText("");
                                                ed_p_details.setText("");
                                                adapter.removeAll();

                                                Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();

                                            }
                                        });
                                } else {
                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                btn_send.setVisibility(View.VISIBLE);
                                                pb_send.setVisibility(View.GONE);
                                                Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                            }
                                        });
                                }

                            }
                        }).start();
                    }
                } else
                    Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
            }
        });


        up_load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionGallery();
            }
        });


        return view;

    }

    private boolean isValid() {
        boolean isValid = true;

        if (ed_p_name.getEditableText().toString().isEmpty() || ed_p_name.getEditableText().toString().length() < 4) {
            isValid = false;
            ed_p_name.setBackgroundResource(R.drawable.back_edittext_red);
        } else {

            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                ed_p_name.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                ed_p_name.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        if (ed_p_details.getEditableText().toString().isEmpty()) {
            isValid = false;
            ed_p_details.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                ed_p_details.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                ed_p_details.setBackgroundResource(R.drawable.back_ground_edit_text);

        }


        if (adapter != null && !adapter.isValid()) {
            isValid = false;
            textViewError.setVisibility(View.VISIBLE);
        } else {

            textViewError.setVisibility(View.GONE);

        }


        return isValid;
    }


    private void permissionGallery() {
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (!CheckAndRequestPermission.hasPermissions(getContext(), permissions)) {
            CheckAndRequestPermission.requestStorage(this);
        } else {
            openGallery();
        }

    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Utils.PICK_IMAGE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utils.PICK_IMAGE) {
            if (resultCode == RESULT_OK) {

                final String profilePath = FileUtils.getPath(getContext(), data.getData());
                ImageUpload imageUpload = new ImageUpload();
                imageUpload.setPath(profilePath);
                imageUpload.setUri(data.getData());
                adapter.addItem(imageUpload);

            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && !CheckAndRequestPermission.isFoundPermissionDenied(grantResults)) {
            openGallery();
        }
    }


}
