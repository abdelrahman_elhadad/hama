package hama.alsaygh.kw.fragment.settings.calculator;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.slider.Slider;
import com.neberox.lib.ringsizer.RingSizer;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class RingSizeFences extends BaseFragment {
    static FragmentManager fragmentManager;
    ImageView shabak_img, circle_img;
    TextView size_txt;
    ConstraintLayout parent_ring_size;

    public RingSizeFences() {
    }

    public static RingSizeFences newInstance(String param1, String param2) {
        RingSizeFences fragment = new RingSizeFences();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.MyMaterialThemeDark);
        }else
            this.getActivity().setTheme(R.style.MyMaterialTheme);

        // return inflater.inflate(R.layout.fragment_canceled, container, false);
        fragmentManager = getChildFragmentManager();
        View view = inflater.inflate(R.layout.ring_size_fences, container, false);
        shabak_img = (ImageView) view.findViewById(R.id.shabak_img);
        circle_img = (ImageView) view.findViewById(R.id.circle_img);
        size_txt = (TextView) view.findViewById(R.id.size_txt);
        parent_ring_size = (ConstraintLayout) view.findViewById(R.id.parent_ring_size);

        Slider sb_ring = view.findViewById(R.id.sb_ring);
        final RingSizer bar = view.findViewById(R.id.rz);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {

            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            shabak_img.setImageResource(R.drawable.ic_grid_dark);
//            circle_img.setImageResource(R.drawable.ic_ring_size_dark);
            size_txt.setTextColor(ContextCompat.getColor(getContext(), R.color.my_profile));
            parent_ring_size.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark11));
            bar.setRingStrokeColor(ContextCompat.getColor(getContext(), R.color.my_profile));
        } else {
            bar.setRingStrokeColor(ContextCompat.getColor(getContext(), R.color.color_navigation));
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        bar.setTextColor(Color.BLACK); //(Optional)
        bar.setTextBgColor(Color.WHITE); //(Optional)

        sb_ring.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                bar.setSize(value);
                size_txt.setText(getString(R.string.ring_size_value).replace("xx", value + ""));
            }
        });

        sb_ring.setValue(12.0f);
        bar.setSize(12.0f);
        size_txt.setText(getString(R.string.ring_size_value).replace("xx", 12.0f + ""));

        return view;
    }
}
