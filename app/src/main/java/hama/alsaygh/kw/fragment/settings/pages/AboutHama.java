package hama.alsaygh.kw.fragment.settings.pages;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.PageResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class AboutHama extends BaseFragment {
    ImageView img_back;
    TextView txt_ttolbar, textView84;
    LinearLayout parent_about_hama;
    private Skeleton skeleton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.about_hama, container, false);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();

            }
        });

        txt_ttolbar = (TextView) view.findViewById(R.id.txt_ttolbar);
        textView84 = (TextView) view.findViewById(R.id.textView84);
        parent_about_hama = (LinearLayout) view.findViewById(R.id.parent_about_hama);
        skeleton = SkeletonLayoutUtils.createSkeleton(textView84);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);

            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            parent_about_hama.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            txt_ttolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView84.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        if (MainApplication.isConnected) {

            skeleton.showSkeleton();

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final PageResponse getStoresResponse = RequestWrapper.getInstance().getAboutUs(getContext());

                    if (getStoresResponse.isStatus()) {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    textView84.setText(Html.fromHtml(getStoresResponse.getData().getContent(), Html.FROM_HTML_MODE_LEGACY));

                                }
                            });
                    } else {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    Snackbar.make(parent_about_hama, getStoresResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });
                    }

                }
            }).start();
        }

        return view;

    }
}
