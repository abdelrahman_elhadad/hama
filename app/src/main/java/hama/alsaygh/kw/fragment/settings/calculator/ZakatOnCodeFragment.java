package hama.alsaygh.kw.fragment.settings.calculator;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.CalculatorResponse;
import hama.alsaygh.kw.model.calculator.Calculator;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class ZakatOnCodeFragment extends BaseFragment {
    static FragmentManager fragmentManager;

    TextView cliber18, cliber21, cliper22, cliper24, z18, x21, z22, z24;
    EditText ed_cliber18, ed_cliber21, ed_cliper22, ed_cliper24;
    TextView ed_z24, ed_z22, ed_z21, et_z18;
    Button btn_calculate;

    Calculator calculator;

    public ZakatOnCodeFragment() {
    }

    public static ZakatOnCodeFragment newInstance() {
        ZakatOnCodeFragment fragment = new ZakatOnCodeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.zakat_on_code_fragment, container, false);

        fragmentManager = getChildFragmentManager();
        cliber18 = (TextView) view.findViewById(R.id.cliber18);
        cliber21 = (TextView) view.findViewById(R.id.cliber21);
        cliper22 = (TextView) view.findViewById(R.id.cliper22);
        cliper24 = (TextView) view.findViewById(R.id.cliper24);
        z18 = (TextView) view.findViewById(R.id.z18);
        et_z18 = (TextView) view.findViewById(R.id.et_z18);
        x21 = (TextView) view.findViewById(R.id.x21);
        ed_z21 = (TextView) view.findViewById(R.id.ed_z21);
        z22 = (TextView) view.findViewById(R.id.z22);
        ed_z22 = (TextView) view.findViewById(R.id.ed_z22);
        z24 = (TextView) view.findViewById(R.id.z24);
        ed_z24 = (TextView) view.findViewById(R.id.ed_z24);
        ed_cliber18 = (EditText) view.findViewById(R.id.ed_cliber18);
        ed_cliber21 = (EditText) view.findViewById(R.id.ed_cliber21);
        ed_cliper22 = (EditText) view.findViewById(R.id.ed_cliper22);
        ed_cliper24 = (EditText) view.findViewById(R.id.ed_cliper24);
        btn_calculate = (Button) view.findViewById(R.id.btn_calculate);


        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            cliber18.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            cliber21.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            cliper22.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            cliper24.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            z18.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            x21.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            z24.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            z18.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            ed_cliber18.setBackgroundResource(R.drawable.back_edittext_dark);
            ed_cliber21.setBackgroundResource(R.drawable.back_edittext_dark);
            ed_cliper22.setBackgroundResource(R.drawable.back_edittext_dark);
            ed_cliper24.setBackgroundResource(R.drawable.back_edittext_dark);
            btn_calculate.setBackgroundResource(R.drawable.back_button_dark);
            //  et_z18.setBackgroundResource(R.drawable.back_edittext_dark);
            //  ed_z21.setBackgroundResource(R.drawable.back_edittext_dark);
            z22.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            //  ed_z22.setBackgroundResource(R.drawable.back_edittext_dark);
            //  ed_z24.setBackgroundResource(R.drawable.back_edittext_dark);
            et_z18.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            ed_z21.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            ed_z22.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            ed_z24.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            ed_cliber18.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            ed_cliber21.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            ed_cliper22.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            ed_cliper24.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            et_z18.setBackgroundResource(R.drawable.back_zakat_dark);
            ed_z21.setBackgroundResource(R.drawable.back_zakat_dark);
            ed_z22.setBackgroundResource(R.drawable.back_zakat_dark);
            ed_z24.setBackgroundResource(R.drawable.back_zakat_dark);


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        new Thread(new Runnable() {
            @Override
            public void run() {

                final CalculatorResponse calculatorResponse = RequestWrapper.getInstance().getCalculator(getActivity());
                if (calculatorResponse.isStatus()) {
                    calculator = calculatorResponse.getData();
                } else {
                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Snackbar.make(view, calculatorResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        });
                }
            }
        }).start();


        btn_calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calculate(ed_cliber18, et_z18, 18);
                calculate(ed_cliber21, ed_z21, 21);
                calculate(ed_cliper22, ed_z22, 22);
                calculate(ed_cliper24, ed_z24, 24);

            }
        });

        return view;
    }

    private void calculate(EditText from, TextView after, int type) {
        String sizes = from.getEditableText().toString();
        if (sizes.isEmpty())
            sizes = "0.0";
        double size = Double.parseDouble(sizes);
        double total;
        double cal = 2.5;
        if (calculator != null) {
            switch (type) {
                case 18:
                    cal = Double.parseDouble(calculator.getSize18());
                    break;
                case 21:
                    cal = Double.parseDouble(calculator.getSize21());
                    break;
                case 22:
                    cal = Double.parseDouble(calculator.getSize22());
                    break;
                case 24:
                    cal = Double.parseDouble(calculator.getSize24());
                    break;
            }
        }

        total = size * (cal / 100);
        after.setText(String.valueOf(total));
    }
}
