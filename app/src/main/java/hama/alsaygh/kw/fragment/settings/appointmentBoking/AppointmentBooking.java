package hama.alsaygh.kw.fragment.settings.appointmentBoking;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.AppointmentResponse;
import hama.alsaygh.kw.api.responce.ContactUsResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.db.table.User;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.model.appointment.Appointment;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class AppointmentBooking extends BaseFragment {

    FragmentManager fragmentManager;
    LinearLayout parent_appointment;
    TextView txt_toolbar, textView1, textView2, phone_unmber, textView110, info_txt, textView113, textView114;
    ImageView img_back, ic_phone_dark, icon_email;
    Button button10;
    EditText edit_msg;
    ProgressBar pb_send;

    Skeleton skeleton;
    Appointment appointment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.appointment_booking, container, false);
        fragmentManager = getFragmentManager();

        final LinearLayout llMain = view.findViewById(R.id.llMain);
        parent_appointment = view.findViewById(R.id.parent_appointment);
        txt_toolbar = view.findViewById(R.id.txt_toolbar);
        img_back = view.findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        });
        textView1 = view.findViewById(R.id.textView1);
        textView2 = view.findViewById(R.id.textView2);
        ic_phone_dark = view.findViewById(R.id.phone_img);
        phone_unmber = view.findViewById(R.id.phone_unmber);
        textView110 = view.findViewById(R.id.textView110);
        icon_email = view.findViewById(R.id.icon_email);
        info_txt = view.findViewById(R.id.info_txt);
        textView113 = view.findViewById(R.id.textView113);
        textView114 = view.findViewById(R.id.textView114);
        button10 = view.findViewById(R.id.button10);
        edit_msg = view.findViewById(R.id.edit_msg);
        pb_send = view.findViewById(R.id.pb_send);
        if (pb_send.getIndeterminateDrawable() != null) {
            pb_send.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.whiteColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }


        skeleton = SkeletonLayoutUtils.createSkeleton(llMain);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);


        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);

            parent_appointment.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            //  imageView12.setBackgroundResource(R.drawable.upload_file);
            button10.setBackgroundResource(R.drawable.back_button_dark);
            textView1.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView2.setTextColor(ContextCompat.getColor(getContext(), R.color.white_tranperant));
            ic_phone_dark.setImageResource(R.drawable.ic_phone_dark);
            phone_unmber.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView110.setTextColor(ContextCompat.getColor(getContext(), R.color.white_tranperant));
            icon_email.setImageResource(R.drawable.ic_email_icon);
            info_txt.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView113.setTextColor(ContextCompat.getColor(getContext(), R.color.white_tranperant));
            textView114.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            button10.setBackgroundResource(R.drawable.back_button_dark);
            pb_send.setBackgroundResource(R.drawable.back_button_dark);

            edit_msg.setBackgroundResource(R.drawable.back_edittext_dark);
            edit_msg.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        if (MainApplication.isConnected) {

            skeleton.showSkeleton();
            new Thread(new Runnable() {
                @Override
                public void run() {

                    final AppointmentResponse appointmentResponse = RequestWrapper.getInstance().getAppointment(getActivity());
                    if (appointmentResponse.isStatus()) {
                        appointment = appointmentResponse.getData();
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    phone_unmber.setText(appointmentResponse.getData().getPhone());
                                    textView110.setText(appointmentResponse.getData().getWorktime());
                                    info_txt.setText(appointmentResponse.getData().getEmail());


                                    if (!phone_unmber.getText().toString().isEmpty()) {
                                        phone_unmber.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                String phone = appointmentResponse.getData().getPhone();
                                                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                                                startActivity(intent);
                                            }
                                        });
                                    }

                                    if (!info_txt.getText().toString().isEmpty()) {
                                        info_txt.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                                                String[] aEmailList = {appointmentResponse.getData().getEmail()};
                                                String[] aEmailCCList = {appointmentResponse.getData().getEmail()};
                                                String[] aEmailBCCList = {appointmentResponse.getData().getEmail()};

                                                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, aEmailList);
                                                emailIntent.putExtra(android.content.Intent.EXTRA_CC, aEmailCCList);
                                                emailIntent.putExtra(android.content.Intent.EXTRA_BCC, aEmailBCCList);

                                                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
                                                emailIntent.setType("plain/text");
                                                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                                                startActivity(emailIntent);

                                            }
                                        });
                                    }
                                }
                            });
                    } else {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    Snackbar.make(llMain, appointmentResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });
                    }


                }
            }).start();
        } else {
            Snackbar.make(llMain, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
        }


        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (MainApplication.isConnected) {
                    if (isValid()) {
                        if (!SharedPreferenceConstant.getSharedPreferenceUserToken(v.getContext()).isEmpty()) {
                            pb_send.setVisibility(View.VISIBLE);
                            button10.setVisibility(View.GONE);

                            final String msg = edit_msg.getEditableText().toString();

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    AppDatabase appDatabase = AppDatabase.newInstance(v.getContext());
                                    List<User> userList = appDatabase.userDao().getAll();
                                    if (userList != null && !userList.isEmpty()) {
                                        User user = userList.get(0);


                                        final String phone = user.getMobile();
                                        final String subject = appointment.getAppointments_title();
                                        final String name = user.getF_name() + " " + user.getL_name();
                                        final String email = user.getEmail();


                                        final ContactUsResponse loginResponse = RequestWrapper.getInstance().makeAppointment(view.getContext(), name, email, phone, subject, msg);
                                        if (loginResponse.isStatus()) {

                                            if (getActivity() != null)
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        button10.setVisibility(View.VISIBLE);
                                                        pb_send.setVisibility(View.GONE);
                                                        edit_msg.setText("");

                                                        Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();

                                                    }
                                                });
                                        } else {
                                            if (getActivity() != null)
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        button10.setVisibility(View.VISIBLE);
                                                        pb_send.setVisibility(View.GONE);

                                                        if (loginResponse.getCode().equalsIgnoreCase("401")) {
                                                            LoginDialog loginDialog = LoginDialog.newInstance();
                                                            loginDialog.show(fragmentManager, "login");
                                                        } else
                                                            Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                                    }
                                                });
                                        }
                                    } else {
                                        if (getActivity() != null)
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    button10.setVisibility(View.VISIBLE);
                                                    pb_send.setVisibility(View.GONE);

                                                    LoginDialog loginDialog = LoginDialog.newInstance();
                                                    loginDialog.show(fragmentManager, "login");
                                                }
                                            });
                                    }
                                }
                            }).start();
                        } else {
                            LoginDialog loginDialog = LoginDialog.newInstance();
                            loginDialog.show(fragmentManager, "login");
                        }
                    }
                } else
                    Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();

            }
        });


        return view;

    }

    private boolean isValid() {
        boolean isValid = true;

        if (edit_msg.getEditableText().toString().isEmpty()) {
            isValid = false;
            edit_msg.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                edit_msg.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edit_msg.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        return isValid;
    }

}
