package hama.alsaygh.kw.fragment.order;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.order.AdapterMyOrder;
import hama.alsaygh.kw.adapter.order.AdapterMyOrderDark;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.OrdersResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.model.order.Order;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;


public class CanceledFragment extends BaseFragment {


    RecyclerView recycle_cancle;
    ArrayList<Order> myordersArray;
    static FragmentManager fragmentManager;
    LinearLayout ll_no_order;
    ImageView my_order_img;
    TextView no_order;

    public CanceledFragment() {
    }

    public static CanceledFragment newInstance() {
        return new CanceledFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_canceled, container, false);
        fragmentManager = getChildFragmentManager();

        myordersArray = new ArrayList<>();
        recycle_cancle = (RecyclerView) view.findViewById(R.id.cancle_fragment);
        ll_no_order = view.findViewById(R.id.ll_no_order);
        my_order_img = view.findViewById(R.id.my_order_img);
        no_order = view.findViewById(R.id.no_order);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            my_order_img.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_my_orders_dark));
          //  no_order.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (MainApplication.isConnected) {

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final OrdersResponse ordersResponse = RequestWrapper.getInstance().getOrders(getContext(), "canceled");
                    if (ordersResponse.isStatus()) {

                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    myordersArray.clear();
                                    myordersArray.addAll(ordersResponse.getData());
                                    if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {

                                        AdapterMyOrderDark adapter_pending_tab_dark = new AdapterMyOrderDark(myordersArray, "canceled");
                                        recycle_cancle.setAdapter(adapter_pending_tab_dark);
                                    } else {
                                        AdapterMyOrder adapter_pending_tab = new AdapterMyOrder(myordersArray, "canceled");
                                        recycle_cancle.setAdapter(adapter_pending_tab);
                                    }

                                    if (myordersArray.isEmpty()) {
                                        ll_no_order.setVisibility(View.VISIBLE);
                                    } else
                                        ll_no_order.setVisibility(View.GONE);

                                }
                            });
                        }
                    } else {

                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (ordersResponse.getCode().equalsIgnoreCase("401")) {
                                        LoginDialog loginDialog = LoginDialog.newInstance();
                                        loginDialog.show(fragmentManager, "login");
                                    } else

                                        Snackbar.make(recycle_cancle, ordersResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }
            }).start();

        } else
            Snackbar.make(recycle_cancle, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();

    }
}
