package hama.alsaygh.kw.fragment.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.checkout.CheckOutSummeryActivity;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.activity.marketPrice.MarketPriceActivity;
import hama.alsaygh.kw.activity.notification.NotificationsActivity;
import hama.alsaygh.kw.activity.product.ProductDetailsActivity;
import hama.alsaygh.kw.activity.product.WishListActivity;
import hama.alsaygh.kw.adapter.store.AdapterStoresCart;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.CartsResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnMyCartListener;
import hama.alsaygh.kw.listener.OnStoreClickListener;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.Cons;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class AllCartFragment extends BaseFragment implements View.OnClickListener, OnMyCartListener {
    RecyclerView recycle_mycart;
    TextView homeText, tvCount, tvSubTotal, textView111, tvShipping, textView121, tvTotal, textView23, textView21, textView22;
    ImageView imageLove, notifigation_img, img_toolbar;
    Button button, button4;
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    LinearLayout liner_mycart1, liner_mycart, ll_empty;
    NestedScrollView nsv_cart;
    View view;
    Skeleton skeleton;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_cart_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        fragmentManager = getFragmentManager();
        ll_empty = view.findViewById(R.id.ll_empty);
        nsv_cart = view.findViewById(R.id.nsv_cart);
        button = (Button) view.findViewById(R.id.button3);
        textView23 = view.findViewById(R.id.textView23);
        textView21 = view.findViewById(R.id.textView21);
        textView22 = view.findViewById(R.id.textView22);
        button4 = view.findViewById(R.id.button4);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), CheckOutSummeryActivity.class);
                startActivity(intent);
            }
        });

        textView23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((HomeActivity) getActivity()).openAllStore();
            }
        });
        imageLove = (ImageView) view.findViewById(R.id.imageView444);
        imageLove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), WishListActivity.class);
                startActivity(intent);
            }
        });

        recycle_mycart = (RecyclerView) view.findViewById(R.id.recyclerView_MY_cart);
        recycle_mycart.setLayoutManager(new GridLayoutManager(getContext(),2));

        homeText = (TextView) view.findViewById(R.id.text_toolbar);
        notifigation_img = (ImageView) view.findViewById(R.id.img_notifigation);
        img_toolbar = (ImageView) view.findViewById(R.id.img_toolbar);
        img_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), MarketPriceActivity.class));

            }
        });
        notifigation_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(v.getContext(), NotificationsActivity.class));
            }
        });
        tvCount = (TextView) view.findViewById(R.id.textView31);
        tvSubTotal = (TextView) view.findViewById(R.id.textView61);
        textView111 = (TextView) view.findViewById(R.id.textView111);
        tvShipping = (TextView) view.findViewById(R.id.textView611);
        textView121 = (TextView) view.findViewById(R.id.textView121);
        tvTotal = (TextView) view.findViewById(R.id.textView661);
        liner_mycart = (LinearLayout) view.findViewById(R.id.liner_mycart);
        liner_mycart1 = (LinearLayout) view.findViewById(R.id.liner_mycart1);
        skeleton = SkeletonLayoutUtils.applySkeleton(recycle_mycart, R.layout.card_view_rv_my_cart, 1);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);
            homeText.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            img_toolbar.setImageResource(R.drawable.ic_toolbar_img_dark);
            imageLove.setImageResource(R.drawable.ic_love_dark);
            notifigation_img.setImageResource(R.drawable.ic_notifigation_img_dark);
            liner_mycart1.setBackgroundResource(R.drawable.back_mycart_liner_dark);
            tvCount.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            tvSubTotal.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView111.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            tvShipping.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView23.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView21.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView22.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView121.setTextColor(ContextCompat.getColor(getContext(), R.color.color_navigation));
            tvTotal.setTextColor(ContextCompat.getColor(getContext(), R.color.color_navigation));
            button.setBackgroundResource(R.drawable.back_button_dark);
            button4.setBackgroundResource(R.drawable.back_button_dark);
            liner_mycart.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            Window window = getActivity().getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.sign_in));
        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        getCart();

    }

    private void getCart() {
        if (MainApplication.isConnected) {
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        skeleton.showSkeleton();
                    }
                });

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final CartsResponse cartResponse = RequestWrapper.getInstance().getCart(getContext());
                    if (cartResponse.isStatus()) {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    skeleton.showOriginal();

                                    if (cartResponse.getData().getCart() == null || cartResponse.getData().getCart().isEmpty()) {

                                        ll_empty.setVisibility(View.VISIBLE);
                                        nsv_cart.setVisibility(View.GONE);

                                    } else {

                                        ll_empty.setVisibility(View.GONE);
                                        nsv_cart.setVisibility(View.VISIBLE);

                                        AdapterStoresCart adapterMyCart = new AdapterStoresCart(new OnStoreClickListener() {
                                            @Override
                                            public void onStoreClick(Store store, int position, String tag) {
                                                HomeActivity.position = HomeActivity.StoreCartFromCarts;
                                                HomeActivity.store = store;
                                                fragmentTransaction = fragmentManager.beginTransaction();
                                                fragmentTransaction.replace(R.id.liner1, MyCartFragment.newInstance(store.getId()));
                                                fragmentTransaction.commit();
                                            }
                                        }, cartResponse.getData().getCart(), getContext());
                                        recycle_mycart.setAdapter(adapterMyCart);

                                        String count = getActivity().getString(R.string.item) + " (" + cartResponse.getData().getCount() + ")";
                                        tvCount.setText(count);

                                    }
                                }
                            });
                    } else {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    skeleton.showOriginal();
                                    Snackbar.make(view, cartResponse.getMessage(), Snackbar.LENGTH_SHORT).show();

                                    ll_empty.setVisibility(View.VISIBLE);
                                    nsv_cart.setVisibility(View.GONE);

                                }
                            });
                    }

                }
            }).start();

        } else {
            Snackbar.make(view, getActivity().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onProductClick(Product product, int position) {
        Intent intent = new Intent(getContext(), ProductDetailsActivity.class);
        intent.putExtra(Cons.PRODUCT, product);
        intent.putExtra(Cons.PRODUCT_ID, product.getId());
        startActivity(intent);
    }

    @Override
    public void onCountChange() {
        getCart();
    }
}
