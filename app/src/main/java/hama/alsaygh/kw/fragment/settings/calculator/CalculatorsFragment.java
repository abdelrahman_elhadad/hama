package hama.alsaygh.kw.fragment.settings.calculator;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.calculator.AdapterpagerCalculators;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class CalculatorsFragment extends BaseFragment {
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    ImageView imageback;
    Button orderTracking;
    TabLayout tabLayout;
    TabItem zakat_on_gold, one_fifth_of_gold, ring_size_fonces;
    ViewPager viewPager;
    TextView txt_toolbar;
    LinearLayout parent_calculators;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.calculators, container, false);
        fragmentManager = getChildFragmentManager();
        tabLayout = (TabLayout) view.findViewById(R.id.tab_calculators);
        zakat_on_gold = (TabItem) view.findViewById(R.id.zakat);
        ring_size_fonces = (TabItem) view.findViewById(R.id.ring_size);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager_calcilator);
        AdapterpagerCalculators adapterpagerCalculators = new AdapterpagerCalculators(fragmentManager, tabLayout.getTabCount());
        viewPager.setAdapter(adapterpagerCalculators);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setTabIndicatorFullWidth(false);
        tabLayout.getTabAt(0).setText(R.string.zakat_on_gold);
        tabLayout.getTabAt(1).setText(R.string.Ring_size_fances);
        txt_toolbar = (TextView) view.findViewById(R.id.textView9);
        txt_toolbar = (TextView) view.findViewById(R.id.txt_toolbar);
        parent_calculators = (LinearLayout) view.findViewById(R.id.parent_calculators);
        imageback = (ImageView) view.findViewById(R.id.img_back_c);
        txt_toolbar = (TextView) view.findViewById(R.id.txt_toolbar);
        parent_calculators = (LinearLayout) view.findViewById(R.id.parent_calculators);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            parent_calculators.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
            tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getContext(), R.color.color_navigation));
            tabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.color_store_page), ContextCompat.getColor(getContext(), R.color.color_navigation));
        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        imageback.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                if (null == view) {
                    tab.setCustomView(R.layout.tab_title);
                }
                TextView textView = tab.getCustomView().findViewById(android.R.id.text1);
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                tabLayout.setTabIndicatorFullWidth(false);

                if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                    textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_navigation));
                    tabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.dark111), ContextCompat.getColor(getContext(), R.color.color_navigation));
                } else {
                    textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.blackcolor));
                    tabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.color_store_page), ContextCompat.getColor(getContext(), R.color.blackcolor));
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                if (null == view) {
                    tab.setCustomView(R.layout.tab_title);
                }

                TextView textView = tab.getCustomView().findViewById(android.R.id.text1);
                textView.setTypeface(Typeface.DEFAULT);
                textView.setTextColor(ContextCompat.getColor(getContext(), R.color.color_store_page));

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;
    }
}
