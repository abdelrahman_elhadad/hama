package hama.alsaygh.kw.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class RateHama extends BaseFragment {
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    LinearLayout parent_rate_hama;
    ImageView img_back;
    TextView textView9,textView95,textView96,textView66,textView93,textView94;
    Button button3;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.rate_hama, container, false);
        fragmentManager = getFragmentManager();
        parent_rate_hama=(LinearLayout)view.findViewById(R.id.parent_rate_hama);
        img_back=(ImageView)view.findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();

            }
        });
        textView9=(TextView)view.findViewById(R.id.textView9);
        textView95=(TextView)view.findViewById(R.id.textView95);
        button3=(Button)view.findViewById(R.id.button3);
        textView96=(TextView)view.findViewById(R.id.textView96);
        textView66=(TextView)view.findViewById(R.id.textView66);
        textView93=(TextView)view.findViewById(R.id.textView93);
        textView94=(TextView)view.findViewById(R.id.textView94);
        if(SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            //   txt_toolbar.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            parent_rate_hama.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.sign_in));
            textView9.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            textView93.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            textView66.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            textView95.setTextColor(ContextCompat.getColor(getContext(),R.color.color_navigation));
            button3.setBackgroundResource(R.drawable.back_button_dark);
            textView96.setTextColor(ContextCompat.getColor(getContext(),R.color.white_tranperant));
            textView94.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        return view;

    }
}
