package hama.alsaygh.kw.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.event.AdapterEventAndAdvetisment;
import hama.alsaygh.kw.model.MyCartArray;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class HamaEventAndAdvertisment extends BaseFragment {
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    AdapterEventAndAdvetisment adapterEventAndAdvetisment;
    RecyclerView rv_event;
    ArrayList<MyCartArray> myCartArrays;
    ImageView img_back;
    TextView txt_toolbar;
    LinearLayout parent_event;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.hama_events_and_advertisements, container, false);
        fragmentManager = getFragmentManager();
        img_back=(ImageView)view.findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        rv_event=(RecyclerView)view.findViewById(R.id.rv_event);
        txt_toolbar=(TextView)view.findViewById(R.id.txt_toolbar);
        myCartArrays = new ArrayList<>();
        MyCartArray bs = new MyCartArray(R.drawable.advertisingagency, "Black Friday with HAMA!!");
        MyCartArray bs2 = new MyCartArray(R.drawable.advertisingagency, "Black Friday with HAMA!!");
        MyCartArray bs3 = new MyCartArray(R.drawable.advertisingagency, "Black Friday with HAMA!!");
        MyCartArray bs4 = new MyCartArray(R.drawable.advertisingagency, "Black Friday with HAMA!!");
        myCartArrays.add(bs);
        myCartArrays.add(bs2);
        myCartArrays.add(bs3);
        myCartArrays.add(bs4);
        adapterEventAndAdvetisment = new AdapterEventAndAdvetisment(myCartArrays);
        rv_event.setAdapter(adapterEventAndAdvetisment);
        parent_event=(LinearLayout)view.findViewById(R.id.parent_event);
        if(SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            parent_event.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.sign_in));

        }  else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }



        return view;

    }
}
