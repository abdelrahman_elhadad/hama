package hama.alsaygh.kw.fragment.settings.appSetting;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.Locale;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.fragment.home.MenuFragment;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class Setting extends BaseFragment {
    SwitchCompat aSwitch;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    View view12, view11;
    TextView language_setting, g_setting, textView11, textView81;
    LinearLayout perant_setting;
    ImageView imageView53, imageView55;
    AppCompatImageView img_back;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.setting, container, false);
        fragmentManager = getFragmentManager();
        Log.i("lll", " after ");
        view12 = (View) view.findViewById(R.id.view12);
        view11 = (View) view.findViewById(R.id.view11);
        language_setting = (TextView) view.findViewById(R.id.language_setting);
        language_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.SettingLanguage;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new SettingLanguage());
                fragmentTransaction.commit();
            }
        });

        perant_setting = (LinearLayout) view.findViewById(R.id.oapent_setting);
        g_setting = (TextView) view.findViewById(R.id.g_setting);
        textView11 = (TextView) view.findViewById(R.id.textView11);
        imageView53 = (ImageView) view.findViewById(R.id.imageView53);
        imageView55 = (ImageView) view.findViewById(R.id.imageView55);
        img_back = view.findViewById(R.id.img_back);
        textView81 = (TextView) view.findViewById(R.id.textView81);
        textView81.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!SharedPreferenceConstant.getSharedPreferenceUserToken(getContext()).isEmpty()) {
                    HomeActivity.position = HomeActivity.SettingLanguage;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, new NotificationSetting());
                    fragmentTransaction.commit();
                } else {
                    LoginDialog loginDialog = LoginDialog.newInstance();
                    loginDialog.show(fragmentManager, "login");
                }

            }
        });
        if (Locale.getDefault().getLanguage().equals("ar")) {
            Drawable d = ContextCompat.getDrawable(getContext(), R.drawable.back_view_seeting_ar);
            view11.setBackground(d);
            view12.setBackground(d);
        } else {
        }
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.MyProfile;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new MenuFragment());
                fragmentTransaction.commit();
            }
        });

        aSwitch = view.findViewById(R.id.switch2);

        aSwitch.setOnCheckedChangeListener(null);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            imageView53.setImageResource(R.drawable.ic_lamguage_s_img_dark);
            imageView55.setImageResource(R.drawable.ic_notification_setting);
            perant_setting.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            g_setting.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            language_setting.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView81.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView11.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            img_back.setImageTintList(ContextCompat.getColorStateList(getContext(), R.color.whiteColor));
            aSwitch.setTrackTintList(ContextCompat.getColorStateList(getContext(), R.color.switch_tracker_dark));
            aSwitch.setThumbTintList(ContextCompat.getColorStateList(getContext(), R.color.switch_thum_dark));
        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        aSwitch.setChecked(SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext()));

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);


                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

                }
                SharedPreferenceConstant.setSharedPreferenceDarkMode(getContext(), isChecked);
                RestartApp();
            }

        });

        return view;

    }


    public void RestartApp() {
//        Intent intent = new Intent(this, Setting.class);
//        finish();
//        startActivity(intent);
        getActivity().recreate();
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        Log.i("lll", " on pause ");
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        Log.i("lll", " on stop ");
//    }
}
