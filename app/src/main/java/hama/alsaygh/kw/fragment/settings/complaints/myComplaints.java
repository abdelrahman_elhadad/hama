package hama.alsaygh.kw.fragment.settings.complaints;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.adapter.complaint.AdapterMyComplaints;
import hama.alsaygh.kw.adapter.complaint.AdapterMyComplaintsDark;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.AllComplaintsResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.listener.OnNoteListener;
import hama.alsaygh.kw.model.complaint.Complaint;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class myComplaints extends BaseFragment implements OnNoteListener {

    RecyclerView recycle_mycomplints;
    ArrayList<Complaint> myordersArray;
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;

    LinearLayout ll_no_order;
    ImageView my_order_img;
    TextView no_order;

    public myComplaints() {
    }

    public static myComplaints newInstance(String param1, String param2) {
        myComplaints fragment = new myComplaints();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.fragment_canceled, container, false);
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.my_complaints, container, false);

        return view;

    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragmentManager = getActivity().getSupportFragmentManager();

        recycle_mycomplints = (RecyclerView) view.findViewById(R.id.recycle_mycomplints);
        myordersArray = new ArrayList<>();

        ll_no_order = view.findViewById(R.id.ll_no_event);
        my_order_img = view.findViewById(R.id.my_order_img);
        no_order = view.findViewById(R.id.no_order);


        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            my_order_img.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_complaints_dark));
           // no_order.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        if (MainApplication.isConnected) {

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final AllComplaintsResponse allComplaintsResponse = RequestWrapper.getInstance().getComplaints(getContext());

                    if (allComplaintsResponse.isStatus()) {

                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    myordersArray.clear();
                                    myordersArray.addAll(allComplaintsResponse.getData());

                                    if (myordersArray.isEmpty()) {
                                        ll_no_order.setVisibility(View.VISIBLE);
                                        recycle_mycomplints.setVisibility(View.GONE);
                                    } else {
                                        ll_no_order.setVisibility(View.GONE);
                                        recycle_mycomplints.setVisibility(View.VISIBLE);
                                        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                                            AdapterMyComplaintsDark AdapterMyComplaintsDark = new AdapterMyComplaintsDark(myComplaints.this, getActivity(), myordersArray);
                                            recycle_mycomplints.setAdapter(AdapterMyComplaintsDark);
                                        } else {
                                            AdapterMyComplaints adapterMyComplaints = new AdapterMyComplaints(myComplaints.this, getActivity(), myordersArray);
                                            recycle_mycomplints.setAdapter(adapterMyComplaints);

                                        }
                                    }
                                }
                            });

                    } else {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    Snackbar.make(view, allComplaintsResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                    if (allComplaintsResponse.getCode().equalsIgnoreCase("401")) {
                                        LoginDialog loginDialog = LoginDialog.newInstance();
                                        loginDialog.show(getChildFragmentManager(), "login");
                                    }

                                }
                            });
                    }

                }
            }).start();

        } else {
            Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onNoteClick(int position, String tag) {
        HomeActivity.position = HomeActivity.complaintArea;
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.liner1, ComplaintArea.newInstance(myordersArray.get(position)));
        fragmentTransaction.commit();

    }
}
