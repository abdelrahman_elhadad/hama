package hama.alsaygh.kw.fragment.settings.storePackage;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arasthel.spannedgridlayoutmanager.SpanSize;
import com.arasthel.spannedgridlayoutmanager.SpannedGridLayoutManager;
import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import java.io.Serializable;
import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.adapter.store.AdapterStoresSpanned;
import hama.alsaygh.kw.adapter.store.AdapterStoresSpannedDark;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.BannerAdsResponse;
import hama.alsaygh.kw.api.responce.GetStoresPackageResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnStoreClickListener;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;
import kotlin.jvm.functions.Function1;

public class AllStoresPackage extends BaseFragment implements OnStoreClickListener {
    RecyclerView recyclerView1;
    ArrayList<Serializable> best_stores;
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    boolean isReverse = false;
    TextView homeText;
    LinearLayout liner_all_stores;
    private Skeleton skeleton;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.packaging_stores, container, false);
        recyclerView1 = (RecyclerView) view.findViewById(R.id.home_rf);
        ImageView img_back = view.findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        });

        fragmentManager = getFragmentManager();
        best_stores = new ArrayList<>();

        homeText = (TextView) view.findViewById(R.id.toolbar_text);

        liner_all_stores = (LinearLayout) view.findViewById(R.id.linet_packaging_stores);
        recyclerView1.setLayoutManager(new GridLayoutManager(getContext(), 2));
        skeleton = SkeletonLayoutUtils.applySkeleton(recyclerView1, R.layout.row_stores_spanned, 3);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            homeText.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));

            liner_all_stores.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));

            img_back.setImageResource(R.drawable.ic_back_icon_dark);


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        if (MainApplication.isConnected) {

            skeleton.showSkeleton();

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final GetStoresPackageResponse getStoresResponse = RequestWrapper.getInstance().getStoresPackaging(getContext());
                    final BannerAdsResponse mainAdsResponse = RequestWrapper.getInstance().getBanner(getContext());

                    if (getStoresResponse.isStatus()) {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    best_stores.clear();
                                    best_stores.addAll(getStoresResponse.getStores());
                                }
                            });
                    }


                    if (mainAdsResponse.isStatus()) {

                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    skeleton.showOriginal();
                                    if (!best_stores.isEmpty() && !mainAdsResponse.getData().isEmpty()) {

                                        layoutMangerWithAds();
                                        int position = 3;
                                        for (int i = 0; i < mainAdsResponse.getData().size(); i++) {

                                            Log.i("ppppppp", "postion :" + position + " - i :" + i + " - size/3 : " + getStoresResponse.getStores().size() / 3);
                                            if (i < getStoresResponse.getStores().size() / 3) {

                                                best_stores.add(position, mainAdsResponse.getData().get(i));
                                                position += 4;
                                            } else
                                                break;
                                        }


                                    } else
                                        layoutMangerWithOutAds();
                                }
                            });
                    } else {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    layoutMangerWithOutAds();
                                    Snackbar.make(recyclerView1, mainAdsResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });
                    }


                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                                if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                                    AdapterStoresSpannedDark adapterStoresSpannedDark = new AdapterStoresSpannedDark(AllStoresPackage.this, best_stores, getActivity());
                                    recyclerView1.setAdapter(adapterStoresSpannedDark);
                                } else {
                                    AdapterStoresSpanned adapterStoresSpanned = new AdapterStoresSpanned(AllStoresPackage.this, best_stores, getActivity());
                                    recyclerView1.setAdapter(adapterStoresSpanned);
                                }
                            }
                        });

                }
            }).start();
        }


        return view;

    }

    private void layoutMangerWithAds() {
        SpannedGridLayoutManager spannedGridLayoutManager = new SpannedGridLayoutManager(SpannedGridLayoutManager.Orientation.VERTICAL, 4);
        spannedGridLayoutManager.setItemOrderIsStable(true);

        spannedGridLayoutManager.setSpanSizeLookup(new SpannedGridLayoutManager.SpanSizeLookup(new Function1<Integer, SpanSize>() {
            @Override
            public SpanSize invoke(Integer position) {
                Log.i("nnnnnnnnnn", " with ads  best_stores.size() " + best_stores.size() + " pppp " + (position + 1));

                if (position != 0 && ((position + 1) % 4 == 0)) {

                    return new SpanSize(4, 1);
                } else if (position != 1 && position % 4 == 1) {
                    if (position + 1 == best_stores.size()) {
                        return new SpanSize(2, 2);
                    } else {
                        if (!isReverse)
                            return new SpanSize(2, 4);
                        else
                            return new SpanSize(2, 2);
                    }
                } else if (position % 4 == 0) {

                    if (position + 1 == best_stores.size()) {
                        return new SpanSize(2, 2);
                    } else {

                        if (!isReverse) {
                            isReverse = true;
                            return new SpanSize(2, 4);
                        } else {
                            isReverse = false;
                            return new SpanSize(2, 2);
                        }
                    }
                } else {
                    return new SpanSize(2, 2);
                }
            }
        }));

        recyclerView1.setLayoutManager(spannedGridLayoutManager);
    }


    private void layoutMangerWithOutAds() {
        SpannedGridLayoutManager spannedGridLayoutManager = new SpannedGridLayoutManager(SpannedGridLayoutManager.Orientation.VERTICAL, 4);
        spannedGridLayoutManager.setItemOrderIsStable(true);

        spannedGridLayoutManager.setSpanSizeLookup(new SpannedGridLayoutManager.SpanSizeLookup(new Function1<Integer, SpanSize>() {
            @Override
            public SpanSize invoke(Integer position) {

                Log.i("nnnnnnnnnn", "best_stores.size() " + best_stores.size() + " pppp " + (position + 1));
                if (position % 6 == 1 || position % 6 == 3) {
                    if (position + 1 == best_stores.size())
                        return new SpanSize(2, 2);
                    else
                        return new SpanSize(2, 4);
                } else
                    return new SpanSize(2, 2);
            }
        }));

        recyclerView1.setLayoutManager(spannedGridLayoutManager);
    }

    @Override
    public void onStoreClick(Store store, int position, String tag) {
        HomeActivity.position = HomeActivity.StoreDetailsFromAllStorePackage;
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.liner1, StorePagePackage.newInstance(store));
        fragmentTransaction.commit();
    }
}
