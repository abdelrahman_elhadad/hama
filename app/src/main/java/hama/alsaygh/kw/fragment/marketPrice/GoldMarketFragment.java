package hama.alsaygh.kw.fragment.marketPrice;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.marketPrice.AdapterGoldMarketCaliber;
import hama.alsaygh.kw.adapter.marketPrice.AdapterGoldMarketCurrency;
import hama.alsaygh.kw.listener.OnCurrencyListener;
import hama.alsaygh.kw.model.marketPrice.CaliberItem;
import hama.alsaygh.kw.model.marketPrice.MainCaliberItem;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class GoldMarketFragment extends BaseFragment implements OnCurrencyListener {

    RecyclerView rv_gold_market, rv_currency;
    TextView tv_timer, textView17, textView27, textView41, textView44, textView68, textView69, textView73, textView74, textView76;
    LinearLayout liner, parent_gold_market, liner_top, liner_dawon;
    View view2;
    ImageView imageView13;
    AdapterGoldMarketCurrency currencyAdapter;
    List<String> currency = new ArrayList<>();
    FirebaseFirestore db;
    Thread timer;
    int count = 0;
    CountDownTimer countDownTimer;

    public GoldMarketFragment() {
    }

    public static GoldMarketFragment newInstance() {
        return new GoldMarketFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.gold_market, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        db = FirebaseFirestore.getInstance();

        tv_timer = view.findViewById(R.id.tv_timer);
        rv_currency = view.findViewById(R.id.rv_currency);
        rv_gold_market = (RecyclerView) view.findViewById(R.id.rv_gold_market);
        textView17 = (TextView) view.findViewById(R.id.textView17);
        textView27 = (TextView) view.findViewById(R.id.textView27);
        textView41 = (TextView) view.findViewById(R.id.textView41);
        textView44 = (TextView) view.findViewById(R.id.textView44);
        textView68 = (TextView) view.findViewById(R.id.textView68);
        textView73 = (TextView) view.findViewById(R.id.textView73);
        liner = (LinearLayout) view.findViewById(R.id.liner);
        view2 = (View) view.findViewById(R.id.view2);

        parent_gold_market = (LinearLayout) view.findViewById(R.id.parent_gold_market);
        liner_dawon = (LinearLayout) view.findViewById(R.id.liner_dawon);
        liner_top = (LinearLayout) view.findViewById(R.id.liner_top);
        textView74 = (TextView) view.findViewById(R.id.textView74);
        textView76 = (TextView) view.findViewById(R.id.textView76);
        textView69 = (TextView) view.findViewById(R.id.textView69);
        imageView13 = (ImageView) view.findViewById(R.id.imageView13);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            textView17.setTextColor(ContextCompat.getColor(getContext(), R.color.buttonColor));
            textView27.setTextColor(ContextCompat.getColor(getContext(), R.color.buttonColor));
            textView41.setTextColor(ContextCompat.getColor(getContext(), R.color.textviewhome));
            tv_timer.setTextColor(ContextCompat.getColor(getContext(), R.color.color_navigation));
            textView44.setTextColor(ContextCompat.getColor(getContext(), R.color.buttonColor));
            textView68.setTextColor(ContextCompat.getColor(getContext(), R.color.buttonColor));
            liner.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark11));
            view2.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            parent_gold_market.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            textView69.setTextColor(ContextCompat.getColor(getContext(), R.color.white_tranperant));
            textView73.setTextColor(ContextCompat.getColor(getContext(), R.color.white_tranperant));
            liner_top.setBackgroundResource(R.drawable.back_liner_gold_top_dark);
            liner_dawon.setBackgroundResource(R.drawable.back_liner_gold_buttom_dark);
            textView74.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView76.setTextColor(ContextCompat.getColor(getContext(), R.color.color_navigation));
            imageView13.setImageResource(R.drawable.ic_path_334);


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        DocumentReference docRef = db.collection("Prices").document("Gold");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {

                        currency.addAll(document.getData().keySet());
                        currencyAdapter = new AdapterGoldMarketCurrency(currency, GoldMarketFragment.this);
                        rv_currency.setAdapter(currencyAdapter);
                        for (int i = 0; i < currency.size(); i++) {
                            if (currency.get(i).equalsIgnoreCase("KWD")) {
                                currencyAdapter.setSelected(i);
                                break;
                            }
                        }
                         countDownTimer = new CountDownTimer(10000, 1000) {

                            @Override
                            public void onTick(long millisUntilFinished) {
                                String timer = getString(R.string.timer_price).replace("xx", 10 - (millisUntilFinished/1000) + "");
                                tv_timer.setText(timer);
                            }

                            @Override
                            public void onFinish() {

                                getCurrencyPrice(currencyAdapter.getSelected());
                                countDownTimer.start();
                            }
                        };
                        countDownTimer.start();
//                        timer = new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//
//
//                                while (count != 10) {
//
//                                    if (getActivity() != null)
//                                        getActivity().runOnUiThread(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                String timer = getString(R.string.timer_price).replace("xx", 10 - count + "");
//                                                tv_timer.setText(timer);
//                                            }
//                                        });
//                                    if (count == 9) {
//                                        count = 0;
//                                        //  getCurrencyPrice(currencyAdapter.getSelected());
//
//                                    } else
//                                        count++;
//
//                                }
//
//                            }
//                        });
//
//                        timer.start();

                        Log.d("TAG", "DocumentSnapshot data: " + document.getData() + " " + document.getId());
                    } else {
                        Log.d("TAG", "No such document");
                    }
                } else {
                    Log.d("TAG", "get failed with ", task.getException());
                }
            }
        });

    }

    @Override
    public void onCurrencyClick(String currency) {

        getCurrencyPrice(currency);
//        getGold(currency);
//        getOunca(currency);
    }

    private void getCurrencyPrice(String currency) {
        CollectionReference docRef = db.collection("Prices");

        docRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<MainCaliberItem> mainCaliberItems = new ArrayList<>();
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Log.d("TAG", document.getId() + " => " + document.getData());
                        if (document.getId().equalsIgnoreCase("ounca")) {
                            getOunca(currency, document);
                        } else {
                            getMainCaliberItem(currency, document, mainCaliberItems);
                        }
                    }
                    AdapterGoldMarketCaliber adapterGoldMarketCaliber = new AdapterGoldMarketCaliber(mainCaliberItems);
                    rv_gold_market.setAdapter(adapterGoldMarketCaliber);
                } else {
                    Log.w("TAG", "Error getting documents.", task.getException());
                }
            }
        });

    }

    private void getMainCaliberItem(String currency) {
        DocumentReference docRef = db.collection("Prices").document("Gold");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        MainCaliberItem mainCaliberItem = new MainCaliberItem();
                        List<CaliberItem> caliberItemList = new ArrayList<>();
                        for (String key : document.getData().keySet()) {
                            if (key.equalsIgnoreCase(currency)) {
                                String object = document.getData().get(key).toString();
                                try {
                                    JSONArray object1 = new JSONArray(object);
                                    for (int i = 0; i < object1.length(); i++) {
                                        JSONObject jsonObject = object1.getJSONObject(i);
                                        CaliberItem caliberItem = new CaliberItem();
                                        caliberItem.setCaliber(jsonObject.getString("caliber"));
                                        caliberItem.setIs_up(jsonObject.getString("is_up"));
                                        caliberItem.setPrice(jsonObject.getDouble("price"));

                                        caliberItemList.add(caliberItem);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mainCaliberItem.setCaliberItems(caliberItemList);
                                List<MainCaliberItem> mainCaliberItems = new ArrayList<>();
                                mainCaliberItems.add(mainCaliberItem);
                                AdapterGoldMarketCaliber adapterGoldMarketCaliber = new AdapterGoldMarketCaliber(mainCaliberItems);
                                rv_gold_market.setAdapter(adapterGoldMarketCaliber);
                                break;
                            }
                        }

                        Log.d("TAG", "DocumentSnapshot data: " + document.getData() + " " + document.getId());
                    } else {
                        Log.d("TAG", "No such document");
                    }
                } else {
                    Log.d("TAG", "get failed with ", task.getException());
                }
            }
        });
    }

    private void getOunca(String currency) {
        DocumentReference docRef = db.collection("Prices").document("ounca");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        for (String key : document.getData().keySet()) {

                            if (key.equalsIgnoreCase(currency)) {
                                try {
                                    String object = document.getData().get(key).toString();
                                    JSONObject jsonObject = new JSONObject(object);
                                    CaliberItem caliberItem = new CaliberItem();
                                    caliberItem.setIs_up(jsonObject.getString("is_up"));
                                    caliberItem.setPrice(jsonObject.getDouble("price"));
                                    textView73.setText(getString(R.string.oz_price).replace("xx", caliberItem.getPrice() + ""));
                                    textView74.setText(caliberItem.getPrice() + "");
                                    if (!caliberItem.isUp())
                                        imageView13.setImageResource(R.drawable.ic_ounca_down);
                                    else {
                                        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                                            imageView13.setImageResource(R.drawable.ic_path_334);
                                        } else {
                                            imageView13.setImageResource(R.drawable.path_gold);
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;
                            }
                        }

                        Log.d("TAG", "DocumentSnapshot data: " + document.getData() + " " + document.getId());
                    } else {
                        Log.d("TAG", "No such document");
                    }
                } else {
                    Log.d("TAG", "get failed with ", task.getException());
                }
            }
        });
    }

    private void getOunca(String currency, QueryDocumentSnapshot document) {
        for (String key : document.getData().keySet()) {

            if (key.equalsIgnoreCase(currency)) {
                try {
                    String object = document.getData().get(key).toString();
                    JSONObject jsonObject = new JSONObject(object);
                    CaliberItem caliberItem = new CaliberItem();
                    caliberItem.setIs_up(jsonObject.getString("is_up"));
                    caliberItem.setPrice(jsonObject.getDouble("price"));
                    String priceFormatted=String.format(Locale.ENGLISH,"%.3f", caliberItem.getPrice());
                    textView73.setText(getString(R.string.oz_price).replace("xx",priceFormatted ));
                    textView74.setText(priceFormatted);
                    if (!caliberItem.isUp())
                        imageView13.setImageResource(R.drawable.ic_ounca_down);
                    else {
                        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                            imageView13.setImageResource(R.drawable.ic_path_334);
                        } else {
                            imageView13.setImageResource(R.drawable.path_gold);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            }
        }
    }

    private void getMainCaliberItem(String currency, QueryDocumentSnapshot document, List<MainCaliberItem> mainCaliberItems) {
        MainCaliberItem mainCaliberItem = new MainCaliberItem();
        List<CaliberItem> caliberItemList = new ArrayList<>();
        for (String key : document.getData().keySet()) {
            if (key.equalsIgnoreCase(currency)) {
                String object = document.getData().get(key).toString();
                try {
                    JSONArray object1 = new JSONArray(object);
                    for (int i = 0; i < object1.length(); i++) {
                        JSONObject jsonObject = object1.getJSONObject(i);
                        CaliberItem caliberItem = new CaliberItem();
                        caliberItem.setCaliber(jsonObject.getString("caliber"));
                        caliberItem.setIs_up(jsonObject.getString("is_up"));
                        caliberItem.setPrice(jsonObject.getDouble("price"));

                        caliberItemList.add(caliberItem);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mainCaliberItem.setCaliberItems(caliberItemList);

                mainCaliberItems.add(mainCaliberItem);

                break;
            }

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        count = 10;
        if(countDownTimer!=null) {
            countDownTimer.cancel();
            countDownTimer=null;
        }
    }
}
