package hama.alsaygh.kw.fragment.search;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.adapter.search.store.AdapterSearchForStores;
import hama.alsaygh.kw.adapter.search.store.AdapterSearchForStoresDark;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.GetStoresSearchResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.fragment.settings.storePackage.StorePagePackage;
import hama.alsaygh.kw.listener.OnStoreClickListener;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class SearchResultStorePackageFragment extends BaseFragment implements OnStoreClickListener {
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    ImageView imageback;
    TextView text_store_serch, recently_add;
    LinearLayout parent_serch_stores, liner_search_stores;
    SearchView edit_serch_for_stores;
    RecyclerView rv_search_for_stores;
    ArrayList<Store> best_stores;
    private Skeleton skeleton;
    String search;

    public static SearchResultStorePackageFragment newInstance(String search) {

        SearchResultStorePackageFragment fragment = new SearchResultStorePackageFragment();
        fragment.setSearch(search);
        return fragment;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        ((AppCompatActivity)getActivity()).getSupportActionBar().hide(); //<< this
//        SharedPreferences editor = getActivity().getApplicationContext().getSharedPreferences("share", MODE_PRIVATE);
//        if (editor.getBoolean("checked",true))
//            getActivity().setTheme(R.style.darktheme);
//        else
//            getActivity().setTheme(R.style.AppTheme);
        View view = inflater.inflate(R.layout.search_for_stores, container, false);
//        setLightStatusBar();
        fragmentManager = getFragmentManager();
        imageback = (ImageView) view.findViewById(R.id.imageView4);
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new SearchFragment());
                fragmentTransaction.commit();
            }
        });
        edit_serch_for_stores = view.findViewById(R.id.edit_serch_for_stores);
        parent_serch_stores = (LinearLayout) view.findViewById(R.id.parent_serch_stores);
        recently_add = (TextView) view.findViewById(R.id.recently_add);
        text_store_serch = (TextView) view.findViewById(R.id.text_store_serch);
        liner_search_stores = (LinearLayout) view.findViewById(R.id.liner_search_stores);
        rv_search_for_stores = (RecyclerView) view.findViewById(R.id.recycle_search_for_stores);
        best_stores = new ArrayList<>();
        rv_search_for_stores.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        skeleton = SkeletonLayoutUtils.applySkeleton(rv_search_for_stores, R.layout.row_stores_spanned, 3);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);

            text_store_serch.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            edit_serch_for_stores.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark11));
            ImageView searchIcon = edit_serch_for_stores.findViewById(androidx.appcompat.R.id.search_button);
            searchIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_icon_search));
            TextView searchAutoComplete = edit_serch_for_stores.findViewById(androidx.appcompat.R.id.search_src_text);
            searchAutoComplete.setHintTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            searchAutoComplete.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));


            recently_add.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
            parent_serch_stores.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            liner_search_stores.setBackgroundResource(R.drawable.back_liner_search_dark);


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        if (search != null && !search.isEmpty())
            getSearch();


        edit_serch_for_stores.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                search = "";
                edit_serch_for_stores.setQuery("", true);
                return false;
            }
        });


        edit_serch_for_stores.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search = query;
                getSearch();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        return view;

    }

    private void getSearch() {
        if (MainApplication.isConnected) {

            skeleton.showSkeleton();

            new Thread(() -> {

                final GetStoresSearchResponse getStoresResponse = RequestWrapper.getInstance().getSearchStorePackage(getContext(), search);
                if (getStoresResponse.isStatus()) {

                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                skeleton.showOriginal();
                                best_stores.clear();
                                best_stores.addAll(getStoresResponse.getStores());

                                if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                                    AdapterSearchForStoresDark adapterStoresSpannedDark = new AdapterSearchForStoresDark(best_stores, getActivity(), SearchResultStorePackageFragment.this);
                                    rv_search_for_stores.setAdapter(adapterStoresSpannedDark);
                                } else {
                                    AdapterSearchForStores adapterStoresSpanned = new AdapterSearchForStores(best_stores, getActivity(), SearchResultStorePackageFragment.this);
                                    rv_search_for_stores.setAdapter(adapterStoresSpanned);
                                }
                            }
                        });
                }

            }).start();
        }

    }

    private void setLightStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = getActivity().getWindow().getDecorView().getSystemUiVisibility(); // get current flag
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;   // add LIGHT_STATUS_BAR to flag
            getActivity().getWindow().getDecorView().setSystemUiVisibility(flags);
            getActivity().getWindow().setStatusBarColor(Color.TRANSPARENT); // optional
        }
    }

    @Override
    public void onStoreClick(Store store, int position, String tag) {
        HomeActivity.search=search;
        HomeActivity.position = HomeActivity.StorePackageDetailsFromSearch;
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.liner1, StorePagePackage.newInstance(store));
        fragmentTransaction.commit();
    }
}
