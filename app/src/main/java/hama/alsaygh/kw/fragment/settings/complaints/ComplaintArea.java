package hama.alsaygh.kw.fragment.settings.complaints;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.complaint.AdapterComplaintReplay;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.ReplayComplaintsResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.dialog.PopUpConfirmSolution;
import hama.alsaygh.kw.model.complaint.Complaint;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class ComplaintArea extends BaseFragment {
    static FragmentManager fragmentManager;
    ImageView imageBack, imagecheck, imageView45;
    TextView textView9, textView64, textView72;
    EditText editText7;
    LinearLayout send_liner, parent_area, liner_perant;
    Button button6;
    Complaint complaint;

    RecyclerView rv_replay;


    public static ComplaintArea newInstance(Complaint complaint) {

        ComplaintArea fragment = new ComplaintArea();
        fragment.setComplaint(complaint);
        return fragment;
    }

    public void setComplaint(Complaint complaint) {
        this.complaint = complaint;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.complaint_area, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        imageBack = view.findViewById(R.id.back_img);
        imagecheck = view.findViewById(R.id.image_check);
        textView9 = view.findViewById(R.id.textView9);
        textView64 = view.findViewById(R.id.textView64);
        imageView45 = view.findViewById(R.id.imageView45);
        editText7 = view.findViewById(R.id.editText7);
        textView72 = view.findViewById(R.id.textView72);
        button6 = view.findViewById(R.id.button6);
        send_liner = view.findViewById(R.id.send_liner);
        parent_area = view.findViewById(R.id.parent_area);
        liner_perant = view.findViewById(R.id.liner_perant);
        rv_replay = view.findViewById(R.id.rv_replay);
        rv_replay.setLayoutManager(new LinearLayoutManager(getContext()));
        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();

            }
        });
        imagecheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopUpConfirmSolution popUpConfirmSolution = new PopUpConfirmSolution();
                popUpConfirmSolution.show(getFragmentManager(), "pop up confirm solution");


            }
        });

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            imageBack.setImageResource(R.drawable.ic_back_icon_dark);
            imagecheck.setImageResource(R.drawable.ic_checkaaa);
//            textView9.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            // textView64.setTextColor(ContextCompat.getColor(getContext(), R.color.white_tranperant));
            imageView45.setImageResource(R.drawable.ic_icon_emoticon_dark);
            editText7.setHintTextColor(ContextCompat.getColor(getContext(), R.color.dark111));
            button6.setBackgroundResource(R.drawable.ic_send_button);
            //  textView72.setTextColor(ContextCompat.getColor(getContext(), R.color.textviewhome));
            // send_liner.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark11));
//            parent_area.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
//            liner_perant.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView72.performClick();
            }
        });

        textView72.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {


                if (MainApplication.isConnected) {

                    if (!editText7.getEditableText().toString().isEmpty()) {
                        final String msg = editText7.getEditableText().toString();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                final ReplayComplaintsResponse replayComplaintsResponse = RequestWrapper.getInstance().replayComplaint(getContext(), complaint.getId(), msg);
                                if (replayComplaintsResponse.isStatus()) {
                                    complaint = replayComplaintsResponse.getData();
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            if (complaint != null) {
                                                AdapterComplaintReplay adapterComplaintReplay = new AdapterComplaintReplay(getContext(), complaint.getReplies());
                                                rv_replay.setAdapter(adapterComplaintReplay);
                                                String time = getActivity().getString(R.string.compliaint) + " " + complaint.getSent_time();
                                                textView64.setText(time);
                                                editText7.setText("");
                                            }
                                        }
                                    });
                                } else {

                                    if (getActivity() != null) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                Snackbar.make(view, replayComplaintsResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                            }
                                        });
                                    }

                                }
                            }
                        }).start();
                    }

                } else {
                    Snackbar.make(v, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }


            }
        });

        if (complaint != null) {
            AdapterComplaintReplay adapterComplaintReplay = new AdapterComplaintReplay(getContext(), complaint.getReplies());
            rv_replay.setAdapter(adapterComplaintReplay);

            String time = getString(R.string.compliaint) + " " + complaint.getSent_time();
            textView64.setText(time);

        }

    }
}

