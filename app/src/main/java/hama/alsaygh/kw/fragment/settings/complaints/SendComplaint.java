package hama.alsaygh.kw.fragment.settings.complaints;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.AddComplaintsResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.db.table.User;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.dialog.PopUpComplaints;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class SendComplaint extends BaseFragment {
    FragmentManager fragmentManager;
    TextView textView43;
    EditText editEmail, editSubject, editMsg;
    Button button_send;
    LinearLayout parent_sendp;
    ProgressBar pb_send;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.send_complaint, container, false);
        fragmentManager = getChildFragmentManager();

        pb_send = view.findViewById(R.id.pb_send);
        if (pb_send.getIndeterminateDrawable() != null) {
            pb_send.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.whiteColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        textView43 = (TextView) view.findViewById(R.id.textView43);
        parent_sendp = (LinearLayout) view.findViewById(R.id.parent_sendp);
        button_send = (Button) view.findViewById(R.id.button_send);
        button_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (MainApplication.isConnected) {
                    if (isValid()) {
                        pb_send.setVisibility(View.VISIBLE);
                        button_send.setVisibility(View.GONE);
                        final String msg = editMsg.getEditableText().toString();
                        final String email = editEmail.getEditableText().toString();
                        final String subject = editSubject.getEditableText().toString();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                AppDatabase appDatabase = AppDatabase.newInstance(getContext());
                                List<User> userList = appDatabase.userDao().getAll();
                                if (userList != null && !userList.isEmpty()) {
                                    User user = userList.get(0);

                                    final AddComplaintsResponse loginResponse = RequestWrapper.getInstance().addComplaint(view.getContext(), user.getF_name() + " " + user.getL_name(), email, subject, msg);
                                    if (loginResponse.isStatus()) {

                                        if (getActivity() != null)
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {

                                                    editEmail.setText("");
                                                    editMsg.setText("");
                                                    editSubject.setText("");

                                                    button_send.setVisibility(View.VISIBLE);
                                                    pb_send.setVisibility(View.GONE);
                                                    PopUpComplaints popUpComplaints = PopUpComplaints.newInstance(getActivity().getSupportFragmentManager());
                                                    popUpComplaints.show(fragmentManager, "pop up complints");

                                                }
                                            });
                                    } else {
                                        if (getActivity() != null)
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    button_send.setVisibility(View.VISIBLE);
                                                    pb_send.setVisibility(View.GONE);
                                                    if (loginResponse.getCode().equalsIgnoreCase("401")) {
                                                        LoginDialog loginDialog = LoginDialog.newInstance();
                                                        loginDialog.show(getChildFragmentManager(), "login");
                                                    } else
                                                        Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                                }
                                            });
                                    }
                                } else {
                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                button_send.setVisibility(View.VISIBLE);
                                                pb_send.setVisibility(View.GONE);
                                                LoginDialog loginDialog = LoginDialog.newInstance();
                                                loginDialog.show(getChildFragmentManager(), "login");
                                            }
                                        });
                                }

                            }
                        }).start();
                    }
                } else
                    Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();

            }
        });
        editEmail = (EditText) view.findViewById(R.id.editText12);
        editSubject = (EditText) view.findViewById(R.id.editText122);
        editMsg = (EditText) view.findViewById(R.id.editText222);


        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
           // textView43.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
           // parent_sendp.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            button_send.setBackgroundResource(R.drawable.back_button_dark);
            editEmail.setBackgroundResource(R.drawable.back_edittext_dark);
            editSubject.setBackgroundResource(R.drawable.back_edittext_dark);
            editMsg.setBackgroundResource(R.drawable.back_edittext_dark);
//            editEmail.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
//            editSubject.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
//            editMsg.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        return view;

    }


    private boolean isValid() {
        boolean isValid = true;


        if (editSubject.getEditableText().toString().isEmpty()) {
            isValid = false;
            editSubject.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                editSubject.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editSubject.setBackgroundResource(R.drawable.back_ground_edit_text);

        }

        if (editEmail.getEditableText().toString().isEmpty() || !Utils.getInstance().isValidEmail(editEmail.getEditableText().toString())) {
            isValid = false;
            editEmail.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                editEmail.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editEmail.setBackgroundResource(R.drawable.back_ground_edit_text);

        }
        if (editMsg.getEditableText().toString().isEmpty()) {
            isValid = false;
            editMsg.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                editMsg.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editMsg.setBackgroundResource(R.drawable.back_ground_edit_text);

        }

        return isValid;
    }

}
