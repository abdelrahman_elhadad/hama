package hama.alsaygh.kw.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.rd.PageIndicatorView;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.imageSlider.SliderAdapter;
import hama.alsaygh.kw.adapter.imageSlider.SliderAdapterDark;
import hama.alsaygh.kw.adapter.review.AdapterProductDetailsReview;
import hama.alsaygh.kw.adapter.review.AdapterProductDetailsReviewDark;
import hama.alsaygh.kw.model.product.Media;
import hama.alsaygh.kw.model.product.review.Review;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import me.relex.circleindicator.CircleIndicator;

public class VideoUpLoad  extends BaseFragment {
    ViewPager viewPager;
    private LinearLayout linearLayout;
    PageIndicatorView pageIndicatorView;
    private int MCurentPage;
    Button button;
    ImageView imageback ,imageView9,imageView9222,share_img;
    TextView textView,text_toolbar_home,tv,tv1,tv2,tv3,textView6,textView633,tv111,lebel,review,write,textView3,textView4;
    Spinner spinnerSize, spinnerColor;
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    SliderAdapter sliderAdapter;
    SliderAdapterDark sliderAdapterDark;
    private TextView[] mDots;
    LinearLayout liner_color, liner_size;
    RecyclerView recycle_prouductdetails;
    //String[] size = {"","10","20","30","40","50","60","70","80","90"};
    // String [] color = {"","red","black","white","gray"};
    AdapterProductDetailsReview adapterProductDetailsReview;
    AdapterProductDetailsReviewDark adapterProductDetailsReviewDark;
    ArrayList<Review> Reviews;
    CircleIndicator indicator;
    ConstraintLayout constraintLayout,constraint2,parent_prouduct_details;
    LinearLayout liner_prouduct_details;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        ((AppCompatActivity)getActivity()).getSupportActionBar().hide(); //<< this
//        SharedPreferences editor = getActivity().getApplicationContext().getSharedPreferences("share", MODE_PRIVATE);
//        if (editor.getBoolean("checked",true))
//            getActivity().setTheme(R.style.darktheme);
//        else
//            getActivity().setTheme(R.style.AppTheme);
        View view = inflater.inflate(R.layout.video_upload, container, false);
        fragmentManager=getFragmentManager();
        spinnerColor =(Spinner)view.findViewById(R.id.spineer_color);
        spinnerSize =(Spinner)view.findViewById(R.id.spinner_size);
//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, color);
//        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerColor.setAdapter(arrayAdapter);
//
//     //  spinnerSize.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
//        ArrayAdapter<String> arrayCountry = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, size);
//        arrayCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerSize.setAdapter(arrayCountry);
        imageback =(ImageView)view.findViewById(R.id.imageView16);
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();
//                fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.liner1,new StorePage());
//                fragmentTransaction.commit();
            }
        });



//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.product_details);
//        getSupportActionBar().hide();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getWindow();
//            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//        }
        viewPager = (ViewPager)view.findViewById(R.id.viewPager11);
        sliderAdapter = new SliderAdapter(getActivity(), getLayoutInflater(),getChildFragmentManager(),new ArrayList<Media>());
        viewPager.setAdapter(sliderAdapter);
        viewPager.addOnPageChangeListener(viewListener);
        indicator = (CircleIndicator)view.findViewById(R.id.pageIndicatorView1);
        indicator.setViewPager(viewPager);



//        pageIndicatorView = view.findViewById(R.id.pageIndicatorView1);
//        pageIndicatorView.setClickListener(new DrawController.ClickListener() {
//            @Override
//            public void onIndicatorClicked(int position) {

//                pageIndicatorView.setSelection(position);
//                pageIndicatorView.setAnimationType(AnimationType.WORM);
//                viewPager.setCurrentItem(position);
        //      }
        //   });
//        pageIndicatorView.setCount(3); // specify total count of indicators
//        pageIndicatorView.setSelection(2);
        recycle_prouductdetails =(RecyclerView)view.findViewById(R.id.rv_proudect_details);


        adapterProductDetailsReview = new AdapterProductDetailsReview(Reviews);
        recycle_prouductdetails.setAdapter(adapterProductDetailsReview);
        liner_color=(LinearLayout)view.findViewById(R.id.liner_color);
        liner_size=(LinearLayout)view.findViewById(R.id.liner_size);
//        liner_color.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                spinnerColor.performClick();
//                liner_color.setVisibility(View.INVISIBLE);
//                liner_color.setEnabled(false);
//                spinnerColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//
//                    @Override
//                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                        spinnerColor.setSelection(position);
//
//
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> parent) {
//                        liner_color.setVisibility(View.VISIBLE);
//                        parent.getEmptyView();
//
//
//
//                    }
//                });




        //              }
        //      });

//        liner_size.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                spinnerSize.performClick();
//                liner_size.setVisibility(View.INVISIBLE);
//                liner_size.setEnabled(false);
//                spinnerSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//
//                    @Override
//                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                        spinnerSize.setSelection(position);
//
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> parent) {
//                        liner_size.setVisibility(View.VISIBLE);
//                        parent.getEmptyView();
//
//
//                    }
//                });
//
//
//            }
//        });

        text_toolbar_home=(TextView)view.findViewById(R.id.textView19);
        tv=(TextView)view.findViewById(R.id.tv);
        constraintLayout=(ConstraintLayout)view.findViewById(R.id.contener);
        tv1=(TextView)view.findViewById(R.id.tv1);
        tv2=(TextView)view.findViewById(R.id.tv2);
        tv3=(TextView)view.findViewById(R.id.tv3);
        textView6=(TextView)view.findViewById(R.id.textView6);
        imageView9=(ImageView)view.findViewById(R.id.imageView9);
        textView633=(TextView)view.findViewById(R.id.textView633);
        imageView9222=(ImageView)view.findViewById(R.id.imageView9222);
        tv111=(TextView)view.findViewById(R.id.tv111);
        lebel=(TextView)view.findViewById(R.id.lebel);
        review=(TextView)view.findViewById(R.id.review);
        write=(TextView)view.findViewById(R.id.write);
        button=(Button)view.findViewById(R.id.button3);
        textView3=(TextView)view.findViewById(R.id.textView3);
        textView4=(TextView)view.findViewById(R.id.textView4);
        constraint2=(ConstraintLayout)view.findViewById(R.id.constraint2);
        share_img=(ImageView)view.findViewById(R.id.imageView71);
        parent_prouduct_details=(ConstraintLayout)view.findViewById(R.id.parent_prouduct_details);
        sliderAdapterDark = new SliderAdapterDark(getActivity(), getLayoutInflater());
        adapterProductDetailsReviewDark = new AdapterProductDetailsReviewDark(Reviews);
        liner_prouduct_details=(LinearLayout)view.findViewById(R.id.liner_prouduct_details);
        if(SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            text_toolbar_home.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
            imageView9.setImageResource(R.drawable.ic_icon_dawn);
            imageView9222.setImageResource(R.drawable.ic_icon_dawn);
            tv.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            tv1.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            tv2.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            tv3.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            textView633.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            tv111.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            lebel.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            write.setTextColor(ContextCompat.getColor(getContext(),R.color.order_tracking1));
            button.setBackgroundResource(R.drawable.back_button_dark);
            textView3.setTextColor(ContextCompat.getColor(getContext(),R.color.sign_in_dark));
            textView4.setTextColor(ContextCompat.getColor(getContext(),R.color.textviewhome));
            textView6.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            constraint2.setBackgroundResource(0);
            constraintLayout.setBackgroundResource(R.drawable.back_spinner_dark);
            spinnerColor.setBackgroundResource(R.drawable.back_spinner_dark);
//            spinnerSize.setBackgroundResource(R.drawable.back_card_best_selling_dark);
            share_img.setImageResource(R.drawable.ic_share_dark);
            parent_prouduct_details.setBackgroundResource(R.color.sign_in);
            review.setTextColor(ContextCompat.getColor(getContext(),R.color.whiteColor));
            viewPager.setAdapter(sliderAdapterDark);
            recycle_prouductdetails.setAdapter(adapterProductDetailsReviewDark);
            liner_prouduct_details.setBackgroundResource(R.color.dark11);


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        return view;
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
//            pageIndicatorView.setSelection(position);
//            pageIndicatorView.setAnimationType(AnimationType.WORM);

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}