package hama.alsaygh.kw.fragment.settings.contactsUs;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.ContactUsResponse;
import hama.alsaygh.kw.api.responce.SocialMediaResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.model.socialMedia.SocialMedia;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class ContactUsFragment extends BaseFragment {
    static FragmentManager fragmentManager;
    ImageView img_back, face_img, tweter, instegram, linked_in;
    Button btn_send;
    TextView txt_toolbar, textView106, textView116, or, textView118, tv_subject_error, tv_full_name_error;
    EditText edtFullName, edtEmail, edtMsg, edt_subject, edt_phone;
    LinearLayout parent_contact_us;
    View view19, view20;
    private ProgressBar pb_send;

    SocialMedia socialMedia;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.contact_us, container, false);
        fragmentManager = getFragmentManager();
        img_back = (ImageView) view.findViewById(R.id.img_back);
        txt_toolbar = (TextView) view.findViewById(R.id.txt_toolbar);
        parent_contact_us = (LinearLayout) view.findViewById(R.id.parent_contact_us);
        textView106 = (TextView) view.findViewById(R.id.textView106);
        textView116 = (TextView) view.findViewById(R.id.textView116);
        edtFullName = (EditText) view.findViewById(R.id.editText17);
        edtEmail = (EditText) view.findViewById(R.id.editText77);
        edtMsg = (EditText) view.findViewById(R.id.editText18);
        btn_send = (Button) view.findViewById(R.id.btn_send);
        view19 = (View) view.findViewById(R.id.view19);
        or = (TextView) view.findViewById(R.id.or);
        textView118 = (TextView) view.findViewById(R.id.textView118);
        face_img = (ImageView) view.findViewById(R.id.face_img);
        tweter = (ImageView) view.findViewById(R.id.tweter);
        instegram = (ImageView) view.findViewById(R.id.instegram);
        linked_in = (ImageView) view.findViewById(R.id.linked_in);
        view20 = (View) view.findViewById(R.id.view20);
        pb_send = view.findViewById(R.id.pb_send);
        edt_subject = view.findViewById(R.id.edt_subject);
        edt_phone = view.findViewById(R.id.edt_phone);
        tv_subject_error = view.findViewById(R.id.tv_subject_error);
        tv_full_name_error = view.findViewById(R.id.tv_full_name_error);

        img_back.setOnClickListener(v -> {

            if (getActivity() != null)
                getActivity().onBackPressed();
        });

        if (pb_send.getIndeterminateDrawable() != null) {
            pb_send.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.whiteColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            parent_contact_us.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            textView106.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView116.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            edtFullName.setBackgroundResource(R.drawable.back_zakat_dark);
            edtFullName.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            edtEmail.setBackgroundResource(R.drawable.back_zakat_dark);
            edtEmail.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            edtMsg.setBackgroundResource(R.drawable.back_zakat_dark);
            edtMsg.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            edt_subject.setBackgroundResource(R.drawable.back_zakat_dark);
            edt_subject.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            edt_phone.setBackgroundResource(R.drawable.back_zakat_dark);
            edt_phone.setHintTextColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));

            btn_send.setBackgroundResource(R.drawable.back_button_dark);
            pb_send.setBackgroundResource(R.drawable.back_button_dark);
            view19.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            view20.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in_dark));
            or.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView118.setTextColor(ContextCompat.getColor(getContext(), R.color.white_tranperant));
            face_img.setImageResource(R.drawable.ic_facebook);
            tweter.setImageResource(R.drawable.ic_tweter);
            instegram.setImageResource(R.drawable.ic_instegram);
            linked_in.setImageResource(R.drawable.ic_linked_in);

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        btn_send.setOnClickListener(view1 -> {

            if (MainApplication.isConnected) {
                if (isValid()) {
                    pb_send.setVisibility(View.VISIBLE);
                    btn_send.setVisibility(View.GONE);
                    final String name = edtFullName.getEditableText().toString();
                    final String email = edtEmail.getEditableText().toString();
                    final String msg = edtMsg.getEditableText().toString();
                    final String phone = edt_phone.getEditableText().toString();
                    final String subject = edt_subject.getEditableText().toString();


                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            final ContactUsResponse loginResponse = RequestWrapper.getInstance().contactUs(view1.getContext(), name, email, phone, subject, msg);
                            if (loginResponse.isStatus()) {

                                if (getActivity() != null)
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            btn_send.setVisibility(View.VISIBLE);
                                            pb_send.setVisibility(View.GONE);
                                            edtEmail.setText("");
                                            edtMsg.setText("");
                                            edt_phone.setText("");
                                            edt_subject.setText("");
                                            edtFullName.setText("");


                                            Snackbar.make(view1, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();

                                        }
                                    });
                            } else {
                                if (getActivity() != null)
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            btn_send.setVisibility(View.VISIBLE);
                                            pb_send.setVisibility(View.GONE);
                                            Snackbar.make(view1, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                        }
                                    });
                            }

                        }
                    }).start();
                }
            } else
                Snackbar.make(view1, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
        });


        new Thread(() -> {

            final SocialMediaResponse socialMediaResponse = RequestWrapper.getInstance().getSocialMedia(getActivity());
            if (socialMediaResponse.isStatus()) {
                socialMedia = socialMediaResponse.getData();
                if (getActivity() != null)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (socialMedia.getFacebook() != null && !socialMedia.getFacebook().isEmpty()) {
                                face_img.setVisibility(View.VISIBLE);
                            } else
                                face_img.setVisibility(View.GONE);


                            if (socialMedia.getInstagram() != null && !socialMedia.getInstagram().isEmpty()) {
                                instegram.setVisibility(View.VISIBLE);
                            } else
                                instegram.setVisibility(View.GONE);

                            if (socialMedia.getLinkedin() != null && !socialMedia.getLinkedin().isEmpty()) {
                                linked_in.setVisibility(View.VISIBLE);
                            } else
                                linked_in.setVisibility(View.GONE);

                            if (socialMedia.getTwitter() != null && !socialMedia.getTwitter().isEmpty()) {
                                tweter.setVisibility(View.VISIBLE);
                            } else
                                tweter.setVisibility(View.GONE);

//                                if (socialMedia.getYoutube() != null && !socialMedia.getYoutube().isEmpty())
//                                {
//                                    tweter.setVisibility(View.VISIBLE);
//                                }else
//                                    tweter.setVisibility(View.GONE);

                        }
                    });
            } else {
                if (getActivity() != null)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(view, socialMediaResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                        }
                    });
            }
        }).start();


        face_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getActivity() != null && socialMedia != null)
                    if (socialMedia.getFacebook() != null && !socialMedia.getFacebook().startsWith("http"))
                        startActivity(newFacebookIntent(getActivity().getPackageManager(), socialMedia.getFacebook()));
                    else if (socialMedia.getFacebook() != null) {
                        try {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(socialMedia.getFacebook()));
                            startActivity(browserIntent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
            }
        });

        linked_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (socialMedia != null) {
                    if (socialMedia.getLinkedin() != null && !socialMedia.getLinkedin().startsWith("http")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://you"));
                        final PackageManager packageManager = getContext().getPackageManager();
                        final List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                        if (list.isEmpty()) {
                            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.linkedin.com/profile/view?id=" + socialMedia.getLinkedin()));
                        }
                        startActivity(intent);
                    }else if (socialMedia.getLinkedin() != null) {
                        try {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(socialMedia.getLinkedin()));
                            startActivity(browserIntent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

//                Intent linkedinIntent = new Intent(Intent.ACTION_VIEW);
//                linkedinIntent.setClassName("com.linkedin.android", "com.linkedin.android.profile.ViewProfileActivity");
//                linkedinIntent.putExtra("memberId", <member id>);
//                startActivity(linkedinIntent);

//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/company/your_company_id/")));
                }
            }
        });


        tweter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (socialMedia != null) {
                    if (socialMedia.getTwitter() != null && !socialMedia.getTwitter().startsWith("http")) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + socialMedia.getTwitter())));
                        } catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/#!/" + socialMedia.getTwitter())));
                        }
                    }else if (socialMedia.getTwitter() != null) {
                        try {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(socialMedia.getTwitter()));
                            startActivity(browserIntent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        });

        instegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (socialMedia.getInstagram() != null && !socialMedia.getInstagram().startsWith("http")) {
                    Intent insta_intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.instagram.android");
                    insta_intent.setComponent(new ComponentName("com.instagram.android", "com.instagram.android.activity.UrlHandlerActivity"));

                    //use this if you want to open an image
//                    insta_intent.setData(Uri.parse("http://instagram.com/p/gjfLqSBQTJ/"));

                    //And if you want to open a user's profile use this
                    insta_intent.setData(Uri.parse("http://instagram.com/_u/" + socialMedia.getInstagram()));
                    startActivity(insta_intent);
                } else if (socialMedia.getInstagram() != null) {
                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(socialMedia.getInstagram()));
                        startActivity(browserIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        return view;
    }

    private boolean isValid() {
        boolean isValid = true;


        if (edtFullName.getEditableText().toString().isEmpty() || edtFullName.getEditableText().toString().length() < 4) {
            isValid = false;
            edtFullName.setBackgroundResource(R.drawable.back_edittext_red);
            tv_full_name_error.setVisibility(View.VISIBLE);
        } else {

            tv_full_name_error.setVisibility(View.GONE);
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                edtFullName.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edtFullName.setBackgroundResource(R.drawable.back_ground_edit_text);
        }

        if (edtEmail.getEditableText().toString().isEmpty() || !Utils.getInstance().isValidEmail(edtEmail.getEditableText().toString())) {
            isValid = false;
            edtEmail.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                edtEmail.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edtEmail.setBackgroundResource(R.drawable.back_ground_edit_text);

        }
        if (edtMsg.getEditableText().toString().isEmpty()) {
            isValid = false;
            edtMsg.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                edtMsg.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edtMsg.setBackgroundResource(R.drawable.back_ground_edit_text);

        }


        if (edt_subject.getEditableText().toString().isEmpty() || edt_subject.getEditableText().toString().length() < 10) {
            isValid = false;
            edt_subject.setBackgroundResource(R.drawable.back_edittext_red);
            tv_subject_error.setVisibility(View.VISIBLE);
        } else {

            tv_subject_error.setVisibility(View.GONE);
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                edt_subject.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edt_subject.setBackgroundResource(R.drawable.back_ground_edit_text);

        }

        if (edt_phone.getEditableText().toString().isEmpty()) {
            isValid = false;
            edt_phone.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                edt_phone.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edt_phone.setBackgroundResource(R.drawable.back_ground_edit_text);
        }

        return isValid;
    }

    public Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                // http://stackoverflow.com/a/24547437/1048340
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }
}
