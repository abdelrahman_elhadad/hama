package hama.alsaygh.kw.fragment.home.store;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.adapter.store.AdapterStoresProductSort;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.db.table.FilterProduct;
import hama.alsaygh.kw.fragment.settings.storePackage.StorePagePackage;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class Sortingby extends BaseFragment {
    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    ImageView imageclose;
    Button button;
    TextView homeText, clear_all;
    LinearLayout liner_parent_sorting;
    Store store = null;
    RecyclerView rv_sort;
    Skeleton skeleton;
    int type = 1; // 1 for store page , 2 for package store

    public void setStore(Store store) {
        this.store = store;
    }

    public void setType(int type) {
        this.type = type;
    }

    public static Sortingby newInstance(Store store, int type) {

        Sortingby fragment = new Sortingby();
        fragment.setStore(store);
        fragment.setType(type);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sorting, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getFragmentManager();
        imageclose = (ImageView) view.findViewById(R.id.imageView4);
        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 1) {
                    HomeActivity.position = HomeActivity.StoreDetails;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, StorePage.newInstance(store));
                    fragmentTransaction.commit();
                } else {
                    HomeActivity.position = HomeActivity.StoreDetailsFromAllStorePackage;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, StorePagePackage.newInstance(store));
                    fragmentTransaction.commit();
                }
            }
        });
        button = (Button) view.findViewById(R.id.buttom_sort);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 1) {
                    HomeActivity.position = HomeActivity.StoreDetails;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, StorePage.newInstance(store));
                    fragmentTransaction.commit();
                } else {
                    HomeActivity.position = HomeActivity.StoreDetailsFromAllStorePackage;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, StorePagePackage.newInstance(store));
                    fragmentTransaction.commit();
                }
            }
        });
        homeText = (TextView) view.findViewById(R.id.homeText);
        clear_all = (TextView) view.findViewById(R.id.clear_all);
        rv_sort = view.findViewById(R.id.rv_sort);
        rv_sort.setLayoutManager(new LinearLayoutManager(getContext()));
        liner_parent_sorting = (LinearLayout) view.findViewById(R.id.liner_parent_sorting);
        skeleton = SkeletonLayoutUtils.applySkeleton(rv_sort, R.layout.row_sorting, 1);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);
            homeText.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            imageclose.setImageResource(R.drawable.ic_close_dark);
            clear_all.setTextColor(ContextCompat.getColor(getContext(), R.color.textviewhome));
            liner_parent_sorting.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            button.setBackgroundResource(R.drawable.back_button_dark);

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        clear_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().setSort_key("");
                if (rv_sort != null && rv_sort.getAdapter() != null)
                    rv_sort.getAdapter().notifyDataSetChanged();
            }
        });

        skeleton.showSkeleton();
        new Thread(new Runnable() {
            @Override
            public void run() {


                AppDatabase appDatabase = AppDatabase.newInstance(getContext());
                final List<FilterProduct> filterProducts = appDatabase.filterProductDao().getAll();
                if (getActivity() != null)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            skeleton.showOriginal();
                            AdapterStoresProductSort adapterStoresProductSort = new AdapterStoresProductSort(getContext(), filterProducts);
                            rv_sort.setAdapter(adapterStoresProductSort);
                        }
                    });


            }
        }).start();


    }
}