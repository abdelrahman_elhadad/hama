package hama.alsaygh.kw.fragment.home.store;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.adapter.store.AdapterStoresProductFilter;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.CategoriesResponse;
import hama.alsaygh.kw.api.responce.MainCategoriesResponse;
import hama.alsaygh.kw.fragment.settings.storePackage.StorePagePackage;
import hama.alsaygh.kw.model.category.MainCategory;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class Filterby extends BaseFragment {
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    Button button;
    TextView clear_all, homeText;
    ImageView imageclose;
    LinearLayout liner_parent_sorting;

    RecyclerView rv_filter;
    int type = 1; // 1 for store page , 2 for package store
    Store store = null;

    List<MainCategory> categories=new ArrayList<>();

    public void setStore(Store store) {
        this.store = store;
    }

    public void setType(int type) {
        this.type = type;
    }

    public static Filterby newInstance(Store store, int type) {


        Filterby fragment = new Filterby();
        fragment.setStore(store);
        fragment.setType(type);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filter_by, container, false);

        fragmentManager = getFragmentManager();
        button = (Button) view.findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 1) {
                    HomeActivity.position = HomeActivity.StoreDetails;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, StorePage.newInstance(store));
                    fragmentTransaction.commit();
                } else {
                    HomeActivity.position = HomeActivity.StoreDetailsFromAllStorePackage;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, StorePagePackage.newInstance(store));
                    fragmentTransaction.commit();
                }
            }
        });
        rv_filter = view.findViewById(R.id.rv_filter);
        homeText = (TextView) view.findViewById(R.id.textView9);
        imageclose = (ImageView) view.findViewById(R.id.imageView4);
        clear_all = (TextView) view.findViewById(R.id.imageView44);
        liner_parent_sorting = (LinearLayout) view.findViewById(R.id.liner_parent_filter);
        homeText = (TextView) view.findViewById(R.id.textView9);
        clear_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().setType_of_price("");
                Utils.getInstance().setCategory_level_1(-1);
                Utils.getInstance().setCategory_level_2(-1);
                Utils.getInstance().setCategory_level_3(-1);
                Utils.getInstance().setRange_price_from("");
                Utils.getInstance().setRange_price_to("");
                if (rv_filter != null && rv_filter.getAdapter() != null)
                    rv_filter.getAdapter().notifyDataSetChanged();
            }
        });
        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 1) {
                    HomeActivity.position = HomeActivity.StoreDetails;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, StorePage.newInstance(store));
                    fragmentTransaction.commit();
                } else {
                    HomeActivity.position = HomeActivity.StoreDetailsFromAllStorePackage;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, StorePagePackage.newInstance(store));
                    fragmentTransaction.commit();
                }
            }
        });
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            homeText.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            imageclose.setImageResource(R.drawable.ic_close_dark);
            clear_all.setTextColor(ContextCompat.getColor(getContext(), R.color.textviewhome));
            liner_parent_sorting.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            button.setBackgroundResource(R.drawable.back_button_dark);


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        new Thread(new Runnable() {
            @Override
            public void run() {
                MainCategoriesResponse response = RequestWrapper.getInstance().getMainCategoies(getContext());
                CategoriesResponse categoriesResponse = RequestWrapper.getInstance().getCategoies(getContext());
                if (response.isStatus() && categoriesResponse.isStatus()) {
                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                categories.addAll(response.getData());

                                MainCategory category=new MainCategory();
                                category.setId(-1);
                                category.setName(getString(R.string.price));
                                categories.add(category);
                                MainCategory category2=new MainCategory();
                                category2.setId(-2);
                                category2.setName(getString(R.string.FILTER_BY4));
                                categories.add(category2);

                                AdapterStoresProductFilter adapterStoresProductFilter = new AdapterStoresProductFilter(getContext(),categories, categoriesResponse.getData());
                                rv_filter.setAdapter(adapterStoresProductFilter);
                            }
                        });
                } else {

                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Snackbar.make(view, response.getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        });
                }
            }
        }).start();


        return view;

    }
}
