package hama.alsaygh.kw.fragment.settings.events;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.event.AdapterEventFragment;
import hama.alsaygh.kw.adapter.event.AdapterEventFragmentDark;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.EventsResponse;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.model.event.Event;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class EventFragment extends BaseFragment {
    FragmentManager fragmentManager;

    ArrayList<Event> myeventArray;
    AdapterEventFragment adapterEventFragment;
    RecyclerView event_recycle;
    LinearLayout parent_event_fragment;
    LinearLayout ll_no_order;
    ImageView my_order_img;
    TextView no_order;
    Skeleton skeleton;
    View view;

    public EventFragment() {
    }

    public static EventFragment newInstance() {
        EventFragment fragment = new EventFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.events_fragment, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragmentManager = getChildFragmentManager();
        myeventArray = new ArrayList<>();

        event_recycle = (RecyclerView) view.findViewById(R.id.recycle_my_event);
        parent_event_fragment = (LinearLayout) view.findViewById(R.id.parent_event_fragment);
        ll_no_order = view.findViewById(R.id.ll_no_event);
        my_order_img = view.findViewById(R.id.my_order_img);
        no_order = view.findViewById(R.id.no_order);

        skeleton = SkeletonLayoutUtils.applySkeleton(event_recycle, R.layout.card_my_event, 1);
        Utils.getInstance().setSkeletonMaskAndShimmer(getContext(), skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            parent_event_fragment.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            Utils.getInstance().setSkeletonMaskAndShimmerDark(getContext(), skeleton);

            my_order_img.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_calendar_dark));
            no_order.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));

        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        getEvents();

    }

    public void getEvents() {
        skeleton.showSkeleton();
        new Thread(new Runnable() {
            @Override
            public void run() {

                final EventsResponse eventsResponse = RequestWrapper.getInstance().getEvents(getContext());
                if (eventsResponse.isStatus()) {
                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                skeleton.showOriginal();

                                myeventArray.clear();
                                myeventArray.addAll(eventsResponse.getData());

                                if(myeventArray.isEmpty())
                                {
                                    ll_no_order.setVisibility(View.VISIBLE);
                                    event_recycle.setVisibility(View.GONE);
                                }else
                                {
                                    ll_no_order.setVisibility(View.GONE);
                                    event_recycle.setVisibility(View.VISIBLE);
                                    if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                                        AdapterEventFragmentDark adapterEventFragmentDark = new AdapterEventFragmentDark(myeventArray);
                                        event_recycle.setAdapter(adapterEventFragmentDark);

                                    } else {

                                        adapterEventFragment = new AdapterEventFragment(myeventArray);
                                        event_recycle.setAdapter(adapterEventFragment);

                                    }
                                }

                            }
                        });
                } else {
                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                skeleton.showOriginal();
                                Snackbar.make(view, eventsResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                if (eventsResponse.getCode().equalsIgnoreCase("401")) {
                                    LoginDialog loginDialog = LoginDialog.newInstance();
                                    loginDialog.show(getChildFragmentManager(), "login");
                                }
                            }
                        });
                }

            }
        }).start();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            getEvents();
        }
    }
}
