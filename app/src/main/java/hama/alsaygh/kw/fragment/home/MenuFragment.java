package hama.alsaygh.kw.fragment.home;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.activity.marketPrice.MarketPriceActivity;
import hama.alsaygh.kw.activity.order.OrdersActivity;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.db.table.User;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.fragment.HamaEventAndAdvertisment;
import hama.alsaygh.kw.fragment.settings.address.MyAddresses;
import hama.alsaygh.kw.fragment.settings.appSetting.Setting;
import hama.alsaygh.kw.fragment.settings.appointmentBoking.AppointmentBooking;
import hama.alsaygh.kw.fragment.settings.calculator.CalculatorsFragment;
import hama.alsaygh.kw.fragment.settings.complaints.Complaints;
import hama.alsaygh.kw.fragment.settings.contactsUs.ContactUsFragment;
import hama.alsaygh.kw.fragment.settings.events.MyEvents;
import hama.alsaygh.kw.fragment.settings.pages.AboutHama;
import hama.alsaygh.kw.fragment.settings.pages.TermsAndConditions;
import hama.alsaygh.kw.fragment.settings.paymentMethod.PaymentMethod;
import hama.alsaygh.kw.fragment.settings.profile.EditProfile;
import hama.alsaygh.kw.fragment.settings.sellYourProduct.SellYourProduct;
import hama.alsaygh.kw.fragment.settings.storePackage.AllStoresPackage;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;
import hama.alsaygh.kw.utils.image.CircleTransform;

public class MenuFragment extends BaseFragment {

    TextView
            textView38, textView39, advertising_txt, sell_txt, apoiment_txt, payment_txt, log_out;

    TextView my_order, term_txt, contact_us_txt;
    TextView setting, txt_toolbar, order_tracking_txt, pakckeging_stores, calculator_txt, complaints_txt, calender_txt, rate_hama_txt, about_hama_txt;
    ImageView img_toolbar, edit_icon, my_order_img, order_tracking_img, pakckeging_stores_img, rate_hama_img,
            calculator, img_advertising, sell_img, apoiment_img, setting_img, payment_method_img, complaints_img,
            calender_img, about_hama_img, log_out_img, contact_us, term, address;
    LinearLayout liner_my_profile_parent;
    LinearLayout ll_contact_us, ll_address, ll_term;

    ImageView imageView26;

    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    View view_mypro, view_mypro1, view_mypro2, view_mypro3, view_mypro4, view_mypro5, view_mypro6, view_mypro7, view_mypro8, view_mypro9, view_mypro10, view_mypro11, view_mypro12, view13, view14, view17, view19;
    LinearLayout linearLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.my_profil, container, false);
        fragmentManager = getFragmentManager();
        final LinearLayout ll_logout = view.findViewById(R.id.ll_logout);
        ll_contact_us = view.findViewById(R.id.ll_contact_us);
        ll_address = view.findViewById(R.id.ll_address);
        ll_term = view.findViewById(R.id.ll_term);
        if (!SharedPreferenceConstant.getSharedPreferenceUserToken(getContext()).isEmpty()) {
            ll_logout.setVisibility(View.VISIBLE);
        } else
            ll_logout.setVisibility(View.GONE);

        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().logOut(getActivity());
            }
        });
        ll_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SharedPreferenceConstant.getSharedPreferenceUserToken(getContext()).isEmpty()) {

                    HomeActivity.position = HomeActivity.MyOrder;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, new MyAddresses());
                    fragmentTransaction.commit();
                } else {
                    LoginDialog loginDialog = LoginDialog.newInstance();
                    loginDialog.show(getChildFragmentManager(), "login");
                }
            }
        });
        ll_contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.MyOrder;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new ContactUsFragment());
                fragmentTransaction.commit();
            }
        });

        ll_term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.MyOrder;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new TermsAndConditions());
                fragmentTransaction.commit();
            }
        });
        my_order = (TextView) view.findViewById(R.id.my_order);
        my_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), OrdersActivity.class);
                startActivity(intent);
            }
        });
        setting = (TextView) view.findViewById(R.id.setting);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.Setting;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new Setting());
                fragmentTransaction.commit();


            }
        });
        imageView26 = view.findViewById(R.id.imageView26);
        view_mypro = (View) view.findViewById(R.id.view969);
        view_mypro1 = (View) view.findViewById(R.id.view1);
        view_mypro2 = (View) view.findViewById(R.id.view2);
        view_mypro3 = (View) view.findViewById(R.id.view3);
        view_mypro4 = (View) view.findViewById(R.id.view4);
        view_mypro5 = (View) view.findViewById(R.id.view5);
        view_mypro6 = (View) view.findViewById(R.id.view6);
        view_mypro7 = (View) view.findViewById(R.id.view7);
        view_mypro8 = (View) view.findViewById(R.id.view8);
        view_mypro9 = (View) view.findViewById(R.id.view9);
        view_mypro10 = (View) view.findViewById(R.id.view10);
        view_mypro11 = (View) view.findViewById(R.id.view11);
        view_mypro12 = (View) view.findViewById(R.id.view12);
        view13 = (View) view.findViewById(R.id.view13);
        view14 = (View) view.findViewById(R.id.view14);
        view17 = (View) view.findViewById(R.id.view17);
        view19 = (View) view.findViewById(R.id.view19);


        if (Locale.getDefault().getLanguage().toString().equals("ar")) {
            Drawable d = ContextCompat.getDrawable(getContext(), R.drawable.back_view_myprofile_ar);
            view_mypro.setBackground(d);
            view_mypro1.setBackground(d);
            view_mypro2.setBackground(d);
            view_mypro3.setBackground(d);
            view_mypro4.setBackground(d);
            view_mypro5.setBackground(d);
            view_mypro6.setBackground(d);
            view_mypro7.setBackground(d);
            view_mypro8.setBackground(d);
            view_mypro9.setBackground(d);
            view_mypro10.setBackground(d);
            view_mypro11.setBackground(d);
            view_mypro12.setBackground(d);
            view13.setBackground(d);
            view14.setBackground(d);
            view17.setBackground(d);
            view19.setBackground(d);

        } else {
            Drawable d = ContextCompat.getDrawable(getContext(), R.drawable.back_my_profile);
            view_mypro.setBackground(d);
            view_mypro1.setBackground(d);
            view_mypro2.setBackground(d);
            view_mypro3.setBackground(d);
            view_mypro4.setBackground(d);
            view_mypro5.setBackground(d);
            view_mypro6.setBackground(d);
            view_mypro7.setBackground(d);
            view_mypro8.setBackground(d);
            view_mypro9.setBackground(d);
            view_mypro10.setBackground(d);
            view_mypro11.setBackground(d);
            view_mypro12.setBackground(d);
            view13.setBackground(d);
            view14.setBackground(d);
            view17.setBackground(d);
            view19.setBackground(d);

        }
        img_toolbar = (ImageView) view.findViewById(R.id.img_toolbar);
        edit_icon = (ImageView) view.findViewById(R.id.edit_icon);
        txt_toolbar = (TextView) view.findViewById(R.id.txt_toolbar);
        linearLayout = (LinearLayout) view.findViewById(R.id.liner_my_profile_parent);
        textView38 = (TextView) view.findViewById(R.id.textView38);
        textView39 = (TextView) view.findViewById(R.id.textView39);
        my_order_img = (ImageView) view.findViewById(R.id.my_order_img);
        order_tracking_img = (ImageView) view.findViewById(R.id.order_tracking_img);
        order_tracking_txt = (TextView) view.findViewById(R.id.order_tracking_txt);
        pakckeging_stores = (TextView) view.findViewById(R.id.pakckeging_stores);
        pakckeging_stores_img = (ImageView) view.findViewById(R.id.pakckeging_stores_img);
        calculator = (ImageView) view.findViewById(R.id.calculator);
        calculator_txt = (TextView) view.findViewById(R.id.calculator_txt);
        img_advertising = (ImageView) view.findViewById(R.id.img_advertising);
        advertising_txt = (TextView) view.findViewById(R.id.advertising_txt);
        sell_img = (ImageView) view.findViewById(R.id.sell_img);
        sell_txt = (TextView) view.findViewById(R.id.sell_txt);
        apoiment_img = (ImageView) view.findViewById(R.id.apoiment_img);
        apoiment_txt = (TextView) view.findViewById(R.id.apoiment_txt);
        setting_img = (ImageView) view.findViewById(R.id.setting_img);
        payment_method_img = (ImageView) view.findViewById(R.id.payment_method_img);
        payment_txt = (TextView) view.findViewById(R.id.payment_txt);
        complaints_img = (ImageView) view.findViewById(R.id.complaints_img);
        complaints_txt = (TextView) view.findViewById(R.id.complaints_txt);
        calender_img = (ImageView) view.findViewById(R.id.calender_img);
        calender_txt = (TextView) view.findViewById(R.id.calender_txt);
        rate_hama_img = (ImageView) view.findViewById(R.id.rate_hama_img);
        rate_hama_txt = (TextView) view.findViewById(R.id.rate_hama_txt);
        about_hama_img = (ImageView) view.findViewById(R.id.about_hama_img);
        log_out = (TextView) view.findViewById(R.id.log_out);
        about_hama_txt = (TextView) view.findViewById(R.id.about_hama_txt);
        log_out_img = (ImageView) view.findViewById(R.id.log_out_img);
        contact_us = (ImageView) view.findViewById(R.id.contact_us);
        term_txt = (TextView) view.findViewById(R.id.term_txt);
        term = (ImageView) view.findViewById(R.id.term);
        address = (ImageView) view.findViewById(R.id.address);
        contact_us_txt = (TextView) view.findViewById(R.id.contact_us_txt);
        contact_us_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.MyOrder;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new ContactUsFragment());
                fragmentTransaction.commit();

            }
        });

        pakckeging_stores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.profileClick;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new AllStoresPackage());
                fragmentTransaction.commit();
            }
        });
        edit_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("nnnnnnnnnn", "kkkkkkkkkk: " + SharedPreferenceConstant.getSharedPreferenceUserToken(getContext()));
                if (!SharedPreferenceConstant.getSharedPreferenceUserToken(getContext()).isEmpty()) {
                    HomeActivity.position = HomeActivity.profileClick;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, new EditProfile());
                    fragmentTransaction.commit();
                } else {
                    LoginDialog loginDialog = LoginDialog.newInstance();
                    loginDialog.show(getChildFragmentManager(), "login");
                }
            }
        });
        advertising_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.profileClick;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new HamaEventAndAdvertisment());
                fragmentTransaction.commit();

            }
        });
        sell_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.profileClick;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new SellYourProduct());
                fragmentTransaction.commit();
            }
        });
        apoiment_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.profileClick;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new AppointmentBooking());
                fragmentTransaction.commit();
            }
        });

        payment_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SharedPreferenceConstant.getSharedPreferenceUserToken(getContext()).isEmpty()) {

                    HomeActivity.position = HomeActivity.profileClick;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, new PaymentMethod());
                    fragmentTransaction.commit();
                } else {
                    LoginDialog loginDialog = LoginDialog.newInstance();
                    loginDialog.show(getChildFragmentManager(), "login");
                }
            }
        });
        complaints_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!SharedPreferenceConstant.getSharedPreferenceUserToken(getContext()).isEmpty()) {

                    HomeActivity.position = HomeActivity.profileClick;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, new Complaints());
                    fragmentTransaction.commit();
                } else {
                    LoginDialog loginDialog = LoginDialog.newInstance();
                    loginDialog.show(getChildFragmentManager(), "login");
                }
            }
        });
        calender_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!SharedPreferenceConstant.getSharedPreferenceUserToken(getContext()).isEmpty()) {
                    HomeActivity.position = HomeActivity.profileClick;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, new MyEvents());
                    fragmentTransaction.commit();
                } else {
                    LoginDialog loginDialog = LoginDialog.newInstance();
                    loginDialog.show(fragmentManager, "login");
                }
            }
        });
        rate_hama_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                HomeActivity.position = HomeActivity.profileClick;
//                fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.liner1, new RateHama());
//                fragmentTransaction.commit();
                final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        about_hama_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.profileClick;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new AboutHama());
                fragmentTransaction.commit();

            }
        });
        calculator_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.position = HomeActivity.profileClick;
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.liner1, new CalculatorsFragment());
                fragmentTransaction.commit();

            }
        });

        edit_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("nnnnnnnnnn", "kkkkkkkkkk: " + SharedPreferenceConstant.getSharedPreferenceUserToken(getContext()));
                if (!SharedPreferenceConstant.getSharedPreferenceUserToken(getContext()).isEmpty()) {
                    HomeActivity.position = HomeActivity.profileClick;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.liner1, new EditProfile());
                    fragmentTransaction.commit();
                } else {
                    LoginDialog loginDialog = LoginDialog.newInstance();
                    loginDialog.show(getChildFragmentManager(), "login");
                }

            }
        });
        img_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), MarketPriceActivity.class));

            }
        });
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

            if (Locale.getDefault().getLanguage().toString().equals("ar")) {
                Drawable d = ContextCompat.getDrawable(getContext(), R.drawable.back_my_profile_are_dark);
                view_mypro.setBackground(d);
                view_mypro1.setBackground(d);
                view_mypro2.setBackground(d);
                view_mypro3.setBackground(d);
                view_mypro4.setBackground(d);
                view_mypro5.setBackground(d);
                view_mypro6.setBackground(d);
                view_mypro7.setBackground(d);
                view_mypro8.setBackground(d);
                view_mypro9.setBackground(d);
                view_mypro10.setBackground(d);
                view_mypro11.setBackground(d);
                view_mypro12.setBackground(d);
                view13.setBackground(d);
                view14.setBackground(d);
                view17.setBackground(d);
                view19.setBackground(d);


            } else {
                Drawable d = ContextCompat.getDrawable(getContext(), R.drawable.back_view_hama_eng_dark);
                view_mypro.setBackground(d);
                view_mypro1.setBackground(d);
                view_mypro2.setBackground(d);
                view_mypro3.setBackground(d);
                view_mypro4.setBackground(d);
                view_mypro5.setBackground(d);
                view_mypro6.setBackground(d);
                view_mypro7.setBackground(d);
                view_mypro8.setBackground(d);
                view_mypro9.setBackground(d);
                view_mypro10.setBackground(d);
                view_mypro11.setBackground(d);
                view_mypro12.setBackground(d);
                view13.setBackground(d);
                view14.setBackground(d);
                view17.setBackground(d);
                view19.setBackground(d);

            }


        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        try {
            if (!SharedPreferenceConstant.getSharedPreferenceUserToken(getContext()).isEmpty()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        AppDatabase appDatabase = AppDatabase.newInstance(getContext());
                        List<User> userList = appDatabase.userDao().getAll();
                        if (userList != null && !userList.isEmpty()) {
                            final User user = userList.get(0);

                            if (getActivity() != null) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        if (user.getAvatar() != null && !user.getAvatar().trim().isEmpty()) {
                                            Picasso.get().load(user.getAvatar()).placeholder(R.drawable.ic_default_user).error(R.drawable.ic_default_user).transform(new CircleTransform()).into(imageView26);
                                        } else
                                            Picasso.get().load(R.drawable.ic_default_user).placeholder(R.drawable.ic_default_user).error(R.drawable.ic_default_user).into(imageView26);
                                        String fullName = user.getF_name() + " " + user.getL_name();
                                        textView38.setText(fullName);

                                        textView39.setText(user.getEmail());
                                    }
                                });
                            }else
                            {
                                SharedPreferenceConstant.clearSharedPreference(getActivity());
                            }
                        }

                    }
                }).start();
            } else {
                textView38.setText("");
                textView39.setText("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;

    }
}
