package hama.alsaygh.kw.fragment.settings.complaints;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.complaint.AdapterPagerComplints;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.LocalUtils;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class Complaints extends BaseFragment {
    FragmentManager fragmentManager;
    ImageView imageback;
    ViewPager viewPager;
    TabLayout tabLayout;
    LinearLayout parent_copmlaints;
    TextView txt_toolbar;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LocalUtils.getInstance().updateResources(getContext(), LocalUtils.getInstance().getLanguageShort(getContext()));
        View view = inflater.inflate(R.layout.complaints, container, false);

        fragmentManager = getChildFragmentManager();
//
        tabLayout = (TabLayout) view.findViewById(R.id.tab_complaints);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager_complaints);

        AdapterPagerComplints adapterPagerComplints = new AdapterPagerComplints(fragmentManager, tabLayout.getTabCount(), getContext());
        viewPager.setAdapter(adapterPagerComplints);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        imageback = (ImageView) view.findViewById(R.id.img_back);
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();

            }
        });
        parent_copmlaints = (LinearLayout) view.findViewById(R.id.parent_copmlaints);
        txt_toolbar = (TextView) view.findViewById(R.id.txt_toolbar);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        return view;
    }
}
