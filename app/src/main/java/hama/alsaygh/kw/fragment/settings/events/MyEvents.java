package hama.alsaygh.kw.fragment.settings.events;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.adapter.event.AdapterPagerMyEvents;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class MyEvents extends BaseFragment {
    static FragmentManager fragmentManager;
    ImageView img_back;
    ViewPager viewPager;
    TabLayout tabLayout;
    LinearLayout parent_my_event;
    TextView txt_toolbar;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        ((AppCompatActivity)getActivity()).getSupportActionBar().hide(); //<< this
//        SharedPreferences editor = getActivity().getApplicationContext().getSharedPreferences("share", MODE_PRIVATE);
//        if (editor.getBoolean("checked",true))
//            getActivity().setTheme(R.style.darktheme);
//        else
//            getActivity().setTheme(R.style.AppTheme);
        View view = inflater.inflate(R.layout.my_events, container, false);
        fragmentManager = getChildFragmentManager();
        tabLayout = (TabLayout) view.findViewById(R.id.tab_complaints);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager_complaints);
        txt_toolbar = (TextView) view.findViewById(R.id.txt_toolbar);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();

            }
        });
        parent_my_event = (LinearLayout) view.findViewById(R.id.parent_my_event);
        fragmentManager = getChildFragmentManager();
        tabLayout = (TabLayout) view.findViewById(R.id.tab_my_event);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager_my_event);
        AdapterPagerMyEvents adapterPagerMyEvents = new AdapterPagerMyEvents(fragmentManager, tabLayout.getTabCount());
        viewPager.setAdapter(adapterPagerMyEvents);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setTabIndicatorFullWidth(false);
        tabLayout.getTabAt(0).setText(R.string.my_event1);
        tabLayout.getTabAt(1).setText(R.string.add_new_event);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            parent_my_event.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getContext(), R.color.color_navigation));
            tabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.color_store_page), ContextCompat.getColor(getContext(), R.color.color_navigation));
        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                if (null == view) {
                    tab.setCustomView(R.layout.tab_title);
                }
                TextView textView = tab.getCustomView().findViewById(android.R.id.text1);
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                tabLayout.setTabIndicatorFullWidth(false);

                if (SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
                    textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_navigation));
                    tabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.dark111), ContextCompat.getColor(getContext(), R.color.color_navigation));
                } else {
                    textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.blackcolor));
                    tabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.color_store_page), ContextCompat.getColor(getContext(), R.color.blackcolor));
                }


                if (viewPager.getAdapter() != null && tab.getPosition() == 0) {
                    try {
                        ((EventFragment) ((AdapterPagerMyEvents) viewPager.getAdapter()).getItem(0)).getEvents();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                if (null == view) {
                    tab.setCustomView(R.layout.tab_title);
                }
                TextView textView = tab.getCustomView().findViewById(android.R.id.text1);
                textView.setTypeface(Typeface.DEFAULT);
                textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_store_page));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;
    }
}
