package hama.alsaygh.kw.fragment.settings.appSetting;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.Locale;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.utils.BaseFragment;
import hama.alsaygh.kw.utils.LocalUtils;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class SettingLanguage extends BaseFragment {
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    View view11;
    TextView txt_toolbar, textView82, textView22;
    AppCompatImageView img_back;
    LinearLayout parent_setting_lan;
    RadioButton radioButton1, radioButton2,rrr;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.setting_language, container, false);
        fragmentManager = getFragmentManager();
        Log.i("lll", " after ");
        view11 = (View) view.findViewById(R.id.view11);
        radioButton1 = (RadioButton) view.findViewById(R.id.radioButton);
        radioButton2 = (RadioButton) view.findViewById(R.id.radioButton1);
        ConstraintLayout ll_ar=view.findViewById(R.id.ll_ar);
        ConstraintLayout  ll_en=view.findViewById(R.id.ll_en);
        if (Locale.getDefault().getLanguage().equals("ar")) {
            Drawable d = ContextCompat.getDrawable(getContext(), R.drawable.back_view_seeting_ar);
            view11.setBackground(d);
            radioButton2.setChecked(false);
            radioButton1.setChecked(true);
        } else {
            radioButton2.setChecked(true);
            radioButton1.setChecked(false);
        }
        txt_toolbar = (TextView) view.findViewById(R.id.txt_toolbar);
        textView22 = (TextView) view.findViewById(R.id.textView22);
        textView82 = (TextView) view.findViewById(R.id.textView82);
        img_back =  view.findViewById(R.id.iv_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.liner1,new Setting());
//                fragmentTransaction.commit();
                getActivity().onBackPressed();
            }
        });
        parent_setting_lan = (LinearLayout) view.findViewById(R.id.parent_setting_lan);

        if(SharedPreferenceConstant.getSharedPreferenceDarkMode(getContext())) {
            this.getActivity().setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            txt_toolbar.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            parent_setting_lan.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.sign_in));
            radioButton1.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.textviewhome)));
            radioButton1.setHighlightColor(ContextCompat.getColor(getContext(), R.color.textviewhome));

            radioButton2.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.textviewhome)));
            radioButton2.setHighlightColor(ContextCompat.getColor(getContext(), R.color.textviewhome));

            textView82.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            textView22.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteColor));
            img_back.setImageTintList(ContextCompat.getColorStateList(getContext(),R.color.whiteColor));
        } else {
            this.getActivity().setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


       radioButton1.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               LocalUtils.getInstance().setLanguage(v.getContext(),"ar");
               LocalUtils.getInstance().updateResources(getContext(), LocalUtils.getInstance().getLanguageShort(getContext()));

               radioButton1.setChecked(true);
               radioButton2.setChecked(false);
               getActivity().recreate();
           }
       });
        ll_ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalUtils.getInstance().setLanguage(v.getContext(),"ar");
                LocalUtils.getInstance().updateResources(getContext(), LocalUtils.getInstance().getLanguageShort(getContext()));

                radioButton1.setChecked(true);
                radioButton2.setChecked(false);
                getActivity().recreate();
            }
        });
        radioButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalUtils.getInstance().setLanguage(v.getContext(),"en");
                LocalUtils.getInstance().updateResources(getContext(), LocalUtils.getInstance().getLanguageShort(getContext()));

                radioButton1.setChecked(false);
                radioButton2.setChecked(true);
                getActivity().recreate();
            }
        });

        ll_en.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalUtils.getInstance().setLanguage(v.getContext(),"en");
                LocalUtils.getInstance().updateResources(getContext(), LocalUtils.getInstance().getLanguageShort(getContext()));

                radioButton1.setChecked(false);
                radioButton2.setChecked(true);
                getActivity().recreate();
            }
        });

        return view;

    }
}
