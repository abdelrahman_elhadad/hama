package hama.alsaygh.kw.activity.auth;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Locale;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.LoginResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class LoginActivity extends BaseActivity {
    Switch aSwitch;
    Button btnLogin;
    TextView forget, wellcome, signin, use, dont, signup;
    ImageView img_back;
    EditText user_name, edit_password;
    LinearLayout parent;
    ProgressBar pbLogin;

    // String Hawk = Locale.getDefault().getDisplayLanguage();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in);
        forget = (TextView) findViewById(R.id.forget);
        edit_password = (EditText) findViewById(R.id.edit_password);
        signin = (TextView) findViewById(R.id.sign_in);
        use = (TextView) findViewById(R.id.use);
        dont = (TextView) findViewById(R.id.dont);
        signup = (TextView) findViewById(R.id.signup);
        btnLogin = (Button) findViewById(R.id.btn_login);
        user_name = (EditText) findViewById(R.id.editText);
        pbLogin = findViewById(R.id.pb_login);
        parent = (LinearLayout) findViewById(R.id.liner1);

        wellcome = (TextView) findViewById(R.id.well_come);

        img_back = (ImageView) findViewById(R.id.img_back_login);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), OnBoardingActivity.class);
                startActivity(i);

            }
        });

        if (pbLogin.getIndeterminateDrawable() != null) {
            pbLogin.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.whiteColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            wellcome.setTextColor(ContextCompat.getColor(this, R.color.order_tracking1));
            signin.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            use.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));
            dont.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            signup.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            btnLogin.setBackgroundResource(R.drawable.back_button_dark);
            pbLogin.setBackgroundResource(R.drawable.back_button_dark);
            user_name.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            edit_password.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            user_name.setBackgroundResource(R.drawable.back_edittext_dark);
            edit_password.setBackgroundResource(R.drawable.back_edittext_dark);
            parent.setBackgroundResource(R.color.sign_in);
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.sign_in));


        } else {
            setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


    }


    private void setLightStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = getWindow().getDecorView().getSystemUiVisibility(); // get current flag
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;   // add LIGHT_STATUS_BAR to flag
            getWindow().getDecorView().setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.TRANSPARENT); // optional
        }
    }

    public void forgetPassword(View view) {
        Intent i = new Intent(getApplicationContext(), ForgetPassword.class);
        startActivity(i);
    }

    public void SignUpNow(View view) {
        Intent i = new Intent(getApplicationContext(), SignUpStep1Activity.class);
        startActivity(i);
    }

    public void signin(final View view) {


        if (MainApplication.isConnected) {
            if (isValid()) {
                pbLogin.setVisibility(View.VISIBLE);
                btnLogin.setVisibility(View.GONE);
                final String email = user_name.getEditableText().toString().trim();
                final String password = edit_password.getEditableText().toString();
                FirebaseMessaging.getInstance().getToken()
                        .addOnCompleteListener(new OnCompleteListener<String>() {
                            @Override
                            public void onComplete(@NonNull Task<String> task) {
                                if (!task.isSuccessful()) {
                                    return;
                                }

                                // Get new FCM registration token
                             final   String token = task.getResult();

                                Log.d("RequestWrapper", token);
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        final LoginResponse loginResponse = RequestWrapper.getInstance().Login(LoginActivity.this, email, password,token);

                                        if (loginResponse.isStatus()) {
                                            AppDatabase appDatabase = AppDatabase.newInstance(LoginActivity.this);
                                            appDatabase.userDao().deleteAll();
                                            appDatabase.userDao().insertAll(loginResponse.getData().getUser());

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    btnLogin.setVisibility(View.VISIBLE);
                                                    pbLogin.setVisibility(View.GONE);

                                                    SharedPreferenceConstant.setSharedPreferenceUserToken(LoginActivity.this, loginResponse.getData().getToken());

                                                    Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(i);
                                                    finish();
                                                }
                                            });
                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    btnLogin.setVisibility(View.VISIBLE);
                                                    pbLogin.setVisibility(View.GONE);
                                                    Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                                }
                                            });
                                        }


                                    }
                                }).start();
                            }
                        });

            }
        } else
            Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
    }

    public static boolean isRTL(Locale locale) {
        final int directionality = Character.getDirectionality(locale.getDisplayName().charAt(0));
        return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT ||
                directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
    }


    private boolean isValid() {

        boolean isValid = true;


        if (user_name.getEditableText().toString().trim().isEmpty() || !Utils.getInstance().isValidEmail(user_name.getEditableText().toString().trim())) {
            isValid = false;
            user_name.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                user_name.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                user_name.setBackgroundResource(R.drawable.back_ground_edit_text);
        }

        if (edit_password.getEditableText().toString().isEmpty()) {
            isValid = false;
            edit_password.setBackgroundResource(R.drawable.back_edittext_red);
        } else {

            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                edit_password.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edit_password.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        return isValid;
    }

}