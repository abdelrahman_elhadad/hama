package hama.alsaygh.kw.activity.marketPrice;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.adapter.marketPrice.AdapterSliderMarketPrice;
import hama.alsaygh.kw.model.MyorderArray;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class MarketPriceActivity extends BaseActivity {
    ImageView imageback;
    Button orderTracking;
    TabLayout tabLayout;
    TabItem complited, pending, canceled;
    ViewPager viewPager;
    ArrayList<MyorderArray> myordersArray;
    TextView txt_toolbar;
    LinearLayout parent_market_price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_price);
        tabLayout = (TabLayout) findViewById(R.id.tab_market_price);
        viewPager = (ViewPager) findViewById(R.id.view_pager_market_price);

        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        imageback = (ImageView) findViewById(R.id.back_myorder);
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        tabLayout.setTabIndicatorFullWidth(false);
        tabLayout.getTabAt(0).setText(R.string.Gold_Market);
//        tabLayout.getTabAt(1).setText(R.string.silver_market);

        AdapterSliderMarketPrice adapterSliderMarketPrice = new AdapterSliderMarketPrice(this,getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapterSliderMarketPrice);
        txt_toolbar = (TextView) findViewById(R.id.txt_toolbar);
        parent_market_price = (LinearLayout) findViewById(R.id.parent_market_price);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            this.setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            txt_toolbar.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            parent_market_price.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
            tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.color_navigation));
            tabLayout.setTabTextColors(ContextCompat.getColor(this, R.color.color_store_page), ContextCompat.getColor(this, R.color.color_navigation));
        } else {
            this.setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                if (null == view) {
                    tab.setCustomView(R.layout.tab_title);
                }
                TextView textView = tab.getCustomView().findViewById(android.R.id.text1);
                //  textView.setTextColor(tabLayout.getTabTextColors());
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                tabLayout.setTabIndicatorFullWidth(false);

                SharedPreferences editor = MarketPriceActivity.this.getSharedPreferences("share", MODE_PRIVATE);
                if (editor.getBoolean("checked", false)) {
                    textView.setTextColor(ContextCompat.getColor(MarketPriceActivity.this, R.color.color_navigation));
                    tabLayout.setTabTextColors(ContextCompat.getColor(MarketPriceActivity.this, R.color.dark111), ContextCompat.getColor(MarketPriceActivity.this, R.color.color_navigation));
                } else {
                    textView.setTextColor(ContextCompat.getColor(MarketPriceActivity.this, R.color.blackcolor));
                    tabLayout.setTabTextColors(ContextCompat.getColor(MarketPriceActivity.this, R.color.color_store_page), ContextCompat.getColor(MarketPriceActivity.this, R.color.blackcolor));
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                if (null == view) {
                    tab.setCustomView(R.layout.tab_title);
                }
                TextView textView = tab.getCustomView().findViewById(android.R.id.text1);
                textView.setTypeface(Typeface.DEFAULT);
                textView.setTextColor(ContextCompat.getColor(MarketPriceActivity.this, R.color.color_store_page));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
}