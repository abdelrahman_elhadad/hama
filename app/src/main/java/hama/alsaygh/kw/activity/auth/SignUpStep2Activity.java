package hama.alsaygh.kw.activity.auth;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.CountriesResponse;
import hama.alsaygh.kw.api.responce.LoginResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.fragment.settings.appSetting.Setting;
import hama.alsaygh.kw.model.country.Country;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class SignUpStep2Activity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    ImageView imageView, back_signup;
    private static final String TAG = "SignUp";
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private TextView tvBirthDay;

    Button button_sign_up;
    private Spinner sexSpinner, countrySpinner;
    String[] arrayForSpinner;
    private LinearLayout ll_hint_spinner;
    private LinearLayout ll_hint_spinner_country;
    TextView well_come_signup, sign_up, gender, country, dont_signup, sign_up_now;
    LinearLayout parent;
    private Country selectedCountry;
    private String email;
    private String selectedGender;
    private ProgressBar pbSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);
        sexSpinner = (Spinner) findViewById(R.id.editText);
        countrySpinner = (Spinner) findViewById(R.id.editText6);
        tvBirthDay = (TextView) findViewById(R.id.textView2);
        imageView = (ImageView) findViewById(R.id.pickerView);
        ll_hint_spinner = findViewById(R.id.ll_hint_spinner);
        back_signup = (ImageView) findViewById(R.id.back_signup);
        ll_hint_spinner_country = (LinearLayout) findViewById(R.id.ll_hint_spinner_country);

        well_come_signup = (TextView) findViewById(R.id.well_come_signup);
        sign_up = (TextView) findViewById(R.id.sign_up);
        gender = (TextView) findViewById(R.id.gender);
        country = (TextView) findViewById(R.id.country);
        button_sign_up = (Button) findViewById(R.id.button_sign_up);
        dont_signup = (TextView) findViewById(R.id.dont_signup);
        sign_up_now = (TextView) findViewById(R.id.sign_up_now);
        parent = (LinearLayout) findViewById(R.id.parent_sign_up);
        pbSignUp = findViewById(R.id.pb_sign_up);

        if (pbSignUp.getIndeterminateDrawable() != null) {
            pbSignUp.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.whiteColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        arrayForSpinner = new String[]{"", getString(R.string.male), getString(R.string.female), getString(R.string.other)};

        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.whiteColor));
        tvBirthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(SignUpStep2Activity.this, android.R.style.Theme_Holo_Dialog_MinWidth
                        , mDateSetListener, year, month, day);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });


        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                Log.d(TAG, "onDateset : mm/dd/yyy :" + month + "/" + dayOfMonth + "/" + year);
                String date = dayOfMonth + "-" + month + "-" + year;
                tvBirthDay.setText(date);

            }
        };


        if (getIntent() != null) {
            email = getIntent().getStringExtra("email");
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayForSpinner);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sexSpinner.setAdapter(arrayAdapter);
        sexSpinner.setOnItemSelectedListener(this);

        back_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SignUpStep1Activity.class);
                startActivity(i);
            }
        });


        ll_hint_spinner_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySpinner.performClick();
                ll_hint_spinner_country.setVisibility(View.INVISIBLE);
                ll_hint_spinner_country.setEnabled(false);
                countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Country country = (Country) countrySpinner.getSelectedItem();
                        if (country.getId() == -1) {
                            ll_hint_spinner_country.setVisibility(View.VISIBLE);
                            selectedCountry = null;
                        } else {
                            selectedCountry = (Country) countrySpinner.getSelectedItem();
                            ll_hint_spinner_country.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        ll_hint_spinner_country.setVisibility(View.VISIBLE);
                    }
                });

            }
        });

        if(SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

            back_signup.setImageResource(R.drawable.ic_back_icon_dark);
            well_come_signup.setTextColor(ContextCompat.getColor(this, R.color.order_tracking1));
            sign_up.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            gender.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            tvBirthDay.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            country.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            sexSpinner.setBackgroundResource(R.drawable.back_edittext_dark);
            countrySpinner.setBackgroundResource(R.drawable.back_edittext_dark);
            tvBirthDay.setBackgroundResource(R.drawable.back_edittext_dark);
            button_sign_up.setBackgroundResource(R.drawable.back_button_dark);
            pbSignUp.setBackgroundResource(R.drawable.back_button_dark);
            dont_signup.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            sign_up_now.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            parent.setBackgroundResource(R.color.sign_in);
            window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.sign_in));


        } else {
            setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        new Thread(new Runnable() {
            @Override
            public void run() {

                final CountriesResponse countriesResponse = RequestWrapper.getInstance().getCountry(SignUpStep2Activity.this);
                if (countriesResponse.isStatus()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            List<Country> countryList = new ArrayList<>(countriesResponse.getData());
                            Country country = new Country();
                            country.setId(-1);
                            country.setName("");
                            countryList.add(0, country);

                            ArrayAdapter<Country> arrayCountry = new ArrayAdapter<Country>(SignUpStep2Activity.this, android.R.layout.simple_spinner_dropdown_item, countryList);
                            arrayCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            countrySpinner.setAdapter(arrayCountry);
                        }
                    });
                }
            }
        }).start();

    }

    public void onClick(View view) {
        signupNow(view);
    }

    public void RestartApp() {
        Intent i = new Intent(getApplicationContext(), Setting.class);
        startActivity(i);
        finish();
    }

    private void setLightStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = getWindow().getDecorView().getSystemUiVisibility(); // get current flag
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;   // add LIGHT_STATUS_BAR to flag
            getWindow().getDecorView().setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.TRANSPARENT); // optional
        }
    }

    public void signupNow(final View view) {

        if (MainApplication.isConnected) {

            if (isValid()) {

                button_sign_up.setVisibility(View.GONE);
                pbSignUp.setVisibility(View.VISIBLE);

                final String birthDay = tvBirthDay.getText().toString();
                FirebaseMessaging.getInstance().getToken()
                        .addOnCompleteListener(new OnCompleteListener<String>() {
                            @Override
                            public void onComplete(@NonNull Task<String> task) {
                                if (!task.isSuccessful()) {
                                    Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                                    return;
                                }

                                // Get new FCM registration token
                                String token = task.getResult();
                                new Thread(new Runnable() {

                                    @Override
                                    public void run() {

                                        final LoginResponse loginResponse = RequestWrapper.getInstance().registerUserStep2(SignUpStep2Activity.this, selectedGender, birthDay, selectedCountry.getId(), email,token);

                                        if (loginResponse.isStatus()) {
                                            AppDatabase appDatabase = AppDatabase.newInstance(SignUpStep2Activity.this);
                                            appDatabase.userDao().update(loginResponse.getData().getUser());

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    button_sign_up.setVisibility(View.VISIBLE);
                                                    pbSignUp.setVisibility(View.GONE);

                                                    SharedPreferenceConstant.setSharedPreferenceUserToken(SignUpStep2Activity.this, loginResponse.getData().getToken());

                                                    Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(i);
                                                }
                                            });
                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    button_sign_up.setVisibility(View.VISIBLE);
                                                    pbSignUp.setVisibility(View.GONE);
                                                    Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                                }
                                            });
                                        }

                                    }
                                }).start();

                                }
                        });

            }

        } else {
            Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
        }


    }

    private boolean isValid() {

        boolean isValid = true;


        if (selectedGender == null || selectedGender.isEmpty()) {
            isValid = false;
            sexSpinner.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                sexSpinner.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                sexSpinner.setBackgroundResource(R.drawable.back_ground_edit_text);

        }


        if (selectedCountry == null) {
            isValid = false;
            countrySpinner.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                countrySpinner.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                countrySpinner.setBackgroundResource(R.drawable.back_ground_edit_text);
        }

        return isValid;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (position) {
            case 0:
                selectedGender = "";
                setLinearVisibility(true);
                break;
            case 1:
                selectedGender = "male";
                setLinearVisibility(false);
                break;

            case 2:
                selectedGender = "female";
                setLinearVisibility(false);
                break;

            case 3:
                selectedGender = "other";
                setLinearVisibility(false);
                break;
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {


    }

    public void setLinearVisibility(boolean visible) {
        if (visible) {
            ll_hint_spinner.setVisibility(View.VISIBLE);
        } else {
            ll_hint_spinner.setVisibility(View.INVISIBLE);
        }
    }

}