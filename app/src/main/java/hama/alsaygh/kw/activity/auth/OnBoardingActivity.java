package hama.alsaygh.kw.activity.auth;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rd.PageIndicatorView;

import java.util.List;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.activity.mainActivity.HomeActivity;
import hama.alsaygh.kw.adapter.onBoard.OnBoardSliderAdapter;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.OnBoardResponse;
import hama.alsaygh.kw.circleIndicator.CustomCircleIndicator;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.db.table.OnBoard;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class OnBoardingActivity extends BaseActivity {
    private ViewPager viewPager;
    private LinearLayout linearLayout;
    PageIndicatorView pageIndicatorView;
    private int MCurentPage;
    Button button;
    TextView textView;
    CustomCircleIndicator indicator;
    OnBoardSliderAdapter sliderAdapter;
    ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        setContentView(R.layout.home);
        animate(linearLayout);
        constraintLayout = (ConstraintLayout) findViewById(R.id.container_of_home);
        textView = (TextView) findViewById(R.id.button2);
        indicator = (CustomCircleIndicator) findViewById(R.id.pageIndicatorView);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        TextView button2 = findViewById(R.id.button2);
        TextView btn_login = findViewById(R.id.btn_login);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {

            constraintLayout.setBackgroundResource(R.drawable.back_dark_home);

            indicator.setSelectedResource(R.drawable.ic_indecator_derk);
            indicator.setUnSelectedResource(R.drawable.ic_indecator_dark_unselected);
            button2.setBackgroundResource(R.drawable.back_text_home_dark);
            btn_login.setBackgroundResource(R.drawable.back_text_home_dark);
            btn_login.setTextColor(ContextCompat.getColor(this, R.color.order_tracking1));
            button2.setTextColor(ContextCompat.getColor(this, R.color.order_tracking1));
        }


//        pageIndicatorView = findViewById(R.id.pageIndicatorView);
//        pageIndicatorView.setClickListener(new DrawController.ClickListener() {
//            @Override
//            public void onIndicatorClicked(int position) {
//
//                pageIndicatorView.setSelection(position);
//                pageIndicatorView.setAnimationType(AnimationType.WORM);
//                viewPager.setCurrentItem(position);
//            }
//        });
        // CircleIndicator indicator = findViewById(R.id.pageIndicatorView);
        // indicator.createIndicators(3,0);


        ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                pageIndicatorView.setSelection(position);
//                pageIndicatorView.setAnimationType(AnimationType.WORM);
            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        };
        viewPager.addOnPageChangeListener(viewListener);


        new Thread(new Runnable() {
            @Override
            public void run() {

                AppDatabase appDatabase = AppDatabase.newInstance(OnBoardingActivity.this);
                final List<OnBoard> onBoards = appDatabase.onBoardDao().getAll();
                if (onBoards != null && !onBoards.isEmpty()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            sliderAdapter = new OnBoardSliderAdapter(getLayoutInflater(), OnBoardingActivity.this, onBoards);
                            viewPager.setAdapter(sliderAdapter);
                            indicator.setViewPager(viewPager);
                        }
                    });

                } else {

                    final OnBoardResponse onBoardResponse = RequestWrapper.getInstance().getOnBoard(OnBoardingActivity.this);
                    if (onBoardResponse.isStatus()) {

                        appDatabase.onBoardDao().deleteAll();
                        for (OnBoard onBoard : onBoardResponse.getData()) {
                            appDatabase.onBoardDao().insertAll(onBoard);
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                sliderAdapter = new OnBoardSliderAdapter(getLayoutInflater(), OnBoardingActivity.this, onBoardResponse.getData());
                                viewPager.setAdapter(sliderAdapter);
                                indicator.setViewPager(viewPager);
                            }
                        });

                    } else {

                    }
                }

            }
        }).start();


    }

    public void animate(View view) {
        ConstraintLayout dialog = (ConstraintLayout) findViewById(R.id.lineranim);
        dialog.setVisibility(LinearLayout.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_home);
        animation.setDuration(2000);
        dialog.setAnimation(animation);
        dialog.animate();
        animation.start();

    }


//    public void animate2(View view) {
//        LinearLayout dialog = (LinearLayout) findViewById(R.id.lineranim);
//        dialog.setVisibility(LinearLayout.VISIBLE);
//        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_home);
//        animation.setDuration(2000);
//        dialog.setAnimation(animation);
//        dialog.animate();
//        animation.start();
//
//    }


//    public void onClick(View view) {
//        Intent intent = new Intent(getApplicationContext(),SignUp.class);
//        startActivity(intent);
//    }

    public void login(View view) {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void visitor(View view) {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
        finish();
    }

}