package hama.alsaygh.kw.activity.auth;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.OnBoardResponse;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.db.table.OnBoard;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class SplashActivity extends BaseActivity {
    SeekBar seekBar;
    private static final String TAG = "MainActivity";
    ConstraintLayout constrain_parent;

    private ValueAnimator anim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        constrain_parent = findViewById(R.id.constrain_parent);
        seekBar = findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d(TAG, "onProgressChanged:" + progress);
                if (progress == 100) {
                    Intent intent = new Intent(getApplicationContext(), OnBoardingActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        new Thread(() -> {

            final OnBoardResponse onBoardResponse = RequestWrapper.getInstance().getOnBoard(SplashActivity.this);
            if (onBoardResponse.isStatus()) {
                AppDatabase appDatabase = AppDatabase.newInstance(SplashActivity.this);
                appDatabase.onBoardDao().deleteAll();
                for (OnBoard onBoard : onBoardResponse.getData()) {
                    appDatabase.onBoardDao().insertAll(onBoard);
                }
            }
        }).start();

        new Thread(() -> RequestWrapper.getInstance().getStoreProductSort(SplashActivity.this)).start();

        anim = ValueAnimator.ofInt(0, seekBar.getMax());
        anim.setDuration(1500);
        anim.addUpdateListener(animation -> {
            int animProgress = (Integer) animation.getAnimatedValue();
            seekBar.setProgress(animProgress);
        });

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            seekBar.setProgressDrawable(ContextCompat.getDrawable(this, R.drawable.back_seekbar_darkmode));
            constrain_parent.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            // constrain_parent.setBackgroundColor(R.color.sign_in);

        } else {
            setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        startAnimation();

    }

    private void startAnimation() {
        if (anim != null)
            anim.start();
    }
}
