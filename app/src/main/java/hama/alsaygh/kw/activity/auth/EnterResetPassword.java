package hama.alsaygh.kw.activity.auth;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import hama.alsaygh.kw.R;

import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.GeneralResponse;
import hama.alsaygh.kw.api.responce.LoginResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

public class EnterResetPassword extends BaseActivity {
    ImageView back_reset;
    TextView timer, textView1, textView8;
    int time = 59;
    PinEntryEditText pinEntryEditText;
    Button button;
    View view;
    LinearLayout parent_enter_reset_pass;

    private String email;
    private ProgressBar pb_reset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_reset_password);

        if (getIntent() != null)
            email = getIntent().getStringExtra("email");

        pb_reset = findViewById(R.id.pb_reset);
        back_reset = (ImageView) findViewById(R.id.back_reset);
        back_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ForgetPassword.class);
                startActivity(i);
            }
        });
        timer = (TextView) findViewById(R.id.timer);
        new CountDownTimer(30000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                timer.setText("0:" + checkDigit(time));
                time--;
            }

            @Override
            public void onFinish() {

            }
        }.start();
        textView1 = (TextView) findViewById(R.id.textView1);
        pinEntryEditText = (PinEntryEditText) findViewById(R.id.txt_pin_entry);
        button = (Button) findViewById(R.id.button3);
        textView8 = (TextView) findViewById(R.id.textView8);
        parent_enter_reset_pass = (LinearLayout) findViewById(R.id.parent_enter_reset_pass);
        view = (View) findViewById(R.id.view);

         if (pb_reset.getIndeterminateDrawable() != null) {
            pb_reset.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.whiteColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)
        ) {
            setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            textView1.setTextColor(ContextCompat.getColor(this, R.color.order_tracking1));

            back_reset.setImageResource(R.drawable.ic_back_icon_dark);
            //  pinEntryEditText.setPinBackground(getResources().getDrawable(R.drawable.back_edittext_dark));
            pinEntryEditText.setPinBackground(ContextCompat.getDrawable(this, R.drawable.back_pin_dark));

            button.setBackgroundResource(R.drawable.back_button_dark);
            timer.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            textView8.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            parent_enter_reset_pass.setBackgroundResource(R.color.sign_in);
            view.setBackgroundResource(R.color.sign_in_dark);
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.sign_in));


        } else {
            setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

    }

    public void reset(final View view) {

        if (MainApplication.isConnected) {
            if (isValid()) {

                final String code = pinEntryEditText.getEditableText().toString();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final LoginResponse loginResponse = RequestWrapper.getInstance().verifyCode(EnterResetPassword.this, email, code);

                        if (loginResponse.isStatus()) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    button.setVisibility(View.VISIBLE);
                                    pb_reset.setVisibility(View.GONE);

                                    Intent i = new Intent(getApplicationContext(), EnterNewPassword.class);
                                    i.putExtra("token", loginResponse.getData().getToken());
                                    startActivity(i);
                                }
                            });
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    button.setVisibility(View.VISIBLE);
                                    pb_reset.setVisibility(View.GONE);
                                    Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });
                        }


                    }
                }).start();
            }
        } else
            Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();

    }

    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    public void ResetCode(final View view) {

        if (MainApplication.isConnected) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final GeneralResponse loginResponse = RequestWrapper.getInstance().forgetPassword(EnterResetPassword.this, email);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                        }
                    });

                }
            }).start();
        } else
            Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();

    }

    private boolean isValid() {
        boolean isValid = true;

        if (pinEntryEditText.getEditableText().toString().isEmpty() || pinEntryEditText.getEditableText().toString().length() < 6) {

            isValid = false;

            pinEntryEditText.setPinBackground(ContextCompat.getDrawable(this, R.drawable.back_edittext_red));

        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                pinEntryEditText.setPinBackground(ContextCompat.getDrawable(this, R.drawable.back_pin_dark));
            } else {
                pinEntryEditText.setPinBackground(ContextCompat.getDrawable(this, R.drawable.back_ground_reset_code));
            }
        }

        return isValid;
    }

}
