package hama.alsaygh.kw.activity.checkout;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.OrderPaymentResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.dialog.CheckOutConfirmPayment;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class PayOrderActivity extends BaseActivity {
    ImageView img_back;
    RadioButton KNET, visa;
    Button button9, button8;
    TextView txt_toolbar;
    LinearLayout parent_a, liner_btn;
    ProgressBar pb_send;
    String order_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_order);

        pb_send = findViewById(R.id.pb_send);
        visa = findViewById(R.id.visa);
        KNET = findViewById(R.id.KNET);
        img_back = (ImageView) findViewById(R.id.img_back);
        txt_toolbar = (TextView) findViewById(R.id.txt_toolbar);
        parent_a = (LinearLayout) findViewById(R.id.parent_a);
        liner_btn = (LinearLayout) findViewById(R.id.liner_btn);
        visa = findViewById(R.id.visa);
        KNET = findViewById(R.id.KNET);
        img_back = (ImageView) findViewById(R.id.img_back);
        txt_toolbar = (TextView) findViewById(R.id.txt_toolbar);
        parent_a = (LinearLayout) findViewById(R.id.parent_a);
        liner_btn = (LinearLayout) findViewById(R.id.liner_btn);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);

        if (getIntent() != null)
            order_id = getIntent().getStringExtra("order_id");


        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (MainApplication.isConnected) {

                    pb_send.setVisibility(View.VISIBLE);
                    button9.setVisibility(View.GONE);

                    int payment_method=1;
                    if (visa.isChecked())
                        payment_method= 2;
                    else if (KNET.isChecked())
                        payment_method= 1;

                    final int finalPayment_method = payment_method;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {


                            final OrderPaymentResponse generalResponse = RequestWrapper.getInstance().getOrderPayment(PayOrderActivity.this, order_id, finalPayment_method);
                            if (generalResponse.isStatus()) {
                                if (generalResponse.getData().getPayment_url() != null && !generalResponse.getData().getPayment_url().trim().isEmpty()) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            pb_send.setVisibility(View.GONE);
                                            button9.setVisibility(View.VISIBLE);

                                            Intent intent = new Intent(PayOrderActivity.this, WebViewPaymentActivity.class);
                                            intent.putExtra("url", generalResponse.getData().getPayment_url());
                                            startActivity(intent);
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            pb_send.setVisibility(View.GONE);
                                            button9.setVisibility(View.VISIBLE);

                                            CheckOutConfirmPayment checkOutConfirmPayment = new CheckOutConfirmPayment();
                                            checkOutConfirmPayment.show(getSupportFragmentManager(), "checkOutConfirmPayment");
                                        }
                                    });
                                }
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pb_send.setVisibility(View.GONE);
                                        button9.setVisibility(View.VISIBLE);

                                        if (generalResponse.getCode().equalsIgnoreCase("401")) {
                                            LoginDialog loginDialog = LoginDialog.newInstance();
                                            loginDialog.show(getSupportFragmentManager(), "login");
                                        } else
                                            Snackbar.make(v, generalResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        }
                    }).start();

                } else
                    Snackbar.make(v, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();

            }
        });

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            this.setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

            txt_toolbar.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            parent_a.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            liner_btn.setBackgroundColor(ContextCompat.getColor(this, R.color.dark11));
            visa.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));
            KNET.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));


            KNET.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_navigation)));
            KNET.setHighlightColor(ContextCompat.getColor(this, R.color.color_navigation));

            visa.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_navigation)));
            visa.setHighlightColor(ContextCompat.getColor(this, R.color.color_navigation));

        } else {
            this.setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

    }
}