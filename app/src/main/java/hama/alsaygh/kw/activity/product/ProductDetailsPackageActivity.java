package hama.alsaygh.kw.activity.product;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.adapter.imageSlider.SliderAdapter;
import hama.alsaygh.kw.adapter.imageSlider.SliderAdapterDark;
import hama.alsaygh.kw.adapter.review.AdapterProductDetailsReview;
import hama.alsaygh.kw.adapter.review.AdapterProductDetailsReviewDark;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.CartResponse;
import hama.alsaygh.kw.api.responce.GeneralResponse;
import hama.alsaygh.kw.api.responce.ProductResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.model.product.review.Review;
import hama.alsaygh.kw.utils.Cons;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;
import me.relex.circleindicator.CircleIndicator;

public class ProductDetailsPackageActivity extends BaseActivity {

    ViewPager viewPager;
    Button button;
    ImageView imageback, share_img;
    TextView text_toolbar_home, tv, tv2, tv3, tv111, lebel, review, write, textView3, textView4;
    SliderAdapter sliderAdapter;
    SliderAdapterDark sliderAdapterDark;
    RecyclerView recycle_prouductdetails;

    AdapterProductDetailsReview adapterProductDetailsReview;
    AdapterProductDetailsReviewDark adapterProductDetailsReviewDark;
    ArrayList<Review> Reviews;
    CircleIndicator indicator;
    RelativeLayout parent_prouduct_details;
    LinearLayout liner_prouduct_details;
    ImageView iv_like;
    Product product;
    int product_id, store_id;

    Skeleton skeleton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details_package);
        if (getIntent() != null) {
            product_id = getIntent().getIntExtra(Cons.PRODUCT_ID, 0);
            store_id = getIntent().getIntExtra(Cons.STORE_ID, 0);
            product = (Product) getIntent().getSerializableExtra(Cons.PRODUCT);
        }


        imageback = findViewById(R.id.imageView16);
        iv_like = findViewById(R.id.iv_like);
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        viewPager = findViewById(R.id.viewPager11);

        indicator = findViewById(R.id.pageIndicatorView1);


        recycle_prouductdetails = findViewById(R.id.rv_proudect_details);

        Reviews = new ArrayList<>();
        adapterProductDetailsReview = new AdapterProductDetailsReview(Reviews);
        recycle_prouductdetails.setAdapter(adapterProductDetailsReview);


        text_toolbar_home = findViewById(R.id.textView19);
        tv = findViewById(R.id.tv);

        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv111 = findViewById(R.id.tv111);
        lebel = findViewById(R.id.lebel);
        review = findViewById(R.id.review);
        write = findViewById(R.id.write);
        button = findViewById(R.id.button3);
        textView3 = findViewById(R.id.textView3);
        textView4 = findViewById(R.id.textView4);
        share_img = findViewById(R.id.imageView71);
        parent_prouduct_details = findViewById(R.id.parent_prouduct_details);
        sliderAdapterDark = new SliderAdapterDark(this, getLayoutInflater());
        adapterProductDetailsReviewDark = new AdapterProductDetailsReviewDark(Reviews);
        liner_prouduct_details = findViewById(R.id.liner_prouduct_details);
        skeleton = SkeletonLayoutUtils.createSkeleton(parent_prouduct_details);
        Utils.getInstance().setSkeletonMaskAndShimmer(this, skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            Utils.getInstance().setSkeletonMaskAndShimmerDark(this, skeleton);
            text_toolbar_home.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
            tv.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            tv2.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            tv3.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            tv111.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            lebel.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            write.setTextColor(ContextCompat.getColor(this, R.color.order_tracking1));
            button.setBackgroundResource(R.drawable.back_button_dark);
            textView3.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            textView4.setTextColor(ContextCompat.getColor(this, R.color.textviewhome));
            share_img.setImageResource(R.drawable.ic_share_dark);
            parent_prouduct_details.setBackgroundResource(R.color.sign_in);
            review.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            viewPager.setAdapter(sliderAdapterDark);
            recycle_prouductdetails.setAdapter(adapterProductDetailsReviewDark);
            liner_prouduct_details.setBackgroundResource(R.color.dark11);


        }

        setProductDetails();
        if (product_id != 0 || product != null)
            if (MainApplication.isConnected) {
                skeleton.showSkeleton();
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        final ProductResponse productResponse = RequestWrapper.getInstance().getProductDetailsPackage(ProductDetailsPackageActivity.this, store_id, product_id);
                        if (productResponse.isStatus()) {
                            product = productResponse.getData();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    setProductDetails();
                                }
                            });
                        } else {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    Snackbar.make(iv_like, productResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }).start();

            } else {
                Snackbar.make(recycle_prouductdetails, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
            }
        iv_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (product != null)
                    if (MainApplication.isConnected) {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                final GeneralResponse productResponse = RequestWrapper.getInstance().getProductLikeUnlike(ProductDetailsPackageActivity.this, product.getId());
                                if (productResponse.isStatus()) {

                                    product.setI_fav(!product.isI_fav());

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            setLikeUnlike();
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Snackbar.make(iv_like, productResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        }).start();
                    } else {
                        Snackbar.make(v, v.getContext().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                    }


            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (product != null)
                    if (MainApplication.isConnected) {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {


                                final CartResponse cartResponse = RequestWrapper.getInstance().addToCart(ProductDetailsPackageActivity.this, product.getId(), 1);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Snackbar.make(v, cartResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                    }
                                });

                            }
                        }).start();

                    } else {
                        Snackbar.make(v, v.getContext().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                    }
            }
        });
    }

    private void setProductDetails() {
        if (product != null) {
            tv.setText(product.getName());
            tv3.setText(product.getFixed_price() + " "+getString(R.string.currency));
            lebel.setText(product.getDescription());
            text_toolbar_home.setText(product.getName());
            textView4.setText(product.getPrice() + " "+getString(R.string.currency));


            setLikeUnlike();

            sliderAdapter = new SliderAdapter(this, getLayoutInflater(),getSupportFragmentManager(), product.getMedia());
            viewPager.setAdapter(sliderAdapter);
            viewPager.addOnPageChangeListener(viewListener);
            indicator.setViewPager(viewPager);
        }

    }

    private void setLikeUnlike() {
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            if (product.isI_fav()) {
                Drawable drawable = AppCompatResources.getDrawable(this, R.drawable.ic_love_cart_dark);
                iv_like.setImageDrawable(drawable);
            } else {
                Drawable drawable = AppCompatResources.getDrawable(this, R.drawable.ic_loove_dark);
                iv_like.setImageDrawable(drawable);
            }
        } else {
            if (product.isI_fav()) {
                Drawable drawable = AppCompatResources.getDrawable(this, R.drawable.ic_loveeecolor);
                iv_like.setImageDrawable(drawable);
            } else {
                Drawable drawable = AppCompatResources.getDrawable(this, R.drawable.ic_loveee);
                iv_like.setImageDrawable(drawable);
            }
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
//            pageIndicatorView.setSelection(position);
//            pageIndicatorView.setAnimationType(AnimationType.WORM);

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}