package hama.alsaygh.kw.activity;

import android.os.Bundle;

import hama.alsaygh.kw.R;

import hama.alsaygh.kw.activity.base.BaseActivity;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class Gold extends BaseActivity {
    RecyclerView recyclerView , recyclerView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gold);
        recyclerView = (RecyclerView)findViewById(R.id.home_rf);
        recyclerView1 = (RecyclerView)findViewById(R.id.home_rf1);
        recyclerView1.setLayoutManager(new GridLayoutManager(this,2));
    }
    }
