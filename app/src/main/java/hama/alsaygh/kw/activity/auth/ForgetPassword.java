package hama.alsaygh.kw.activity.auth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import hama.alsaygh.kw.R;

import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.GeneralResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

public class ForgetPassword extends BaseActivity {
    ImageView back_forget;
    TextView back_to_login, forget_pass, dont, textView5, textView3;
    LinearLayout parent_forget;
    EditText edit_email_mobile;
    Button button3;
ProgressBar pb_reset;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_password);
        getSupportActionBar().hide();
        pb_reset=findViewById(R.id.pb_reset);
        back_forget = (ImageView) findViewById(R.id.back_forget);
        back_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
            }
        });
        back_to_login = (TextView) findViewById(R.id.back_to_login);
        back_to_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        parent_forget = (LinearLayout) findViewById(R.id.parent_forget);
        forget_pass = (TextView) findViewById(R.id.forget_pass);
        dont = (TextView) findViewById(R.id.dont);
        edit_email_mobile = (EditText) findViewById(R.id.edit_email_mobile);
        textView5 = (TextView) findViewById(R.id.textView5);
        button3 = (Button) findViewById(R.id.button3);
        textView3 = (TextView) findViewById(R.id.textView3);

          if (pb_reset.getIndeterminateDrawable() != null) {
            pb_reset.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.whiteColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            forget_pass.setTextColor(ContextCompat.getColor(this, R.color.order_tracking1));
            dont.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            parent_forget.setBackgroundResource(R.color.sign_in);
            back_forget.setImageResource(R.drawable.ic_back_icon_dark);
            edit_email_mobile.setBackgroundResource(R.drawable.back_edittext_dark);
            textView5.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));
            button3.setBackgroundResource(R.drawable.back_button_dark);
           pb_reset.setBackgroundResource(R.drawable.back_button_dark);
            textView3.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            back_to_login.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.sign_in));


        } else {
            setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }

    public void reset(final View view) {

        if (MainApplication.isConnected) {
            if (isValid()) {
                pb_reset.setVisibility(View.VISIBLE);
                button3.setVisibility(View.GONE);
                final String email = edit_email_mobile.getEditableText().toString();
               new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final GeneralResponse loginResponse = RequestWrapper.getInstance().forgetPassword(ForgetPassword.this, email);

                        if (loginResponse.isStatus()) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    button3.setVisibility(View.VISIBLE);
                                    pb_reset.setVisibility(View.GONE);

                                    Intent i = new Intent(getApplicationContext(), EnterResetPassword.class);
                                   i.putExtra("email",email);
                                    startActivity(i);
                                }
                            });
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    button3.setVisibility(View.VISIBLE);
                                    pb_reset.setVisibility(View.GONE);
                                    Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });
                        }


                    }
                }).start();
            }
        } else
            Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();

    }

    public void backToLogin(View view) {

        onBackPressed();

    }

    public void useMobileOrEmail(View view) {
    }

    private boolean isValid() {
        boolean isValid = true;

        if (edit_email_mobile.getEditableText().toString().isEmpty()) {
            isValid = false;
            edit_email_mobile.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                edit_email_mobile.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edit_email_mobile.setBackgroundResource(R.drawable.back_ground_edit_text);
        }

        return isValid;
    }
}
