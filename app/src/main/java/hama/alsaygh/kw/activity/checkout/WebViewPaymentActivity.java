package hama.alsaygh.kw.activity.checkout;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.dialog.CheckOutConfirmPayment;

public class WebViewPaymentActivity extends BaseActivity {
    String url = "";
    private ProgressBar pbWeb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_payment);
        WebView mWebView = findViewById(R.id.activity_main_webview);
        pbWeb = findViewById(R.id.pb_web);
        ImageView img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        if (getIntent() != null) {
            url = getIntent().getStringExtra("url");
        }

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        mWebView.loadUrl(url);
        mWebView.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

                final Uri uri = request.getUrl();
                Log.i("PaymentMethodView", "shouldOverrideUrlLoadinUri :" + uri.getPath());

                if (uri.getPath().contains("payment/pending") || uri.getPath().contains("payment/success")) {

                    CheckOutConfirmPayment checkOutConfirmPayment = new CheckOutConfirmPayment();
                    checkOutConfirmPayment.show(getSupportFragmentManager(), "checkOutConfirmPayment");

                }

                pbWeb.setVisibility(View.GONE);
                return false;
            }

            public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
                Log.i("PaymentMethodView", "shouldOverrideUrlLoading :" + url);

                if (url.contains("payment/pending") || url.contains("payment/success")) {

                    CheckOutConfirmPayment checkOutConfirmPayment = new CheckOutConfirmPayment();
                    checkOutConfirmPayment.show(getSupportFragmentManager(), "checkOutConfirmPayment");

                } else {
                    view.loadUrl(url);
                }

                pbWeb.setVisibility(View.GONE);
                return true;
            }

            public void onLoadResource(WebView view, String url) {
                Log.i("PaymentMethodView", "onLoadResource :" + url);
            }

        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}