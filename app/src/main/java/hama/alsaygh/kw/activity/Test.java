package hama.alsaygh.kw.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import hama.alsaygh.kw.R;

import hama.alsaygh.kw.activity.auth.LoginActivity;
import hama.alsaygh.kw.activity.base.BaseActivity;

import androidx.appcompat.app.AppCompatDelegate;

public class Test extends BaseActivity {

    Switch aSwitch;
    Button btn_switch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        aSwitch = (Switch) findViewById(R.id.switch2);
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            aSwitch.setChecked(true);
        }

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    RestartApp();
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    RestartApp();

                }
                SharedPreferences.Editor editor = getSharedPreferences("share", MODE_PRIVATE).edit();
                editor.putBoolean("checked", isChecked);
                editor.apply();
            }
        });


    }

    public void RestartApp() {
        Intent i = new Intent(getApplicationContext(), Test.class);
        startActivity(i);
        finish();
    }


    public void onClick(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}