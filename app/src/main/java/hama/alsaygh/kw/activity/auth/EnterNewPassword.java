package hama.alsaygh.kw.activity.auth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import hama.alsaygh.kw.R;

import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.GeneralResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

public class EnterNewPassword extends BaseActivity {
    ImageView back_enter_new_password;
    TextView sign_in_enter_new_password, textView, textView1, textView3;
    Button confirm;
    LinearLayout liner_enter_new_password;
    EditText editText2, editText22;
    ProgressBar pb_confirm;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_new_password);

        if (getIntent() != null) {
            token = getIntent().getStringExtra("token");
        }

        pb_confirm = findViewById(R.id.pb_confirm);

        if (pb_confirm.getIndeterminateDrawable() != null) {
            pb_confirm.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.whiteColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        back_enter_new_password = findViewById(R.id.back_enter_new_password);
        back_enter_new_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), EnterResetPassword.class);
                startActivity(i);
            }
        });
        sign_in_enter_new_password = findViewById(R.id.sign_in_enter_new_password);
        sign_in_enter_new_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        confirm = findViewById(R.id.confirm);

        liner_enter_new_password = findViewById(R.id.liner_enter_new_password);
        textView = findViewById(R.id.textView);
        textView1 = findViewById(R.id.textView1);
        editText2 = findViewById(R.id.editText2);
        editText22 = findViewById(R.id.editText22);
        textView3 = findViewById(R.id.textView3);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            textView.setTextColor(ContextCompat.getColor(this, R.color.order_tracking1));
            textView1.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));

            liner_enter_new_password.setBackgroundResource(R.color.sign_in);
            back_enter_new_password.setImageResource(R.drawable.ic_back_icon_dark);
            editText2.setBackgroundResource(R.drawable.back_edittext_dark);
            editText22.setBackgroundResource(R.drawable.back_edittext_dark);
            editText2.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            editText22.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            sign_in_enter_new_password.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            textView3.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            confirm.setBackgroundResource(R.drawable.back_button_dark);
            pb_confirm.setBackgroundResource(R.drawable.back_button_dark);

            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.sign_in));


        } else {
            setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }


    public void signIN(View view) {
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    public void Confirm(final View view) {

        if (MainApplication.isConnected) {
            if (isValid()) {
                pb_confirm.setVisibility(View.VISIBLE);
                confirm.setVisibility(View.GONE);
                final String password = editText2.getEditableText().toString();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final GeneralResponse loginResponse = RequestWrapper.getInstance().resetPassword(EnterNewPassword.this, token, password);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                confirm.setVisibility(View.VISIBLE);
                                pb_confirm.setVisibility(View.GONE);
                                Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        });
                    }
                }).start();
            }
        } else
            Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();

    }


    private boolean isValid() {
        boolean isValid = true;

        if (editText2.getEditableText().toString().isEmpty()) {
            isValid = false;
            editText2.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                editText2.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editText2.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        if (editText22.getEditableText().toString().isEmpty()) {
            isValid = false;
            editText22.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                editText22.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editText22.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        String password = editText2.getEditableText().toString();
        String co_password = editText22.getEditableText().toString();

        if (!password.isEmpty() && !co_password.isEmpty() && !password.equals(co_password)) {
            isValid = false;
            editText22.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                editText22.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                editText22.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        return isValid;
    }

}
