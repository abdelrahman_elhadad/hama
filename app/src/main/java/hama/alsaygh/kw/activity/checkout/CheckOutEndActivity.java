package hama.alsaygh.kw.activity.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.OrderPaymentResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.dialog.CheckOutConfirmPayment;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.model.order.CreateOrder;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class CheckOutEndActivity extends BaseActivity {

    ImageView img_back;
    Button button8, button9;
    TextView txt_toolbar, textView129, textView1129, text1129;
    EditText edit_msg;
    LinearLayout parent_a, liner_btn;
    View dash;

    String delivery_type, receipt_at, city, street, zip, building;
    int country_id,city_id, save_my_location_check, card_id, payment_method_id;
    ProgressBar pb_send;

    int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.check_out_confirm_payment_end);

        if (getIntent() != null) {
            id=getIntent().getIntExtra("id",0);
            delivery_type = getIntent().getStringExtra("delivery_type");
           // card_id = getIntent().getIntExtra("card_id", -1);
            payment_method_id = getIntent().getIntExtra("payment_method_id", 2);
            if (delivery_type.equalsIgnoreCase("hand_by_hand")) {
                receipt_at = getIntent().getStringExtra("receipt_at");
            } else {
                save_my_location_check = getIntent().getIntExtra("save_my_location_check", 0);
                country_id = getIntent().getIntExtra("country_id", -1);
                city_id=getIntent().getIntExtra("city_id",-1);
                city = getIntent().getStringExtra("city");
                street = getIntent().getStringExtra("street");
                zip = getIntent().getStringExtra("zip");
                building = getIntent().getStringExtra("building");
            }
        }


        pb_send = findViewById(R.id.pb_send);
        img_back = (ImageView) findViewById(R.id.img_back);
        txt_toolbar = (TextView) findViewById(R.id.txt_toolbar);
        parent_a = (LinearLayout) findViewById(R.id.parent_a);
        liner_btn = (LinearLayout) findViewById(R.id.liner_btn);
        textView1129 = (TextView) findViewById(R.id.textView1129);
        text1129 = (TextView) findViewById(R.id.text1129);
        textView129 = (TextView) findViewById(R.id.textView129);
        edit_msg = (EditText) findViewById(R.id.edit_msg);
        dash = (View) findViewById(R.id.dash);
        button8 = (Button) findViewById(R.id.button8);
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        button9 = (Button) findViewById(R.id.button9);
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (MainApplication.isConnected) {

                    final CreateOrder order = new CreateOrder();
                    order.setDelivery_type(delivery_type);
                    order.setCard_id(card_id);
                    order.setPayment_method_id(payment_method_id);
                    order.setGift_note(edit_msg.getEditableText().toString());
                    order.setCountry_id(country_id);
                    order.setCity(city);
                    order.setStreet(street);
                    order.setZip(zip);
                    order.setCity_id(city_id);
                    order.setBuilding(building);
                    order.setSave_my_location_check(save_my_location_check);
                    order.setReceipt_at(receipt_at);
                    order.setStore_id(id);
                    pb_send.setVisibility(View.VISIBLE);
                    button9.setVisibility(View.GONE);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            final OrderPaymentResponse generalResponse = RequestWrapper.getInstance().createOrder(CheckOutEndActivity.this, order);
                            if (generalResponse.isStatus()) {
                                if (generalResponse.getData().getPayment_url() != null && !generalResponse.getData().getPayment_url().trim().isEmpty()) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            pb_send.setVisibility(View.GONE);
                                            button9.setVisibility(View.VISIBLE);

                                            Intent intent = new Intent(CheckOutEndActivity.this, WebViewPaymentActivity.class);
                                            intent.putExtra("url", generalResponse.getData().getPayment_url());
                                            startActivity(intent);
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            pb_send.setVisibility(View.GONE);
                                            button9.setVisibility(View.VISIBLE);

                                            CheckOutConfirmPayment checkOutConfirmPayment = new CheckOutConfirmPayment();
                                            checkOutConfirmPayment.show(getSupportFragmentManager(), "checkOutConfirmPayment");
                                        }
                                    });
                                }
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pb_send.setVisibility(View.GONE);
                                        button9.setVisibility(View.VISIBLE);

                                        if (generalResponse.getCode().equalsIgnoreCase("401")) {
                                            LoginDialog loginDialog = LoginDialog.newInstance();
                                            loginDialog.show(getSupportFragmentManager(), "login");
                                        } else
                                            Snackbar.make(v, generalResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        }
                    }).start();

                } else
                    Snackbar.make(v, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
            }
        });
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            this.setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            txt_toolbar.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            parent_a.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            textView129.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            liner_btn.setBackgroundColor(ContextCompat.getColor(this, R.color.dark11));
            parent_a.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            textView1129.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            text1129.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            edit_msg.setBackgroundResource(R.drawable.back_zakat_dark);
            edit_msg.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            dash.setBackgroundColor(ContextCompat.getColor(this, R.color.whiteColor));

        } else {
            this.setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

    }
}