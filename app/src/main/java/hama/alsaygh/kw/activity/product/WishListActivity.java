package hama.alsaygh.kw.activity.product;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.adapter.product.AdapterWishList;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.WishListResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnProductClickListener;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.utils.Cons;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;
import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class WishListActivity extends BaseActivity implements OnProductClickListener {
    RecyclerView recyclewish_list;
    ImageView img_back;
    TextView txt_toolbar;
    LinearLayout parent_wish_list;
    Skeleton skeleton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whish_list);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide(); //<< this

        recyclewish_list = findViewById(R.id.recyclewish_list);
        txt_toolbar = findViewById(R.id.txt_toolbar);
        img_back = findViewById(R.id.img_back);
        parent_wish_list = findViewById(R.id.parent_wish_list);
        skeleton = SkeletonLayoutUtils.applySkeleton(recyclewish_list, R.layout.card_wash_list, 1);

        Utils.getInstance().setSkeletonMaskAndShimmer(this,skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            Utils.getInstance().setSkeletonMaskAndShimmerDark(this,skeleton);
            txt_toolbar.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            parent_wish_list.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
        }

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        if (MainApplication.isConnected) {
            skeleton.showSkeleton();

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final WishListResponse generalResponse = RequestWrapper.getInstance().getWishList(WishListActivity.this);
                    if (generalResponse.isStatus()) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                skeleton.showOriginal();
                                AdapterWishList adapterWishList = new AdapterWishList(WishListActivity.this, generalResponse.getData(), WishListActivity.this);
                                recyclewish_list.setAdapter(adapterWishList);

                            }
                        });
                    } else
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                skeleton.showOriginal();
                                Snackbar.make(recyclewish_list, generalResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        });
                }
            }).start();

        } else {
            Snackbar.make(recyclewish_list, this.getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onProductClick(Product product, int position) {

        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra(Cons.PRODUCT_ID, product.getId());
        intent.putExtra(Cons.PRODUCT, product);
        startActivity(intent);
    }
}