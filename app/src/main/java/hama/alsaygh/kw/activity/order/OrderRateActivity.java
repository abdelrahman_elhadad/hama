package hama.alsaygh.kw.activity.order;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.adapter.AdapterRateHamaSlider;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.OrderResponse;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.db.table.User;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.model.cart.CartItem;
import hama.alsaygh.kw.model.order.Order;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class OrderRateActivity extends BaseActivity {
    ImageView imageback;
    TextView txt_toolbar,tv_msg;
    LinearLayout parent_my_order;
    Button but_submit;
    EditText editText_comment;
    ProgressBar pb_submit;

    ViewPager2 viewPager;
    AdapterRateHamaSlider adapterRateHamaSlider;

    Order order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_rate);

        if(getIntent()!=null)
        {
            order=(Order) getIntent().getSerializableExtra("order");
        }

        imageback = (ImageView) findViewById(R.id.back_myorder);
        imageback.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                }
        );

        editText_comment=findViewById(R.id.editText_comment);
        but_submit=findViewById(R.id.but_submit);
        tv_msg=findViewById(R.id.textView93);
        txt_toolbar = (TextView) findViewById(R.id.textView9);
        parent_my_order = (LinearLayout) findViewById(R.id.parent_my_order);
        pb_submit=findViewById(R.id.pb_submit);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            this.setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            txt_toolbar.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            tv_msg.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            parent_my_order.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
            but_submit.setBackgroundResource(R.drawable.back_button_dark);
            editText_comment.setBackgroundResource(R.drawable.back_zakat_dark);
            editText_comment.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            pb_submit.setBackgroundResource(R.drawable.back_button_dark);
        } else {
            this.setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        viewPager = (ViewPager2) findViewById(R.id.viewpager);
        adapterRateHamaSlider = new AdapterRateHamaSlider(getLayoutInflater(), this, order.getItems());
        viewPager.setAdapter(adapterRateHamaSlider);
        viewPager.setClipToPadding(false);
        viewPager.setClipChildren(false);
        viewPager.setOffscreenPageLimit(3);
        // viewPager.getChildAt(1).setOverScrollMode(View.OVER_SCROLL_NEVER);
        CompositePageTransformer transformer = new CompositePageTransformer();
        transformer.addTransformer(new MarginPageTransformer(8));
        transformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float v = 1- Math.abs(position);
                page.setScaleY(0.8f + v * 0.2f);

            }
        });
        viewPager.setPageTransformer(transformer);


        but_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               try {
                   List<CartItem> cartItems = adapterRateHamaSlider.getCartItems();
                   SparseArray<Float> rateItems = adapterRateHamaSlider.getRateArray();
                   JSONArray jsonArray = new JSONArray();
                   for (int i = 0; i < cartItems.size(); i++) {
                       JSONObject jsonObject = new JSONObject();
                       jsonObject.put("product_id", cartItems.get(i).getProduct().getId()+"");
                       if (rateItems.get(i) != null)
                       jsonObject.put("rate", rateItems.get(i)+"");
                       else
                           jsonObject.put("rate", "0");
                       jsonArray.put(jsonObject);
                   }

                   String comment=editText_comment.getEditableText().toString();
                   pb_submit.setVisibility(View.VISIBLE);
                   but_submit.setVisibility(View.GONE);
                   new Thread(new Runnable() {
                       @Override
                       public void run() {

                           AppDatabase appDatabase = AppDatabase.newInstance(OrderRateActivity.this);
                           List<User> userList = appDatabase.userDao().getAll();
                           if (userList != null && !userList.isEmpty()) {

                               final OrderResponse productResponse = RequestWrapper.getInstance().rateOrder(OrderRateActivity.this, order.getId(),jsonArray.toString(),comment);
                               if (productResponse.isStatus()) {
                                   order = productResponse.getData();
                                   runOnUiThread(new Runnable() {
                                       @Override
                                       public void run() {
                                           pb_submit.setVisibility(View.GONE);
                                           but_submit.setVisibility(View.VISIBLE);
                                           Snackbar.make(parent_my_order, productResponse.getMessage(), Snackbar.LENGTH_SHORT).show();

                                       }
                                   });
                               } else {

                                   runOnUiThread(new Runnable() {
                                       @Override
                                       public void run() {
                                           pb_submit.setVisibility(View.GONE);
                                           but_submit.setVisibility(View.VISIBLE);
                                           if (productResponse.getCode().equalsIgnoreCase("401")) {
                                               LoginDialog loginDialog = LoginDialog.newInstance();
                                               loginDialog.show(getSupportFragmentManager(), "login");
                                           } else
                                               Snackbar.make(parent_my_order, productResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                       }
                                   });
                               }
                           } else {
                               runOnUiThread(new Runnable() {
                                   @Override
                                   public void run() {
                                       pb_submit.setVisibility(View.GONE);
                                       but_submit.setVisibility(View.VISIBLE);
                                       LoginDialog loginDialog = LoginDialog.newInstance();
                                       loginDialog.show(getSupportFragmentManager(), "login");
                                   }
                               });
                           }
                       }
                   }).start();



               }catch (Exception e)
               {
                   e.printStackTrace();
               }

            }
        });

    }
}