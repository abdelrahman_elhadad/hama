package hama.alsaygh.kw.activity.mainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.activity.order.OrdersActivity;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.fragment.checkout.AllCartFragment;
import hama.alsaygh.kw.fragment.home.HomeFragment;
import hama.alsaygh.kw.fragment.home.MenuFragment;
import hama.alsaygh.kw.fragment.home.store.AllStores;
import hama.alsaygh.kw.fragment.home.store.StorePage;
import hama.alsaygh.kw.fragment.search.SearchFragment;
import hama.alsaygh.kw.fragment.search.SearchResultStoreFragment;
import hama.alsaygh.kw.fragment.search.SearchResultStorePackageFragment;
import hama.alsaygh.kw.fragment.settings.appSetting.Setting;
import hama.alsaygh.kw.fragment.settings.appSetting.SettingLanguage;
import hama.alsaygh.kw.fragment.settings.complaints.Complaints;
import hama.alsaygh.kw.fragment.settings.storePackage.AllStoresPackage;
import hama.alsaygh.kw.fragment.settings.storePackage.StorePagePackage;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.LocalUtils;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "Home11";
    DrawerLayout drawerLayout;


    public static final int HOME1 = 0;
    public static final int MyCart = 1;
    public static final int AllStores = 2;
    public static final int Search = 3;
    public static final int MyProfile = 4;
    public static final int MyOrder = 5;
    public static final int StoreDetails = 7;
    public static final int StoreDetailsFromAllStore = 9;
    public static final int ProductDetailsFromStorePage = 10;
    public static final int SettingLanguage = 11;
    public static final int Setting = 12;
    public static final int profileClick = 13;
    public static final int complaintArea = 14;
    public static final int SORT_PRODUCT = 15;
    public static final int StoreDetailsFromAllStorePackage = 16;
    public static final int SORT_PRODUCT_PACKAGE = 17;
    public static final int ProductDetailsFromStorePagePackage = 18;
    public static final int SearchResult = 19;
    public static final int StoreDetailsFromSearch = 20;
    public static final int StorePackageDetailsFromSearch = 21;
    public static final int StoreCartFromStore = 22;
    public static final int StoreCartFromCarts = 23;

    public static int position = HOME1;
    public static Store store;
    public static String search;


    FragmentTransaction fragmentTransaction;
    static FragmentManager fragmentManager;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalUtils.getInstance().updateResources(this, LocalUtils.getInstance().getLanguageShort(this));
        if (getIntent() != null) {
            boolean from_order = getIntent().getBooleanExtra("from_order", false);
            if (from_order) {
                Intent intent = new Intent(this, OrdersActivity.class);
                startActivity(intent);
            }
        }

        setContentView(R.layout.home11);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.buttom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        // bottomNavigationView.setItemIconTintList(null);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);

        fragmentManager = getSupportFragmentManager();

        if (position == Setting) {
            bottomNavigationView.setSelectedItemId(R.id.page_6);
        } else if (position == SettingLanguage) {
            bottomNavigationView.setSelectedItemId(R.id.page_6);
        } else {
            bottomNavigationView.setSelectedItemId(R.id.page_1);
        }
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
//            bottomNavigationView.setItemIconTintList(ContextCompat.getColorStateList(this, R.color.tab_color_dark));
//            bottomNavigationView.setItemTextColor(ContextCompat.getColorStateList(this, R.color.tab_color_dark));
           // bottomNavigationView.setBackgroundColor(ContextCompat.getColor(this, R.color.dark11));


        } else {
            setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestWrapper.getInstance().getStoreProductSort(HomeActivity.this);
            }
        }).start();
    }


    private void commitFragment(Fragment fragment, int position) {
        HomeActivity.position = position;
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.liner1, fragment);
        fragmentTransaction.commit();
    }

    //    public static void interaction(Fragment fragment){
//
//        fragmentManager.beginTransaction().replace(R.id.liner1 ,fragment).commit();
//    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.page_1) {
            commitFragment(new HomeFragment(), HOME1);
            Log.e(TAG, "home1" + item.getItemId());

            return true;
        }
        if (item.getItemId() == R.id.page_3) {
            commitFragment(new AllCartFragment(), MyCart);
            Log.e(TAG, "cart" + item.getItemId());
            return true;
        }
        if (item.getItemId() == R.id.page_4) {
            commitFragment(new AllStores(), AllStores);
            Log.e(TAG, "stores" + item.getItemId());

            return true;
        }
        if (item.getItemId() == R.id.page_5) {
            commitFragment(new SearchFragment(), Search);
            Log.e(TAG, "search" + item.getItemId());

            return true;
        }
        if (item.getItemId() == R.id.page_6) {

            if (position == Setting) {
                commitFragment(new Setting(), Setting);
            } else if (position == SettingLanguage) {
                commitFragment(new SettingLanguage(), SettingLanguage);
            } else
                commitFragment(new MenuFragment(), MyProfile);
            Log.e(TAG, "myprofile" + item.getItemId());

            return true;
        }
        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    @Override
    public void onBackPressed() {

        if(HomeActivity.position==StoreCartFromCarts)
        {
            commitFragment(new AllCartFragment(), MyCart);
            bottomNavigationView.setSelectedItemId(R.id.page_3);
        }else
        if (HomeActivity.position == StorePackageDetailsFromSearch) {
            commitFragment(SearchResultStorePackageFragment.newInstance(search), SearchResult);
        } else if (HomeActivity.position == StoreDetailsFromSearch) {
            commitFragment(SearchResultStoreFragment.newInstance(search), SearchResult);
        } else if (HomeActivity.position == SearchResult) {
            commitFragment(new SearchFragment(), Search);
            search = "";
        } else if (HomeActivity.position == ProductDetailsFromStorePagePackage || HomeActivity.position == SORT_PRODUCT_PACKAGE) {
            commitFragment(StorePagePackage.newInstance(store), StoreDetailsFromAllStorePackage);
        } else if (HomeActivity.position == StoreDetailsFromAllStorePackage) {
            commitFragment(new AllStoresPackage(), profileClick);
        } else if (HomeActivity.position == complaintArea) {
            commitFragment(new Complaints(), profileClick);
        } else if (HomeActivity.position == StoreDetailsFromAllStore) {
            commitFragment(new AllStores(), AllStores);
        } else if (HomeActivity.position == ProductDetailsFromStorePage || HomeActivity.position == SORT_PRODUCT||HomeActivity.position==StoreCartFromStore) {
            commitFragment(StorePage.newInstance(store), StoreDetails);
        } else if (HomeActivity.position == MyOrder || HomeActivity.position == Setting || HomeActivity.position == profileClick) {
            commitFragment(new MenuFragment(), MyProfile);
            bottomNavigationView.setSelectedItemId(R.id.page_6);

        } else if (HomeActivity.position == SettingLanguage) {
            commitFragment(new Setting(), Setting);

            bottomNavigationView.setSelectedItemId(R.id.page_6);
        } else if (HomeActivity.position != HOME1) {
            commitFragment(new HomeFragment(), HOME1);
            bottomNavigationView.setSelectedItemId(R.id.page_1);
        } else {
            super.onBackPressed();
        }
    }


    public void openAllStore() {
        bottomNavigationView.setSelectedItemId(R.id.page_4);
    }


    public void openStoreCart()
    {

    }

//    private void commitFragment(Fragment fragment, boolean isUp) {
//        try {
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            if (isUp)
//                ft.setCustomAnimations(R.anim.slide_up, 0);
//            else
//                ft.setCustomAnimations(0, R.anim.slide_down);
//
//            ft.replace(R.id.liner1, fragment);
//            ft.commitAllowingStateLoss();
//        } catch (Exception e) {
//            Log.d("ABSDIALOGFRAG", "Exception", e);
//        }
//    }

//    OnBackPressedCallback callback = new OnBackPressedCallback(true) {
//        @Override
//        public void handleOnBackPressed() {
//
//        }
//    };
//    getActivity().getOnBackPressedDispatcher().addCallback(this, callback);

}
