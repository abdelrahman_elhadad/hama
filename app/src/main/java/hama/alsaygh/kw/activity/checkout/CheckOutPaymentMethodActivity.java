package hama.alsaygh.kw.activity.checkout;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.adapter.paymentCard.AdapterSliderCheckOPaymentMethod;
import hama.alsaygh.kw.dialog.card.PopUpAddNewCart;
import hama.alsaygh.kw.listener.OnCardClickListener;
import hama.alsaygh.kw.model.paymentCard.Card;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class CheckOutPaymentMethodActivity extends BaseActivity implements OnCardClickListener {
    ImageView img_back;
    Button button3, button9, button8;
    TextView txt_toolbar, textView128;
    LinearLayout parent_a, liner_btn;
    ViewPager2 viewPager;
    AdapterSliderCheckOPaymentMethod adapterSliderCheckOPaymentMethod;
    Skeleton skeleton;

    String delivery_type, receipt_at, city, street, zip, building;
    int country_id,city_id, save_my_location_check;

    RadioButton KNET, visa;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.check_out_payment_method);

        if (getIntent() != null) {
            delivery_type = getIntent().getStringExtra("delivery_type");

            if (delivery_type.equalsIgnoreCase("hand_by_hand")) {
                receipt_at = getIntent().getStringExtra("receipt_at");
            } else {
                save_my_location_check = getIntent().getIntExtra("save_my_location_check", 0);
                country_id = getIntent().getIntExtra("country_id", -1);
                city_id = getIntent().getIntExtra("city_id", -1);
                city = getIntent().getStringExtra("city");
                street = getIntent().getStringExtra("street");
                zip = getIntent().getStringExtra("zip");
                building = getIntent().getStringExtra("building");
            }
            id=getIntent().getIntExtra("id",0);
        }
        visa = findViewById(R.id.visa);
        KNET = findViewById(R.id.KNET);
        img_back = (ImageView) findViewById(R.id.img_back);
        txt_toolbar = (TextView) findViewById(R.id.txt_toolbar);
        parent_a = (LinearLayout) findViewById(R.id.parent_a);
        liner_btn = (LinearLayout) findViewById(R.id.liner_btn);
        viewPager = (ViewPager2) findViewById(R.id.viewpager);
        textView128 = (TextView) findViewById(R.id.textView128);
        button3 = (Button) findViewById(R.id.button3);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (visa.isChecked() || KNET.isChecked()) {
//                if (adapterSliderCheckOPaymentMethod != null && adapterSliderCheckOPaymentMethod.getItemCount() > 0) {
//                    Card card = (Card) adapterSliderCheckOPaymentMethod.getCurrentItem(viewPager.getCurrentItem());

                    Intent intent = new Intent(CheckOutPaymentMethodActivity.this, CheckOutEndActivity.class);
                    intent.putExtra("delivery_type", delivery_type);
                    intent.putExtra("id",id);
                    if (delivery_type.equalsIgnoreCase("hand_by_hand")) {
                        intent.putExtra("receipt_at", receipt_at);
                    } else {
                        intent.putExtra("delivery_type", delivery_type);
                        intent.putExtra("country_id", country_id);
                        intent.putExtra("city_id",city_id);
                        intent.putExtra("city", city);
                        intent.putExtra("street", street);
                        intent.putExtra("zip", zip);
                        intent.putExtra("building", building);
                        intent.putExtra("save_my_location_check", save_my_location_check);
                    }
//                    intent.putExtra("card_id", card.getId());
                    if (visa.isChecked())
                        intent.putExtra("payment_method_id", 2);
                    else if (KNET.isChecked())
                        intent.putExtra("payment_method_id", 1);
                    startActivity(intent);


                } else
                    Snackbar.make(v, getString(R.string.add), Snackbar.LENGTH_SHORT).show();

            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopUpAddNewCart popUpAddNewCart = new PopUpAddNewCart();
                popUpAddNewCart.setOnCardClickListener(CheckOutPaymentMethodActivity.this);
                popUpAddNewCart.show(getSupportFragmentManager(), "pop up add new card");

            }
        });

        viewPager.setClipToPadding(false);
        viewPager.setClipChildren(false);
        viewPager.setOffscreenPageLimit(3);
        CompositePageTransformer transformer = new CompositePageTransformer();
        transformer.addTransformer(new MarginPageTransformer(8));
        transformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float v = 1 - Math.abs(position);
                page.setScaleY(0.8f + v * 0.2f);

            }
        });
        viewPager.setPageTransformer(transformer);
        skeleton = SkeletonLayoutUtils.createSkeleton(parent_a);
        Utils.getInstance().setSkeletonMaskAndShimmer(this, skeleton);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            this.setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(this, skeleton);

            txt_toolbar.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            parent_a.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            textView128.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            liner_btn.setBackgroundColor(ContextCompat.getColor(this, R.color.dark11));
            button3.setBackgroundResource(R.drawable.back_button_dark);
            visa.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));
            KNET.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));


            KNET.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_navigation)));
            KNET.setHighlightColor(ContextCompat.getColor(this, R.color.color_navigation));

            visa.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_navigation)));
            visa.setHighlightColor(ContextCompat.getColor(this, R.color.color_navigation));

        } else {
            this.setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh(parent_a);
    }

    private void refresh(@NonNull final View view) {
//        if (MainApplication.isConnected) {
//
//            skeleton.showSkeleton();
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//
//                    final CardsResponse cardsResponse = RequestWrapper.getInstance().getCards(CheckOutPaymentMethodActivity.this);
//                    if (cardsResponse.isStatus()) {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                skeleton.showOriginal();
//                                adapterSliderCheckOPaymentMethod = new AdapterSliderCheckOPaymentMethod(getLayoutInflater(), CheckOutPaymentMethodActivity.this, cardsResponse.getData());
//                                viewPager.setAdapter(adapterSliderCheckOPaymentMethod);
//                            }
//                        });
//                    } else {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                skeleton.showOriginal();
//                                Snackbar.make(view, cardsResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
//                            }
//                        });
//                    }
//
//                }
//            }).start();
//
//        } else {
//            Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
//        }
    }

    @Override
    public void onCardClick(View view, Card card, int position) {

    }

    @Override
    public void onCardRefresh() {
        refresh(parent_a);
    }
}