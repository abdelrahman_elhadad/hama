package hama.alsaygh.kw.activity.auth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.messaging.FirebaseMessaging;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.LoginResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class SignUpStep1Activity extends BaseActivity {
    ImageView img_back_register;
    TextView sign_in_register, wel_come_reg, sign_up_tocontinue, textView5;
    EditText edtFirstName, edtLastName, edtEmail, edtPassword;
    Button btnSign;
    LinearLayout linearLayout;
    ProgressBar pbSign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        img_back_register = (ImageView) findViewById(R.id.img_back_register);
        img_back_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
            }
        });
        sign_in_register = (TextView) findViewById(R.id.sign_in_register);
        sign_in_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);

            }
        });
        wel_come_reg = (TextView) findViewById(R.id.wel_come_register);
        sign_up_tocontinue = (TextView) findViewById(R.id.sign_up_tocontinue);
        edtFirstName = (EditText) findViewById(R.id.edt_first_name);
        edtLastName = (EditText) findViewById(R.id.edt_last_name);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        textView5 = (TextView) findViewById(R.id.textView5);
        btnSign = (Button) findViewById(R.id.btn_sign);
        linearLayout = (LinearLayout) findViewById(R.id.parent_register);
        pbSign = findViewById(R.id.pb_sign);

        if (pbSign.getIndeterminateDrawable() != null) {
            pbSign.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.whiteColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        if(SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            img_back_register.setImageResource(R.drawable.ic_back_icon_dark);
            wel_come_reg.setTextColor(ContextCompat.getColor(this, R.color.order_tracking1));
            sign_up_tocontinue.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            edtFirstName.setBackgroundResource(R.drawable.back_edittext_dark);
            edtLastName.setBackgroundResource(R.drawable.back_edittext_dark);
            edtEmail.setBackgroundResource(R.drawable.back_edittext_dark);
            edtPassword.setBackgroundResource(R.drawable.back_edittext_dark);
            textView5.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));
            edtFirstName.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            edtLastName.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            edtEmail.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            edtPassword.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            btnSign.setBackgroundResource(R.drawable.back_button_dark);
            pbSign.setBackgroundResource(R.drawable.back_button_dark);
            linearLayout.setBackgroundResource(R.color.sign_in);


            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.sign_in));


        } else {
            setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

    }

    public void nextRegister(final View view) {

        if (MainApplication.isConnected) {

            if (isValid()) {

                btnSign.setVisibility(View.GONE);
                pbSign.setVisibility(View.VISIBLE);
                final String email = edtEmail.getEditableText().toString().trim();
                final String fName = edtFirstName.getEditableText().toString().trim();
                final String lName = edtLastName.getEditableText().toString().trim();
                final String password = edtPassword.getEditableText().toString();
                FirebaseMessaging.getInstance().getToken()
                        .addOnCompleteListener(new OnCompleteListener<String>() {
                            @Override
                            public void onComplete(@NonNull Task<String> task) {
                                if (!task.isSuccessful()) {
                                    return;
                                }

                                // Get new FCM registration token
                               final String token = task.getResult();
                                new Thread(new Runnable() {

                                    @Override
                                    public void run() {

                                        final LoginResponse loginResponse = RequestWrapper.getInstance().registerUserStep1(SignUpStep1Activity.this, fName, lName, email, password,token);

                                        if (loginResponse.isStatus()) {
                                            AppDatabase appDatabase = AppDatabase.newInstance(SignUpStep1Activity.this);
                                            appDatabase.userDao().insertAll(loginResponse.getData().getUser());

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    btnSign.setVisibility(View.VISIBLE);
                                                    pbSign.setVisibility(View.GONE);

                                                    Intent i = new Intent(getApplicationContext(), SignUpStep2Activity.class);
                                                    i.putExtra("email", email);
                                                    startActivity(i);
                                                }
                                            });
                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    btnSign.setVisibility(View.VISIBLE);
                                                    pbSign.setVisibility(View.GONE);
                                                    Snackbar.make(view, loginResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                                }
                                            });
                                        }

                                    }
                                }).start();

                               }
                        });

            }

        } else {
            Snackbar.make(view, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
        }


    }

    private boolean isValid() {

        boolean isValid = true;


        if (edtEmail.getEditableText().toString().trim().isEmpty() || !Utils.getInstance().isValidEmail(edtEmail.getEditableText().toString().trim())) {
            isValid = false;
            edtEmail.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                edtEmail.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edtEmail.setBackgroundResource(R.drawable.back_ground_edit_text);

        }

        if (edtFirstName.getEditableText().toString().trim().isEmpty()) {
            isValid = false;
            edtFirstName.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                edtFirstName.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edtFirstName.setBackgroundResource(R.drawable.back_ground_edit_text);

        }

        if (edtLastName.getEditableText().toString().trim().isEmpty()) {
            isValid = false;
            edtLastName.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                edtLastName.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edtLastName.setBackgroundResource(R.drawable.back_ground_edit_text);

        }


        if (edtPassword.getEditableText().toString().isEmpty()) {
            isValid = false;
            edtPassword.setBackgroundResource(R.drawable.back_edittext_red);
        } else {

            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                edtPassword.setBackgroundResource(R.drawable.back_edittext_dark);
            } else
                edtPassword.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        return isValid;
    }

}