package hama.alsaygh.kw.activity.product;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import java.util.Locale;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.adapter.imageSlider.SliderAdapter;
import hama.alsaygh.kw.adapter.imageSlider.SliderAdapterDark;
import hama.alsaygh.kw.adapter.product.AdapterProductOption;
import hama.alsaygh.kw.adapter.review.AdapterProductDetailsReview;
import hama.alsaygh.kw.adapter.review.AdapterProductDetailsReviewDark;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.CartResponse;
import hama.alsaygh.kw.api.responce.GeneralResponse;
import hama.alsaygh.kw.api.responce.ProductResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnChildOptionListener;
import hama.alsaygh.kw.model.product.Option;
import hama.alsaygh.kw.model.product.OptionChild;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.utils.Cons;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;
import me.relex.circleindicator.CircleIndicator;

public class ProductDetailsActivity extends BaseActivity implements OnChildOptionListener {

    ViewPager viewPager;
    Button button;
    ImageView imageback, share_img;
    TextView text_toolbar_home, tv_no_reviews, tv, tv1, tv2, tv3, tv111, lebel, review, write, textView3, textView4, textView32, tv_out_of_stock;
    SliderAdapter sliderAdapter;
    SliderAdapterDark sliderAdapterDark;
    LinearLayout liner_color, liner_size, liner2;
    RecyclerView recycle_prouductdetails;

    CircleIndicator indicator;
    RelativeLayout parent_prouduct_details;
    LinearLayout liner_prouduct_details;
    ImageView iv_like;
    Product product;
    int product_id;

    Skeleton skeleton;
    RecyclerView rv_option;
    Option selectedOption;
    SwipeRefreshLayout srl_refresh;
    CountDownTimer countDownTimer;
    RatingBar rb_review;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details);


        if (getIntent() != null) {
            product_id = getIntent().getIntExtra(Cons.PRODUCT_ID, 0);
            product = (Product) getIntent().getSerializableExtra(Cons.PRODUCT);
        }

        recycle_prouductdetails = findViewById(R.id.rv_proudect_details);
        srl_refresh = findViewById(R.id.srl_refresh);
        rv_option = findViewById(R.id.rv_option);
        liner2 = findViewById(R.id.liner2);
        textView32 = findViewById(R.id.textView32);
        imageback = findViewById(R.id.imageView16);
        iv_like = findViewById(R.id.iv_like);
        tv_out_of_stock = findViewById(R.id.tv_out_of_stock);
        rb_review = findViewById(R.id.rb_review);
        imageback.setOnClickListener(v -> onBackPressed());

        tv_no_reviews = findViewById(R.id.tv_no_reviews);
        viewPager = findViewById(R.id.viewPager11);
        indicator = findViewById(R.id.pageIndicatorView1);
        liner_color = findViewById(R.id.liner_color);
        liner_size = findViewById(R.id.liner_size);
        text_toolbar_home = findViewById(R.id.textView19);
        tv = findViewById(R.id.tv);
        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv111 = findViewById(R.id.tv111);
        lebel = findViewById(R.id.lebel);
        review = findViewById(R.id.review);
        write = findViewById(R.id.write);
        button = findViewById(R.id.button3);
        textView3 = findViewById(R.id.textView3);
        textView4 = findViewById(R.id.textView4);
        share_img = findViewById(R.id.imageView71);
        parent_prouduct_details = findViewById(R.id.parent_prouduct_details);
        sliderAdapterDark = new SliderAdapterDark(this, getLayoutInflater());
        liner_prouduct_details = findViewById(R.id.liner_prouduct_details);
        skeleton = SkeletonLayoutUtils.createSkeleton(parent_prouduct_details);
        Utils.getInstance().setSkeletonMaskAndShimmer(this, skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            Utils.getInstance().setSkeletonMaskAndShimmerDark(this, skeleton);
            // text_toolbar_home.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            imageback.setImageResource(R.drawable.ic_back_icon_dark);
//            tv.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv1.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv2.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv3.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv111.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            lebel.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv_no_reviews.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
           // write.setTextColor(ContextCompat.getColor(this, R.color.order_tracking1));
            button.setBackgroundResource(R.drawable.back_button_dark);
            textView3.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
           // textView4.setTextColor(ContextCompat.getColor(this, R.color.textviewhome));
            share_img.setImageResource(R.drawable.ic_share_dark);
           // parent_prouduct_details.setBackgroundResource(R.color.sign_in);
           // review.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            viewPager.setAdapter(sliderAdapterDark);
          //  liner_prouduct_details.setBackgroundResource(R.color.dark11);
            //liner2.setBackgroundColor(ContextCompat.getColor(this, R.color.color_navigation));
//            textView32.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv_out_of_stock.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));


        }

        srl_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl_refresh.setRefreshing(true);
                getProductDetailsRefresh();
            }
        });

        getProductDetailsRefresh();
        iv_like.setOnClickListener(v -> {

            if (product != null)
                if (MainApplication.isConnected) {

                    new Thread(() -> {
                        final GeneralResponse productResponse = RequestWrapper.getInstance().getProductLikeUnlike(ProductDetailsActivity.this, product.getId());
                        if (productResponse.isStatus()) {

                            product.setI_fav(!product.isI_fav());

                            runOnUiThread(() -> setLikeUnlike());
                        } else {
                            runOnUiThread(() -> Snackbar.make(iv_like, productResponse.getMessage(), Snackbar.LENGTH_SHORT).show());
                        }
                    }).start();
                } else {
                    Snackbar.make(v, v.getContext().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }


        });

        button.setOnClickListener(v -> {

            if (product != null)
                if (MainApplication.isConnected) {

                    new Thread(() -> {

                        if (selectedOption != null) {
                            final CartResponse cartResponse = RequestWrapper.getInstance().addToCart(ProductDetailsActivity.this, product.getId(), 1, selectedOption.getId());
                            runOnUiThread(() -> Snackbar.make(v, cartResponse.getMessage(), Snackbar.LENGTH_SHORT).show());

                        } else {
                            final CartResponse cartResponse = RequestWrapper.getInstance().addToCart(ProductDetailsActivity.this, product.getId(), 1);
                            runOnUiThread(() -> Snackbar.make(v, cartResponse.getMessage(), Snackbar.LENGTH_SHORT).show());
                        }
                    }).start();
                } else {
                    Snackbar.make(v, v.getContext().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }
        });

        countDownTimer = new CountDownTimer(10000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                if (product != null && product.isBind_to_market()) {
                    getProductDetails();
                    countDownTimer.start();
                }
            }
        };
        countDownTimer.start();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    private void getProductDetailsRefresh() {
        setProductDetails();
        if (product_id != 0 || product != null)
            if (MainApplication.isConnected) {
                skeleton.showSkeleton();
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        final ProductResponse productResponse = RequestWrapper.getInstance().getProductDetails(ProductDetailsActivity.this, product_id);
                        if (productResponse.isStatus()) {
                            product = productResponse.getData();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    setProductDetails();
                                    srl_refresh.setRefreshing(false);
                                }
                            });
                        } else {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();
                                    Snackbar.make(iv_like, productResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                    srl_refresh.setRefreshing(false);
                                }
                            });
                        }
                    }
                }).start();

            } else {
                Snackbar.make(recycle_prouductdetails, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
            }
    }

    private void getProductDetails() {
        setProductDetails();
        if (product_id != 0 || product != null)
            if (MainApplication.isConnected) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        final ProductResponse productResponse = RequestWrapper.getInstance().getProductDetails(ProductDetailsActivity.this, product_id);
                        if (productResponse.isStatus()) {
                            product = productResponse.getData();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    setProductDetails();
                                    srl_refresh.setRefreshing(false);
                                }
                            });
                        } else {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    Snackbar.make(iv_like, productResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                    srl_refresh.setRefreshing(false);
                                }
                            });
                        }
                    }
                }).start();

            } else {
                Snackbar.make(recycle_prouductdetails, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
            }
    }

    private void setProductDetails() {
        if (product != null) {
            tv.setText(product.getName());


            if (product.getCaliber() == null)
                tv3.setText("");
            else
                tv3.setText(product.getCaliber().getValue() + "");

            lebel.setText(product.getDescription());
            text_toolbar_home.setText(product.getName());
            textView4.setText(product.getPrice() + " " + getString(R.string.currency));

            String weight = getString(R.string.weight) + " : " + String.format(Locale.ENGLISH, "%.3f", Double.parseDouble(product.getWeight()));
            tv1.setText(weight);

            if (product.isBind_to_market()) {
                textView32.setText(getString(R.string.market));
                textView3.setText(getString(R.string.market));
            } else {
                textView32.setText(getString(R.string.fixed));
                textView3.setText(getString(R.string.fixed));
            }

            setLikeUnlike();

            sliderAdapter = new SliderAdapter(this, getLayoutInflater(), getSupportFragmentManager(), product.getMedia());
            viewPager.setAdapter(sliderAdapter);
            viewPager.addOnPageChangeListener(viewListener);
            indicator.setViewPager(viewPager);

            rv_option.setLayoutManager(new LinearLayoutManager(this));
            AdapterProductOption adapterProductOption = new AdapterProductOption(this, product.getOptions(), this);
            rv_option.setAdapter(adapterProductOption);

            if (selectedOption != null) {
                adapterProductOption.setSelectedByID(selectedOption.getId());
            }

            if (product.isIn_stock()) {
                button.setVisibility(View.VISIBLE);
                tv_out_of_stock.setVisibility(View.GONE);
            } else {
                button.setVisibility(View.GONE);
                tv_out_of_stock.setVisibility(View.VISIBLE);
                tv_out_of_stock.setText(product.getIn_stock_str());
            }

            if (product.getReviews() != null && !product.getReviews().isEmpty()) {
                tv_no_reviews.setVisibility(View.GONE);
                recycle_prouductdetails.setVisibility(View.VISIBLE);
                if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
                    AdapterProductDetailsReviewDark adapterProductDetailsReview = new AdapterProductDetailsReviewDark(product.getReviews());
                    recycle_prouductdetails.setAdapter(adapterProductDetailsReview);
                } else {

                    AdapterProductDetailsReview adapterProductDetailsReview = new AdapterProductDetailsReview(product.getReviews());
                    recycle_prouductdetails.setAdapter(adapterProductDetailsReview);
                }
            } else {
                tv_no_reviews.setVisibility(View.VISIBLE);
                recycle_prouductdetails.setVisibility(View.GONE);
            }

            rb_review.setRating(product.getRate());
        }

    }

    private void setLikeUnlike() {
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            if (product.isI_fav()) {
                Drawable drawable = AppCompatResources.getDrawable(this, R.drawable.ic_love_cart_dark);
                iv_like.setImageDrawable(drawable);
            } else {
                Drawable drawable = AppCompatResources.getDrawable(this, R.drawable.ic_loove_dark);
                iv_like.setImageDrawable(drawable);
            }
        } else {
            if (product.isI_fav()) {
                Drawable drawable = AppCompatResources.getDrawable(this, R.drawable.ic_loveeecolor);
                iv_like.setImageDrawable(drawable);
            } else {
                Drawable drawable = AppCompatResources.getDrawable(this, R.drawable.ic_loveee);
                iv_like.setImageDrawable(drawable);
            }
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
//            pageIndicatorView.setSelection(position);
//            pageIndicatorView.setAnimationType(AnimationType.WORM);

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    @Override
    public void onChildOptionSelect(Option optionChild) {

        selectedOption = optionChild;
        setPrice();
    }

    @Override
    public void onChildOptionUnSelect(OptionChild optionChild) {

    }

    private void setPrice() {
        double total = product.getPrice();
        if (selectedOption != null) {
            total = selectedOption.getPrice();

            String weight = getString(R.string.weight) + " : " + String.format(Locale.ENGLISH, "%.3f", Double.parseDouble(selectedOption.getTotal_weight()));
            tv1.setText(weight);

            if (selectedOption.isBind_to_market()) {
                textView32.setText(getString(R.string.market));
                textView3.setText(getString(R.string.market));
            } else {
                textView32.setText(getString(R.string.fixed));
                textView3.setText(getString(R.string.fixed));
            }
        }

        textView4.setText(total + " " + getString(R.string.currency));

    }
}
