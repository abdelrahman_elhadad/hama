package hama.alsaygh.kw.activity.notification;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.adapter.notofication.AdapterNotifications;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.NotificationsResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class NotificationsActivity extends BaseActivity {
    RecyclerView recyclewish_list;
    ImageView img_back;
    TextView txt_toolbar;
    LinearLayout parent_wish_list;
    Skeleton skeleton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whish_list);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide(); //<< this

        recyclewish_list = findViewById(R.id.recyclewish_list);
        txt_toolbar = findViewById(R.id.txt_toolbar);
        img_back = findViewById(R.id.img_back);
        parent_wish_list = findViewById(R.id.parent_wish_list);
        skeleton = SkeletonLayoutUtils.applySkeleton(recyclewish_list, R.layout.card_view_notification1, 1);

        txt_toolbar.setText(getString(R.string.notification));

        Utils.getInstance().setSkeletonMaskAndShimmer(this,skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            Utils.getInstance().setSkeletonMaskAndShimmerDark(this,skeleton);
            txt_toolbar.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            parent_wish_list.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
        }

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        if (MainApplication.isConnected) {
            skeleton.showSkeleton();

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final NotificationsResponse generalResponse = RequestWrapper.getInstance().getNotifications(NotificationsActivity.this);
                    if (generalResponse.isStatus()) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                skeleton.showOriginal();
                                AdapterNotifications adapterWishList = new AdapterNotifications(generalResponse.getData());
                                recyclewish_list.setAdapter(adapterWishList);

                            }
                        });
                    } else
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                skeleton.showOriginal();
                                if(generalResponse.getCode().equalsIgnoreCase("401"))
                                {
                                    LoginDialog loginDialog = LoginDialog.newInstance();
                                    loginDialog.show(getSupportFragmentManager(), "login");
                                }else
                                Snackbar.make(recyclewish_list, generalResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        });
                }
            }).start();

        } else {
            Snackbar.make(recyclewish_list, this.getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
        }
    }
}