package hama.alsaygh.kw.activity.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.snackbar.Snackbar;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.activity.product.ProductDetailsActivity;
import hama.alsaygh.kw.adapter.cart.AdapterMyCart;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.CartResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.listener.OnMyCartListener;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.utils.Cons;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class CheckOutSummeryActivity extends BaseActivity implements OnMyCartListener {
    ImageView img_back;
    Button button8, button9;
    TextView txt_toolbar, textView126, textView31, tv_sub_total, textView111, tv_shipping, textView121, tv_total;
    LinearLayout parent_checkout, linearLayout4, liner_view2, liner_view3, liner6, liner_btn;
    View view22, view23, line;
    RecyclerView recyclerView_checkout;
    Skeleton skeleton;

    int id=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout);

        if(getIntent()!=null)
        {
            id=getIntent().getIntExtra("id",0);
        }

        img_back = findViewById(R.id.img_back);
        txt_toolbar = findViewById(R.id.txt_toolbar);
        parent_checkout = findViewById(R.id.parent_checkout);
        linearLayout4 = findViewById(R.id.linearLayout4);
        view22 = findViewById(R.id.view22);
        view23 = findViewById(R.id.view23);
        liner_view2 = findViewById(R.id.liner_view2);
        liner_view3 = findViewById(R.id.liner_view3);
        textView126 = findViewById(R.id.textView126);
        recyclerView_checkout = findViewById(R.id.recyclerView_checkout);
        liner6 = findViewById(R.id.liner6);
        textView31 = findViewById(R.id.textView31);
        tv_sub_total = findViewById(R.id.textView61);
        textView111 = findViewById(R.id.textView111);
        tv_shipping = findViewById(R.id.textView611);
        line = findViewById(R.id.line);
        textView121 = findViewById(R.id.textView121);
        tv_total = findViewById(R.id.textView661);
        button8 = findViewById(R.id.button8);
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
       img_back.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               onBackPressed();
           }
       });


        button9 = findViewById(R.id.button9);
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!SharedPreferenceConstant.getSharedPreferenceUserToken(CheckOutSummeryActivity.this).isEmpty()) {

                    Intent intent = new Intent(CheckOutSummeryActivity.this, CheckOutAddressActivity.class);
                    intent.putExtra("id",id);
                    startActivity(intent);

                } else {
                    LoginDialog loginDialog = LoginDialog.newInstance();
                    loginDialog.show(getSupportFragmentManager(), "login");
                }
            }
        });

        liner_btn = findViewById(R.id.liner_btn);
        recyclerView_checkout.setLayoutManager(new LinearLayoutManager(this));
        skeleton = SkeletonLayoutUtils.applySkeleton(recyclerView_checkout, R.layout.card_view_rv_my_cart, 1);
        Utils.getInstance().setSkeletonMaskAndShimmer(this, skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            this.setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(this, skeleton);
            txt_toolbar.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            parent_checkout.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            liner6.setBackgroundResource(R.drawable.back_mycart_liner_dark);
            textView31.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            tv_sub_total.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            textView111.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            tv_shipping.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            textView121.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));
            tv_total.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));
            liner_btn.setBackgroundColor(ContextCompat.getColor(this, R.color.dark11));
            line.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            view22.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            view23.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            liner_view2.setBackgroundResource(R.drawable.ic_ellips_dark);
            liner_view3.setBackgroundResource(R.drawable.ic_ellips_dark);
        } else {
            this.setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        getCart();


    }

    private void getCart() {
        if (MainApplication.isConnected) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    skeleton.showSkeleton();
                }
            });

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final CartResponse cartResponse = RequestWrapper.getInstance().getCart(CheckOutSummeryActivity.this,id);
                    if (cartResponse.isStatus()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                skeleton.showOriginal();

                                AdapterMyCart adapterMyCart = new AdapterMyCart(CheckOutSummeryActivity.this, cartResponse.getData().getCart(), getSupportFragmentManager(), CheckOutSummeryActivity.this);
                                recyclerView_checkout.setAdapter(adapterMyCart);

                                tv_sub_total.setText(String.valueOf(cartResponse.getData().getSub_total()));
                                tv_shipping.setText(String.valueOf(cartResponse.getData().getShipping()));
                                tv_total.setText(String.valueOf(cartResponse.getData().getTotal()));

                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                skeleton.showOriginal();
                                Snackbar.make(recyclerView_checkout, cartResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        });
                    }

                }
            }).start();

        } else {
            Snackbar.make(recyclerView_checkout, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCountChange() {
        getCart();
    }

    @Override
    public void onProductClick(Product product, int position) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra(Cons.PRODUCT, product);
        intent.putExtra(Cons.PRODUCT_ID, product.getId());
        startActivity(intent);
    }
}