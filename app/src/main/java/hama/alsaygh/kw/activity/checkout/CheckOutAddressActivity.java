package hama.alsaygh.kw.activity.checkout;

import android.Manifest;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.CitiesResponse;
import hama.alsaygh.kw.api.responce.CountriesResponse;
import hama.alsaygh.kw.model.country.City;
import hama.alsaygh.kw.model.country.Country;
import hama.alsaygh.kw.utils.CheckAndRequestPermission;
import hama.alsaygh.kw.utils.GPSTracker;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class CheckOutAddressActivity extends BaseActivity {
    ImageView img_back;
    Button button8, button9;
    TextView txt_toolbar, textView127;
    LinearLayout parent_a, linearLayout4, liner_view3, liner_btn;
    View view23;
    RadioButton rd_hama, rd_hand_by_hand;
    EditText edt_city, edt_street, edt_zip, edt_building_no;
    CheckBox ch_my_location, cb_save_my_address;
    Spinner sp_country, spinner_city;
    TextView tv_country, editText_time, tv_city;
    LinearLayout ll_hama, ll_hand_by_hand;
    RelativeLayout ll_country, ll_city;
    private Country selectedCountry;
    private City selectedCity;
    private CalendarView calender;
    private String city_area;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.check_out_delivery_method);

        if(getIntent()!=null)
        {
            id=getIntent().getIntExtra("id",0);
        }
        ll_city = findViewById(R.id.ll_city);
        ll_country = findViewById(R.id.ll_country);
        editText_time = findViewById(R.id.editText_time);
        calender = findViewById(R.id.calender);
        ll_hand_by_hand = findViewById(R.id.ll_hand_by_hand);
        ll_hama = findViewById(R.id.ll_hama);
        tv_country = findViewById(R.id.tv_country);
        tv_city = findViewById(R.id.tv_city);
        img_back = (ImageView) findViewById(R.id.img_back);
        txt_toolbar = (TextView) findViewById(R.id.txt_toolbar);
        parent_a = (LinearLayout) findViewById(R.id.parent_a);
        view23 = (View) findViewById(R.id.view23);
        linearLayout4 = (LinearLayout) findViewById(R.id.linearLayout4);
        textView127 = (TextView) findViewById(R.id.textView127);
        edt_city = (EditText) findViewById(R.id.editText99);
        sp_country = (Spinner) findViewById(R.id.editText19);
        spinner_city = findViewById(R.id.spinner_city);
        edt_street = (EditText) findViewById(R.id.editText1);
        edt_zip = (EditText) findViewById(R.id.editText);
        edt_building_no = (EditText) findViewById(R.id.editTex);
        liner_btn = (LinearLayout) findViewById(R.id.liner_btn);
        cb_save_my_address = (CheckBox) findViewById(R.id.checkBox);
        ch_my_location = (CheckBox) findViewById(R.id.checkBox2);
        button9 = (Button) findViewById(R.id.button9);
        button8 = (Button) findViewById(R.id.button8);
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (rd_hand_by_hand.isChecked()) {
                    String delivery_type = "hand_by_hand";

                    if (isValidHandByHand()) {
                        Calendar recipCalender = Calendar.getInstance();
                        recipCalender.setTimeInMillis(calender.getDate());
                        String receipt_at = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(recipCalender.getTime()) + " " + editText_time.getText().toString();

                        Intent intent = new Intent(CheckOutAddressActivity.this, CheckOutPaymentMethodActivity.class);
                        intent.putExtra("delivery_type", delivery_type);
                        intent.putExtra("receipt_at", receipt_at);
                        intent.putExtra("id",id);
                        startActivity(intent);

                    }

                } else {
                    String delivery_type = "hama";

                    if (isValidHama()) {

                        int country_id = selectedCountry.getId();
                        int city_id = selectedCity.getId();
                        String city = selectedCity.getName();
                        // String city = edt_city.getEditableText().toString();
                        String street = edt_street.getEditableText().toString();
                        String zip = edt_zip.getEditableText().toString();
                        String building = edt_building_no.getEditableText().toString();
                        int save_my_location_check = cb_save_my_address.isChecked() ? 1 : 0;

                        Intent intent = new Intent(CheckOutAddressActivity.this, CheckOutPaymentMethodActivity.class);
                        intent.putExtra("delivery_type", delivery_type);
                        intent.putExtra("country_id", country_id);
                        intent.putExtra("city", city);
                        intent.putExtra("street", street);
                        intent.putExtra("city_id", city_id);
                        intent.putExtra("zip", zip);
                        intent.putExtra("building", building);
                        intent.putExtra("save_my_location_check", save_my_location_check);
                        intent.putExtra("id",id);
                        startActivity(intent);
                    }
                }

            }
        });

        rd_hama = (RadioButton) findViewById(R.id.radioButton5);
        rd_hand_by_hand = (RadioButton) findViewById(R.id.radioButton);
        ch_my_location = (CheckBox) findViewById(R.id.checkBox2);
        cb_save_my_address = (CheckBox) findViewById(R.id.checkBox);
        liner_view3 = (LinearLayout) findViewById(R.id.liner_view3);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            this.setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            txt_toolbar.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            parent_a.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            img_back.setImageResource(R.drawable.ic_back_icon_dark);
            textView127.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
            sp_country.setBackgroundResource(R.drawable.back_zakat_dark);
            spinner_city.setBackgroundResource(R.drawable.back_zakat_dark);
            tv_country.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            tv_city.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            edt_city.setBackgroundResource(R.drawable.back_zakat_dark);
            edt_city.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            edt_street.setBackgroundResource(R.drawable.back_zakat_dark);
            edt_street.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            edt_zip.setBackgroundResource(R.drawable.back_zakat_dark);
            edt_zip.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            edt_building_no.setBackgroundResource(R.drawable.back_zakat_dark);
            edt_building_no.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            liner_btn.setBackgroundColor(ContextCompat.getColor(this, R.color.dark11));
            edt_zip.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            ll_country.setBackgroundResource(R.drawable.back_zakat_dark);
            ll_city.setBackgroundResource(R.drawable.back_zakat_dark);
            editText_time.setBackgroundResource(R.drawable.back_zakat_dark);
            editText_time.setHintTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));

            rd_hama.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));
            rd_hand_by_hand.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));
            cb_save_my_address.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));
            ch_my_location.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));

            ch_my_location.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_navigation)));
            cb_save_my_address.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_navigation)));
            rd_hand_by_hand.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_navigation)));
            rd_hand_by_hand.setHighlightColor(ContextCompat.getColor(this, R.color.color_navigation));
            view23.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in_dark));
            liner_view3.setBackgroundResource(R.drawable.ic_ellips_dark);

        } else {
            this.setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        rd_hama.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    rd_hand_by_hand.setChecked(false);
                    ll_hama.setVisibility(View.VISIBLE);
                } else {
                    ll_hama.setVisibility(View.GONE);
                }

            }
        });

        rd_hand_by_hand.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    rd_hama.setChecked(false);
                    ll_hand_by_hand.setVisibility(View.VISIBLE);

                } else {
                    ll_hand_by_hand.setVisibility(View.GONE);

                }

            }
        });
//city
        tv_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner_city.performClick();
                tv_city.setVisibility(View.GONE);

                spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        City country = (City) spinner_city.getSelectedItem();
                        if (country.getId() == -1) {
                            tv_city.setVisibility(View.VISIBLE);
                            selectedCity = null;
                        } else {
                            selectedCity = (City) spinner_city.getSelectedItem();
                            tv_city.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        tv_city.setVisibility(View.VISIBLE);
                    }
                });

            }
        });
        ///country

        new Thread(new Runnable() {
            @Override
            public void run() {

                final CountriesResponse countriesResponse = RequestWrapper.getInstance().getCountry(CheckOutAddressActivity.this);
                if (countriesResponse.isStatus()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            List<Country> countryList = new ArrayList<>(countriesResponse.getData());
                            Country country = new Country();
                            country.setId(-1);
                            country.setName("");
                            countryList.add(0, country);

                            ArrayAdapter<Country> arrayCountry = new ArrayAdapter<Country>(CheckOutAddressActivity.this, android.R.layout.simple_spinner_dropdown_item, countryList);
                            arrayCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_country.setAdapter(arrayCountry);
                        }
                    });
                }
            }
        }).start();

        tv_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sp_country.performClick();
                tv_country.setVisibility(View.GONE);
                //tv_country.setEnabled(false);
                sp_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Country country = (Country) sp_country.getSelectedItem();
                        if (country.getId() == -1) {
                            tv_country.setVisibility(View.VISIBLE);
                            selectedCountry = null;
                            selectedCity = null;
                            getCites(-1);
                        } else {
                            selectedCountry = (Country) sp_country.getSelectedItem();
                            tv_country.setVisibility(View.GONE);
                            getCites(selectedCountry.getId());
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        tv_country.setVisibility(View.VISIBLE);
                    }
                });

            }
        });


        editText_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.HOUR_OF_DAY);
                int month = calendar.get(Calendar.MINUTE);
                TimePickerDialog datePickerDialog = new TimePickerDialog(CheckOutAddressActivity.this, android.R.style.Theme_Holo_Dialog_MinWidth
                        , new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {


                        Calendar time = Calendar.getInstance();
                        time.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        time.set(Calendar.MINUTE, minute);
                        time.set(Calendar.SECOND, hourOfDay);

                        String timeStr = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH).format(time.getTime());
                        editText_time.setText(timeStr);

                    }
                }, year, month, true);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });


        ch_my_location.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                    if (!CheckAndRequestPermission.hasPermissions(CheckOutAddressActivity.this, permissions)) {
                        CheckAndRequestPermission.requestLocation(CheckOutAddressActivity.this);
                    } else {
                        getMyLocation();
                    }
                }
            }
        });


    }

    private void getCites(int id) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                final CitiesResponse countriesResponse = RequestWrapper.getInstance().getCities(CheckOutAddressActivity.this, id);
                if (countriesResponse.isStatus()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            List<City> countryList = new ArrayList<>(countriesResponse.getData());
                            City country = new City();
                            country.setId(-1);
                            country.setName("");
                            countryList.add(0, country);

                            ArrayAdapter<City> arrayCountry = new ArrayAdapter<City>(CheckOutAddressActivity.this, android.R.layout.simple_spinner_dropdown_item, countryList);
                            arrayCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner_city.setAdapter(arrayCountry);

                            if (city_area != null && !city_area.isEmpty()) {
                                int index2 = -1;
                                for (int i = 0; i < spinner_city.getAdapter().getCount(); i++) {
                                    City city = (City) spinner_city.getAdapter().getItem(i);
                                    if (city.getName().equalsIgnoreCase(city_area)) ;
                                    {
                                        index2 = i;
                                        break;
                                    }
                                }
                                if (index2 != -1)
                                    spinner_city.setSelection(index2);
                            }
                        }
                    });
                }
            }
        }).start();

    }

    private void getMyLocation() {

        GPSTracker tracker = new GPSTracker(this, new android.location.LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                getAddressFromLocation(new LatLng(location.getLatitude(), location.getLongitude()));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });
        if (tracker.canGetLocation()) {

            double latitude = tracker.getLatitude();
            double longitude = tracker.getLongitude();

            if (latitude != 0 && longitude != 0) {

                getAddressFromLocation(new LatLng(latitude, longitude));
                tracker.stopUsingGPS();
            }
        } else
            tracker.showSettingsAlert(this);

    }

    private void getAddressFromLocation(LatLng point) {
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> list = new ArrayList<>();
        try {
            list = geocoder.getFromLocation(point.latitude, point.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        setAddressOnly(list);

    }

    private void setAddressOnly(List<Address> list) {
        if (!list.isEmpty()) {
            String address = list.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city_area = list.get(0).getLocality();
            String street = address;
            String zip = list.get(0).getPostalCode();
            edt_city.setText(city_area);
            edt_street.setText(street);
            edt_zip.setText(zip);
            edt_building_no.setText(list.get(0).getFeatureName());
            if (sp_country.getAdapter() != null) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        int index = -1;
                        for (int i = 0; i < sp_country.getAdapter().getCount(); i++) {
                            Country country = (Country) sp_country.getAdapter().getItem(i);
                            if (country.getName().equalsIgnoreCase(list.get(0).getCountryCode())) ;
                            {
                                index = i;
                                break;
                            }
                        }
                        if (index != -1) {
                            int finalIndex = index;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    sp_country.setSelection(finalIndex);
                                }
                            });
                        }
                    }

                }).start();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && !CheckAndRequestPermission.isFoundPermissionDenied(grantResults)) {
            getMyLocation();
        }
    }

    private boolean isValidHama() {
        boolean isValid = true;

        if (edt_building_no.getEditableText().toString().isEmpty()) {
            isValid = false;
            edt_building_no.setBackgroundResource(R.drawable.back_edittext_red);
        } else {

            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this))
                edt_building_no.setBackgroundResource(R.drawable.back_zakat_dark);
            else
                edt_building_no.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        if (edt_street.getEditableText().toString().isEmpty()) {
            isValid = false;
            edt_street.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this))
                edt_street.setBackgroundResource(R.drawable.back_zakat_dark);
            else
                edt_street.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        if (edt_zip.getEditableText().toString().isEmpty()) {
            isValid = false;
            edt_zip.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this))
                edt_zip.setBackgroundResource(R.drawable.back_zakat_dark);
            else
                edt_zip.setBackgroundResource(R.drawable.back_ground_edit_text);
        }

        if (selectedCountry == null) {
            isValid = false;
            ll_country.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this))
                ll_country.setBackgroundResource(R.drawable.back_zakat_dark);
            else
                ll_country.setBackgroundResource(R.drawable.back_ground_edit_text);
        }

        if (selectedCity == null) {
            isValid = false;
            ll_city.setBackgroundResource(R.drawable.back_edittext_red);
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this))
                ll_city.setBackgroundResource(R.drawable.back_zakat_dark);
            else
                ll_city.setBackgroundResource(R.drawable.back_ground_edit_text);
        }

        return isValid;
    }


    private boolean isValidHandByHand() {
        boolean isValid = true;

        if (editText_time.getText().toString().isEmpty()) {
            isValid = false;
            editText_time.setBackgroundResource(R.drawable.back_edittext_red);
        } else {

            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this))
                editText_time.setBackgroundResource(R.drawable.back_zakat_dark);
            else
                editText_time.setBackgroundResource(R.drawable.back_ground_edit_text);
        }


        return isValid;
    }


}