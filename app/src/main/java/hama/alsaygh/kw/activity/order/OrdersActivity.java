package hama.alsaygh.kw.activity.order;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.adapter.order.AdapterPagerMyOrder;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class OrdersActivity extends BaseActivity {
    ImageView imageback;
    Button orderTracking;
    TabLayout tabLayout;
    TabItem complited, pending, canceled;
    ViewPager viewPager;
    TextView txt_toolbar;
    LinearLayout parent_my_order;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_order);


        tabLayout = (TabLayout) findViewById(R.id.tab_myorder);
        complited = (TabItem) findViewById(R.id.complete_tab);
        pending = (TabItem) findViewById(R.id.pending_order_tab);
        canceled = (TabItem) findViewById(R.id.cancel_tab);
        viewPager = (ViewPager) findViewById(R.id.view_pager_myorder);

        imageback = (ImageView) findViewById(R.id.back_myorder);
        imageback.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                }
        );


        txt_toolbar = (TextView) findViewById(R.id.textView9);
        parent_my_order = (LinearLayout) findViewById(R.id.parent_my_order);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            this.setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
           imageback.setImageResource(R.drawable.ic_back_icon_dark);
        } else {
            this.setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        AdapterPagerMyOrder adapterPagerMyOrder = new AdapterPagerMyOrder(this,getSupportFragmentManager());
        viewPager.setAdapter(adapterPagerMyOrder);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setTabIndicatorFullWidth(false);


    }
}