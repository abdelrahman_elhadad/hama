package hama.alsaygh.kw.activity.order;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.base.BaseActivity;
import hama.alsaygh.kw.activity.checkout.PayOrderActivity;
import hama.alsaygh.kw.adapter.order.OrderItemList;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.OrderResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.db.AppDatabase;
import hama.alsaygh.kw.db.table.User;
import hama.alsaygh.kw.dialog.LoginDialog;
import hama.alsaygh.kw.model.order.Order;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class OrderDetailsActivity extends BaseActivity {
    ImageView imageback;

    TextView tv_shipping_price, tv_order_no, tv_payment_type, tv_delivery_type, textView9,
            tv_username, tv_email, tv_phone, tv_address, tv_address_title, tv_item_title, tv_item_price, tv_gift_note, tv_total_price,
            tvPendingStr, tv_confirm_srt, tv_package, tv_delivery, tv_complete, tv_order_placed, tv_order_confirmed, tv_being_packging,
            tv_out_of_delivery, tv_order_complete, tv_track_order_sheet, tv_track_order, tv_status;

    CoordinatorLayout ll_parent;
    RelativeLayout ll_main;
    LinearLayout ll_top;
    ImageView ll_pending_img, ll_order_confirmed, ll_peing_packeging, ll_out_of_delivery, ll_complete,
            imageView474, imageView36;
    View viewPending, viewConfirm, viewLineConfirm, viewPackaging, viewLinePackage,
            viewDelivery, viewLineDelivery, viewComplete, viewLineComplete, viewLine;

    Skeleton skeleton;

    Order order;
    int order_id;
    User user;
    RecyclerView rv_product;
    ConstraintLayout con_tracking;

    Button cancel_order, pay_order, btn_confirm_delivery;
    Toolbar toolbar;

    BottomSheetBehavior sheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);


        if (getIntent() != null)
            order_id = getIntent().getIntExtra("order_id", -1);

        toolbar = findViewById(R.id.toolbar);
        tv_gift_note = findViewById(R.id.tv_gift_note);
        tv_item_price = findViewById(R.id.tv_item_price);
        tv_total_price = findViewById(R.id.tv_total_price);
        tv_shipping_price = findViewById(R.id.tv_shipping_price);
        tv_item_title = findViewById(R.id.tv_item_title);
        tv_order_no = findViewById(R.id.tv_order_no);
        tv_payment_type = findViewById(R.id.tv_payment_type);
        tv_delivery_type = findViewById(R.id.tv_delivery_type);
        tv_username = findViewById(R.id.tv_username);
        tv_email = findViewById(R.id.tv_email);
        tv_phone = findViewById(R.id.tv_phone);
        tv_address = findViewById(R.id.tv_address);
        tv_address_title = findViewById(R.id.tv_address_title);
        ll_parent = findViewById(R.id.ll_parent);
        ll_main = findViewById(R.id.ll_main);
        imageback = findViewById(R.id.back_myorder);
        rv_product = findViewById(R.id.rv_product);
        textView9 = findViewById(R.id.textView9);
        con_tracking = findViewById(R.id.con_tracking);
        imageView36 = findViewById(R.id.imageView36);
        tv_track_order = findViewById(R.id.tv_track_order);
        tv_status = findViewById(R.id.tv_status);
        pay_order = findViewById(R.id.pay_order);
        btn_confirm_delivery = findViewById(R.id.btn_confirm_delivery);
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rv_product.setLayoutManager(new LinearLayoutManager(this));

        LinearLayout bottom_sheet = findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        ll_top = bottom_sheet.findViewById(R.id.ll_top);
        tvPendingStr = bottom_sheet.findViewById(R.id.tvPendingStr);
        ll_pending_img = bottom_sheet.findViewById(R.id.ll_pending_img);
        viewPending = bottom_sheet.findViewById(R.id.viewPending);
        viewConfirm = bottom_sheet.findViewById(R.id.viewConfirm);
        tv_confirm_srt = bottom_sheet.findViewById(R.id.tv_confirm_srt);
        ll_order_confirmed = bottom_sheet.findViewById(R.id.ll_order_confirmed);
        ll_peing_packeging = bottom_sheet.findViewById(R.id.ll_peing_packeging);
        tv_package = bottom_sheet.findViewById(R.id.tv_package);
        viewPackaging = bottom_sheet.findViewById(R.id.viewPackaging);
        ll_out_of_delivery = bottom_sheet.findViewById(R.id.ll_out_of_delivery);
        tv_delivery = bottom_sheet.findViewById(R.id.tv_delivery);
        viewDelivery = bottom_sheet.findViewById(R.id.viewDelivery);
        ll_complete = bottom_sheet.findViewById(R.id.ll_complete);
        tv_complete = bottom_sheet.findViewById(R.id.tv_complete);
        viewComplete = bottom_sheet.findViewById(R.id.viewComplete);
        viewLineConfirm = bottom_sheet.findViewById(R.id.viewLineConfirm);
        viewLineDelivery = bottom_sheet.findViewById(R.id.viewLineDelivery);
        viewLinePackage = bottom_sheet.findViewById(R.id.viewLinePackage);
        viewLineComplete = bottom_sheet.findViewById(R.id.viewLineComplete);
        cancel_order = bottom_sheet.findViewById(R.id.cancel_order);
        viewLine = bottom_sheet.findViewById(R.id.viewLine);
        tv_order_placed = bottom_sheet.findViewById(R.id.tv_order_placed);
        tv_order_confirmed = bottom_sheet.findViewById(R.id.tv_order_confirmed);
        tv_being_packging = bottom_sheet.findViewById(R.id.tv_being_packging);
        tv_out_of_delivery = bottom_sheet.findViewById(R.id.tv_out_of_delivery);
        tv_order_complete = bottom_sheet.findViewById(R.id.tv_order_complete);
        imageView474 = bottom_sheet.findViewById(R.id.imageView474);
        tv_track_order_sheet = bottom_sheet.findViewById(R.id.tv_track_order_sheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);


        skeleton = SkeletonLayoutUtils.createSkeleton(ll_main);
        Utils.getInstance().setSkeletonMaskAndShimmer(this, skeleton);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(this)) {
            this.setTheme(R.style.darktheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            Utils.getInstance().setSkeletonMaskAndShimmerDark(this, skeleton);
//            toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
//            ll_parent.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
           // bottom_sheet.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            con_tracking.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in));
            imageback.setImageResource(R.drawable.ic_back_icon_dark);

           // textView9.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));

//            tv_order_placed.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv_order_confirmed.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv_being_packging.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv_out_of_delivery.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv_order_complete.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv_track_order_sheet.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));
//            tv_track_order.setTextColor(ContextCompat.getColor(this, R.color.whiteColor));

//            tv_complete.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
//            tv_delivery.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
//            tv_package.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
//            tv_confirm_srt.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));
//            tvPendingStr.setTextColor(ContextCompat.getColor(this, R.color.sign_in_dark));

            ll_complete.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_complete_dark));
            ll_out_of_delivery.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_delivery_dark));
            ll_peing_packeging.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_package_dark));
            ll_order_confirmed.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_confirm_dark));
            ll_pending_img.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_pending_dark));

            viewComplete.setBackgroundResource(R.drawable.ic_ellipse_71);
            viewDelivery.setBackgroundResource(R.drawable.ic_ellipse_71);
            viewPackaging.setBackgroundResource(R.drawable.ic_ellipse_71);
            viewConfirm.setBackgroundResource(R.drawable.ic_ellipse_71);
            viewPending.setBackgroundResource(R.drawable.ic_ellipse_71);

            viewLine.setBackgroundColor(ContextCompat.getColor(this, R.color.sign_in_dark));

            cancel_order.setBackgroundResource(R.drawable.back_liner_search_dark);
         //   cancel_order.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));

            pay_order.setBackgroundResource(R.drawable.back_liner_search_dark);
           // pay_order.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));

            btn_confirm_delivery.setBackgroundResource(R.drawable.back_liner_search_dark);
           // btn_confirm_delivery.setTextColor(ContextCompat.getColor(this, R.color.color_navigation));

            imageView474.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_down_dark));
            imageView36.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_dawn_dark));
        } else {
            this.setTheme(R.style.AppTheme);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        con_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        ll_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

            }
        });

        pay_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), PayOrderActivity.class);
                intent.putExtra("order_id", order.getId() + "");
                startActivity(intent);
            }
        });

        btn_confirm_delivery.setOnClickListener(view -> {


            if (MainApplication.isConnected) {
                skeleton.showSkeleton();
                sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        AppDatabase appDatabase = AppDatabase.newInstance(OrderDetailsActivity.this);
                        List<User> userList = appDatabase.userDao().getAll();
                        if (userList != null && !userList.isEmpty()) {
                            user = userList.get(0);

                            final OrderResponse productResponse = RequestWrapper.getInstance().deliveredOrder(OrderDetailsActivity.this, order_id);
                            if (productResponse.isStatus()) {
                                order = productResponse.getData();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        skeleton.showOriginal();
                                        setOrderDetails();
                                        Intent intent = new Intent(OrderDetailsActivity.this, OrderRateActivity.class);
                                        intent.putExtra("order", order);
                                        startActivity(intent);
                                    }
                                });
                            } else {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        skeleton.showOriginal();

                                        if (productResponse.getCode().equalsIgnoreCase("401")) {
                                            LoginDialog loginDialog = LoginDialog.newInstance();
                                            loginDialog.show(getSupportFragmentManager(), "login");
                                        } else
                                            Snackbar.make(ll_parent, productResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    skeleton.showOriginal();

                                    LoginDialog loginDialog = LoginDialog.newInstance();
                                    loginDialog.show(getSupportFragmentManager(), "login");
                                }
                            });
                        }
                    }
                }).start();

            } else {
                Snackbar.make(ll_parent, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
            }
        });
        cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (MainApplication.isConnected) {
                    skeleton.showSkeleton();
                    sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            AppDatabase appDatabase = AppDatabase.newInstance(OrderDetailsActivity.this);
                            List<User> userList = appDatabase.userDao().getAll();
                            if (userList != null && !userList.isEmpty()) {
                                user = userList.get(0);

                                final OrderResponse productResponse = RequestWrapper.getInstance().cancelOrder(OrderDetailsActivity.this, order_id);
                                if (productResponse.isStatus()) {
                                    order = productResponse.getData();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            skeleton.showOriginal();
                                            setOrderDetails();
                                        }
                                    });
                                } else {

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            skeleton.showOriginal();

                                            if (productResponse.getCode().equalsIgnoreCase("401")) {
                                                LoginDialog loginDialog = LoginDialog.newInstance();
                                                loginDialog.show(getSupportFragmentManager(), "login");
                                            } else
                                                Snackbar.make(ll_parent, productResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        skeleton.showOriginal();

                                        LoginDialog loginDialog = LoginDialog.newInstance();
                                        loginDialog.show(getSupportFragmentManager(), "login");
                                    }
                                });
                            }
                        }
                    }).start();

                } else {
                    Snackbar.make(ll_parent, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }
            }
        });


        skeleton.showSkeleton();
        if (MainApplication.isConnected) {
            skeleton.showSkeleton();
            new Thread(new Runnable() {
                @Override
                public void run() {

                    AppDatabase appDatabase = AppDatabase.newInstance(OrderDetailsActivity.this);
                    user = appDatabase.userDao().getAll().get(0);

                    final OrderResponse productResponse = RequestWrapper.getInstance().getOrder(OrderDetailsActivity.this, order_id);
                    if (productResponse.isStatus()) {
                        order = productResponse.getData();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                skeleton.showOriginal();
                                setOrderDetails();
                            }
                        });
                    } else {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                skeleton.showOriginal();

                                if (productResponse.getCode().equalsIgnoreCase("401")) {
                                    LoginDialog loginDialog = LoginDialog.newInstance();
                                    loginDialog.show(getSupportFragmentManager(), "login");
                                } else
                                    Snackbar.make(ll_parent, productResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }).start();

        } else {
            Snackbar.make(ll_parent, getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
        }

    }

    private void setOrderDetails() {
        if (order != null) {

            tv_order_no.setText(order.getOrder_number());

            String total = order.getTotal() + " " + order.getCurrency();
            tv_total_price.setText(total);

            String tax = order.getTax() + " % ";
            tv_shipping_price.setText(tax);

            String itemsPrice = order.getSub_total1() + " " + order.getCurrency();
            tv_item_price.setText(itemsPrice);

            String items = getString(R.string.item) + " (" + order.getItems_count() + ")";
            tv_item_title.setText(items);

            String deliveryType = order.getDelivery_type_trans();
            tv_status.setText(order.getStatus_trans());

            tv_delivery_type.setText(deliveryType);
            tv_payment_type.setText(order.getCreated_at());
            tv_gift_note.setText(order.getGift_note());


            if (order.getDelivery_type().equalsIgnoreCase("hama")) {
                String deliveryTitle = getString(R.string.address);
                String address = "";
                if (order.getDelivery() != null)
                    address = order.getDelivery().getCountry().getName() + "," + order.getDelivery().getCity() + "," + order.getDelivery().getStreet() + "," + order.getDelivery().getBuilding_no() + "," + order.getDelivery().getZip_code();
                tv_address_title.setText(deliveryTitle);
                tv_address.setText(address);
            } else {
                String deliveryTitle = getString(R.string.receive_at);
                tv_address_title.setText(deliveryTitle);
                tv_address.setText(order.getReceipt_at());
            }


            String name = user.getF_name() + " " + user.getL_name();
            tv_username.setText(name);
            tv_email.setText(user.getEmail());
            tv_phone.setText(user.getMobile());


            OrderItemList orderItemList = new OrderItemList(this, order.getItems(), order.getCurrency());
            rv_product.setAdapter(orderItemList);


        }

        if (order.getStatus().equalsIgnoreCase("unpaid")) {
            cancel_order.setVisibility(View.VISIBLE);
            pay_order.setVisibility(View.VISIBLE);
        } else
            pay_order.setVisibility(View.GONE);

        if (order.isIs_canceled()) {
            cancel_order.setVisibility(View.GONE);
        } else if (order.isIs_pending()) {
            setPendingTrack();
            cancel_order.setVisibility(View.VISIBLE);
        } else if (order.isIs_in_progress()) {
            setInProgressTrack();
            cancel_order.setVisibility(View.GONE);
        } else if (order.isIs_in_delivery()) {
            setDeliveryTrack();
            cancel_order.setVisibility(View.GONE);
        } else if (order.isIs_completed() || order.isIs_delivered()) {
            setCompleteTrack();
            cancel_order.setVisibility(View.GONE);
        }


        if (!order.isIs_delivered() && order.isIs_completed()) {
            btn_confirm_delivery.setVisibility(View.VISIBLE);
        } else
            btn_confirm_delivery.setVisibility(View.GONE);

    }


    private void setPendingTrack() {


        tvPendingStr.setText(order.getPending_str());
        ll_pending_img.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_pending_progress));
        viewPending.setBackgroundResource(R.drawable.order_tracking);
    }

    private void setInProgressTrack() {


        setPendingTrack();

        tv_confirm_srt.setText(order.getIn_progress_str());
        viewConfirm.setBackgroundResource(R.drawable.order_tracking);
        ll_order_confirmed.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_confirm_progress));
        viewLineConfirm.setVisibility(View.VISIBLE);

    }

    private void setPackageTrack() {


        setInProgressTrack();

        tv_package.setText("--");
        viewPackaging.setBackgroundResource(R.drawable.order_tracking);
        ll_peing_packeging.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_package_progress));
        viewLinePackage.setVisibility(View.VISIBLE);

    }

    private void setDeliveryTrack() {


        setPackageTrack();
        tv_delivery.setText(order.getIn_delivery_str());
        viewDelivery.setBackgroundResource(R.drawable.order_tracking);
        ll_out_of_delivery.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_delivery_progress));
        viewLineDelivery.setVisibility(View.VISIBLE);

    }

    private void setCompleteTrack() {

        setDeliveryTrack();
        tv_complete.setText(order.getCompleted_str());
        viewComplete.setBackgroundResource(R.drawable.order_tracking);
        ll_complete.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_complete_progress));
        viewLineComplete.setVisibility(View.VISIBLE);

    }

}