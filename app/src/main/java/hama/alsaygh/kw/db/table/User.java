package hama.alsaygh.kw.db.table;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import hama.alsaygh.kw.model.country.Country;

@Entity
public class User implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private int id;

    @ColumnInfo(name = "f_name")
    @SerializedName("f_name")
    private String f_name;

    @ColumnInfo(name = "l_name")
    @SerializedName("l_name")
    private String l_name;

    @ColumnInfo(name = "email")
    @SerializedName("email")
    private String email;

    @ColumnInfo(name = "gender")
    @SerializedName("gender")
    private String gender;

    @ColumnInfo(name = "birth_date")
    @SerializedName("birth_date")
    private String birth_date;

    @ColumnInfo(name = "language")
    @SerializedName("language")
    private String language;

    @ColumnInfo(name = "appearance_mode")
    @SerializedName("appearance_mode")
    private boolean appearance_mode;

    @ColumnInfo(name = "general_notifications")
    @SerializedName("general_notifications")
    private boolean general_notifications;

    @ColumnInfo(name = "new_offers_notification")
    @SerializedName("new_offers_notification")
    private boolean new_offers_notification;

    @ColumnInfo(name = "order_notification")
    @SerializedName("order_notification")
    private boolean order_notification;

    @ColumnInfo(name = "event_notification")
    @SerializedName("event_notification")
    private boolean event_notification;

    @ColumnInfo(name = "adv_notification")
    @SerializedName("adv_notification")
    private boolean adv_notification;

    @ColumnInfo(name = "payment_method_notification")
    @SerializedName("payment_method_notification")
    private boolean payment_method_notification;

    @Embedded(prefix = "country_")
    @SerializedName("country")
    private Country country;

    @ColumnInfo(name = "avatar")
    @SerializedName("avatar")
    private String avatar;

    @ColumnInfo(name = "mobile")
    @SerializedName("mobile")
    private String mobile;

    @ColumnInfo(name = "identifier_type")
    @SerializedName("identifier_type")
    private String identifier_type = "";

    @ColumnInfo(name = "identifier")
    @SerializedName("identifier")
    private String identifier = "";

    @ColumnInfo(name = "identifier_image")
    @SerializedName("identifier_image")
    private String identifier_image = "";

    public String getIdentifier_type() {
        if(identifier_type==null)
            identifier_type="";
        return identifier_type;
    }

    public void setIdentifier_type(String identifier_type) {
        this.identifier_type = identifier_type;
    }

    public String getIdentifier() {
        if(identifier==null)
            identifier="";
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier_image() {
        return identifier_image;
    }

    public void setIdentifier_image(String identifier_image) {
        this.identifier_image = identifier_image;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAvatar() {

        if (avatar != null)
            avatar = avatar.replace("\\", "");

        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isAppearance_mode() {
        return appearance_mode;
    }

    public void setAppearance_mode(boolean appearance_mode) {
        this.appearance_mode = appearance_mode;
    }

    public boolean isGeneral_notifications() {
        return general_notifications;
    }

    public void setGeneral_notifications(boolean general_notifications) {
        this.general_notifications = general_notifications;
    }

    public boolean isNew_offers_notification() {
        return new_offers_notification;
    }

    public void setNew_offers_notification(boolean new_offers_notification) {
        this.new_offers_notification = new_offers_notification;
    }

    public boolean isOrder_notification() {
        return order_notification;
    }

    public void setOrder_notification(boolean order_notification) {
        this.order_notification = order_notification;
    }

    public boolean isEvent_notification() {
        return event_notification;
    }

    public void setEvent_notification(boolean event_notification) {
        this.event_notification = event_notification;
    }

    public boolean isAdv_notification() {
        return adv_notification;
    }

    public void setAdv_notification(boolean adv_notification) {
        this.adv_notification = adv_notification;
    }

    public boolean isPayment_method_notification() {
        return payment_method_notification;
    }

    public void setPayment_method_notification(boolean payment_method_notification) {
        this.payment_method_notification = payment_method_notification;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        if (gender == null)
            gender = "";
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
