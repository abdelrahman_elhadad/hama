package hama.alsaygh.kw.db.dao;

import hama.alsaygh.kw.db.table.OnBoard;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface OnBoardDao {

    @Query("SELECT * FROM onboard")
    List<OnBoard> getAll();

    @Query("SELECT * FROM onboard WHERE id IN (:ids)")
    List<OnBoard> loadAllByIds(int[] ids);

    @Insert
    void insertAll(OnBoard... onBoards);

    @Delete
    void delete(OnBoard onBoard);

    @Update
    void update(OnBoard onBoard);

    @Query("DELEtE from onboard")
    void deleteAll();

}
