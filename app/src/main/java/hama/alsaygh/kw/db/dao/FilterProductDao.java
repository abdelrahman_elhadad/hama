package hama.alsaygh.kw.db.dao;

import hama.alsaygh.kw.db.table.FilterProduct;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface FilterProductDao {

    @Query("SELECT * FROM filterproduct")
    List<FilterProduct> getAll();

    @Query("SELECT * FROM filterproduct WHERE id IN (:ids)")
    List<FilterProduct> loadAllByIds(int[] ids);

    @Insert
    void insertAll(FilterProduct... filterProducts);

    @Delete
    void delete(FilterProduct filterProduct);

    @Update
    void update(FilterProduct filterProduct);

    @Query("DELEtE from filterproduct")
    void deleteAll();

}
