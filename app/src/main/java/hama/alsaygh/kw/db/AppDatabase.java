package hama.alsaygh.kw.db;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import hama.alsaygh.kw.db.dao.FilterProductDao;
import hama.alsaygh.kw.db.dao.OnBoardDao;
import hama.alsaygh.kw.db.dao.UserDao;
import hama.alsaygh.kw.db.table.FilterProduct;
import hama.alsaygh.kw.db.table.OnBoard;
import hama.alsaygh.kw.db.table.User;

@Database(entities = {User.class, OnBoard.class, FilterProduct.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDao userDao();

    public abstract OnBoardDao onBoardDao();

    public abstract FilterProductDao filterProductDao();


    public static AppDatabase appDatabase;

    public static synchronized AppDatabase newInstance(Context context) {

        if (appDatabase == null) {

            appDatabase = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "HAMA")
                    .addMigrations(MIGRATION_1_2)
                    .build();
        }
        return appDatabase;
    }

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Since we didn't alter the table, there's nothing else to do here.

            database.execSQL("ALTER TABLE 'user' ADD COLUMN 'identifier_type' TEXT ");
            database.execSQL("ALTER TABLE 'user' ADD COLUMN 'identifier' TEXT ");
            database.execSQL("ALTER TABLE 'user' ADD COLUMN 'identifier_image' TEXT ");


            Log.d("VROM","Migration");
        }
    };

}
