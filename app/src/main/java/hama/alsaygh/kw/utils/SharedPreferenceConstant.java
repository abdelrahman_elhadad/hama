package hama.alsaygh.kw.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferenceConstant {

    private static final String SHARED_PREFERENCE_USER_ACCOUNT = "USER_ACCOUNT";
    private static final String SHARED_PREFERENCE_APP = "USER_APP";
    private static final String KEY_USER_ACCOUNT_TOKEN = "Token";
    private static final String KEY_USER_ACCOUNT_ON_BOARDING = "onBoarding";


    public static boolean getSharedPreferenceDarkMode(Context context) {
        if (context != null) {
            SharedPreferences editor = context.getSharedPreferences("DarkMode", MODE_PRIVATE);
            return editor.getBoolean("isDark", false);
        }
        return false;
    }

    public static void setSharedPreferenceDarkMode(Context context, boolean isChecked) {

        if (context != null) {
            SharedPreferences.Editor editor = context.getSharedPreferences("DarkMode", MODE_PRIVATE).edit();
            editor.putBoolean("isDark", isChecked);
            editor.apply();
            Log.i("lll", "isDark" + isChecked + "");
        }
    }

    public static void setSharedPreferenceUserToken(Context context, String token) {

        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(SharedPreferenceConstant
                    .SHARED_PREFERENCE_USER_ACCOUNT, MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(KEY_USER_ACCOUNT_TOKEN, token);
            editor.apply();
        }
    }


    public static String getSharedPreferenceUserToken(Context context) {

        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(SharedPreferenceConstant
                    .SHARED_PREFERENCE_USER_ACCOUNT, MODE_PRIVATE);

            return preferences.getString(KEY_USER_ACCOUNT_TOKEN, "");
        }
        return "";
    }

    public static void setSharedPreferenceOnBoarding(Context context, boolean onBoarding) {

        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(SharedPreferenceConstant
                    .SHARED_PREFERENCE_APP, MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(KEY_USER_ACCOUNT_ON_BOARDING, onBoarding);
            editor.apply();
        }
    }

    public static boolean getSharedPreferenceOnBoarding(Context context) {

        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(SharedPreferenceConstant
                    .SHARED_PREFERENCE_APP, MODE_PRIVATE);

            return preferences.getBoolean(KEY_USER_ACCOUNT_ON_BOARDING, false);
        }
        return false;
    }


    public static void clearSharedPreference(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SharedPreferenceConstant.SHARED_PREFERENCE_USER_ACCOUNT,
                MODE_PRIVATE);
        preferences.edit().clear().apply();
    }


}
