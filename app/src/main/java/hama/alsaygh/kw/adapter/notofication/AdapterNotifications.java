package hama.alsaygh.kw.adapter.notofication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.notifications.Notifications;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class AdapterNotifications extends RecyclerView.Adapter<AdapterNotifications.Holder> {


    private List<Notifications> notifications;

    public AdapterNotifications(List<Notifications> notifications) {
        this.notifications = notifications;
    }

    @NonNull
    @Override
    public AdapterNotifications.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_notification1, parent, false);
        return new AdapterNotifications.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterNotifications.Holder holder, int position) {
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(holder.tv_desc.getContext())) {
            holder.tv_title.setTextColor(ContextCompat.getColor(holder.tv_desc.getContext(), R.color.whiteColor));
            holder.tv_desc.setTextColor(ContextCompat.getColor(holder.tv_desc.getContext(), R.color.whiteColor));

        } else {
            holder.tv_title.setTextColor(ContextCompat.getColor(holder.tv_desc.getContext(), R.color.blackcolor1));
            holder.tv_desc.setTextColor(ContextCompat.getColor(holder.tv_desc.getContext(), R.color.blackcolor1));

        }
        holder.tv_title.setText(notifications.get(position).getTitle());
        holder.tv_desc.setText(notifications.get(position).getContent());
        holder.tv_time.setText(notifications.get(position).getCreated_at());


    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv_title, tv_desc, tv_time;


        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_desc = (TextView) itemView.findViewById(R.id.tv_desc);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);

        }
    }
}
