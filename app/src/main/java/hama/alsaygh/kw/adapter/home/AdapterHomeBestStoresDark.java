package hama.alsaygh.kw.adapter.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.listener.OnStoreClickListener;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.Utils;

public class AdapterHomeBestStoresDark extends RecyclerView.Adapter<AdapterHomeBestStoresDark.Holder> {
    List<Store> best_stores;
    Context context;
    public String TAG = "adapter home best stores";
    private final OnStoreClickListener onNoteListener;


    public AdapterHomeBestStoresDark(OnStoreClickListener onNoteListener, List<Store> Best_stores, Context context) {
        this.best_stores = Best_stores;
        this.context = context;
        this.onNoteListener = onNoteListener;

    }


    @NonNull
    @Override
    public AdapterHomeBestStoresDark.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_home_rf_dark, parent, false);
        return new AdapterHomeBestStoresDark.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterHomeBestStoresDark.Holder holder, int position) {
        holder.tv1.setText(best_stores.get(position).getStore_name());
        String productCount = best_stores.get(position).getProducts() + " " + context.getString(R.string.products);
        holder.tv2.setText(productCount);

        if (!best_stores.get(position).getLogo().isEmpty()) {
            Utils.getInstance().setBackgroundImageColor(holder.imageView, best_stores.get(position).getLogo(),ContextCompat.getColor(holder.imageView.getContext(), R.color.color_image_not_found));
            Picasso.get().load(best_stores.get(position).getLogo()).error(R.color.whiteColor).placeholder(R.color.whiteColor).into(holder.imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.whiteColor));

                }
            });
        } else {
            Picasso.get().load(R.color.whiteColor).error(R.color.whiteColor).placeholder(R.color.whiteColor).into(holder.imageView);
            holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.whiteColor));
        }
    }

    @Override
    public int getItemCount() {
        return best_stores.size();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv1, tv2;
        View view;
        ImageView imageView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView5);
            tv1 = (TextView) itemView.findViewById(R.id.textView13);
            tv2 = (TextView) itemView.findViewById(R.id.textView11);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onNoteListener.onStoreClick(best_stores.get(getAdapterPosition()), getAdapterPosition(), TAG);

        }
    }
}

