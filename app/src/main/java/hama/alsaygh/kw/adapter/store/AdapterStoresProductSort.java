package hama.alsaygh.kw.adapter.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.db.table.FilterProduct;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterStoresProductSort extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<FilterProduct> best_stores;
    Context context;

    public AdapterStoresProductSort(Context context, List<FilterProduct> Best_stores) {
        this.best_stores = Best_stores;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sorting, parent, false);
        return new AdapterStoresProductSort.Holder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        Holder holder = (Holder) viewHolder;
        final FilterProduct filterProduct = (FilterProduct) best_stores.get(position);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {

            holder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.sign_in_dark));
            holder.radioButton.setButtonTintList(ContextCompat.getColorStateList(context, R.color.textviewhome));
            holder.radioButton.setHighlightColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.radioButton.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));

        } else {
            holder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.my_cart2));
            holder.radioButton.setButtonTintList(ContextCompat.getColorStateList(context, R.color.color_navigation));
            holder.radioButton.setHighlightColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.radioButton.setTextColor(ContextCompat.getColor(context, R.color.text));
        }

        holder.radioButton.setText(filterProduct.getTrans());
        holder.radioButton.setClickable(false);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.getInstance().setSort_key(filterProduct.getKey());

                best_stores.remove(position);
                best_stores.add(position, filterProduct);
                notifyDataSetChanged();
            }
        });

        if (Utils.getInstance().getSort_key().equalsIgnoreCase(filterProduct.getKey())) {
            holder.radioButton.setChecked(true);
        } else
            holder.radioButton.setChecked(false);
    }

    @Override
    public int getItemCount() {
        return best_stores.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        RadioButton radioButton;
        View view;


        public Holder(@NonNull View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.radioButton1);
            view = itemView.findViewById(R.id.view8);

        }


    }

}
