package hama.alsaygh.kw.adapter.store;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.category.Category;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class AdapterStoresProductSubChildFilter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<Category> categories;
    private final Context context;
    private final int mainCategoryId;
    private final int categoryId2;
   private final AdapterStoresProductFilter adpter;

    public AdapterStoresProductSubChildFilter(Context context, List<Category> categories, int mainCategoryId, int CategoryId2,AdapterStoresProductFilter adpter) {
        this.context = context;
        this.categories = categories;
        this.mainCategoryId = mainCategoryId;
        this.categoryId2 = CategoryId2;
        this.adpter=adpter;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_filter, parent, false);
        return new Holder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        Holder holder = (Holder) viewHolder;
        final Category filterProduct = (Category) categories.get(position);

        holder.tv_item.setText(filterProduct.getName());
        holder.itemView.setOnClickListener(v -> {
            Utils.getInstance().setCategory_level_1(mainCategoryId);
            Utils.getInstance().setCategory_level_2(categoryId2);
            Utils.getInstance().setCategory_level_3(filterProduct.getId());
            notifyDataSetChanged();
            adpter.notifyDataSetChanged();
        });

        Log.i("rrrrrr","level3 : "+Utils.getInstance().getCategory_level_3()+" -- level2 : "+Utils.getInstance().getCategory_level_2()+" -- level1 : "+Utils.getInstance().getCategory_level_1()+" ");
        if (Utils.getInstance().getCategory_level_3() == filterProduct.getId() && Utils.getInstance().getCategory_level_1() == mainCategoryId && Utils.getInstance().getCategory_level_2() == categoryId2) {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {

                holder.tv_item.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));
                holder.ll_item.setBackgroundResource(R.drawable.back_ground_rv_filter_dark_select);
            } else {
                holder.tv_item.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));
                holder.ll_item.setBackgroundResource(R.drawable.back_ground_rv_filter);
            }
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {

                holder.tv_item.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));
                holder.ll_item.setBackgroundResource(R.drawable.back_ground_rv_filter_dark_unselect);
            } else {
                holder.tv_item.setTextColor(ContextCompat.getColor(context, R.color.blackcolor));
                holder.ll_item.setBackgroundResource(R.drawable.back_ground_rv_filter_unselect);
            }
        }


    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {
        TextView tv_item;
        LinearLayout ll_item;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ll_item = itemView.findViewById(R.id.liner_filter);
            tv_item = itemView.findViewById(R.id.tv_item);

        }


    }

}
