package hama.alsaygh.kw.adapter.complaint;

import android.content.Context;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.fragment.settings.complaints.SendComplaint;
import hama.alsaygh.kw.fragment.settings.complaints.myComplaints;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class AdapterPagerComplints extends FragmentPagerAdapter {
    private int number_of_Tab;
    private Context context;

    public AdapterPagerComplints(@NonNull FragmentManager fm, int number_of_Tab, Context context) {
        super(fm);
        this.number_of_Tab = number_of_Tab;
        this.context = context;
    }

//    public AdapterPagerMyOrder(@NonNull FragmentManager fm, int behavior int number_of_Tab) {
//        super(fm, behavior);
//        this.number_of_Tab=number_of_Tab;
//
//    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new SendComplaint();
            case 1:
                return new myComplaints();

            default:
                return null;


        }
    }


    @Override
    public int getCount() {
        return number_of_Tab;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.send_complaints);
            case 1:
                return context.getString(R.string.my_complaint);

            default:
                return context.getString(R.string.my_complaint);
        }
    }
}
