package hama.alsaygh.kw.adapter.complaint;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hama.alsaygh.kw.listener.OnNoteListener;
import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.complaint.Complaint;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Locale;

public class AdapterMyComplaints extends  RecyclerView.Adapter<AdapterMyComplaints.Holder> {
    ArrayList<Complaint> myordersArray;
    public static String TAG = "adapter my complints";
    private final OnNoteListener onNoteListener;
    Context context;


    public AdapterMyComplaints(OnNoteListener onNoteListener,Context context,ArrayList<Complaint> myordersArray) {
        this.myordersArray = myordersArray;
        this.context= context;
        this.onNoteListener = onNoteListener;
    }

    @NonNull
    @Override
    public AdapterMyComplaints.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_my_complaints,null,false);
        AdapterMyComplaints.Holder holder = new AdapterMyComplaints.Holder(v);
        return new  AdapterMyComplaints.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMyComplaints.Holder holder, int position) {
//        holder.imageView.setImageResource(myordersArray.get(position).getImg());
         holder.tv1.setText(myordersArray.get(position).getSubject());
           holder.tv2.setText(myordersArray.get(position).getDescription());
        if (Locale.getDefault().getLanguage().equals("ar")) {
            holder.view11.setBackgroundResource(R.drawable.back_view_mu_complints_ar);
        }


        if(myordersArray.get(position).getStatus().equalsIgnoreCase("pending"))
        {
            holder.view11.getBackground().setColorFilter(ContextCompat.getColor(context, R.color.pending_color_complaint), android.graphics.PorterDuff.Mode.SRC_IN);
        }else  if(myordersArray.get(position).getStatus().equalsIgnoreCase("solved"))
        {
            holder.view11.getBackground().setColorFilter(ContextCompat.getColor(context, R.color.solved_color_complaint), android.graphics.PorterDuff.Mode.SRC_IN);
        }else  if(myordersArray.get(position).getStatus().equalsIgnoreCase("canceled"))
        {
            holder.view11.getBackground().setColorFilter(ContextCompat.getColor(context, R.color.canceled_color_complaint), android.graphics.PorterDuff.Mode.SRC_IN);
        }


    }

    @Override
    public int getItemCount() {
        return myordersArray.size();
    }

    public class Holder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        TextView tv1 , tv2;
        View view;
        ImageView imageView ;
        View view11;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView =(ImageView)itemView.findViewById(R.id.imageView15);
            tv1=(TextView)itemView.findViewById(R.id.textView48);
            tv2=(TextView)itemView.findViewById(R.id.textView49);
            view11=(View)itemView.findViewById(R.id.view11);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onNoteListener.onNoteClick(getAdapterPosition(), TAG);

        }
    }
}
