package hama.alsaygh.kw.adapter.home;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.listener.OnProductClickListener;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.utils.Utils;

public class AdapterHomeBestProductDark extends RecyclerView.Adapter<AdapterHomeBestProductDark.Holder> {
    List<Product> best_seling;
    private final OnProductClickListener onNoteListener;

    public AdapterHomeBestProductDark(List<Product> best_seling, OnProductClickListener onNoteListener) {
        this.best_seling = best_seling;
        this.onNoteListener = onNoteListener;
    }


    @NonNull
    @Override
    public AdapterHomeBestProductDark.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_home_rf2_dark, null, false);
        return new AdapterHomeBestProductDark.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterHomeBestProductDark.Holder holder, int position) {
        holder.tv1.setText(best_seling.get(position).getName());
        String price = best_seling.get(position).getPrice() + ""+holder.tv1.getContext().getString(R.string.currency);
        holder.tv3.setText(price);

        if (best_seling.get(position).getMedia() != null && !best_seling.get(position).getMedia().isEmpty()) {
            Utils.getInstance().setBackgroundImageColor(holder.imageView, best_seling.get(position).getMedia().get(0).getLink(),ContextCompat.getColor(holder.imageView.getContext(), R.color.color_image_not_found));
            Picasso.get().load(best_seling.get(position).getMedia().get(0).getLink()).error(R.drawable.image_not_foundpng).into(holder.imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.color_image_not_found));
                }
            });
        } else {
            Picasso.get().load(R.drawable.image_not_foundpng).into(holder.imageView);
            holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.color_image_not_found));

        }
        if (best_seling.get(position).isI_fav()) {
            Drawable drawable = AppCompatResources.getDrawable(holder.imageView1.getContext(), R.drawable.ic_loveeecolor);
            holder.imageView1.setImageDrawable(drawable);
        } else {
            Drawable drawable = AppCompatResources.getDrawable(holder.imageView1.getContext(), R.drawable.ic_loveee);
            holder.imageView1.setImageDrawable(drawable);
        }


    }

    @Override
    public int getItemCount() {
        return best_seling.size();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv1, tv2, tv3;
        View view;
        ImageView imageView, imageView1;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.img_twinz);
            imageView1 = (ImageView) itemView.findViewById(R.id.imageView15);
            tv1 = (TextView) itemView.findViewById(R.id.gold_ring);
            tv3 = (TextView) itemView.findViewById(R.id.textView100);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (onNoteListener != null)
                onNoteListener.onProductClick(best_seling.get(getAdapterPosition()), getAdapterPosition());
        }
    }
}

