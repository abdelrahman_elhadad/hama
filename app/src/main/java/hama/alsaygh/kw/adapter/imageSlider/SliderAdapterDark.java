package hama.alsaygh.kw.adapter.imageSlider;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import hama.alsaygh.kw.R;

public class SliderAdapterDark  extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    TextView textView;

    public SliderAdapterDark(Context context, LayoutInflater layoutInflater) {
        this.context = context;
        this.layoutInflater = layoutInflater;
    }

    public int[] slide_image = {
            R.drawable.twinzz,
            R.drawable.twinzz,
            R.drawable.twinzz,
    };
    public String [] slide_heading ={
            "fixed price",
            "price",
            "price"
    };


    @Override
    public int getCount() {
        return slide_image.length;
    }
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //  container.removeView((LinearLayout)object);
        ((ViewPager) container).removeView((View) object);


    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view== ((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = LayoutInflater.from(context).inflate(R.layout.card_view_slider_adapter_dark, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView24);
        ImageView imageView1 = (ImageView) view.findViewById(R.id.imageView15);
       // TextView textView = (TextView)view.findViewById(R.id.textView32);
        imageView.setImageResource(slide_image[position]);
        imageView1.setImageResource(R.drawable.love);
       // textView.setText(slide_heading[position]);
        container.addView(view);
        return view;
    }


}

