package hama.alsaygh.kw.adapter.order;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.product.ProductDetailsActivity;
import hama.alsaygh.kw.model.cart.CartItem;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.utils.Cons;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class OrderItemList extends RecyclerView.Adapter<OrderItemList.Holder> {
    List<CartItem> favItems;
    Context context;
    String currency;

    public OrderItemList(FragmentActivity context, List<CartItem> myordersArray, String currency) {
        this.favItems = myordersArray;
        this.context = context;
        this.currency = currency;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_wash_list_dark, parent, false);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_wash_list, parent, false);
        }
        return new OrderItemList.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        final CartItem favItem = favItems.get(position);

        final Product product = favItem.getProduct();
        if (product != null) {

            if (!favItem.getProduct().getMedia().isEmpty()) {
                String image = favItem.getProduct().getMedia().get(0).getLink();
                if (image != null && !image.isEmpty()) {
                    Utils.getInstance().setBackgroundImageColor(holder.imageView, image,ContextCompat.getColor(holder.imageView.getContext(), R.color.color_image_not_found));
                    Picasso.get().load(image).error(R.drawable.image_not_foundpng).into(holder.imageView);

                } else {
                    Picasso.get().load(R.drawable.image_not_foundpng).error(R.drawable.image_not_foundpng).into(holder.imageView);
                    holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.color_image_not_found));

                }
            } else {
                holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.color_image_not_found));
                Picasso.get().load(R.drawable.image_not_foundpng).error(R.drawable.image_not_foundpng).into(holder.imageView);
            }
            holder.tv1.setText(favItem.getProduct().getName());
            String price = favItem.getPrice() + " " + currency;
            holder.tv2.setText(price);
        } else {
            Picasso.get().load(R.drawable.image_not_foundpng).error(R.drawable.image_not_foundpng).into(holder.imageView);
            holder.tv1.setText("");
            holder.tv2.setText("");
        }

        String item = favItem.getQuantity() + "";
        holder.tv3.setText(item);

        holder.ivLike.setVisibility(View.GONE);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product != null) {
                    Intent intent = new Intent(v.getContext(), ProductDetailsActivity.class);
                    intent.putExtra(Cons.PRODUCT_ID, favItem.getProduct().getId());
                    intent.putExtra(Cons.PRODUCT, favItem.getProduct());
                    v.getContext().startActivity(intent);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return favItems.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv1, tv2, tv3;
        View view;
        ImageView imageView, ivLike;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.ring_secand_myorder);
            tv1 = (TextView) itemView.findViewById(R.id.gold_rin);
            tv3 = (TextView) itemView.findViewById(R.id.pro);
            tv2 = (TextView) itemView.findViewById(R.id.kwd15);
            ivLike = itemView.findViewById(R.id.garpeg_secand);

        }
    }
}
