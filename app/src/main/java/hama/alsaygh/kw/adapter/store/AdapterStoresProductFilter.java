package hama.alsaygh.kw.adapter.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.appyvet.materialrangebar.RangeBar;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.category.Category;
import hama.alsaygh.kw.model.category.MainCategory;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class AdapterStoresProductFilter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<MainCategory> best_stores;
    List<Category> categories;
    Context context;

    public AdapterStoresProductFilter(Context context, List<MainCategory> Best_stores, List<Category> categories) {
        this.best_stores = Best_stores;
        this.context = context;
        this.categories = categories;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_filter_main, parent, false);
        return new Holder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        Holder holder = (Holder) viewHolder;
        final MainCategory filterProduct = (MainCategory) best_stores.get(position);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {

            holder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.sign_in_dark));
            holder.rangebar1.setBarColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.rangebar1.setConnectingLineColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.rangebar1.setLeftThumbColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.rangebar1.setPinColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.rangebar1.setRightThumbColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.rangebar1.setThumbBoundaryColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.rangebar1.setTickDefaultColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.rangebar1.setTickLabelColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.rangebar1.setTickLabelSelectedColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.rangebar1.setPinTextColor(ContextCompat.getColor(context, R.color.whiteColor));

            holder.rb_fixed_price.setButtonTintList(ContextCompat.getColorStateList(context, R.color.textviewhome));
            holder.rb_fixed_price.setHighlightColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.rb_fixed_price.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));

            holder.rb_market_price.setButtonTintList(ContextCompat.getColorStateList(context, R.color.textviewhome));
            holder.rb_market_price.setHighlightColor(ContextCompat.getColor(context, R.color.textviewhome));
            holder.rb_market_price.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));
            holder.tv_filter_name.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));
            if (filterProduct.getId() == -1) {
                if (holder.rangebar1.getVisibility() == View.VISIBLE) {
                    holder.iv_close.setImageResource(R.drawable.ic_dawn_dark);
                } else {
                    holder.iv_close.setImageResource(R.drawable.ic_down_dark);
                }
            }else  if (filterProduct.getId() == -2) {
                if (holder.ll_price_radio.getVisibility() == View.VISIBLE) {
                    holder.iv_close.setImageResource(R.drawable.ic_down_dark);
                } else {

                    holder.iv_close.setImageResource(R.drawable.ic_dawn_dark);
                }
            }else  {
                if (holder.rc_item.getVisibility() == View.VISIBLE) {
                    holder.iv_close.setImageResource(R.drawable.ic_dawn_dark);
                } else {

                    holder.iv_close.setImageResource(R.drawable.ic_down_dark);
                }
            }
        } else {
            holder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.my_cart2));
            holder.rb_fixed_price.setButtonTintList(ContextCompat.getColorStateList(context, R.color.color_navigation));
            holder.rb_fixed_price.setHighlightColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.rb_fixed_price.setTextColor(ContextCompat.getColor(context, R.color.text));

            holder.rb_market_price.setButtonTintList(ContextCompat.getColorStateList(context, R.color.color_navigation));
            holder.rb_market_price.setHighlightColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.rb_market_price.setTextColor(ContextCompat.getColor(context, R.color.text));

            holder.rangebar1.setBarColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.rangebar1.setConnectingLineColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.rangebar1.setLeftThumbColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.rangebar1.setPinColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.rangebar1.setRightThumbColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.rangebar1.setThumbBoundaryColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.rangebar1.setTickDefaultColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.rangebar1.setTickLabelColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.rangebar1.setTickLabelSelectedColor(ContextCompat.getColor(context, R.color.color_navigation));
            holder.rangebar1.setPinTextColor(ContextCompat.getColor(context, R.color.blackcolor));
            holder.tv_filter_name.setTextColor(ContextCompat.getColor(context, R.color.blackcolor));

             if (filterProduct.getId() == -1) {
                 if (holder.rangebar1.getVisibility() == View.VISIBLE) {
                        holder.iv_close.setImageResource(R.drawable.ic_icon_upp);
                 } else {

                         holder.iv_close.setImageResource(R.drawable.ic_dawnicon);
                 }
             }else  if (filterProduct.getId() == -2) {
                 if (holder.ll_price_radio.getVisibility() == View.VISIBLE) {
                     holder.iv_close.setImageResource(R.drawable.ic_icon_upp);
                 } else {

                     holder.iv_close.setImageResource(R.drawable.ic_dawnicon);
                 }
             }else  {
                 if (holder.rc_item.getVisibility() == View.VISIBLE) {
                     holder.iv_close.setImageResource(R.drawable.ic_icon_upp);
                 } else {

                     holder.iv_close.setImageResource(R.drawable.ic_dawnicon);
                 }
             }
        }
        holder.tv_filter_name.setText(filterProduct.getName());

        holder.itemView.setOnClickListener(v -> {

            if (filterProduct.getId() == -1) {
                if (holder.rangebar1.getVisibility() == View.VISIBLE) {
                    holder.rangebar1.setVisibility(View.GONE);
                    if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
                        holder.iv_close.setImageResource(R.drawable.ic_dawn_dark);

                    } else {
                        holder.iv_close.setImageResource(R.drawable.ic_icon_upp);

                    }

                } else {
                    holder.rangebar1.setVisibility(View.VISIBLE);
                    if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
                        holder.iv_close.setImageResource(R.drawable.ic_down_dark);

                    } else {
                        holder.iv_close.setImageResource(R.drawable.ic_dawnicon);

                    }
                }
            } else if (filterProduct.getId() == -2) {
                if (holder.ll_price_radio.getVisibility() == View.VISIBLE) {
                    holder.ll_price_radio.setVisibility(View.GONE);
                    if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
                        holder.iv_close.setImageResource(R.drawable.ic_dawn_dark);

                    } else {
                        holder.iv_close.setImageResource(R.drawable.ic_icon_upp);

                    }

                } else {
                    holder.ll_price_radio.setVisibility(View.VISIBLE);
                    if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
                        holder.iv_close.setImageResource(R.drawable.ic_down_dark);

                    } else {
                        holder.iv_close.setImageResource(R.drawable.ic_dawnicon);

                    }
                }
            } else {
                if (holder.rc_item.getVisibility() == View.VISIBLE) {
                    holder.rc_item.setVisibility(View.GONE);
                    holder.rc_item_child.setVisibility(View.GONE);
                    if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
                        holder.iv_close.setImageResource(R.drawable.ic_dawn_dark);

                    } else {
                        holder.iv_close.setImageResource(R.drawable.ic_icon_upp);

                    }

                } else {
                    holder.rc_item.setVisibility(View.VISIBLE);
                    holder.rc_item_child.setVisibility(View.VISIBLE);
                    if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
                       holder.iv_close.setImageResource(R.drawable.ic_down_dark);

                    } else {
                        holder.iv_close.setImageResource(R.drawable.ic_dawnicon);

                    }
                }
            }
            best_stores.remove(position);
            best_stores.add(position, filterProduct);
            notifyDataSetChanged();
        });


        holder.rangebar1.setDrawTicks(true);
        holder.rangebar1.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {

                Utils.getInstance().setRange_price_from(leftPinValue);
                Utils.getInstance().setRange_price_to(rightPinValue);
            }

            @Override
            public void onTouchStarted(RangeBar rangeBar) {

            }

            @Override
            public void onTouchEnded(RangeBar rangeBar) {

            }
        });
        holder.rb_market_price.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Utils.getInstance().setType_of_price("market");
                }
            }
        });

        holder.rb_fixed_price.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Utils.getInstance().setType_of_price("fixed");
                }
            }
        });


        if (Utils.getInstance().getType_of_price().equalsIgnoreCase("fixed")) {
            holder.rb_fixed_price.setChecked(true);
        } else if (Utils.getInstance().getType_of_price().equalsIgnoreCase("market")) {
            holder.rb_market_price.setChecked(true);
        } else {
            holder.rb_fixed_price.setChecked(false);
            holder.rb_market_price.setChecked(false);
        }

        if (!Utils.getInstance().getRange_price_from().isEmpty() && !Utils.getInstance().getRange_price_to().isEmpty())
            holder.rangebar1.setRangePinsByValue(Float.parseFloat(Utils.getInstance().getRange_price_from()), Float.parseFloat(Utils.getInstance().getRange_price_to()));


        AdapterStoresProductSubFilter adapter = new AdapterStoresProductSubFilter(context, categories, filterProduct.getId(),holder.rc_item_child,this);
        holder.rc_item.setAdapter(adapter);
        holder.rc_item.setLayoutManager(new GridLayoutManager(context, 3));
    }

    @Override
    public int getItemCount() {
        return best_stores.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {
        RadioButton rb_market_price, rb_fixed_price;
        View view;
        TextView tv_filter_name;
        ImageView iv_close;
        RecyclerView rc_item,rc_item_child;
        LinearLayout ll_price_radio;
        RangeBar rangebar1;

        public Holder(@NonNull View itemView) {
            super(itemView);
            rangebar1 = itemView.findViewById(R.id.rangebar1);
            rb_fixed_price = itemView.findViewById(R.id.rb_fixed_price);
            rb_market_price = itemView.findViewById(R.id.rb_market_price);
            ll_price_radio = itemView.findViewById(R.id.ll_price_radio);
            tv_filter_name = itemView.findViewById(R.id.tv_filter_name);
            iv_close = itemView.findViewById(R.id.iv_close);
            rc_item = itemView.findViewById(R.id.rc_item);
            rc_item_child=itemView.findViewById(R.id.rc_item_child);
            view = itemView.findViewById(R.id.view88);

        }


    }

}
