package hama.alsaygh.kw.adapter.complaint;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.complaint.ComplaintReplay;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterComplaintReplay extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<ComplaintReplay> myordersArray;
    Context context;

    private final int TYPE_ME = 0;
    private final int TYPE_VENDOR = 1;

    public AdapterComplaintReplay(Context context, List<ComplaintReplay> myordersArray) {
        this.myordersArray = myordersArray;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        return myordersArray.get(position).isMe() ? TYPE_ME : TYPE_VENDOR;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == TYPE_ME) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_complaint_chat_me, parent, false);
            return new HolderMe(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_complaint_chat_vendor, parent, false);
            return new HolderVendor(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof HolderMe) {

            HolderMe holderMe = (HolderMe) holder;
            holderMe.tv_msg.setText(myordersArray.get(position).getMessage());
            holderMe.tv_time.setText(myordersArray.get(position).getSend_at());

            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
                holderMe.tv_msg.setTextColor(ContextCompat.getColor(context, R.color.white_tranperant));
                holderMe.tv_time.setTextColor(ContextCompat.getColor(context, R.color.white_tranperant));
                holderMe.tv_msg.setBackgroundResource(R.drawable.back_chating_dark1);
            } else {
                holderMe.tv_msg.setTextColor(ContextCompat.getColor(context, R.color.textColor));
                holderMe.tv_time.setTextColor(ContextCompat.getColor(context, R.color.textColor));
                holderMe.tv_msg.setBackgroundResource(R.drawable.back_ground_chating);
            }

        } else {
            HolderVendor holderVendor = (HolderVendor) holder;
            holderVendor.tv_msg.setText(myordersArray.get(position).getMessage());
            holderVendor.tv_time.setText(myordersArray.get(position).getSend_at());


            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
                holderVendor.tv_msg.setTextColor(ContextCompat.getColor(context, R.color.white_tranperant));
                holderVendor.tv_time.setTextColor(ContextCompat.getColor(context, R.color.white_tranperant));
                holderVendor.tv_msg.setBackgroundResource(R.drawable.back_chating_dark2);
            } else {
                holderVendor.tv_msg.setTextColor(ContextCompat.getColor(context, R.color.textColor));
                holderVendor.tv_time.setTextColor(ContextCompat.getColor(context, R.color.textColor));
                holderVendor.tv_msg.setBackgroundResource(R.drawable.back_ground_chating2);
            }


        }

    }

    @Override
    public int getItemCount() {
        return myordersArray.size();
    }

    public class HolderMe extends RecyclerView.ViewHolder {
        TextView tv_msg, tv_time;

        ImageView iv_user;


        public HolderMe(@NonNull View itemView) {
            super(itemView);
            iv_user = (ImageView) itemView.findViewById(R.id.imageView33);
            tv_msg = (TextView) itemView.findViewById(R.id.textView65);
            tv_time = (TextView) itemView.findViewById(R.id.textView70);

        }
    }

    public class HolderVendor extends RecyclerView.ViewHolder {
        TextView tv_msg, tv_time;

        ImageView iv_user;

        public HolderVendor(@NonNull View itemView) {
            super(itemView);
            iv_user = (ImageView) itemView.findViewById(R.id.imageView313);
            tv_msg = (TextView) itemView.findViewById(R.id.textView661);
            tv_time = (TextView) itemView.findViewById(R.id.textView71);
        }
    }
}
