package hama.alsaygh.kw.adapter.order;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.fragment.order.CanceledFragment;
import hama.alsaygh.kw.fragment.order.CompletedFagment;
import hama.alsaygh.kw.fragment.order.InProgressFragment;
import hama.alsaygh.kw.fragment.order.PendingFragment;

public class AdapterPagerMyOrder extends FragmentPagerAdapter {
    List<String> title = new ArrayList<>();

    public AdapterPagerMyOrder(Context context, @NonNull FragmentManager fm) {
        super(fm);

        title.add(context.getString(R.string.Completed));
        title.add(context.getString(R.string.Pending));
        title.add(context.getString(R.string.in_progress));
        title.add(context.getString(R.string.Canceled));
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CompletedFagment();
            case 1:
                return new PendingFragment();
            case 2:
                return new InProgressFragment();
            default:
                return new CanceledFragment();


        }
    }


    @Override
    public int getCount() {
        return title.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return title.get(position);
    }
}

