package hama.alsaygh.kw.adapter.order;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.activity.order.OrderDetailsActivity;
import hama.alsaygh.kw.model.cart.CartItem;
import hama.alsaygh.kw.model.order.Order;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.utils.Utils;

public class AdapterMyOrder extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<Order> myordersArray;
    private String type;

    public AdapterMyOrder(ArrayList<Order> myordersArray, String type) {
        this.myordersArray = myordersArray;
        this.type = type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case 0:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_my_order, parent, false);
                return new Holder(v);
            case 2:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_my_order_firest, parent, false);
                return new AdapterMyOrder.SecandHolder(v);

        }
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_my_order_firest, parent, false);
        return new AdapterMyOrder.SecandHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int index) {
        final int position = index;

        if (viewHolder instanceof Holder) {
            Holder holder = (Holder) viewHolder;
            holder.tv1.setText(getName(myordersArray.get(position)));
            holder.tv2.setText(myordersArray.get(position).getTotal() + " " + myordersArray.get(position).getCurrency());
            holder.tv3.setText(myordersArray.get(position).getItems_count() + " " + holder.tv3.getContext().getString(R.string.products));
            holder.tv4.setText(myordersArray.get(position).getCreateDate());
            holder.status.setText(myordersArray.get(position).getStatus_trans());


            if (myordersArray.get(position).getItems() != null && !myordersArray.get(position).getItems().isEmpty() &&
                    myordersArray.get(position).getItems().get(0).getProduct().getMedia() != null && !myordersArray.get(position).getItems().get(0).getProduct().getMedia().isEmpty())
                Picasso.get().load(myordersArray.get(position).getItems().get(0).getProduct().getMedia().get(0).getLink()).error(R.drawable.logo).into(holder.imageView1);
            else
                Picasso.get().load(R.drawable.logo).into(holder.imageView1);

            if (myordersArray.get(position).getItems() != null && !myordersArray.get(position).getItems().isEmpty() &&
                    myordersArray.get(position).getItems().get(1).getProduct().getMedia() != null && !myordersArray.get(position).getItems().get(1).getProduct().getMedia().isEmpty())
                Picasso.get().load(myordersArray.get(position).getItems().get(1).getProduct().getMedia().get(0).getLink()).error(R.drawable.logo).into(holder.imageView2);
            else
                Picasso.get().load(R.drawable.logo).into(holder.imageView2);

            if (myordersArray.get(position).getItems() != null && !myordersArray.get(position).getItems().isEmpty() &&
                    myordersArray.get(position).getItems().get(2).getProduct().getMedia() != null && !myordersArray.get(position).getItems().get(2).getProduct().getMedia().isEmpty())
                Picasso.get().load(myordersArray.get(position).getItems().get(2).getProduct().getMedia().get(0).getLink()).error(R.drawable.logo).into(holder.imageView3);
            else
                Picasso.get().load(R.drawable.logo).into(holder.imageView3);

            if (myordersArray.get(position).getItems() != null && !myordersArray.get(position).getItems().isEmpty() &&
                    myordersArray.get(position).getItems().get(3).getProduct().getMedia() != null && !myordersArray.get(position).getItems().get(3).getProduct().getMedia().isEmpty())
                Picasso.get().load(myordersArray.get(position).getItems().get(3).getProduct().getMedia().get(0).getLink()).error(R.drawable.logo).into(holder.imageView4);
            else
                Picasso.get().load(R.drawable.logo).into(holder.imageView4);


            if (Locale.getDefault().getLanguage().equals("ar")) {


                if (type.equalsIgnoreCase("pending")) {
                    holder.viewStatus.setBackgroundResource(R.drawable.card_pending_ar);
                } else if (type.equalsIgnoreCase("completed")) {
                    holder.viewStatus.setBackgroundResource(R.drawable.back_view_myorder_ar);
                } else if (type.equalsIgnoreCase("canceled")) {
                    holder.viewStatus.setBackgroundResource(R.drawable.card_canceld_ar);

                }

            } else {

                if (type.equalsIgnoreCase("pending")) {
                    holder.viewStatus.setBackgroundResource(R.drawable.back_card_pending);
                } else if (type.equalsIgnoreCase("completed")) {
                    holder.viewStatus.setBackgroundResource(R.drawable.back_view_my_card_order);
                } else if (type.equalsIgnoreCase("canceled")) {
                    holder.viewStatus.setBackgroundResource(R.drawable.card_tab_canceled);
                }

            }
        } else if (viewHolder instanceof SecandHolder) {

            SecandHolder holder = (SecandHolder) viewHolder;
            if (!myordersArray.get(position).getItems().isEmpty()) {
                Product product = myordersArray.get(position).getItems().get(0).getProduct();
                if (product != null) {
                    if (product.getMedia() != null && !product.getMedia().isEmpty()) {
                        Utils.getInstance().setBackgroundImageColor(holder.img, product.getMedia().get(0).getLink(),ContextCompat.getColor(holder.img.getContext(), R.color.whiteColor));
                        Picasso.get().load(product.getMedia().get(0).getLink()).error(R.drawable.logo).into(holder.img, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError(Exception e) {
                                holder.img.setBackgroundColor(ContextCompat.getColor(holder.img.getContext(), R.color.whiteColor));
                            }
                        });
                    } else {
                        Picasso.get().load(R.drawable.logo).into(holder.img);
                        holder.img.setBackgroundColor(ContextCompat.getColor(holder.img.getContext(), R.color.whiteColor));
                    }

                } else {
                    Picasso.get().load(R.drawable.logo).into(holder.img);
                    holder.img.setBackgroundColor(ContextCompat.getColor(holder.img.getContext(), R.color.whiteColor));

                }
            } else {
                Picasso.get().load(R.drawable.logo).into(holder.img);
                holder.img.setBackgroundColor(ContextCompat.getColor(holder.img.getContext(), R.color.whiteColor));

            }
            holder.t1.setText(getName(myordersArray.get(position)));
            holder.t2.setText(myordersArray.get(position).getTotal() + " " + myordersArray.get(position).getCurrency());
            holder.t3.setText(myordersArray.get(position).getItems_count() + " " + holder.t3.getContext().getString(R.string.products));
            holder.t4.setText(myordersArray.get(position).getCreateDate());
            holder.status.setText(myordersArray.get(position).getStatus_trans());

            if (Locale.getDefault().getLanguage().equals("ar")) {
                if (type.equalsIgnoreCase("pending")) {

                    holder.viewStatus.setBackgroundResource(R.drawable.card_pending_ar);
                } else if (type.equalsIgnoreCase("completed")) {
                    holder.viewStatus.setBackgroundResource(R.drawable.back_view_myorder_ar);
                } else if (type.equalsIgnoreCase("canceled")) {
                    holder.viewStatus.setBackgroundResource(R.drawable.card_canceld_ar);
                }
            } else {

                if (type.equalsIgnoreCase("pending")) {

                    holder.viewStatus.setBackgroundResource(R.drawable.back_card_pending);
                } else if (type.equalsIgnoreCase("completed")) {
                    holder.viewStatus.setBackgroundResource(R.drawable.back_view_my_card_order);
                } else if (type.equalsIgnoreCase("canceled")) {
                    holder.viewStatus.setBackgroundResource(R.drawable.card_tab_canceled);
                }
            }
        }


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), OrderDetailsActivity.class);
                intent.putExtra("order_id", myordersArray.get(position).getId());
                v.getContext().startActivity(intent);
            }
        });
    }


    @Override
    public int getItemViewType(int position) {
        return myordersArray.get(position).getItems_count() >= 4 ? 0 : 2;
    }

    public String getName(Order storeModel) {
        String name = "";
        if (storeModel.getItems() != null && !storeModel.getItems().isEmpty())
            for (CartItem item : storeModel.getItems()) {
                if (item.getProduct() != null) {
                    name = name + "," + item.getProduct().getName();
                }
            }
        if (name.startsWith(","))
            name = name.replaceFirst(",", "");

        return name;
    }

    @Override
    public int getItemCount() {
        return myordersArray.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv1, tv2, tv3, tv4, status;
        View view, viewStatus;
        ImageView imageView1, imageView2, imageView3, imageView4, imageView5;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView1 = (ImageView) itemView.findViewById(R.id.ring1);
            imageView2 = (ImageView) itemView.findViewById(R.id.ring2);
            imageView3 = (ImageView) itemView.findViewById(R.id.ring3);
            imageView4 = (ImageView) itemView.findViewById(R.id.ring4);
            imageView5 = (ImageView) itemView.findViewById(R.id.garpeg);
            tv1 = (TextView) itemView.findViewById(R.id.gold_ring11);
            tv2 = (TextView) itemView.findViewById(R.id.kwd150);
            tv3 = (TextView) itemView.findViewById(R.id.prouduct);
            tv4 = (TextView) itemView.findViewById(R.id.history);
            status = (TextView) itemView.findViewById(R.id.status);
            viewStatus = (View) itemView.findViewById(R.id.viewStatus);

        }
    }

    public class SecandHolder extends RecyclerView.ViewHolder {
        ImageView img, img2;
        TextView t1, t2, t3, t4, t5, t6, status;
        View viewStatus;

        public SecandHolder(@NonNull View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ring_secand_myorder);
            img2 = (ImageView) itemView.findViewById(R.id.garpeg_secand);
            t1 = (TextView) itemView.findViewById(R.id.gold_rin);
            t2 = (TextView) itemView.findViewById(R.id.kwd15);
            t3 = (TextView) itemView.findViewById(R.id.pro);
            t4 = (TextView) itemView.findViewById(R.id.his);
            status = (TextView) itemView.findViewById(R.id.status);
            viewStatus = itemView.findViewById(R.id.viewStatus);

        }


    }

}
