package hama.alsaygh.kw.adapter.cart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.dialog.MyCartDeleteProduct;
import hama.alsaygh.kw.dialog.PopUpCheckOut;
import hama.alsaygh.kw.listener.OnMyCartListener;
import hama.alsaygh.kw.model.cart.CartItem;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class AdapterMyCart extends RecyclerView.Adapter<AdapterMyCart.Holder> {
    List<CartItem> myCartArrays;
    FragmentManager fragmentManager;
    Context context;
    OnMyCartListener onMyCartListener;

    public AdapterMyCart(Context context, List<CartItem> myCartArrays, FragmentManager fragmentManager, OnMyCartListener onMyCartListener) {
        this.myCartArrays = myCartArrays;
        this.fragmentManager = fragmentManager;
        this.context = context;
        this.onMyCartListener=onMyCartListener;

    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_rv_my_cart_dark, parent, false);
        } else

            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_rv_my_cart, parent, false);
        return new AdapterMyCart.Holder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int position) {

        final CartItem cartItem = myCartArrays.get(position);

        if (!cartItem.getProduct().getMedia().isEmpty()) {
            String image = cartItem.getProduct().getMedia().get(0).getLink();
            if (image != null && !image.isEmpty()) {
                Picasso.get().load(image).error(R.drawable.image_not_foundpng).resize(150, 150).into(holder.imageView);
            } else
                Picasso.get().load(R.drawable.image_not_foundpng).error(R.drawable.image_not_foundpng).resize(150, 150).into(holder.imageView);
        } else
            Picasso.get().load(R.drawable.image_not_foundpng).error(R.drawable.image_not_foundpng).resize(150, 150).into(holder.imageView);

        holder.tv1.setText(cartItem.getProduct().getName());

               holder.tv2.setText(cartItem.getPrice()+" "+context.getString(R.string.currency));


        holder.tvCount.setText(String.valueOf(cartItem.getQuantity()));

        holder.tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int count = cartItem.getQuantity() + 1;
                cartItem.setQuantity(count);
                holder.tvCount.setText(String.valueOf(count));

                if (count > 1)
                    holder.tvMin.setEnabled(true);

                if (MainApplication.isConnected) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            RequestWrapper.getInstance().addToCart(context, cartItem.getProduct().getId(), count);
                            if (onMyCartListener != null)
                                onMyCartListener.onCountChange();
                        }
                    }).start();
                }
            }
        });


        holder.tvMin.setEnabled(cartItem.getQuantity() != 1);
        holder.tvMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int count = cartItem.getQuantity() - 1;
                cartItem.setQuantity(count);
                holder.tvCount.setText(String.valueOf(count));
                holder.tvMin.setEnabled(count != 1);

                if (MainApplication.isConnected) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            RequestWrapper.getInstance().addToCart(context, cartItem.getProduct().getId(), count);
                            if (onMyCartListener != null)
                                onMyCartListener.onCountChange();
                        }
                    }).start();
                }
            }
        });

        holder.box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopUpCheckOut popUpCheckOut = new PopUpCheckOut();
                popUpCheckOut.show(fragmentManager, "checkOutConfirmPayment");

            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyCartDeleteProduct myCartDeleteProduct = new MyCartDeleteProduct();
                myCartDeleteProduct.setCartItem(cartItem);
                myCartDeleteProduct.setOnMyCartListener(onMyCartListener);
                myCartDeleteProduct.show(fragmentManager, "ModalBottomSheet");

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onMyCartListener != null)
                    onMyCartListener.onProductClick(cartItem.getProduct(), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myCartArrays.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv1, tv2, tvCount, tvPlus, tvMin;
        View view;
        ImageView imageView, imageView1, box, delete;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.ring_mycart);
            tv1 = (TextView) itemView.findViewById(R.id.goldRing);
            tv2 = (TextView) itemView.findViewById(R.id.kwd);
            imageView1 = (ImageView) itemView.findViewById(R.id.love);
            box = (ImageView) itemView.findViewById(R.id.box);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            tvCount = itemView.findViewById(R.id.textView34);
            tvPlus = itemView.findViewById(R.id.textView33);
            tvMin = itemView.findViewById(R.id.textView35);

        }
    }
}
