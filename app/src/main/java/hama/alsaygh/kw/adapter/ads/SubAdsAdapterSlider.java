package hama.alsaygh.kw.adapter.ads;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.listener.OnAdsClickListener;
import hama.alsaygh.kw.model.ads.SubAds;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class SubAdsAdapterSlider extends PagerAdapter {
    LayoutInflater layoutInflater;
    Context context;
    ImageView imageView, iv_sub_ads_image;
    TextView tv_sub_ads, textView50;

    List<SubAds> subAdsList;
    OnAdsClickListener onAdsClickListener;


    public SubAdsAdapterSlider(LayoutInflater layoutInflater, Context context, List<SubAds> subAds, OnAdsClickListener onAdsClickListener) {
        this.layoutInflater = layoutInflater;
        this.context = context;
        this.subAdsList = subAds;
        this.onAdsClickListener = onAdsClickListener;
    }


    @Override
    public int getCount() {
        return subAdsList.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, @NotNull Object object) {
        (container).removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = LayoutInflater.from(context).inflate(R.layout.card_slider2_home1, container, false);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
            view = LayoutInflater.from(context).inflate(R.layout.card_slider2_home1_dark, container, false);
        }

        iv_sub_ads_image = view.findViewById(R.id.iv_sub_ads_image);
        imageView = view.findViewById(R.id.imageView35);
        textView50 = view.findViewById(R.id.textView50);
        tv_sub_ads = view.findViewById(R.id.tv_sub_ads);
        final ProgressBar pb_ads_image = view.findViewById(R.id.pb_ads_image);
        tv_sub_ads.setText(subAdsList.get(position).getTitle() + "\n" + subAdsList.get(position).getDescription());
        // imageView1.setBackgroundResource(slide_img[position]);
        textView50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        Picasso.get().load(subAdsList.get(position).getImage()).into(iv_sub_ads_image, new Callback() {
            @Override
            public void onSuccess() {
                pb_ads_image.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                pb_ads_image.setVisibility(View.GONE);
            }
        });

        view.setOnClickListener(v -> {
            if (subAdsList.get(position).getRedirect_type().equalsIgnoreCase("url")) {
                String url = subAdsList.get(position).getUrl();
                if (url != null && !url.isEmpty()) {

                    try {
                        if (!url.startsWith("http://") && !url.startsWith("https://"))
                            url = "http://" + url;
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        context.startActivity(browserIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (subAdsList.get(position).getRedirect_type().equalsIgnoreCase("store")) {
                if (onAdsClickListener != null)
                    onAdsClickListener.onAdsClick(subAdsList.get(position).getRedirect_type(), subAdsList.get(position).getVendor_id());
            } else if (subAdsList.get(position).getRedirect_type().equalsIgnoreCase("product")) {
                onAdsClickListener.onAdsClick(subAdsList.get(position).getRedirect_type(), subAdsList.get(position).getProduct_id());
            } else
                onAdsClickListener.onAdsClick(subAdsList.get(position).getRedirect_type(), subAdsList.get(position).getCategory_id());
        });

        container.addView(view);
        return view;
    }
}
