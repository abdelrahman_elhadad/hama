package hama.alsaygh.kw.adapter.store;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.listener.OnStoreClickListener;
import hama.alsaygh.kw.model.cart.CartHomeItem;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class AdapterStoresCart extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<CartHomeItem> best_stores;
    public static String TAG = "adapter stores spanned";
    Context context;
    private static OnStoreClickListener onNoteListener;


    public AdapterStoresCart(OnStoreClickListener onNoteListener, List<CartHomeItem> Best_stores, Context context) {
        this.best_stores = Best_stores;
        this.context = context;
        AdapterStoresCart.onNoteListener = onNoteListener;

    }

    public  List<CartHomeItem> getMainCategorts() {
        return best_stores;
    }

    public void setMainCategorts( List<CartHomeItem> mainCategorts) {
        this.best_stores = mainCategorts;
    }

    @Override
    public int getItemViewType(int position) {


        return position;
    }


    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_stores_cart_dark, parent, false);
            return new AdapterStoresCart.AdsHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_stores_cart, parent, false);
            return new AdapterStoresCart.Holder(v);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {


        if (viewHolder instanceof Holder) {
            Log.i("ppppppp", "postion :" + position + " typeView store");
            Holder holder = (Holder) viewHolder;

            Store bestStores = (Store) best_stores.get(position).getStore();
            holder.tv1.setText(bestStores.getStore_name());
            holder.tv2.setText(best_stores.get(position).getCount() + " " + context.getString(R.string.products));
            if (bestStores.getLogo() != null && !bestStores.getLogo().isEmpty()) {
                Utils.getInstance().setBackgroundImageColor(holder.imageView, bestStores.getLogo(),ContextCompat.getColor(holder.imageView.getContext(), R.color.whiteColor));
                Picasso.get().load(bestStores.getLogo()).error(R.color.whiteColor).placeholder(R.color.whiteColor).into(holder.imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.whiteColor));

                    }
                });
            } else {
                Picasso.get().load(R.color.whiteColor).into(holder.imageView);
                holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.whiteColor));
            }

        }
    }

    @Override
    public int getItemCount() {
        return best_stores.size();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv1, tv2;
        View view;
        ImageView imageView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.img_sberena);
            tv1 = (TextView) itemView.findViewById(R.id.text_sberena);
            tv2 = (TextView) itemView.findViewById(R.id.text_product);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onNoteListener.onStoreClick((Store) best_stores.get(getAdapterPosition()).getStore(), getAdapterPosition(), TAG);

        }
    }

    public class AdsHolder extends RecyclerView.ViewHolder {

        TextView tv_ads;
        ImageView iv_ads;
        ProgressBar pb_ads;

        public AdsHolder(@NonNull View itemView) {
            super(itemView);
            tv_ads = itemView.findViewById(R.id.tv_ads);
            iv_ads = itemView.findViewById(R.id.iv_ads);
            pb_ads = itemView.findViewById(R.id.pb_ads);
        }
    }
}
