package hama.alsaygh.kw.adapter.event;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.MyCartArray;

public class AdapterEventAndAdvetisment extends  RecyclerView.Adapter<AdapterEventAndAdvetisment.Holder> {
    ArrayList<MyCartArray> myCartArrays;

    public AdapterEventAndAdvetisment(ArrayList<MyCartArray> myCartArrays) {
        this.myCartArrays = myCartArrays;
    }

    @NonNull
    @Override
    public AdapterEventAndAdvetisment.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_hama_event_and_advertisements,null,false);
        AdapterEventAndAdvetisment.Holder holder = new AdapterEventAndAdvetisment.Holder(v);
        return new  AdapterEventAndAdvetisment.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterEventAndAdvetisment.Holder holder, int position) {
        holder.imageView.setImageResource(myCartArrays.get(position).getImg());
       // holder.tv1.setText("Gold Ring");
     //   holder.tv2.setText("1500KWD");

    }

    @Override
    public int getItemCount() {
        return myCartArrays.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv1 , tv2,tv3,tv4;
        View view;
        ImageView imageView ,imageView1,imageView2,imageView3;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView =(ImageView)itemView.findViewById(R.id.imageView63);
            tv1=(TextView)itemView.findViewById(R.id.textView104);
            tv2=(TextView)itemView.findViewById(R.id.textView105);
            tv3=(TextView)itemView.findViewById(R.id.textView154);
            tv4=(TextView)itemView.findViewById(R.id.textView155);

            imageView1 =(ImageView)itemView.findViewById(R.id.imageView64);




        }
    }
}
