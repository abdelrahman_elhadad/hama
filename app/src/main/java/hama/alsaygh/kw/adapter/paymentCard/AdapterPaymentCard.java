package hama.alsaygh.kw.adapter.paymentCard;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.listener.OnCardClickListener;
import hama.alsaygh.kw.model.paymentCard.Card;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterPaymentCard extends RecyclerView.Adapter<AdapterPaymentCard.Holder> {
    List<Card> products;
    private final OnCardClickListener onNoteListener;
    public static String TAG = "adapter store page";


    public AdapterPaymentCard(OnCardClickListener onNoteListener, List<Card> products) {
        this.onNoteListener = onNoteListener;
        this.products = products;
    }

    @NonNull
    @Override
    public AdapterPaymentCard.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_payment_card, null, false);
        return new AdapterPaymentCard.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterPaymentCard.Holder holder, final int position) {


        final Card card = products.get(position);
        holder.tv_card_holder_name.setText(card.getHolder_name());
        holder.tv_card_number.setText(card.getCard_number());
        holder.tv_type.setText(card.getType());
        String date = card.getMonth() + "/" + card.getYear();
        holder.tv_expire.setText(date);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(holder.itemView.getContext())) {
            holder.parent_payment_method.setBackgroundResource(R.drawable.shape_round_light_brawon);
        } else {
            holder.parent_payment_method.setBackgroundResource(R.drawable.shape_round_light_green);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onNoteListener != null)
                    onNoteListener.onCardClick(holder.iv_image, card, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv_type, tv_card_number, tv_card_holder_name, tv_expire;
        RelativeLayout parent_payment_method;
        ImageView iv_image;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_type = (TextView) itemView.findViewById(R.id.tv_type);
            tv_card_number = (TextView) itemView.findViewById(R.id.tv_card_number);
            tv_expire = (TextView) itemView.findViewById(R.id.tv_expire);
            tv_card_holder_name = (TextView) itemView.findViewById(R.id.tv_card_holder_name);
            parent_payment_method = itemView.findViewById(R.id.parent_payment_method);
            iv_image = itemView.findViewById(R.id.iv_image);

        }


    }
}
