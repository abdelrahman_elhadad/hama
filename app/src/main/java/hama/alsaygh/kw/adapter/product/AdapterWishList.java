package hama.alsaygh.kw.adapter.product;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.GeneralResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnProductClickListener;
import hama.alsaygh.kw.model.product.favorite.FavItem;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class AdapterWishList extends RecyclerView.Adapter<AdapterWishList.Holder> {
    List<FavItem> favItems;
    FragmentActivity context;
    OnProductClickListener onProductClickListener;

    public AdapterWishList(FragmentActivity context, List<FavItem> myordersArray, OnProductClickListener onProductClickListener) {
        this.favItems = myordersArray;
        this.context = context;
        this.onProductClickListener = onProductClickListener;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_wash_list_dark, parent, false);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_wash_list, parent, false);
        }
        return new AdapterWishList.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        final FavItem favItem = favItems.get(position);
        if (!favItem.getProduct().getMedia().isEmpty()) {
            String image = favItem.getProduct().getMedia().get(0).getLink();
            if (image != null && !image.isEmpty()) {
                Picasso.get().load(image).error(R.drawable.image_not_foundpng).resize(150, 150).into(holder.imageView);
            } else
                Picasso.get().load(R.drawable.image_not_foundpng).error(R.drawable.image_not_foundpng).resize(150, 150).into(holder.imageView);
        } else
            Picasso.get().load(R.drawable.image_not_foundpng).error(R.drawable.image_not_foundpng).resize(150, 150).into(holder.imageView);

        holder.tv1.setText(favItem.getProduct().getName());
        holder.tv2.setText(favItem.getProduct().getPrice() + " "+context.getString(R.string.currency));

        String cat = "";
        if (favItem.getProduct().getMain_category() != null)
            cat = favItem.getProduct().getMain_category().getName();
        if (favItem.getProduct().getCategory() != null)
            cat = cat + " " + favItem.getProduct().getCategory().getName();

        holder.tv3.setText(cat);


        holder.ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (MainApplication.isConnected) {

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            final GeneralResponse generalResponse = RequestWrapper.getInstance().getProductLikeUnlike(v.getContext(), favItem.getProduct().getId());
                            if (generalResponse.isStatus()) {
                                favItems.remove(position);
                                if (context != null)
                                    context.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            notifyDataSetChanged();
                                        }
                                    });
                            } else if (context != null)
                                context.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Snackbar.make(v, generalResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                    }
                                });
                        }
                    }).start();

                } else {
                    Snackbar.make(v, v.getContext().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onProductClickListener != null)
                    onProductClickListener.onProductClick(favItem.getProduct(), position);

            }
        });


    }

    @Override
    public int getItemCount() {
        return favItems.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv1, tv2, tv3;
        View view;
        ImageView imageView, ivLike;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.ring_secand_myorder);
            tv1 = (TextView) itemView.findViewById(R.id.gold_rin);
            tv3 = (TextView) itemView.findViewById(R.id.pro);
            tv2 = (TextView) itemView.findViewById(R.id.kwd15);
            ivLike = itemView.findViewById(R.id.garpeg_secand);

        }
    }
}
