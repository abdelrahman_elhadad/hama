package hama.alsaygh.kw.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.cart.CartItem;
import hama.alsaygh.kw.model.product.Product;

public class AdapterRateHamaSlider extends RecyclerView.Adapter<AdapterRateHamaSlider.ViewHolder> {
    LayoutInflater layoutInflater;

    List<CartItem> cartItems = new ArrayList<>();
    SparseArray<Float> sparseArray = new SparseArray<>();

    Context context;

    public AdapterRateHamaSlider(LayoutInflater layoutInflater, Context context, List<CartItem> cartItems) {
        this.layoutInflater = layoutInflater;
        this.context = context;
        this.cartItems = cartItems;
    }


    @NonNull
    @Override
    public AdapterRateHamaSlider.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_slider_rate_hama, parent, false);

        return new ViewHolder(view);
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(@NonNull AdapterRateHamaSlider.ViewHolder holder, int position) {

        final CartItem favItem = cartItems.get(position);

        final Product product = favItem.getProduct();
        if (product != null) {

            if (!favItem.getProduct().getMedia().isEmpty()) {
                String image = favItem.getProduct().getMedia().get(0).getLink();
                if (image != null && !image.isEmpty()) {
                    Picasso.get().load(image).error(R.drawable.image_not_foundpng).resize(150, 150).into(holder.imageView);
                } else
                    Picasso.get().load(R.drawable.image_not_foundpng).error(R.drawable.image_not_foundpng).resize(150, 150).into(holder.imageView);
            } else
                Picasso.get().load(R.drawable.image_not_foundpng).error(R.drawable.image_not_foundpng).resize(150, 150).into(holder.imageView);

        }

        if (sparseArray.get(position) != null)
            holder.rb_product.setRating(sparseArray.get(position));
        else
            holder.rb_product.setRating(0);
        holder.rb_product.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {


                sparseArray.put(position, rating);
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public SparseArray<Float> getRateArray() {
        return sparseArray;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        RatingBar rb_product;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView72);
            rb_product = itemView.findViewById(R.id.rb_product);
        }
    }
}
