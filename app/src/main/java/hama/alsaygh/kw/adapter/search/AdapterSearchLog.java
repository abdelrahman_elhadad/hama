package hama.alsaygh.kw.adapter.search;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.listener.OnSearchLogListener;
import hama.alsaygh.kw.model.searchLog.SearchLog;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class AdapterSearchLog extends RecyclerView.Adapter<AdapterSearchLog.Holder> {
    List<SearchLog> addresses;
    private final OnSearchLogListener onNoteListener;
    public static String TAG = "AdapterSearchLog";

    public AdapterSearchLog(OnSearchLogListener onNoteListener, List<SearchLog> products) {
        this.onNoteListener = onNoteListener;
        this.addresses = products;
    }

    @NonNull
    @Override
    public AdapterSearchLog.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search_log, parent, false);
        return new AdapterSearchLog.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterSearchLog.Holder holder, final int position) {


        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(holder.itemView.getContext())) {
            holder.iv_item.setImageResource(R.drawable.ic_icon_time_dark);
            holder.tv_item.setTextColor(ContextCompat.getColor(holder.tv_item.getContext(), R.color.whiteColor));
        } else {
            holder.iv_item.setImageResource(R.drawable.clock);
            holder.tv_item.setTextColor(ContextCompat.getColor(holder.tv_item.getContext(), R.color.blackcolor1));
        }

        final SearchLog searchLog = addresses.get(position);
        holder.tv_item.setText(searchLog.getKey());
        holder.itemView.setOnClickListener(v -> {
            if (onNoteListener != null)
                onNoteListener.onSearchLogClick(searchLog, position);
        });

    }

    @Override
    public int getItemCount() {
        return addresses.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv_item;
        ImageView iv_item;
        View viewLine;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_item = itemView.findViewById(R.id.tv_item);
            viewLine = itemView.findViewById(R.id.viewLine);
            iv_item = itemView.findViewById(R.id.iv_item);
        }
    }
}
