package hama.alsaygh.kw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import hama.alsaygh.kw.R;
import com.rd.PageIndicatorView;

public class Adapter_pager_rate_order extends PagerAdapter {
    LayoutInflater layoutInflater;
    private ViewPager viewPager;
    private LinearLayout linearLayout;
    PageIndicatorView pageIndicatorView;
    private int MCurentPage;
    Context context;
    ImageView imageView,imageView1;
    TextView textView;
    public Adapter_pager_rate_order(LayoutInflater layoutInflater, Context context) {
        this.layoutInflater = layoutInflater;
        this.context = context;
    }
    public int[] slide_img = {
            R.drawable.twinzz,
            R.drawable.twinzz,
            R.drawable.twinzz
    };
    @Override
    public int getCount() {
        return slide_img.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view== ((View) object);
    }
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = LayoutInflater.from(context).inflate(R.layout.card_pager_rate_hama,container, false);
        imageView1 = (ImageView)view.findViewById(R.id.imageView49);
        imageView1.setBackgroundResource(slide_img[position]);
        container.addView(view);
        return view;
    }
}
