package hama.alsaygh.kw.adapter.marketPrice;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.marketPrice.CaliberItem;

public class AdapterGoldMarketDark extends RecyclerView.Adapter<AdapterGoldMarketDark.Holder> {
    List<CaliberItem> myordersArray;

    public AdapterGoldMarketDark(List<CaliberItem> myordersArray) {
        this.myordersArray = myordersArray;
    }

    @NonNull
    @Override
    public AdapterGoldMarketDark.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_market_price_dark, parent, false);
        return new AdapterGoldMarketDark.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterGoldMarketDark.Holder holder, int position) {
        CaliberItem caliberItem = myordersArray.get(position);
        if (caliberItem.isUp())
            holder.imageView.setImageResource(R.drawable.ic_solid12);
        else
            holder.imageView.setImageResource(R.drawable.ic_solid);

        holder.tv1.setText(holder.tv1.getContext().getString(R.string.Caliber_A).replace("xx", caliberItem.getCaliber()));
        holder.tv2.setText(caliberItem.getPrice() + "");
    }

    @Override
    public int getItemCount() {
        return myordersArray.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv1, tv2;
        ImageView imageView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView19);
            tv1 = (TextView) itemView.findViewById(R.id.textView112);
            tv2 = (TextView) itemView.findViewById(R.id.textView134);


        }
    }
}
