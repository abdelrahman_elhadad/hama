package hama.alsaygh.kw.adapter.marketPrice;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.fragment.marketPrice.GoldMarketFragment;


public class AdapterSliderMarketPrice extends FragmentPagerAdapter {
    private int number_of_Tab;
    private Context context;
    public AdapterSliderMarketPrice(Context contex,@NonNull FragmentManager fm , int number_of_Tab) {
        super(fm);
        this.number_of_Tab=number_of_Tab;
        this.context=contex;
    }



    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return  GoldMarketFragment.newInstance();

            default:
                return GoldMarketFragment.newInstance();
        }
    }


    @Override
    public int getCount() {
        return number_of_Tab;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.Gold_Market);
            case 1:
                return   context.getString(R.string.silver_market);

            default:
                return context.getString(R.string.Gold_Market);
        }
    }
}
