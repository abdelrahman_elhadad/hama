package hama.alsaygh.kw.adapter.search.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.listener.OnStoreClickListener;
import hama.alsaygh.kw.model.store.Store;

public class AdapterSearchForStores extends  RecyclerView.Adapter<AdapterSearchForStores.Holder> {
    ArrayList<Store> best_stores;
    Context context;
    public static String TAG = "adapter home best stores";
    private static OnStoreClickListener onNoteListener;


    public AdapterSearchForStores(ArrayList<Store> Best_stores,Context context,OnStoreClickListener onNoteListener) {
        this.best_stores = Best_stores;
        this.context= context;
        this.onNoteListener = onNoteListener;

    }

    public ArrayList<Store> getMainCategorts() {
        return best_stores;
    }

    public void setMainCategorts(ArrayList<Store> mainCategorts) {
        this.best_stores = mainCategorts;
    }

    @NonNull
    @Override
    public AdapterSearchForStores.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_search_for_stores,parent,false);
        return new  AdapterSearchForStores.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterSearchForStores.Holder holder, int position) {
        holder.tv1.setText(best_stores.get(position).getStore_name());
        holder.tv2.setText(best_stores.get(position).getProducts()+ " " + context.getString(R.string.products));
        if (best_stores.get(position).getLogo() != null && !best_stores.get(position).getLogo().isEmpty())
            Picasso.get().load(best_stores.get(position).getLogo()).error(R.color.whiteColor).placeholder(R.color.whiteColor).into(holder.imageView);
        else
            holder.imageView.setImageResource(R.color.whiteColor);

    }

    @Override
    public int getItemCount() {
        return best_stores.size();
    }
    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv1 ,tv2;
        View view;
        ImageView imageView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView =(ImageView)itemView.findViewById(R.id.imageView5);
            tv1 =(TextView)itemView.findViewById(R.id.textView13);
            tv2 =(TextView)itemView.findViewById(R.id.textView11);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onNoteListener.onStoreClick(best_stores.get(getAdapterPosition()),getAdapterPosition(), TAG);

        }
    }
}