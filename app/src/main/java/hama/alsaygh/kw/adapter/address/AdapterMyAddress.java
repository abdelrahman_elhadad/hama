package hama.alsaygh.kw.adapter.address;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.AddressResponse;
import hama.alsaygh.kw.app.MainApplication;
import hama.alsaygh.kw.listener.OnAddressListener;
import hama.alsaygh.kw.model.address.Address;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterMyAddress extends RecyclerView.Adapter<AdapterMyAddress.Holder> {
    List<Address> addresses;
    private final OnAddressListener onNoteListener;
    public static String TAG = "adapter store page";

    private Activity activity;

    public AdapterMyAddress(Activity activity, OnAddressListener onNoteListener, List<Address> products) {
        this.onNoteListener = onNoteListener;
        this.addresses = products;
        this.activity = activity;
    }

    @NonNull
    @Override
    public AdapterMyAddress.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_address_item, null, false);
        return new AdapterMyAddress.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterMyAddress.Holder holder, final int position) {


        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(holder.itemView.getContext())) {
            holder.radioButton.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(holder.iv_edit.getContext(), R.color.textviewhome)));
            holder.radioButton.setHighlightColor(ContextCompat.getColor(holder.iv_edit.getContext(), R.color.textviewhome));
            holder.tv_address.setTextColor(ContextCompat.getColor(holder.iv_edit.getContext(), R.color.whiteColor));
            holder.tv_title.setTextColor(ContextCompat.getColor(holder.iv_edit.getContext(), R.color.whiteColor));
            holder.iv_edit.setImageResource(R.drawable.ic_path_dark);
        } else {
            holder.tv_address.setTextColor(ContextCompat.getColor(holder.iv_edit.getContext(), R.color.blackcolor));
            holder.tv_title.setTextColor(ContextCompat.getColor(holder.iv_edit.getContext(), R.color.blackcolor));
            holder.iv_edit.setImageResource(R.drawable.ic_path_12934);
            holder.radioButton.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(holder.iv_edit.getContext(), R.color.color_navigation)));
            holder.radioButton.setHighlightColor(ContextCompat.getColor(holder.iv_edit.getContext(), R.color.color_navigation));
        }

        final Address address = addresses.get(position);
        String addressTitle = address.getStreet() + " " + address.getBuilding_no();
        String addressLine = address.getCity() + "," + address.getStreet() + "," + address.getBuilding_no()+" - "+address.getZip_code();

        holder.tv_title.setText(addressTitle);
        holder.tv_address.setText(addressLine);

        holder.radioButton.setChecked(address.isPrimary());

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (MainApplication.isConnected) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            final AddressResponse addressResponse = RequestWrapper.getInstance().setAsPrimaryAddress(v.getContext(), address.getId());
                            if (addressResponse.isStatus()) {

                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        refresh(address.getId());
                                    }
                                });

                            } else {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Snackbar.make(v, addressResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                                    }
                                });

                            }

                        }
                    }).start();
                } else {
                    Snackbar.make(v, v.getContext().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        holder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onNoteListener != null)
                    onNoteListener.onAddressClick(v, address, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return addresses.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv_title, tv_address;
        ImageView iv_edit;
        RadioButton radioButton;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_address = (TextView) itemView.findViewById(R.id.tv_address);
            iv_edit = itemView.findViewById(R.id.iv_edit);
            radioButton = itemView.findViewById(R.id.radioButton);
        }


    }


    private void refresh(int id) {
        for (int i = 0; i < addresses.size(); i++) {
            Address address = addresses.get(i);
            if (address.getId() == id) {
                address.setPrimary(true);
            } else
                address.setPrimary(false);

            addresses.remove(i);
            addresses.add(i, address);
        }
        notifyDataSetChanged();
    }

}
