package hama.alsaygh.kw.adapter.store;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.listener.OnStoreClickListener;
import hama.alsaygh.kw.model.ads.BannerAds;
import hama.alsaygh.kw.model.store.Store;
import hama.alsaygh.kw.utils.Utils;

public class AdapterStoresSpannedDark extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<Serializable> best_stores;
    public static String TAG = "adapter stores spanned dark";
    private static OnStoreClickListener onNoteListener;


    Context context;
    private final int TYPE_Ads = 1;
    private final int TYPE_STORE = 2;

    public AdapterStoresSpannedDark(OnStoreClickListener onNoteListener, ArrayList<Serializable> Best_stores, Context context) {
        this.best_stores = Best_stores;
        this.context = context;
        this.onNoteListener = onNoteListener;

    }

    public ArrayList<Serializable> getMainCategorts() {
        return best_stores;
    }

    public void setMainCategorts(ArrayList<Serializable> mainCategorts) {
        this.best_stores = mainCategorts;
    }

    @Override
    public int getItemViewType(int position) {

        if (best_stores.get(position) instanceof BannerAds) {
            return TYPE_Ads;
        }
        return TYPE_STORE;
    }


    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (viewType == TYPE_Ads) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ads_dark, parent, false);
            return new AdapterStoresSpannedDark.AdsHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_stores_spanned_dark, parent, false);
            return new AdapterStoresSpannedDark.Holder(v);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {


        if (viewHolder instanceof Holder) {
            Log.i("ppppppp", "postion :" + position + " typeView store");
            Holder holder = (Holder) viewHolder;
            Store bestStores = (Store) best_stores.get(position);
            holder.tv1.setText(bestStores.getStore_name());
            holder.tv2.setText(bestStores.getProducts() + " " + context.getString(R.string.products));
            if (bestStores.getLogo() != null && !bestStores.getLogo().isEmpty()) {
                Utils.getInstance().setBackgroundImageColor(holder.imageView, bestStores.getLogo(),ContextCompat.getColor(holder.imageView.getContext(), R.color.whiteColor));
                Picasso.get().load(bestStores.getLogo()).error(R.color.whiteColor).placeholder(R.color.whiteColor).into(holder.imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.whiteColor));
                    }
                });
            } else {
                Picasso.get().load(R.color.whiteColor).into(holder.imageView);
                holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.whiteColor));
            }
        } else if (viewHolder instanceof AdsHolder) {
            Log.i("ppppppp", "postion :" + position + " typeView ads");
            final AdsHolder holder = (AdsHolder) viewHolder;
            BannerAds bannerAds = (BannerAds) best_stores.get(position);
            holder.tv_ads.setText(bannerAds.getTitle() + "\n" + bannerAds.getDescription());

            Picasso.get().load(bannerAds.getImage()).into(holder.iv_ads, new Callback() {
                @Override
                public void onSuccess() {
                    holder.pb_ads.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    holder.pb_ads.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return best_stores.size();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv1, tv2;
        View view;
        ImageView imageView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.img_sberena);
            tv1 = (TextView) itemView.findViewById(R.id.text_sberena);
            tv2 = (TextView) itemView.findViewById(R.id.text_product);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            onNoteListener.onStoreClick((Store) best_stores.get(getAdapterPosition()), getAdapterPosition(), TAG);

        }
    }

    public class AdsHolder extends RecyclerView.ViewHolder {

        TextView tv_ads;
        ImageView iv_ads;
        ProgressBar pb_ads;

        public AdsHolder(@NonNull View itemView) {
            super(itemView);
            tv_ads = itemView.findViewById(R.id.tv_ads);
            iv_ads = itemView.findViewById(R.id.iv_ads);
            pb_ads = itemView.findViewById(R.id.pb_ads);
        }
    }
}
