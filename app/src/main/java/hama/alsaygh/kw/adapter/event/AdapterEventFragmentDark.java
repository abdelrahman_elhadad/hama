package hama.alsaygh.kw.adapter.event;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.event.Event;

import java.util.ArrayList;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterEventFragmentDark extends RecyclerView.Adapter<AdapterEventFragmentDark.Holder> {

    ArrayList<Event> myeventArray;


    public AdapterEventFragmentDark(ArrayList<Event> myeventArray) {
        this.myeventArray = myeventArray;
    }

    @NonNull
    @Override
    public AdapterEventFragmentDark.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_my_event_dark, parent, false);
        return new AdapterEventFragmentDark.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterEventFragmentDark.Holder holder, int position) {
        holder.tv1.setText(myeventArray.get(position).getTitle());
        holder.tv2.setText(myeventArray.get(position).getDate());
        holder.tv3.setText(myeventArray.get(position).getTime());

        if (Locale.getDefault().getLanguage().equals("ar")) {
            holder.view11.setBackgroundResource(R.drawable.back_card_my_event_ar);
        }

    }

    @Override
    public int getItemCount() {
        return myeventArray.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv1, tv2, tv3;
        View view;
        ImageView imageView;
        View view11;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tv1 = (TextView) itemView.findViewById(R.id.textView101);
            tv2 = (TextView) itemView.findViewById(R.id.textView102);
            tv3 = (TextView) itemView.findViewById(R.id.textView103);
            view11 = (View) itemView.findViewById(R.id.view16);

        }
    }
}
