package hama.alsaygh.kw.adapter.onBoard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.db.table.OnBoard;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class OnBoardSliderAdapter extends PagerAdapter {
    LayoutInflater layoutInflater;
    TextView tvTitle, tvDesc;
    Context context;
    ImageView ivImage, ivBg;
    List<OnBoard> onBoards;

    public OnBoardSliderAdapter(LayoutInflater layoutInflater, Context context, List<OnBoard> onBoards) {
        this.layoutInflater = layoutInflater;
        this.context = context;
        this.onBoards = onBoards;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // super.destroyItem(container, position, object);
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public int getCount() {
        return onBoards.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = LayoutInflater.from(context).inflate(R.layout.card_home, container, false);
        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
            view = LayoutInflater.from(context).inflate(R.layout.card_home_dark, container, false);
        }

        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvDesc = (TextView) view.findViewById(R.id.tv_desc);
        ivImage = (ImageView) view.findViewById(R.id.iv_image);
        ivBg = (ImageView) view.findViewById(R.id.iv_bg);
        tvTitle.setText(onBoards.get(position).getTitle());
        tvDesc.setText(onBoards.get(position).getShort_description());
//        ivImage.setBackgroundResource(img_logo[position]);
//        imageView43.setBackgroundResource(slider_img_lady[position]);
        Picasso.get().load(onBoards.get(position).getImage()).error(R.drawable.group).into(ivBg);

        container.addView(view);

        return view;
    }


}
