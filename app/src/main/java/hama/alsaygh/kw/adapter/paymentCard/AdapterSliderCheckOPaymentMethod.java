package hama.alsaygh.kw.adapter.paymentCard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.paymentCard.Card;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class AdapterSliderCheckOPaymentMethod extends RecyclerView.Adapter<AdapterSliderCheckOPaymentMethod.ViewHolder> {
    LayoutInflater layoutInflater;
    List<Card> cards;
    Context context;

    public AdapterSliderCheckOPaymentMethod(LayoutInflater layoutInflater, Context context, List<Card> images) {
        this.layoutInflater = layoutInflater;
        this.context = context;
        this.cards = images;
    }


    @NonNull
    @Override
    public AdapterSliderCheckOPaymentMethod.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_slider_payment_method,parent,false);

        return new AdapterSliderCheckOPaymentMethod.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterSliderCheckOPaymentMethod.ViewHolder holder, int position) {

        final Card card = cards.get(position);
        holder.tv_card_holder_name.setText(card.getHolder_name());
        holder.tv_card_number.setText(card.getCard_number());
        holder.tv_type.setText(card.getType());
        String date = card.getMonth() + "/" + card.getYear();
        holder.tv_expire.setText(date);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(holder.itemView.getContext())) {
            holder.parent_payment_method.setBackgroundResource(R.drawable.shape_round_light_brawon);
        } else {
            holder.parent_payment_method.setBackgroundResource(R.drawable.shape_round_light_green);
        }
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_type, tv_card_number, tv_card_holder_name, tv_expire;
        RelativeLayout parent_payment_method;
        ImageView iv_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_type = (TextView) itemView.findViewById(R.id.tv_type);
            tv_card_number = (TextView) itemView.findViewById(R.id.tv_card_number);
            tv_expire = (TextView) itemView.findViewById(R.id.tv_expire);
            tv_card_holder_name = (TextView) itemView.findViewById(R.id.tv_card_holder_name);
            parent_payment_method = itemView.findViewById(R.id.parent_payment_method);
            iv_image = itemView.findViewById(R.id.iv_image);

        }
    }

    public Card getCurrentItem(int position)
    {
        return cards.get(position);
    }
}
