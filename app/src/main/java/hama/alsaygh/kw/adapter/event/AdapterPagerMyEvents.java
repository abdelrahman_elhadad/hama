package hama.alsaygh.kw.adapter.event;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import hama.alsaygh.kw.fragment.settings.events.CalenderAddNewEvent;
import hama.alsaygh.kw.fragment.settings.events.EventFragment;

public class AdapterPagerMyEvents extends FragmentPagerAdapter {
    private int number_of_Tab;

    public AdapterPagerMyEvents(@NonNull FragmentManager fm, int number_of_Tab) {
        super(fm);
        this.number_of_Tab = number_of_Tab;
    }

    //    public AdapterPagerMyOrder(@NonNull FragmentManager fm, int behavior int number_of_Tab) {
//        super(fm, behavior);
//        this.number_of_Tab=number_of_Tab;
//
//    }
    EventFragment eventFragment = new EventFragment();

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return eventFragment;
            case 1:
                return new CalenderAddNewEvent();

            default:
                return null;


        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return number_of_Tab;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "My_event";
            case 1:
                return "CalenderAddNewEvent";

            default:
                return "pending";
        }
    }
}
