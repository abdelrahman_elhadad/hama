package hama.alsaygh.kw.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hama.alsaygh.kw.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterGoldSubCategories2 extends  RecyclerView.Adapter<AdapterGoldSubCategories2.Holder> {
    @NonNull
    @Override
    public AdapterGoldSubCategories2.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_gold_sub_categories2,null,false);
        AdapterGoldSubCategories2.Holder holder = new AdapterGoldSubCategories2.Holder(v);
        return new  AdapterGoldSubCategories2.Holder(v);    }

    @Override
    public void onBindViewHolder(@NonNull AdapterGoldSubCategories2.Holder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
    public class Holder extends RecyclerView.ViewHolder {
        TextView tv1;
        View view;
        ImageView imageView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView =(ImageView)itemView.findViewById(R.id.imageView6);
            imageView =(ImageView)itemView.findViewById(R.id.imageView15);

            tv1=(TextView)itemView.findViewById(R.id.textView14);

        }
    }
}
