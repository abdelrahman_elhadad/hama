package hama.alsaygh.kw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import hama.alsaygh.kw.R;
import com.rd.PageIndicatorView;

public class AdapterVideoUpload extends PagerAdapter {
    LayoutInflater layoutInflater;
    private ViewPager viewPager;
    private LinearLayout linearLayout;
    PageIndicatorView pageIndicatorView;
    private int MCurentPage;
    Context context;
    VideoView videoView;
    ImageView imageView,imageView1;
    TextView textView;

    public AdapterVideoUpload(LayoutInflater layoutInflater, Context context) {
        this.layoutInflater = layoutInflater;
        this.context = context;
    }
    public int[] slide_img = {

    };
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return false;
    }
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = LayoutInflater.from(context).inflate(R.layout.card_pager_upload_video,container, false);
        videoView= (VideoView)view.findViewById(R.id.imageView24);
        imageView1 = (ImageView)view.findViewById(R.id.imageView24);
        imageView1.setBackgroundResource(slide_img[position]);
        container.addView(view);
        return view;
    }
}
