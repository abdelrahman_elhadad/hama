package hama.alsaygh.kw.adapter.ads;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.rd.PageIndicatorView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.listener.OnAdsClickListener;
import hama.alsaygh.kw.model.ads.MainAds;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class MainAdsHomeAdapterSlider extends PagerAdapter {
    LayoutInflater layoutInflater;
    private ViewPager viewPager;
    private LinearLayout linearLayout;
    PageIndicatorView pageIndicatorView;
    private int MCurentPage;
    Context context;
    ImageView iv_ads_image, iv_ads_bg;
    TextView tv_ads_title;
    List<MainAds> mainAdsList;
    OnAdsClickListener onAdsClickListener;


    public MainAdsHomeAdapterSlider(LayoutInflater layoutInflater, Context context, List<MainAds> mainAds, OnAdsClickListener onAdsClickListener) {
        this.layoutInflater = layoutInflater;
        this.context = context;
        this.mainAdsList = mainAds;
        this.onAdsClickListener = onAdsClickListener;
    }


    @Override
    public int getCount() {
        return mainAdsList.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // super.destroyItem(container, position, object);
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        View view = LayoutInflater.from(context).inflate(R.layout.card_slider_home1, container, false);

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
            view = LayoutInflater.from(context).inflate(R.layout.card_slider_home1_dark, container, false);
        }

        iv_ads_bg = (ImageView) view.findViewById(R.id.iv_ads_bg);
        iv_ads_image = (ImageView) view.findViewById(R.id.iv_ads_image);
        tv_ads_title = (TextView) view.findViewById(R.id.tv_ads_title);
        final ProgressBar pb_ads_image = view.findViewById(R.id.pb_ads_image);
        tv_ads_title.setText(mainAdsList.get(position).getTitle() + "\n" + mainAdsList.get(position).getDescription());

        Picasso.get().load(mainAdsList.get(position).getImage()).into(iv_ads_image, new Callback() {
            @Override
            public void onSuccess() {
                pb_ads_image.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                pb_ads_image.setVisibility(View.GONE);
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainAdsList.get(position).getRedirect_type().equalsIgnoreCase("url")) {

                    String url = mainAdsList.get(position).getUrl();
                    if (url != null && !url.isEmpty()) {

                        try {
                            if (!url.startsWith("http://") && !url.startsWith("https://"))
                                url = "http://" + url;
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            context.startActivity(browserIntent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (mainAdsList.get(position).getRedirect_type().equalsIgnoreCase("store")) {
                    if (onAdsClickListener != null)
                        onAdsClickListener.onAdsClick(mainAdsList.get(position).getRedirect_type(), mainAdsList.get(position).getVendor_id());
                } else if (mainAdsList.get(position).getRedirect_type().equalsIgnoreCase("product")) {
                    onAdsClickListener.onAdsClick(mainAdsList.get(position).getRedirect_type(), mainAdsList.get(position).getProduct_id());
                } else
                    onAdsClickListener.onAdsClick(mainAdsList.get(position).getRedirect_type(), mainAdsList.get(position).getCategory_id());


            }
        });

        container.addView(view);
        return view;
    }
}
