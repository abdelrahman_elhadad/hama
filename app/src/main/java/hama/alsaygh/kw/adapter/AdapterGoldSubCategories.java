package hama.alsaygh.kw.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hama.alsaygh.kw.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterGoldSubCategories extends RecyclerView.Adapter<AdapterGoldSubCategories.Holder> {
    @NonNull
    @Override
    public AdapterGoldSubCategories.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_gold_sub_categories,null,false);
        AdapterGoldSubCategories.Holder holder = new AdapterGoldSubCategories.Holder(v);
        return new  AdapterGoldSubCategories.Holder(v);      }

    @Override
    public void onBindViewHolder(@NonNull AdapterGoldSubCategories.Holder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
    public class Holder extends RecyclerView.ViewHolder {
        TextView tv1;
        View view;
        ImageView imageView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView =(ImageView)itemView.findViewById(R.id.imageView6);
            tv1=(TextView)itemView.findViewById(R.id.textView14);

        }
    }
}
