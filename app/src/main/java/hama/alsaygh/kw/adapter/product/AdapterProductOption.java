package hama.alsaygh.kw.adapter.product;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.listener.OnChildOptionListener;
import hama.alsaygh.kw.model.product.Option;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;

public class AdapterProductOption extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Option> favItems;
    FragmentActivity context;
    OnChildOptionListener onProductClickListener;

    final int DROPDOWN = 2;

    private int selectedPosition = -1;

    public AdapterProductOption(FragmentActivity context, List<Option> myordersArray, OnChildOptionListener onProductClickListener) {
        this.favItems = myordersArray;
        this.context = context;
        this.onProductClickListener = onProductClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return DROPDOWN;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_option_spinner_item, parent, false);
        return new HolderSpinner(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int index) {
        final int position = index;
        HolderSpinner holderSpinner = (HolderSpinner) holder;
        holderSpinner.tv_name.setText(favItems.get(position).getName());
        String price = favItems.get(position).getPrice() + " " + holderSpinner.tv_price.getContext().getString(R.string.currency);
        holderSpinner.tv_price.setText(price);
        holderSpinner.rb_name.setChecked(selectedPosition == position);
        if (favItems.get(position).getAvailable_quantity() == 0) {
            holderSpinner.tv_out_of_stock.setVisibility(View.VISIBLE);
            holderSpinner.rb_name.setEnabled(false);
        } else {
            holderSpinner.tv_out_of_stock.setVisibility(View.GONE);
            holderSpinner.rb_name.setEnabled(true);
        }

        if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {
            holderSpinner.tv_name.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));
            holderSpinner.tv_price.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));
        }

        holderSpinner.rl_main.setOnClickListener(v -> {
            if (favItems.get(position).getAvailable_quantity() != 0) {
                setSelected(position);
            }
        });

        if (favItems.get(position).getColor() != null && !favItems.get(position).getColor().isEmpty()) {
            int color = ContextCompat.getColor(holderSpinner.tv_color.getContext(), R.color.whiteColor);
            try {
                String hexa = favItems.get(position).getColor();
                if (hexa.length() == 4) {
                    hexa = hexa.substring(1);
                    hexa = "#" + hexa + hexa;
                }

                color = Color.parseColor(hexa);
            } catch (Exception e) {
                e.printStackTrace();
            }
            holderSpinner.tv_color.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_IN);
            holderSpinner.ll_color.setVisibility(View.VISIBLE);
        } else {
            holderSpinner.ll_color.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return favItems.size();
    }

    private void setSelected(int position) {
        if (selectedPosition != -1) {
            Option option = favItems.get(selectedPosition);
            favItems.remove(selectedPosition);
            favItems.add(selectedPosition, option);
        }

        selectedPosition = position;

        Option option = favItems.get(position);
        favItems.remove(position);
        favItems.add(position, option);

        if (onProductClickListener != null)
            onProductClickListener.onChildOptionSelect(favItems.get(position));
        notifyDataSetChanged();
    }


    public void setSelectedByID(int id) {

        for (int i = 0; i < favItems.size(); i++) {
            if (favItems.get(i).getId() == id) {
                setSelected(i);
                break;
            }

        }
    }


    public static class HolderSpinner extends RecyclerView.ViewHolder {
        TextView tv_price, tv_name, tv_out_of_stock;
        LinearLayout rl_main, ll_color;
        RadioButton rb_name;
        View tv_color;

        public HolderSpinner(@NonNull View itemView) {
            super(itemView);
            rl_main = itemView.findViewById(R.id.rl_main);
            ll_color = itemView.findViewById(R.id.ll_color);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_color = itemView.findViewById(R.id.tv_color);
            tv_out_of_stock = itemView.findViewById(R.id.tv_out_of_stock);
            rb_name = itemView.findViewById(R.id.rb_name);
        }
    }
}
