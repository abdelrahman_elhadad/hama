package hama.alsaygh.kw.adapter.review;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.product.review.Review;
import hama.alsaygh.kw.utils.image.CircleTransform;

public class AdapterProductDetailsReview extends RecyclerView.Adapter<AdapterProductDetailsReview.Holder_details2> {

    List<Review> Reviews;

    public AdapterProductDetailsReview(List<Review> Reviews) {
        this.Reviews = Reviews;
    }

    @NonNull
    @Override
    public Holder_details2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_product_details2, parent, false);
        return new AdapterProductDetailsReview.Holder_details2(v);
    }

    @Override
    public int getItemCount() {
        return Reviews.size();
    }

    @Override
    public void onBindViewHolder(@NonNull Holder_details2 holder, int position) {
        holder.tv1.setText(Reviews.get(position).getUser().getF_name() + " " + Reviews.get(position).getUser().getL_name());
        holder.rb_review.setRating(Reviews.get(position).getRate());
        holder.tv2.setText(Reviews.get(position).getReview());

        if (Reviews.get(position).getUser().getAvatar()!= null && !Reviews.get(position).getUser().getAvatar().isEmpty())
            Picasso.get().load(Reviews.get(position).getUser().getAvatar()).fit().transform(new CircleTransform()).into(holder.imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    Picasso.get().load(R.drawable.image_not_foundpng).transform(new CircleTransform()).into(holder.imageView);
                }
            });
        else
            Picasso.get().load(R.drawable.image_not_foundpng).transform(new CircleTransform()).into(holder.imageView);

    }

    public class Holder_details2 extends RecyclerView.ViewHolder {
        TextView tv1, tv2;
        View view;
        ImageView imageView;
        RatingBar rb_review;

        public Holder_details2(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.person);
            rb_review = itemView.findViewById(R.id.rb_review);
            tv1 = itemView.findViewById(R.id.text_person);
            tv2 = itemView.findViewById(R.id.text_person1);


        }
    }
}
