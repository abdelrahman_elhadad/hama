package hama.alsaygh.kw.adapter.calculator;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import hama.alsaygh.kw.fragment.settings.calculator.RingSizeFences;
import hama.alsaygh.kw.fragment.settings.calculator.ZakatOnCodeFragment;

public class AdapterpagerCalculators extends FragmentPagerAdapter {
    private int number_of_Tab;
    public AdapterpagerCalculators(@NonNull FragmentManager fm , int number_of_Tab) {
        super(fm);
        this.number_of_Tab=number_of_Tab;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new ZakatOnCodeFragment();
            case 1:
               // return new CalculatorOneFifthOfGold();
            case 2:
                return new RingSizeFences();
            default:
                return null;


        }
    }


    @Override
    public int getCount() {
        return number_of_Tab;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "zakat_on_gold";
            case 1:
                return   "one_fifth_of_gold";
            case 3:
                return "ring_size_fonces";
            default:
                return "ring_size_fonces";
        }
    }
}
