package hama.alsaygh.kw.adapter.imageSlider;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.product.Media;
import hama.alsaygh.kw.utils.image.TouchImageView;

public class SliderZoomAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    TextView textView;
    List<Media> mediaList = new ArrayList<>();
    FragmentManager fragmentManager;

    public SliderZoomAdapter(Context context, LayoutInflater layoutInflater, FragmentManager fragmentManager, List<Media> media) {
        this.context = context;
        this.layoutInflater = layoutInflater;
        this.mediaList = media;
        this.fragmentManager = fragmentManager;
    }


    @Override
    public int getCount() {
        return mediaList.size();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //  container.removeView((LinearLayout)object);
        ((ViewPager) container).removeView((View) object);


    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = LayoutInflater.from(context).inflate(R.layout.row_zoom_image, container, false);
        TouchImageView touchImageView = view.findViewById(R.id.iv_display);
        final ProgressBar pbLoadingImage = view.findViewById(R.id.pb_loading_image);

        pbLoadingImage.setVisibility(View.VISIBLE);
        Picasso.get().load(mediaList.get(position).getLink()).placeholder(R.drawable.shape_alpha).into(touchImageView, new Callback() {
            @Override
            public void onSuccess() {
                pbLoadingImage.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                pbLoadingImage.setVisibility(View.GONE);
            }
        });

        container.addView(view);
        return view;
    }


}
