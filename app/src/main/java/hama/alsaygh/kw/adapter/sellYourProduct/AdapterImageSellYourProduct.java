package hama.alsaygh.kw.adapter.sellYourProduct;

import android.app.Activity;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.api.RequestWrapper;
import hama.alsaygh.kw.api.responce.ImageResponse;
import hama.alsaygh.kw.model.image.ImageUpload;

import org.json.JSONArray;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterImageSellYourProduct extends RecyclerView.Adapter<AdapterImageSellYourProduct.Holder> {
    ArrayList<ImageUpload> imageUploads;
    SparseArray<Thread> threadSparseArray = new SparseArray<>();
    Activity activity;

    public AdapterImageSellYourProduct(Activity activity, ArrayList<ImageUpload> imageUploads) {
        this.imageUploads = imageUploads;
        this.activity = activity;
    }

    @NonNull
    @Override
    public AdapterImageSellYourProduct.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_request_image, parent, false);
        return new AdapterImageSellYourProduct.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterImageSellYourProduct.Holder holder, final int position) {

        holder.iv_item.setImageURI(imageUploads.get(position).getUri());
        if (imageUploads.get(position).getImage() == null) {
            holder.tv_name.setText("");
            holder.pb_load.setVisibility(View.VISIBLE);
            holder.iv_delete.setVisibility(View.GONE);
        } else {
            holder.tv_name.setText(imageUploads.get(position).getImage().getFile_name());
            holder.pb_load.setVisibility(View.GONE);
            holder.iv_delete.setVisibility(View.VISIBLE);
        }

        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imageUploads.remove(position);
                notifyItemRemoved(position);
                notifyDataSetChanged();
            }
        });


        if (threadSparseArray.get(position) == null && imageUploads.get(position).getImage() == null) {

            startUploadImage(position, holder);

        } else {
            Log.i("nnnn", "jjjjj");
        }

    }

    private void startUploadImage(final int position, final Holder holder) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                final ImageResponse imageResponse = RequestWrapper.getInstance().uploadImage(holder.iv_item.getContext(), imageUploads.get(position).getPath());
                if (imageResponse.isStatus()) {

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            ImageUpload imageUpload = imageUploads.get(position);
                            imageUpload.setImage(imageResponse.getData());
                            imageUploads.remove(position);
                            imageUploads.add(position, imageUpload);

                            notifyItemChanged(position);
                            notifyDataSetChanged();

                        }
                    });
                }
            }
        });
        thread.start();
        threadSparseArray.put(position, thread);
    }

    @Override
    public int getItemCount() {
        return imageUploads.size();
    }

    public void addItem(ImageUpload imageUpload) {
        imageUploads.add(imageUpload);
        notifyDataSetChanged();
    }

    public void removeAll() {
        imageUploads.clear();
        notifyDataSetChanged();
    }

    public boolean isValid() {
        boolean isValid = true;

        for (int i = 0; i < imageUploads.size(); i++) {
            if (imageUploads.get(i).getImage() == null) {
                isValid = false;
                break;
            }
        }

        return isValid;
    }

    public JSONArray getImageName() {

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < imageUploads.size(); i++) {
            if (imageUploads.get(i).getImage() != null) {
                jsonArray.put(imageUploads.get(i).getImage().getFile_name());
            }
        }

        return jsonArray;
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_size;
        ImageView iv_item, iv_delete;
        ProgressBar pb_load;

        public Holder(@NonNull View itemView) {
            super(itemView);
            iv_item = itemView.findViewById(R.id.iv_item);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_size = itemView.findViewById(R.id.tv_size);
            pb_load = itemView.findViewById(R.id.pb_load);
        }
    }
}
