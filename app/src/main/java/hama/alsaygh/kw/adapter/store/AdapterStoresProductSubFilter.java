package hama.alsaygh.kw.adapter.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.model.category.Category;
import hama.alsaygh.kw.utils.SharedPreferenceConstant;
import hama.alsaygh.kw.utils.Utils;

public class AdapterStoresProductSubFilter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<Category> categories;
    private final Context context;
    private final int mainCategoryId;
    private final RecyclerView rc_item_child;
    private final AdapterStoresProductFilter adpter;

    public AdapterStoresProductSubFilter(Context context, List<Category> categories, int mainCategoryId, RecyclerView rc_item_child,AdapterStoresProductFilter adpter) {
        this.context = context;
        this.categories = categories;
        this.mainCategoryId = mainCategoryId;
        this.rc_item_child = rc_item_child;
        this.adpter=adpter;

        for (Category filterProduct:categories) {
            AdapterStoresProductSubChildFilter adapter2 = new AdapterStoresProductSubChildFilter(context, new ArrayList<>(), mainCategoryId, filterProduct.getId(),adpter);
            rc_item_child.setAdapter(adapter2);
            rc_item_child.setLayoutManager(new GridLayoutManager(context, 3));

        }

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_filter, parent, false);
        return new Holder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        Holder holder = (Holder) viewHolder;
        final Category filterProduct = (Category) categories.get(position);

        holder.tv_item.setText(filterProduct.getName());
        holder.itemView.setOnClickListener(v -> {

            Utils.getInstance().setCategory_level_1(mainCategoryId);
            Utils.getInstance().setCategory_level_2(filterProduct.getId());
            Utils.getInstance().setCategory_level_3(-1);

            adpter.notifyDataSetChanged();
            notifyDataSetChanged();

        });



        if (Utils.getInstance().getCategory_level_2() == filterProduct.getId() && Utils.getInstance().getCategory_level_1() == mainCategoryId) {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {

                holder.tv_item.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));
                holder.ll_item.setBackgroundResource(R.drawable.back_ground_rv_filter_dark_select);
            } else {
                holder.tv_item.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));
                holder.ll_item.setBackgroundResource(R.drawable.back_ground_rv_filter);
            }
            if (filterProduct.getChilds() != null && !filterProduct.getChilds().isEmpty()) {
                AdapterStoresProductSubChildFilter adapter = new AdapterStoresProductSubChildFilter(context, filterProduct.getChilds(), mainCategoryId, filterProduct.getId(),adpter);
                rc_item_child.setAdapter(adapter);
                rc_item_child.setLayoutManager(new GridLayoutManager(context, 3));
            } else {
                AdapterStoresProductSubChildFilter adapter = new AdapterStoresProductSubChildFilter(context, new ArrayList<>(), mainCategoryId, filterProduct.getId(),adpter);
                rc_item_child.setAdapter(adapter);
                rc_item_child.setLayoutManager(new GridLayoutManager(context, 3));
            }
        } else {
            if (SharedPreferenceConstant.getSharedPreferenceDarkMode(context)) {

                holder.tv_item.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));
                holder.ll_item.setBackgroundResource(R.drawable.back_ground_rv_filter_dark_unselect);
            } else {
                holder.tv_item.setTextColor(ContextCompat.getColor(context, R.color.blackcolor));
                holder.ll_item.setBackgroundResource(R.drawable.back_ground_rv_filter_unselect);
            }
            }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {
        TextView tv_item;
        LinearLayout ll_item;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ll_item = itemView.findViewById(R.id.liner_filter);
            tv_item = itemView.findViewById(R.id.tv_item);

        }


    }

}
