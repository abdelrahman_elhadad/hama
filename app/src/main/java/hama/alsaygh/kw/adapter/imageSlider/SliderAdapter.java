package hama.alsaygh.kw.adapter.imageSlider;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.dialog.OpenImageDialog;
import hama.alsaygh.kw.model.product.Media;
import hama.alsaygh.kw.utils.Utils;

public class SliderAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    TextView textView;
    List<Media> mediaList = new ArrayList<>();
    FragmentManager fragmentManager;

    public SliderAdapter(Context context, LayoutInflater layoutInflater, FragmentManager fragmentManager, List<Media> media) {
        this.context = context;
        this.layoutInflater = layoutInflater;
        this.mediaList = media;
        this.fragmentManager = fragmentManager;
    }


    @Override
    public int getCount() {
        return mediaList.size();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //  container.removeView((LinearLayout)object);
        ((ViewPager) container).removeView((View) object);


    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = LayoutInflater.from(context).inflate(R.layout.card_view_slider_adapter, container, false);
        ImageView imageView24 = view.findViewById(R.id.imageView24);
        if (mediaList.get(position).getLink() != null && !mediaList.get(position).getLink().isEmpty()) {
            Utils.getInstance().setBackgroundImageColor(imageView24, mediaList.get(position).getLink(),ContextCompat.getColor(imageView24.getContext(), R.color.color_image_not_found));
            Picasso.get().load(mediaList.get(position).getLink()).error(R.drawable.image_not_foundpng).into(imageView24, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    imageView24.setBackgroundColor(ContextCompat.getColor(imageView24.getContext(), R.color.color_image_not_found));

                }
            });
        } else {
            Picasso.get().load(R.drawable.image_not_foundpng).into(imageView24);
            imageView24.setBackgroundColor(ContextCompat.getColor(imageView24.getContext(), R.color.color_image_not_found));

        }
        imageView24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenImageDialog open = OpenImageDialog.newInstance(mediaList, position);
                open.show(fragmentManager, "open");

            }
        });

        container.addView(view);
        return view;
    }


}
