package hama.alsaygh.kw.adapter.product;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import hama.alsaygh.kw.R;
import hama.alsaygh.kw.listener.OnProductClickListener;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.utils.Utils;

public class AdapterStorePageProductsDark extends RecyclerView.Adapter<AdapterStorePageProductsDark.Holder> {
    ArrayList<Product> products;
    private static OnProductClickListener onNoteListener;
    public static String TAG = "adapter store page";
    private Context context;


    public AdapterStorePageProductsDark(Context context, OnProductClickListener onNoteListener, ArrayList<Product> products) {
        this.onNoteListener = onNoteListener;
        this.products = products;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterStorePageProductsDark.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_store_page_dark, parent, false);
        return new AdapterStorePageProductsDark.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterStorePageProductsDark.Holder holder, final int index) {

        final int position = index;
        holder.tv1.setText(products.get(position).getName());
        holder.tv2.setText(products.get(position).getPrice() + " " + context.getString(R.string.currency));
        holder.tv3.setText(products.get(position).getRate() + "");

        if (products.get(position).isI_fav()) {
            Drawable drawable = AppCompatResources.getDrawable(context, R.drawable.ic_loveeecolor);
            holder.imageView1.setImageDrawable(drawable);
        } else {
            Drawable drawable = AppCompatResources.getDrawable(context, R.drawable.ic_loveee);
            holder.imageView1.setImageDrawable(drawable);
        }

        if (products.get(position).getMedia() != null && !products.get(position).getMedia().isEmpty()) {
            Utils.getInstance().setBackgroundImageColor(holder.imageView, products.get(position).getMedia().get(0).getLink(),ContextCompat.getColor(holder.imageView.getContext(), R.color.color_image_not_found));
            Picasso.get().load(products.get(position).getMedia().get(0).getLink()).error(R.drawable.image_not_foundpng).into(holder.imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.color_image_not_found));

                }
            });
        } else {
            Picasso.get().load(R.drawable.image_not_foundpng).into(holder.imageView);
            holder.imageView.setBackgroundColor(ContextCompat.getColor(holder.imageView.getContext(), R.color.color_image_not_found));

        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onNoteListener != null)
                    onNoteListener.onProductClick(products.get(position), position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tv1, tv2, tv3;
        View view;
        ImageView imageView, imageView1, imageView2;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.twenz_store_page);
            imageView1 = (ImageView) itemView.findViewById(R.id.love_img);
            tv1 = (TextView) itemView.findViewById(R.id.storepage_text);
            tv2 = (TextView) itemView.findViewById(R.id.store_page_number);
            imageView2 = (ImageView) itemView.findViewById(R.id.store_page_star);
            tv3 = (TextView) itemView.findViewById(R.id.store_page_number1);


        }


    }
}
