package hama.alsaygh.kw.model.cart;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import hama.alsaygh.kw.model.product.OptionChild;
import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.model.store.Store;

public class CartItem implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("product")
    private Product product;

    @SerializedName("option")
    private OptionChild option;

    @SerializedName("store")
    private Store store;

    @SerializedName("quantity")
    private int quantity;

    @SerializedName("unit_price")
    private double unit_price;

    @SerializedName("total")
    private double total;

    public double getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(double unit_price) {
        this.unit_price = unit_price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public OptionChild getOption() {
        return option;
    }

    public void setOption(OptionChild option) {
        this.option = option;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        if (product == null)
            product = new Product();
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        double price = 0;
        if (unit_price > 0) {
            price = unit_price;
        } else {

            if (getProduct() != null)
                price = getProduct().getPrice();
            if (getOption() != null) {
                price = getOption().getPrice();
            }
        }
        return price;
    }
}
