package hama.alsaygh.kw.model;

public class Bestpackging {
  private   int img;
  private   int img_name;

    public Bestpackging(int img, int img_name) {
        this.img = img;
        this.img_name = img_name;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public int getImg_name() {
        return img_name;
    }

    public void setImg_name(int img_name) {
        this.img_name = img_name;
    }
}
