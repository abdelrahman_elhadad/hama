package hama.alsaygh.kw.model.store;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Store implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("store_name")
    private String store_name;

    @SerializedName("store_description")
    private String store_description;

    @SerializedName("products")
    private int products;

    @SerializedName("logo")
    private String logo;

    @SerializedName("image")
    private String image;

    @SerializedName("cover_image")
    private String coverImage;


    public String getCoverImage() {
        if (coverImage == null)
            coverImage = "";
        coverImage = coverImage.replace("\\", "");
        Log.i("imageLink","coverimageLink : "+coverImage);
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getImage() {
        if (image == null)
            image = "";
        image = image.replace("\\", "");
        Log.i("imageLink","imageLink : "+image);
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getStore_description() {
        return store_description;
    }

    public void setStore_description(String store_description) {
        this.store_description = store_description;
    }

    public int getProducts() {
        return products;
    }

    public void setProducts(int products) {
        this.products = products;
    }

    public String getLogo() {
        if (logo == null)
            logo = "";
        logo = logo.replace("\\", "");
        Log.i("imageLink","logoLink : "+logo);
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
