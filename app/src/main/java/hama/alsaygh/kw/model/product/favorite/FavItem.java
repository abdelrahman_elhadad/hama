package hama.alsaygh.kw.model.product.favorite;

import com.google.gson.annotations.SerializedName;

import hama.alsaygh.kw.model.product.Product;
import hama.alsaygh.kw.model.store.Store;

public class FavItem {


    @SerializedName("product_id")
    private Product product;

    @SerializedName("vendor_id")
    private Store store;


    public Product getProduct() {
        if(product==null)
            product=new Product();
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

}
