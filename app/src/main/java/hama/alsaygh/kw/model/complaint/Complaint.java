package hama.alsaygh.kw.model.complaint;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Complaint implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("complaint_id")
    private String complaint_id;

    @SerializedName("subject")
    private String subject;

    @SerializedName("sender_type")
    private String sender_type;

    @SerializedName("description")
    private String description;

    @SerializedName("status")
    private String status;

    @SerializedName("status_str")
    private String status_str;

    @SerializedName("sent_time")
    private String sent_time;

    @SerializedName("replies")
    private List<ComplaintReplay> replies;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComplaint_id() {
        return complaint_id;
    }

    public void setComplaint_id(String complaint_id) {
        this.complaint_id = complaint_id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSender_type() {
        return sender_type;
    }

    public void setSender_type(String sender_type) {
        this.sender_type = sender_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_str() {
        return status_str;
    }

    public void setStatus_str(String status_str) {
        this.status_str = status_str;
    }

    public String getSent_time() {
        return sent_time;
    }

    public void setSent_time(String sent_time) {
        this.sent_time = sent_time;
    }

    public List<ComplaintReplay> getReplies() {
        return replies;
    }

    public void setReplies(List<ComplaintReplay> replies) {
        this.replies = replies;
    }
}
