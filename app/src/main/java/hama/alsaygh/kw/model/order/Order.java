package hama.alsaygh.kw.model.order;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import hama.alsaygh.kw.model.address.Address;
import hama.alsaygh.kw.model.cart.CartItem;
import hama.alsaygh.kw.model.paymentCard.Card;

public class Order implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("order_number")
    private String order_number;

    @SerializedName("sub_total1")
    private double sub_total1;

    @SerializedName("sub_total2")
    private double sub_total2;

    @SerializedName("tax")
    private double tax;

    @SerializedName("total")
    private double total;

    @SerializedName("currency")
    private String currency;

    @SerializedName("status")
    private String status;

    @SerializedName("status_trans")
    private String status_trans;

    @SerializedName("items_count")
    private int items_count;

    @SerializedName("items")
    private List<CartItem> items;

    @SerializedName("delivery_type")
    private String delivery_type;

    @SerializedName("delivery_type_trans")
    private String delivery_type_trans;

    @SerializedName("receipt_at")
    private String receipt_at;

    @SerializedName("payment_method")
    private String payment_method;

    @SerializedName("payment_method_trans")
    private String payment_method_trans;

    @SerializedName("payment_method_card")
    private Card payment_method_card;

    @SerializedName("gift_note")
    private String gift_note;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("reject_reason")
    private String reject_reason;

    @SerializedName("is_pending")
    private boolean is_pending;

    @SerializedName("pending_str")
    private String pending_str;

    @SerializedName("is_canceled")
    private boolean is_canceled;

    @SerializedName("is_in_progress")
    private boolean is_in_progress;

    @SerializedName("in_progress_str")
    private String in_progress_str;

    @SerializedName("is_in_delivery")
    private boolean is_in_delivery;

    @SerializedName("in_delivery_str")
    private String in_delivery_str;

    @SerializedName("is_completed")
    private boolean is_completed;

    @SerializedName("completed_str")
    private String completed_str;

    @SerializedName("is_delivered")
    private boolean is_delivered;

    @SerializedName("can_confirm_deleiverd")
    private boolean can_confirm_deleiverd;

    @SerializedName("delivery")
    private Address delivery;

    public boolean isIs_delivered() {
        return is_delivered;
    }

    public void setIs_delivered(boolean is_delivered) {
        this.is_delivered = is_delivered;
    }

    public boolean isCan_confirm_deleiverd() {
        return can_confirm_deleiverd;
    }

    public void setCan_confirm_deleiverd(boolean can_confirm_deleiverd) {
        this.can_confirm_deleiverd = can_confirm_deleiverd;
    }

    public String getPending_str() {
        return pending_str;
    }

    public void setPending_str(String pending_str) {
        this.pending_str = pending_str;
    }

    public boolean isIs_in_progress() {
        return is_in_progress;
    }

    public void setIs_in_progress(boolean is_in_progress) {
        this.is_in_progress = is_in_progress;
    }

    public String getIn_progress_str() {
        return in_progress_str;
    }

    public void setIn_progress_str(String in_progress_str) {
        this.in_progress_str = in_progress_str;
    }

    public String getIn_delivery_str() {
        return in_delivery_str;
    }

    public void setIn_delivery_str(String in_delivery_str) {
        this.in_delivery_str = in_delivery_str;
    }

    public String getCompleted_str() {
        return completed_str;
    }

    public void setCompleted_str(String completed_str) {
        this.completed_str = completed_str;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public boolean isIs_pending() {
        return is_pending;
    }

    public void setIs_pending(boolean is_pending) {
        this.is_pending = is_pending;
    }

    public boolean isIs_canceled() {
        return is_canceled;
    }

    public void setIs_canceled(boolean is_canceled) {
        this.is_canceled = is_canceled;
    }

    public boolean isIs_in_delivery() {
        return is_in_delivery;
    }

    public void setIs_in_delivery(boolean is_in_delivery) {
        this.is_in_delivery = is_in_delivery;
    }

    public boolean isIs_completed() {
        return is_completed;
    }

    public void setIs_completed(boolean is_completed) {
        this.is_completed = is_completed;
    }

    public Address getDelivery() {
        return delivery;
    }

    public void setDelivery(Address delivery) {
        this.delivery = delivery;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public double getSub_total1() {
        return sub_total1;
    }

    public void setSub_total1(double sub_total1) {
        this.sub_total1 = sub_total1;
    }

    public double getSub_total2() {
        return sub_total2;
    }

    public void setSub_total2(double sub_total2) {
        this.sub_total2 = sub_total2;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_trans() {
        return status_trans;
    }

    public void setStatus_trans(String status_trans) {
        this.status_trans = status_trans;
    }

    public int getItems_count() {
        return items_count;
    }

    public void setItems_count(int items_count) {
        this.items_count = items_count;
    }

    public List<CartItem> getItems() {
        if(items==null)
            items=new ArrayList<>();
        return items;
    }

    public void setItems(List<CartItem> items) {
        this.items = items;
    }

    public String getDelivery_type() {
        return delivery_type;
    }

    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    public String getDelivery_type_trans() {
        return delivery_type_trans;
    }

    public void setDelivery_type_trans(String delivery_type_trans) {
        this.delivery_type_trans = delivery_type_trans;
    }

    public String getReceipt_at() {
        return receipt_at;
    }

    public void setReceipt_at(String receipt_at) {
        this.receipt_at = receipt_at;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getPayment_method_trans() {
        return payment_method_trans;
    }

    public void setPayment_method_trans(String payment_method_trans) {
        this.payment_method_trans = payment_method_trans;
    }

    public Card getPayment_method_card() {
        return payment_method_card;
    }

    public void setPayment_method_card(Card payment_method_card) {
        this.payment_method_card = payment_method_card;
    }

    public String getGift_note() {
        return gift_note;
    }

    public void setGift_note(String gift_note) {
        this.gift_note = gift_note;
    }

    public String getReject_reason() {
        return reject_reason;
    }

    public void setReject_reason(String reject_reason) {
        this.reject_reason = reject_reason;
    }

    public String getCreateDate() {

        if (created_at != null && !created_at.isEmpty()) {

            String[] date = created_at.split(" ");
            if (date.length > 0)
                return date[0];
            else
                return "";
        }
        return "";
    }
}
