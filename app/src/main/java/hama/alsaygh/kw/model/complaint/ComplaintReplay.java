package hama.alsaygh.kw.model.complaint;

import com.google.gson.annotations.SerializedName;

public class ComplaintReplay {

    @SerializedName("id")
    private int id;

    @SerializedName("is_me")
    private boolean me;

    @SerializedName("message")
    private String message;

    @SerializedName("send_at")
    private String send_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isMe() {
        return me;
    }

    public void setMe(boolean me) {
        this.me = me;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSend_at() {
        return send_at;
    }

    public void setSend_at(String send_at) {
        this.send_at = send_at;
    }
}
