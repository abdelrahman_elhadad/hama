package hama.alsaygh.kw.model.cart;

import com.google.gson.annotations.SerializedName;

import hama.alsaygh.kw.model.store.Store;

public class CartHomeItem {

    @SerializedName("count")
    private int count;


    @SerializedName("vendor")
    private Store store;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}
