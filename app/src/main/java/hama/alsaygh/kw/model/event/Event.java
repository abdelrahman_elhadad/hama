package hama.alsaygh.kw.model.event;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Event implements Serializable {

    @SerializedName("title")
    private String title;

    @SerializedName("date")
    private String date;

    @SerializedName("time")
    private String time;

    @SerializedName("image")
    private String image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
