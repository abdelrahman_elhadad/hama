package hama.alsaygh.kw.model.paymentCard;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Card implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("type")
    private String type;

    @SerializedName("holder_name")
    private String holder_name;

    @SerializedName("card_number")
    private String card_number;

    @SerializedName("month")
    private String month;

    @SerializedName("year")
    private String year;

    @SerializedName("is_primary")
    private boolean is_primary;

    public boolean is_primary() {
        return is_primary;
    }

    public void set_primary(boolean is_primary) {
        this.is_primary = is_primary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHolder_name() {
        return holder_name;
    }

    public void setHolder_name(String holder_name) {
        this.holder_name = holder_name;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
