package hama.alsaygh.kw.model.calculator;

import com.google.gson.annotations.SerializedName;

public class Calculator {

    @SerializedName("18")
    private String size18;


    @SerializedName("21")
    private String size21;


    @SerializedName("22")
    private String size22;


    @SerializedName("24")
    private String size24;

    public String getSize18() {
        if (size18 == null)
            size18 = "0";
        return size18.replace("%", "");
    }

    public void setSize18(String size18) {
        this.size18 = size18;
    }

    public String getSize21() {
        if (size21 == null)
            size21 = "0";
        return size21.replace("%", "");
    }

    public void setSize21(String size21) {
        this.size21 = size21;
    }

    public String getSize22() {
        if (size22 == null)
            size22 = "0";
        return size22.replace("%", "");
    }

    public void setSize22(String size22) {
        this.size22 = size22;
    }

    public String getSize24() {
        if (size24 == null)
            size24 = "0";
        return size24.replace("%", "");
    }

    public void setSize24(String size24) {
        this.size24 = size24;
    }
}
