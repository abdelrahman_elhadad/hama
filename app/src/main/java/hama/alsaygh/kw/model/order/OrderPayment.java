package hama.alsaygh.kw.model.order;

import com.google.gson.annotations.SerializedName;

public class OrderPayment {


    @SerializedName("payment_url")
    private String payment_url;//https://hamakw.site/payment/pending

    public String getPayment_url() {
        return payment_url;
    }

    public void setPayment_url(String payment_url) {
        this.payment_url = payment_url;
    }
}
