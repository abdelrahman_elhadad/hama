package hama.alsaygh.kw.model.cart;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartHome {

    @SerializedName("count")
    private int count;


    @SerializedName("cart")
    private List<CartHomeItem> cart;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<CartHomeItem> getCart() {
        return cart;
    }

    public void setCart(List<CartHomeItem> cart) {
        this.cart = cart;
    }
}
