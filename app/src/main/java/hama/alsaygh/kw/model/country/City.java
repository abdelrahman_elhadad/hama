package hama.alsaygh.kw.model.country;

import com.google.gson.annotations.SerializedName;

public class City {


    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("shipping_price")
    private String shipping_price;

    @SerializedName("country")
    private Country country;

    public String getShipping_price() {
        return shipping_price;
    }

    public void setShipping_price(String shipping_price) {
        this.shipping_price = shipping_price;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;

    }
}
